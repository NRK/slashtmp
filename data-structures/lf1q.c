/*
 * lock-free, single element generic LIFO queue.
 * (more commonly known as "triple buffer": https://en.wikipedia.org/wiki/Triple_buffering)
 * ref: https://nrk.neocities.org/articles/lockfree-1element-queue.html
 *
 * compile with: clang -std=c18 -O2 lf1q.c
 * or with TSan: clang -std=c18 -O2 -fsanitize=thread -g lf1q.c
 */
#define _DEFAULT_SOURCE
#include <errno.h>
#include <stdatomic.h>
#include <stddef.h>
#include <stdio.h>
#include <time.h>

#include <pthread.h>
#include <unistd.h>

#include <linux/futex.h>
#include <sys/syscall.h>

#pragma GCC diagnostic ignored "-Wunused-result"

#define N 128

#define TOMIC _Atomic
#define tomic_load(P, MO)           atomic_load_explicit((P), memory_order_##MO)
#define tomic_store(P, VAL, MO)     atomic_store_explicit((P), (VAL), memory_order_##MO)
#define tomic_xchg(P, VAL, MO)      atomic_exchange_explicit((P), (VAL), memory_order_##MO)

enum { WAIT, STOP };
enum {
	/* if this bit is set, then the slot is either uninitialized or was
	 * just pushed by a reader (i,e stale).
	 * the reader always pushes with this bit set, the writer doesn't care
	 * about this and should clear it.
	 */
	UNINIT_BIT = 0x4,
	WINIT = 0,
	RINIT = 1,
	NEXT_INIT = 2 | UNINIT_BIT,
};
typedef struct {
	int buf[3];
	TOMIC int next;
	TOMIC int cancel;
} Slot;

static int
futex_wait(void *p, int expected, int timeout_ms)
{
	struct timespec t = {
		.tv_sec  = timeout_ms / 1000,
		.tv_nsec = (timeout_ms % 1000) * 1000000L
	};
	return syscall(SYS_futex, p, FUTEX_WAIT, expected, &t);
}

static int
futex_wake(void *p, int n)
{
	return syscall(SYS_futex, p, FUTEX_WAKE, n);
}

static void *
snooper(void *arg)
{
	Slot *ctx = arg;
	int r = RINIT;

	while (futex_wait(&ctx->cancel, WAIT, 8) != 0 && errno == ETIMEDOUT) {
		r = tomic_xchg(&ctx->next, r | UNINIT_BIT, acq_rel);
		if (!(r & UNINIT_BIT)) {
			char buf[64];
			int n = snprintf(buf, sizeof buf, "%d\n", ctx->buf[r]);
			write(1, buf, n);
		}
	}

	char msg[] = "snooper: bye!\n";
	write(1, msg, sizeof msg - 1);
	return 0;
}

static int
worker(void *arg)
{
	Slot *ctx = arg;
	int w = WINIT;

	for (int i = 0; i < N; ++i) {
		ctx->buf[w] = i;
		w = tomic_xchg(&ctx->next, w, acq_rel);
		w &= ~UNINIT_BIT; /* the writer doesn't care about the uninit bit */
		nanosleep(&(struct timespec){ .tv_nsec = 2000 * 1000 }, NULL); /* simulating doing work */
	}

	char msg[] = "worker: sending stop signal\n";
	write(1, msg, sizeof msg - 1);
	tomic_store(&ctx->cancel, STOP, relaxed);
	futex_wake(&ctx->cancel, 1);
	return 0;
}

extern int
main(void)
{
	Slot work = { .next = NEXT_INIT };

	pthread_t pthrd;
	pthread_create(&pthrd, 0, snooper, &work);

	int rc = worker(&work);
	pthread_join(pthrd, NULL);

	return rc;
}
