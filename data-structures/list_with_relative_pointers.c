/*
 * merely a demonstration of how "relative pointers" work using a singly-linked
 * list. not suitable for any real world purposes.
 */
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>

#define LIST_NULL UINT_LEAST32_MAX
struct list {
	char item;
	uint_least32_t next; /* relative pointer. in other words, an index into the backing array. */
};

extern int
main(int argc, char *argv[])
{
	char *arg = argc < 2 ? "hello, world" : argv[1];
	size_t arglen = strlen(arg);

	/* a single flat-array. we'll use this as the backing storage for the list. */
	struct list *list = malloc(arglen * sizeof *list);
	if (list == NULL)
		abort();
	uint_least32_t list_head = LIST_NULL;
	uint_least32_t list_next_free_item = 0;

	/* push each char into the front of the list.
	 * this will reverse the string as each item is getting added to the front.
	 */
	for (size_t i = 0; i < arglen; ++i) {
		uint_least32_t insert = list_next_free_item;
		++list_next_free_item;
		list[insert].item = arg[i];
		list[insert].next = list_head;
		list_head = insert;
	}

	/* iterate over the list and print it out */
	for (uint_least32_t p = list_head; p != LIST_NULL; p = list[p].next) {
		printf("%c -> ", list[p].item);
	}
	printf("END\n");
	free(list);
}
