// benchmarking various different search trees.
// https://nrk.neocities.org/articles/simple-treap.html
// https://nrk.neocities.org/articles/hash-trees-and-tries.html
//
// a preallocated hash-table is used as a baseline for comparison. but keep in
// mind this isn't fair for comparing insertion time since the hash-table
// doesn't need to resize/allocate while the trees need to allocate nodes.
//
// half the input strings are pseudorandomly generated. the other half are
// copies of the first half but with one byte flipped. this is done to simulate
// more realistic scenarios where there are common prefixes and suffixes.
//
// memory usage is measured by simply comparing the stack pointer of the arena.
// this means the usage might be higher than what the data-structure *actually*
// requested due to alignment paddings. (note that the input strings are not
// counted in this).
//
// the "random lookups" can essentially be treated as failed lookups since a
// huge majority of them are just that.
//
// output is in the following format:
//    Items-StrLen-Name :: { Insertion, Successful lookups, Random lookups, Memory Usage }
// turn it into csv:
//   $ ./out | awk -F " " '{ print $1 "," $4 $5 $6 $7 }'
// build:
//   $ gcc -o out data-structures/search-tree-benchmark.c -O3 -march=native
#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

#define ASSERT(X)  ((X) ? (void)0 : __builtin_unreachable())
#define ARRLEN(X)  (sizeof(X)/sizeof(0[X]))
#define ABORT()    __builtin_abort()

typedef uint8_t    u8;
typedef uint32_t   u32;
typedef uint64_t   u64;
typedef ptrdiff_t  Size;
typedef struct { u8 *s; Size len; } Str;
typedef struct { u8 *off, *end; } Arena;
enum { ALIGN = sizeof(void *) };
enum {
	ARENA_ZERO = 1 << 0,
};

static Arena *
arena_create(void *mem, Size n)
{
	static const Arena nil_arena[1] = {{ .off = (void *)nil_arena, .end = (void *)nil_arena }};
	Size aligned_arena = sizeof(Arena) + (-sizeof(Arena) & (ALIGN - 1));
	ASSERT(n > aligned_arena); ASSERT((ALIGN & (ALIGN-1)) == 0);
	Arena *ret = (void *)nil_arena;
	if (mem != NULL) {
		ret = mem;
		ret->off = (u8 *)mem + aligned_arena;
		ret->end = (u8 *)mem + n;
	}
	return ret;
}

__attribute((alloc_size(2), malloc))
static void *
arena_alloc(Arena *a, Size size, u64 flags)
{
	Size rsize = size + (-size & (ALIGN - 1));
	Size avail = a->end - a->off;
	if (rsize > avail) {
		ABORT();
	}
	u8 *ret = a->off;
	a->off += rsize;
	if (flags & ARENA_ZERO) for (Size i = 0; i < size; ++i) {
		ret[i] = 0x0;
	}
	return ret;
}

static Str
str_rand(Arena *a, u64 *state)
{
	Str s = { arena_alloc(a, 256, 0), 256 };
	for (Size k = 0; k < s.len; k += 8) {
		u64 r = (*state >> (*state >> 59)) ^ *state;
		*state = *state * 0x1337ACDCBABE5AAD + 7;
		for (Size i = 0; i < 8; ++i) {
			s.s[k+i] = *((u8 *)&r + i);
		}
	}
	return s;
}

static int
str_cmp(Str a, Str b)
{
	if (a.len != b.len) {
		return (a.len > b.len) - (a.len < b.len);
	}
	for (Size i = 0; i < a.len; ++i) {
		int r = (a.s[i] > b.s[i]) - (a.s[i] < b.s[i]);
		if (r != 0) {
			return r;
		}
	}
	return 0;
}

__attribute((flatten))
static int
str_eq(Str a, Str b)
{
	return str_cmp(a, b) == 0;
}

static u64
str_hash(Str s)
{
	u64 h = 55555 + s.len;
#if 1
	for (Size i = 0; i < s.len; i += 4) {
		u32 b = 0;
		switch ((i+3 < s.len) ? 4 : (s.len % 4)) {
		case 4: b |= (uint32_t)s.s[i+3] << 24; /* fallthrough */
		case 3: b |= (uint32_t)s.s[i+2] << 16; /* fallthrough */
		case 2: b |= (uint32_t)s.s[i+1] <<  8; /* fallthrough */
		case 1: b |= (uint32_t)s.s[i+0] <<  0; break;
		default: ASSERT(!"unreachable"); break;
		}
		b *= UINT32_C(0xCC9E2D51);
		b  = (b >> 17) | (b << 15);
		b *= UINT32_C(0x1B873593);

		h ^= b;
		h  = (h >> 37) | (h << 27);
		h *= 33;
		h += 0xE6546B64CAFEBABE;
	}
	h ^= h >> 33;
	h *= 0x1337ACDCBABE5AAD;
	h ^= h >> 31;
#else
	for (Size i = 0; i < s.len; ++i) {
		h ^= s.s[i];
		h *= 0x1337ACDCBABE5AAD;
	}
	h ^= h >> 33;
#endif
	return h;
}

#ifndef NO_HASHTABLE
// "MSI" HashTable used as a baseline: https://nullprogram.com/blog/2022/08/08/
// uses 50% load factor (but in practice MSI tables tend to sustain about ~75%
// without performance degrading too much).
typedef struct {
	Str *tbl;
	u64 mask;
	u64 step;
	int nelems;
} HashTable;

static Str *
hashtable_find(Arena *a, HashTable *t, Str key)
{
	if (t->tbl == NULL) {
		Size n = t->nelems * 2;
		ASSERT((n & (n-1)) == 0);
		t->tbl = arena_alloc(a, n * sizeof *t->tbl, ARENA_ZERO);
		t->mask = n - 1;
		t->step = __builtin_popcountll(t->mask);
	}

	u64 hash = str_hash(key);
	u64 idx = hash & t->mask;
	u64 step = (hash >> t->step) | 0x1;
	Str *tbl = t->tbl;
	for (;;) {
		if (tbl[idx].s == NULL) {
			if (a == NULL) { // lookup
				return NULL;
			} else {
				tbl[idx] = key;
				return tbl + idx;
			}
		} else if (str_eq(key, tbl[idx])) {
			return tbl + idx;
		} else {
			idx += step;
			idx &= t->mask;
		}
	}
	ASSERT(!"unreachable");
}
#endif // NO_HASHTABLE

#ifndef NO_TREAP
// textbook treap but uses the node pointer hash as a source of randomness and
// doesn't store the parent pointer inside each node.
typedef struct Treap {
	Str key;
	struct Treap *child[2];
} Treap;

static inline uintptr_t
treap_priority(Treap *p)
{
	uintptr_t m = 0x5555555555555555;
	uintptr_t h = (uintptr_t)p;
	h ^= h >> (sizeof h * 4 - 1);
	h *= m;
	h ^= h >> (sizeof h * 4 + 1);
	return h;
}

static Treap *
treap_find(Arena *a, Treap **t, Str key)
{
	Treap *pstack[128], *parent = NULL, **insert = t;
	int phead = 0;
	while (*insert != NULL) {
		parent = *insert;
		if (a != NULL) {
			if (phead == ARRLEN(pstack)) ABORT();
			pstack[phead++] = parent;
		}
		switch (str_cmp(key, parent->key)) {
		case  0: return insert[0]; break;
		case -1: insert = parent->child + 0; break;
		case  1: insert = parent->child + 1; break;
		default: ASSERT(!"unreachable"); break;
		}
	}
	ASSERT(*insert == NULL);
	if (a == NULL) { // lookup
		return NULL;
	}
	Treap *new = arena_alloc(a, sizeof *new, 0x0);
	*new = (Treap){ .key = key };
	*insert = new;

	uintptr_t new_priority = treap_priority(new);
	while (phead > 0 && treap_priority((parent = pstack[--phead])) < new_priority) {
		if (phead > 0) { // update grandpa
			Treap *gp = pstack[phead - 1];
			gp->child[gp->child[1] == parent] = new;
		} else { // otherwise update root
			*t = new;
		}
		int d = parent->child[1] == new;
		parent->child[d] = new->child[!d]; // update parent
		new->child[!d] = parent; // update node
	}

	return new;
}

static int
treap_depth(Treap *t, int depth)
{
	if (t == NULL) {
		return depth;
	}
	int l = treap_depth(t->child[0], depth+1);
	int r = treap_depth(t->child[1], depth+1);
	return l > r ? l : r;
}
#endif // NO_TREAP

#define NO_JOINTREAP // disable by default
#ifndef NO_JOINTREAP
// join-based treap: https://old.reddit.com/r/C_Programming/comments/16ho5ql/implementing_and_simplifying_treap_in_c/k0ghge7/
typedef Treap JTreap;

static JTreap *
treap_split(JTreap *root, Str key, JTreap **equal, JTreap **larger)
{
	if (root == NULL) {
		*larger = NULL;
		return NULL;
	}
	switch (str_cmp(root->key, key)) {
	case  0:
		*equal = root;
		*larger = root->child[1];
		JTreap *s = root->child[0];
		root->child[0] = root->child[1] = 0;
		return s;
	case -1:
		root->child[1] = treap_split(root->child[1], key, equal, larger);
		return root;
	case +1:
		*larger = root;
		return treap_split(root->child[0], key, equal, &root->child[0]);
	default: ASSERT(!"unreachable"); break;
	}
}

static JTreap *
treap_join(JTreap *smaller, JTreap *larger)
{
	if (smaller == NULL) return larger;
	if (larger == NULL) return smaller;
	if (treap_priority(smaller) < treap_priority(larger)) {
		smaller->child[1] = treap_join(smaller->child[1], larger);
		return smaller;
	} else {
		larger->child[0] = treap_join(smaller, larger->child[0]);
		return larger;
	}
}

static JTreap *
treap_join_find(Arena *a, JTreap **t, Str key)
{
	if (a == NULL) { // lookup, use faster binary search
		return treap_find(a, t, key);
	}
	JTreap *s, *e = NULL, *l;
	s = treap_split(*t, key, &e, &l);
	if (e == NULL) {
		e = arena_alloc(a, sizeof *e, 0x0);
		*e = (JTreap){ .key = key };
	}
	*t = treap_join(treap_join(s, e), l);
	return e;
}
#endif // NO_JOINTREAP

#ifndef NO_HTHTREE
// "Hash table of hash trees" or HTHTree
typedef struct HTHNode {
	Str key;
	u64 hash;
	struct HTHNode *child[2];
} HTHNode;
enum { HTH_SLOTS = 16 };
typedef struct { HTHNode *slots[HTH_SLOTS]; } HTHTree;

static HTHNode *
hth_find(Arena *a, HTHTree *t, Str key)
{
	u64 hash = str_hash(key);
	HTHNode **insert = t->slots + (hash & (HTH_SLOTS - 1));
	while (*insert != NULL) {
		u64 phash = insert[0]->hash;
		switch ((hash > phash) - (hash < phash)) {
		case  0: if (str_eq(key, insert[0]->key)) return insert[0]; // fallthrough
		case -1: insert = insert[0]->child + 0; break;
		case +1: insert = insert[0]->child + 1; break;
		default: ASSERT(!"unreachable"); break;
		}
	}
	if (a == NULL) { // lookup
		return NULL;
	} else { // insert
		HTHNode *tmp = arena_alloc(a, sizeof *tmp, 0x0);
		*tmp = (HTHNode){ .key = key, .hash = hash };
		*insert = tmp;
		return insert[0];
	}
}

static int
hth_node_depth(HTHNode *t, int depth)
{
	if (t == NULL) {
		return depth;
	}
	int l = hth_node_depth(t->child[0], depth+1);
	int r = hth_node_depth(t->child[1], depth+1);
	return l > r ? l : r;
}

static int
hth_depth(HTHTree *t)
{
	int max = 0;
	for (Size i = 0; i < HTH_SLOTS; ++i) {
		int d = hth_node_depth(t->slots[i], 0);
		max = d > max ? d : max;
	}
	return max;
}

#include <stdio.h>
static void
hth_graphviz_recur(FILE *out, HTHNode *np)
{
	if (np->child[0]) fprintf(out, "\t\"%p\" -> \"%p\";\n", (void *)np, (void *)np->child[0]);
	if (np->child[1]) fprintf(out, "\t\"%p\" -> \"%p\";\n", (void *)np, (void *)np->child[1]);
	if (np->child[0]) hth_graphviz_recur(out, np->child[0]);
	if (np->child[1]) hth_graphviz_recur(out, np->child[1]);
}

static void
hth_graphviz(FILE *out, HTHNode *root)
{
	fprintf(out, "// depth %d\n", hth_node_depth(root, 0));
	fprintf(out, "digraph {\n");
	hth_graphviz_recur(out, root);
	fprintf(out, "}\n");
}
#endif // NO_HTHTREE

#ifndef NO_HAMT
// my take on hash-array-mapped-trie: https://en.wikipedia.org/wiki/Hash_array_mapped_trie
typedef struct StrList { Str key; struct StrList *next; } StrList;
enum { NODE_BITS = 3, NODE_CNT = 1 << NODE_BITS, NODE_MASK = NODE_CNT - 1 };
enum { SLOT_EMPTY, SLOT_KEY, SLOT_NEXT, SLOT_LIST };
typedef struct {
	Str key;
	uintptr_t morph; // HAMTNode-next || list-next || key-hash
} HAMTNode;
typedef struct { HAMTNode slots[NODE_CNT]; } HAMT;

_Static_assert(SLOT_LIST == 0x3, "");
_Static_assert(_Alignof(HAMT *) >= 4 && _Alignof(StrList *) >= 4, "needed for bit smuggling");
_Static_assert(sizeof(uintptr_t) >= sizeof(uint32_t), "smuggling hash");
#define MORPH_MASK     ((uintptr_t)0x3)
#define MORPH_TYPE(X)  ((X) & MORPH_MASK)
#define MORPH_PTR(X)   ((void *)((X) & ~MORPH_MASK))
_Static_assert(NODE_MASK >= MORPH_MASK, "required to safely drop the low bits of the hash when merging to morph");

static Str *
hamt_find(Arena *a, HAMT *t, Str key)
{
	uint32_t hash = str_hash(key) >> 32;
	for (int i = 0; i < 32; i += NODE_BITS) {
		int idx = (hash >> i) & NODE_MASK;
		HAMTNode *np = t->slots + idx;
		int mtype = MORPH_TYPE(np->morph);
		switch (mtype) {
		case SLOT_EMPTY:
			if (a == NULL) return NULL;
			np->key = key;
			np->morph = (hash & ~MORPH_MASK) | SLOT_KEY;
			return &np->key;
			break;
		case SLOT_KEY:
			if (str_eq(key, np->key)) {
				return &np->key;
			} else if (i + NODE_BITS < 32 && a != NULL) {
				Str oldkey = np->key;
				uintptr_t oldm = np->morph;
				HAMT *tmp = arena_alloc(a, sizeof *tmp, ARENA_ZERO);
				np->morph = (uintptr_t)tmp | SLOT_NEXT;
				// reinsert oldkey
				uint32_t oldhash = oldm & ~MORPH_MASK; // redundant, since it'll get shifted out below
				int oldidx = (oldhash >> (i + NODE_BITS)) & NODE_MASK;
				tmp->slots[oldidx].key = oldkey;
				tmp->slots[oldidx].morph = oldm;
				// "advance" t
				t = tmp;
			} else { // turn it into a list
				if (a == NULL) return NULL;
				StrList *tmp = arena_alloc(a, sizeof *tmp, 0);
				tmp->key = key; tmp->next = NULL;
				np->morph = (uintptr_t)tmp | SLOT_LIST;
				return &tmp->key;
			}
			break;
		case SLOT_NEXT:
			t = MORPH_PTR(np->morph);
			ASSERT(t != NULL);
			break;
		case SLOT_LIST: {
			StrList *tail;
			if (str_eq(key, np->key)) {
				return &np->key;
			} else for (StrList *it = MORPH_PTR(np->morph); it != NULL; tail = it, it = it->next) {
				if (str_eq(key, it->key)) {
					return &it->key;
				}
			}
			if (a == NULL) return NULL;
			StrList *tmp = arena_alloc(a, sizeof *tmp, 0);
			tmp->key = key; tmp->next = NULL;
			tail->next = tmp;
			return &tmp->key;
		} break;
		default: ASSERT(!"unreachable"); break;
		}
	}
	ASSERT(!"unreachable");
	return 0;
}
#endif // NO_HAMT

#ifndef NO_HTRIE
// simpler hash-trie: https://lists.sr.ht/~skeeto/public-inbox/%3C20230902232504.hcihm5fdw6k7kejq%40gen2.localdomain%3E#%3C20230910155653.7kowsdjajyknqhsm@nullprogram.com%3E
typedef struct HTrie {
	Str key;
	struct HTrie *next[4];
} HTrie;

static HTrie *
htrie_find(Arena *a, HTrie **t, Str key)
{
	for (u64 keyhash = str_hash(key); *t != NULL; keyhash <<= 2) {
		if (str_eq(key, t[0]->key)) {
			return *t;
		}
		t = t[0]->next + (keyhash >> 62);
	}
	if (a != NULL) {
		*t = arena_alloc(a, sizeof **t, ARENA_ZERO);
		t[0]->key = key;
	}
	return *t;
}

#include <stdio.h>
static void
htrie_graphvis_recur(FILE *out, HTrie *t)
{
	for (Size i = 0; i < ARRLEN(t->next); ++i) {
		if (t != NULL && t->next[i] != NULL) {
			fprintf(out, "\t\"%p\" -> \"%p\";\n", (void *)t, (void *)t->next[i]);
			htrie_graphvis_recur(out, t->next[i]);
		}
	}
}

static void
htrie_graphvis(FILE *out, HTrie *t)
{
	fprintf(out, "digraph {\n");
	htrie_graphvis_recur(out, t);
	fprintf(out, "}\n");
}
#endif  // NO_HTRIE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>

#include <time.h>

static u64
clock_now(void)
{
	struct timespec t;
	clock_gettime(CLOCK_BOOTTIME, &t);
	return (t.tv_sec * (1000L * 1000L)) + (t.tv_nsec / 1000L);
}

typedef void * Func(Arena *, void *, Str);
static void
bench(
	Arena scratch, void *user, Func *f,
	Str *rstr, Str *rstr2, int rstr_len, int klen, char *name
)
{
	int rl = rstr_len;
	u64 start, diff;

	// flip one of the bytes of the duplicate string to simulate similar strings
	ASSERT((klen & (klen - 1)) == 0);
	for (Size i = 0; i < rl; i += 2) {
		u8 flip = rstr2[i].s[0] & (klen - 1);
		rstr[i].s[flip] = ~rstr[i].s[flip];
	}

	fprintf(stdout, "%7d-%03d-%-7s :: { ", rl, klen, name);
	u8 *start_mem = scratch.off;
	start = clock_now();
	for (Size i = 0; i < rl; ++i) {
		Str s = { rstr[i].s, klen };
		f(&scratch, user, s);
	}
	diff = clock_now() - start;
	Size mem_used = scratch.off - start_mem;
	fprintf(stdout, "%8"PRIu64", ", diff);

	start = clock_now();
	for (Size i = 0; i < rl; ++i) {
		Str s = { rstr[i].s, klen };
		void *p = f(0, user, s);
		ASSERT(p != NULL);
	}
	diff = clock_now() - start;
	fprintf(stdout, "%8"PRIu64", ", diff);

	start = clock_now();
	for (Size i = 0; i < rstr_len; ++i) {
		Str s = { rstr2[i].s, klen };
		void *p = f(0, user, s);
	}
	diff = clock_now() - start;
	fprintf(stdout, "%8"PRIu64", ", diff);

	fprintf(stdout, "%8jd }\n", mem_used / 1024);

	// unflip
	for (Size i = 0; i < rl; i += 2) {
		u8 flip = rstr2[i].s[0] & (klen - 1);
		rstr[i].s[flip] = ~rstr[i].s[flip];
	}
}

extern int
main(void)
{
	Size ASIZE = 1ull << 31;
	Arena *a = arena_create(malloc(ASIZE), ASIZE);
	// force all pages to be mapped to avoid skewing benchmark
	memset(a->off, 0xCD, a->end - a->off);

	u64 r = (u64)a;
	r ^= (u64)&free; r *= 0x1337ACDCBABE5AAD;
	r ^= (u64)&a;    r *= 0x1337ACDCBABE5AAD;
	r ^= r >> 33;

	Size rng_len = 1 << 20;
	Str *rng_str  = arena_alloc(a, sizeof *rng_str * rng_len, 0x0);
	Str *rng_str2 = arena_alloc(a, sizeof *rng_str * rng_len, 0x0);
	for (Size i = 0; i < rng_len; i += 2) {
		rng_str[i+0] = str_rand(a, &(u64){r});
		rng_str[i+1] = str_rand(a, &r); // insert a duplicate which will be flipped later.
		ASSERT(str_eq(rng_str[i+0], rng_str[i+1]));
	}
	// should be mostly failed lookups
	for (Size i = 0; i < rng_len; ++i) {
		rng_str2[i] = str_rand(a, &r);
	}

	int ITEMS[] = {9,11,14,17,20}; // in powers of 2
	int LENS[]  = {64}; // in decimal, but must be power of 2s
#define BENCH_FOR() \
	for (int items, *itemsp = ITEMS, *items_end = 1[&ITEMS]; \
	     itemsp < items_end && (items = 1 << itemsp[0]); \
	     ++itemsp) \
	for (int slen, *slenp = LENS, *slenend = 1[&LENS]; \
	     slenp < slenend && (slen = slenp[0]); ++slenp)

	if (ITEMS[ARRLEN(ITEMS)-1] > rng_len)
		abort();

#ifndef NO_HASHTABLE
	BENCH_FOR() {
		HashTable t[1] = {{ .nelems = items }};
		bench(
			*a, t, (Func *)hashtable_find,
			rng_str, rng_str2, items, slen, "HTable"
		);
	}
#endif

#ifndef NO_TREAP
	BENCH_FOR() {
		Treap *t[1] = {0};
		bench(
			*a, t, (Func *)treap_find,
			rng_str, rng_str2, items, slen, "Treap"
		);
		//fprintf(stdout, "depth :: %d\n", treap_depth(t[0], 0));
	}
#endif

#ifndef NO_JOINTREAP
	BENCH_FOR() {
		JTreap *t[1] = {0};
		bench(
			*a, t, (Func *)treap_join_find,
			rng_str, rng_str2, items, slen, "JTreap"
		);
		//fprintf(stdout, "depth :: %d\n", treap_depth(t[0], 0));
	}
#endif

#ifndef NO_HTHTREE
	BENCH_FOR() {
		HTHTree t[1] = {0};
		bench(
			*a, t, (Func *)hth_find,
			rng_str, rng_str2, items, slen, "HTHTree"
		);
		//fprintf(stdout, "depth :: %d\n", hth_depth(t));
		//hth_graphviz(stderr, t->slots[0]);
	}
#endif

#ifndef NO_HAMT
	BENCH_FOR() {
		HAMT t[1] = {0};
		bench(
			*a, t, (Func *)hamt_find,
			rng_str, rng_str2, items, slen, "HAMT"
		);
		// TODO: hamt_depth
	}
#endif

#ifndef NO_HTRIE
	BENCH_FOR() {
		HTrie *t[1] = {0};
		bench(
			*a, t, (Func *)htrie_find,
			rng_str, rng_str2, items, slen, "HTrie"
		);
		//htrie_graphvis(stderr, *t);
	}
#endif

#if DEBUG
	free(a);
#endif
}
