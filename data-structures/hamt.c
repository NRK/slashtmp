// experimenting with hash array mapped trie (hamt)
//
// preliminary thoughts:
// it's an interesting data structure and given a proper inner array size (32
// from my casual testing) it can compete head to head with some well
// performing (open-addressed double-hashed) hash-tables.
// unfortunately the memory cost even at 16 array size is way too much for my
// taste. combined with how finicky it is to implement, this means i'm likely
// not going to be using this much.
// (note: one of the requirements i had was no resizes, so the trick with
// bitmap and dynamic inner array falls outside the scope.)
//
// some implementation notes:
// the morph member is essentially a tagged union. the lower bits determinate
// the type which can be either a pointer to the next node, pointer to a linked
// list (to resolve hash collusion) or upper bits of the key-hash.
// this is safe to do because due to alignment requirements, the last couple
// bits of either a node pointer or a list pointer will be zero (which is
// statically asserted for). the hash is used when replanting a key into a new
// node to avoid hashing the string again.
//
// note that when a slot is SLOT_NEXT, the key is essentially unused space.
// i haven't spent too much time with this, but i suspect there's a smarter way
// to lay things out in memory to avoid wasting so much space.
//
// for a benchmark: see data-structures/search-tree-benchmark.c
//
// ref: https://en.wikipedia.org/wiki/Hash_array_mapped_trie
#include <stddef.h>
#include <stdint.h>

#include <stdio.h>
#include <stdlib.h>

#define ASSERT(X)  ((X) ? (void)0 : __builtin_unreachable())

typedef uint8_t    u8;
typedef uint64_t   u64;
typedef ptrdiff_t  Size;
typedef struct { u8 *s; Size len; } Str;
typedef struct { u8 *off, *end; } Arena;
enum { ALIGN = sizeof(void *) };
enum {
	ARENA_ZERO = 1 << 0,
};

// hamt types
typedef struct StrList { Str key; struct StrList *next; } StrList;
enum { NODE_BITS = 4, NODE_CNT = 1 << NODE_BITS, NODE_MASK = NODE_CNT - 1 };
enum { SLOT_EMPTY, SLOT_KEY, SLOT_NEXT, SLOT_LIST };
typedef struct {
	Str key;
	uintptr_t morph; // HNode-next || list-next || key-hash
} HNode;
typedef struct { HNode slots[NODE_CNT]; } HTrie;

_Static_assert(SLOT_LIST == 0x3, "");
_Static_assert(_Alignof(HTrie *) >= 4 && _Alignof(StrList *) >= 4, "needed for bit smuggling");
_Static_assert(sizeof(uintptr_t) >= sizeof(uint32_t), "smuggling hash");
#define MORPH_MASK     ((uintptr_t)0x3)
#define MORPH_TYPE(X)  ((X) & MORPH_MASK)
#define MORPH_PTR(X)   ((void *)((X) & ~MORPH_MASK))
_Static_assert(NODE_MASK >= MORPH_MASK, "required to safely drop the low bits of the hash when merging to morph");

static Arena *
arena_create(void *mem, Size n)
{
	static const Arena nil_arena[1] = {{ .off = (void *)nil_arena, .end = (void *)nil_arena }};
	Size aligned_arena = sizeof(Arena) + (-sizeof(Arena) & (ALIGN - 1));
	ASSERT(n > aligned_arena); ASSERT((ALIGN & (ALIGN-1)) == 0);
	Arena *ret = (void *)nil_arena;
	if (mem != NULL) {
		ret = mem;
		ret->off = (u8 *)mem + aligned_arena;
		ret->end = (u8 *)mem + n;
	}
	return ret;
}

__attribute((alloc_size(2), malloc))
static void *
arena_alloc(Arena *a, Size size, u64 flags)
{
	Size rsize = size + (-size & (ALIGN - 1));
	Size avail = a->end - a->off;
	if (rsize > avail) {
		abort();
	}
	u8 *ret = a->off;
	a->off += rsize;
	if (flags & ARENA_ZERO) for (Size i = 0; i < size; ++i) {
		ret[i] = 0x0;
	}
	return ret;
}

static int
str_eq(Str a, Str b)
{
	if (a.len != b.len) {
		return 0;
	}
	for (Size i = 0; i < a.len; ++i) {
		if (a.s[i] != b.s[i]) {
			return 0;
		}
	}
	return 1;
}

static uint32_t
str_hash32(Str s)
{
	uint32_t h = 55555 + s.len;
	for (Size i = 0; i < s.len; ++i) {
		h ^= s.s[i];
		h *= 0x13375AAD;
	}
	h ^= h >> 15;
	return h;
}

static int
read_line(Arena *a, Str *s, FILE *f)
{
	Str max = { a->off, a->end - a->off };
	int rc = 0;
	Size len;
	for (len = 0; len < max.len; ++len) {
		int c = fgetc(f);
		if (c == EOF || c == '\n') {
			rc = (c == EOF && len == 0) ? -1 : 0;
			break;
		} else {
			max.s[len] = c;
		}
	}
	int oom = (len == max.len);
	*s = (Str){ arena_alloc(a, len + oom, 0), len };
	ASSERT(s->s == max.s);
	return rc;
}

static int
hamt_insert(Arena *a, HTrie *t, Str key)
{
	uint32_t hash = str_hash32(key);
	for (int i = 0; i < 32; i += NODE_BITS) {
		int idx = (hash >> i) & NODE_MASK;
		HNode *np = t->slots + idx;
		int mtype = MORPH_TYPE(np->morph);
		switch (mtype) {
		case SLOT_EMPTY:
			np->key = key;
			np->morph = (hash & ~MORPH_MASK) | SLOT_KEY;
			return 1;
			break;
		case SLOT_KEY:
			if (str_eq(key, np->key)) {
				return 0;
			} else if (i + NODE_BITS < 32) {
				Str oldkey = np->key;
				uintptr_t oldm = np->morph;
				HTrie *tmp = arena_alloc(a, sizeof *tmp, ARENA_ZERO);
				np->morph = (uintptr_t)tmp | SLOT_NEXT;
				// reinsert oldkey
				uint32_t oldhash = oldm & ~MORPH_MASK; // redundant, since it'll get shifted out below
				int oldidx = (oldhash >> (i + NODE_BITS)) & NODE_MASK;
				tmp->slots[oldidx].key = oldkey;
				tmp->slots[oldidx].morph = oldm;
				// "advance" t
				t = tmp;
			} else { // turn it into a list
				StrList *tmp = arena_alloc(a, sizeof *tmp, 0);
				tmp->key = key; tmp->next = NULL;
				np->morph = (uintptr_t)tmp | SLOT_LIST;
				return 1;
			}
			break;
		case SLOT_NEXT:
			t = MORPH_PTR(np->morph);
			ASSERT(t != NULL);
			break;
		case SLOT_LIST: {
			StrList head = { np->key, MORPH_PTR(np->morph) };
			StrList *it = &head, *tail = it;
			for (; it != NULL; tail = it, it = it->next) {
				if (str_eq(key, it->key)) {
					return 0;
				}
			}
			StrList *tmp = arena_alloc(a, sizeof *tmp, 0);
			tmp->key = key; tmp->next = NULL;
			tail->next = tmp;
			return 1;
		} break;
		default: ASSERT(!"unreachable"); break;
		}
	}
	ASSERT(!"unreachable");
	return 0;
}

extern int
main(void)
{
	enum { ASIZE = 1 << 30 };
	Arena *a = arena_create(malloc(ASIZE), ASIZE);
	HTrie t[1] = {0};

	for (;;) {
		Arena checkpoint = *a;
		Str line;
		if (read_line(a, &line, stdin) < 0) {
			break;
		}
		int inserted = hamt_insert(a, t, line);
		if (!inserted) {
			*a = checkpoint; // discard the allocations
			puts("Exists! Discarding...");
		} else {
			puts("Doesn't exist, inserting...");
		}
	}

#if DEBUG
	free(a);
#endif
	fflush(stdout);
	return ferror(stdout) || ferror(stdin);
}
