/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#include <stdlib.h>
#include <stdio.h>

#include "td.h"

extern int
main(void)
{
	/*
	 * td(T) declares a 2d array of type T.
	 * can be typedef-ed if you want to pass it around to other functions.
	 *	typedef td(int) Td_Int;
	 *	static void func(Td_Int *tp);
	 */
	td(int) m;

	/*
	 * td_init() returns the size that needs to be allocated.
	 */
	m.buf = malloc(td_init(m, 2, 4));
	if (m.buf == NULL)
		return 1;

	for (size_t r = 0, n = 0; r < m.row; ++r) {
		for (size_t c = 0; c < m.col; ++c) {
			/*
			 * td_deref() yields an lvalue that can be assigned to.
			 */
			td_deref(m, r, c) = n++;
		}
	}

	for (size_t r = 0; r < m.row; ++r) {
		putchar('|');
		for (size_t c = 0; c < m.col; ++c) {
			printf(" %2d |", td_deref(m, r, c));
		}
		putchar('\n');
	}

	/*
	 * the user is responsible for free-ing the buffer
	 */
	free(m.buf);
	return 0;
}
