#ifndef INCLUDE_TD_H_
#define INCLUDE_TD_H_

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#define td(T) struct { T *buf; size_t row, col; }

#define td_init(M, R, C) ( ((M).row = R) * ((M).col = C) * sizeof (M).buf[0] )

#define td_deref(M, R, C) \
	(M).buf[(R) * (M).col + C]

#endif /* INCLUDE_TD_H_ */
