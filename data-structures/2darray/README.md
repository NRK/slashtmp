# td.h

Set of macros implementing type-safe generic 2d array. The implementation is
good enough to fulfill decent amount of use-cases despite being merely a
rudimentary proof-of-concept which wasn't designed to be fully-featured.

The header `td.h` is [freestanding][] as it only requires `<stddef.h>` for
`size_t`. Allocation/deallocation has to be done by the user.

The "library" is quite small and only defines 3 macros, so it should be fairly
self-explanatory how it all works. See [`example.c`](example.c) for an
introduction.

[freestanding]: https://port70.net/~nsz/c/c11/n1570.html#5.1.2.1

### Room for improvement

- [ ] Check for mul wraparound in `td_init`. (trivial difficulty)
- [ ] Allow growing/shrinking the column size. This is probably a bit more
      involved and it'd most likely require a dependency on `<string.h>` for
      `memmove()`-ing things around.
