// generic zero allocation min-heap/priority-queue
//
// pros:
// the heap doesn't make any assumptions on how to allocate memory.
// which means you can pick what's appropriate for your situation: dynamic
// array, custom allocator or even a fixed size array.
// the implementation is also nearly freestanding, requiring only memcpy()
//
// limitation:
// the priority needs to be stored explicitly
// the priority needs to be an `int`
//
// usage:
// the elements of the heap needs to be a struct with an `int` member which
// will act as the "cost"/"priority". you can also use this as a max-heap by
// simply negating the "cost" member.
//
// the first three argument in both push and pop functions are:
//   0. pointer to the backing array
//   1. pointer to a ptrdiff_t which holds the current length of the array (initially 0)
//   2. name of the "cost" member
// the last argument is a pointer to the item to push in case of push() and an
// output pointer in case of pop().
//
// although the implementation uses void pointers internally, the interface
// macros make use of ternary operator to ensure that the 1st and 4th argument
// have compatible pointers. if not, then your compiler should produce an
// incompatible pointer warning.
#include <limits.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>

static int
load_cost(uint8_t *heap, ptrdiff_t item_size, ptrdiff_t keyoffset, ptrdiff_t idx)
{
	int *p = (int *)(heap + (idx * item_size) + keyoffset);
	return *p;
}

static void
heap_push(
	void *heap, ptrdiff_t *heap_len, ptrdiff_t keyoffset,
	void *item, ptrdiff_t item_size
)
{
	ptrdiff_t new = (*heap_len)++;
	int newcost = load_cost(item, 0, keyoffset, 0);

	ptrdiff_t parent = (new - 1) / 2;
	while (new != 0 && load_cost(heap, item_size, keyoffset, parent) > newcost) {
		uint8_t *newp    = heap + (new    * item_size);
		uint8_t *parentp = heap + (parent * item_size);
		memcpy(newp, parentp, item_size);
		new = parent;
		parent = (new - 1) / 2;
	}
	uint8_t *newp = heap + (new * item_size);
	memcpy(newp, item, item_size);
}
#define heap_push(heap, heap_len, cost, item) heap_push( \
	(uint8_t *)(heap), (heap_len), \
	(uint8_t *)&(heap)->cost - (uint8_t *)(heap), \
	(1 ? (item) : (heap)), sizeof (heap)[0] \
)

static void
heap_pop(
	uint8_t *heap, ptrdiff_t *heap_len, ptrdiff_t keyoffset,
	ptrdiff_t item_size, void *out
)
{
	ptrdiff_t last = --(*heap_len);
	memcpy(out, heap, item_size);
	int fix = 0;
	int fixcost = load_cost(heap, item_size, keyoffset, last);
	for (;;) {
		int l = (fix + 1) * 2 - 1;
		int r = (fix + 1) * 2;
		int lcost = (l < *heap_len) ? load_cost(heap, item_size, keyoffset, l) : INT_MAX;
		int rcost = (r < *heap_len) ? load_cost(heap, item_size, keyoffset, r) : INT_MAX;
		int small = (lcost <= rcost) ? l : r;
		int smallcost = (lcost <= rcost) ? lcost : rcost;
		if (small < *heap_len && smallcost < fixcost) {
			memcpy(heap + (fix * item_size), heap + (small * item_size), item_size);
			fix = small;
		} else {
			break;
		}
	}
	memcpy(heap + (fix * item_size), heap + (last * item_size), item_size);
}
#define heap_pop(heap, len, cost, output) heap_pop( \
	(uint8_t *)(heap), (len), \
	(uint8_t *)&(heap)->cost - (uint8_t *)(heap), \
	sizeof (heap)[0], (1 ? (output) : (heap)) \
)

/////////////////
// demo + test
#include <stdio.h>

#define ASSERT(EXPR)   (!(EXPR) ? __builtin_unreachable() : (void)0)

static uint32_t
rng32(uint64_t *state)
{
	uint64_t oldstate = *state;
	*state *= 1111111111111111111;
	*state += 0x1337;
	return (oldstate >> (oldstate >> 59)) ^ oldstate;
}

typedef struct { int cost, key; } Item;

static void
heap_validate(Item *heap, ptrdiff_t len)
{
	for (int k = 0; k < len; ++k) {
		int l = (k + 1) * 2 - 1;
		int r = (k + 1) * 2;
		if (l < len) {
			ASSERT(heap[k].cost <= heap[l].cost);
		}
		if (r < len) {
			ASSERT(heap[k].cost <= heap[r].cost);
		}
	}
}

extern int
main(void)
{
	uint64_t state = -(uintptr_t)&state;

	enum { N = 8 };
	Item heap[N];
	ptrdiff_t heap_len = 0;

	for (int i = 0; i < N; ++i) {
		Item item = { rng32(&state), i };
		heap_push(heap, &heap_len, cost, &item);
		printf("{ %12d, %2d }\n", item.cost, item.key);
		heap_validate(heap, heap_len);
	}
	printf("====== pushed ======\n");


	//////// should produce warnings due to incompatible pointers:
	// heap_push(heap, &heap_len, cost, &(ptrdiff_t){0});
	// heap_pop(heap, &heap_len, cost, &(int){0});

	while (heap_len > 0) {
		Item item;
		heap_pop(heap, &heap_len, cost, &item);
		heap_validate(heap, heap_len);
		printf("{ %12d, %2d }\n", item.cost, item.key);
	}
	printf("====== popped ======\n");
}
