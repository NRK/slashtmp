// rudimentary benchmark for "unrolled linked lists" (i.e a list of arrays).
// ref: https://en.wikipedia.org/wiki/Unrolled_linked_list
//
// linked lists work well with linear allocators since it doesn't require
// contiguous space as opposed to a dynamic array. it's not too difficult to
// support relocation/resize in a custom allocator - but that inherently
// introduces potential fragmentation issues.
//
// but OTOH, iterating a list is quite slow. "cache-misses" is a typical
// response but every time i've benchmarked i've found the culprit to be the
// loop-carried-dependency; i.e each iteration depends on the "next" pointer
// being loaded from the previous iteration.
//
// unrolled list attempts to solve this problem by putting multiple elements
// into a single node. so that the "next" pointer fetch can be overlapped with
// the "inner-loop" hiding the latency of the pointer fetch.
//
// Pros:
//    a) iterating can be as fast as dynamic arrays with an appropriately
//       picked inner array size.
//    b) avoids fragmentation issues due to no relocation requirement.
//    c) works well with simple linear allocators due to point (b).
//    d) less memory usage compared to traditional lists by avoiding the
//       (usually) 8 byte pointer overhead per element.
// Cons:
//    a) awkward to use due to being a hybrid data-structure.
//    b) lots of operation that are easy to do in a "traditional" linked list
//       become much more difficult and error-prone due to point (a).
//    c) no O(1) random access compared to arrays.
//
// benchmark:
//    $ gcc -o out misc/u-list.c -O3 -march=native
// debug build which includes some techniques for catching spatial memory bugs
// with a custom allocator:
//    $ gcc -o out misc/u-list.c -g3 -fsanitize=address,undefined -DDEBUG
#include <stddef.h>
#include <stdint.h>

#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ASSERT(X) ((X) ? (void)0 : __builtin_unreachable())
#if DEBUG
	extern void __asan_poison_memory_region(void const volatile *, size_t);
	extern void __asan_unpoison_memory_region(void const volatile *, size_t);
	#define POISON(P, L)   __asan_poison_memory_region((P), (L))
	#define UNPOISON(P, L) __asan_unpoison_memory_region((P), (L))
	// leaks are rare when using an arena. so disable noisy leak checks.
	extern const char * __asan_default_options(void) { return "detect_leaks=0:abort_on_error=1"; }
#else
	#define POISON(P, L)   ((void)0)
	#define UNPOISON(P, L) ((void)0)
#endif

typedef uint8_t     u8;
typedef uint32_t    u32;
typedef ptrdiff_t   Size;

typedef struct {
	u8 *buf;
	Size off, cap;
} Arena;

__attribute((alloc_size(2), malloc))
static void *
arena_alloc(Arena *a, Size size)
{
	ASSERT(size >= 0);
	Size align = sizeof(void *);
	Size rsz = size + (-size & (align - 1));
	if (rsz > a->cap - a->off) {
		abort(); // OOM
	}
	void *ret = a->buf + a->off;
	ASSERT((uintptr_t)ret % align == 0);
	a->off += rsz;

#if DEBUG
	UNPOISON(ret, size);
	a->off += align * 4; // leave some poisoned hole
	u8 *poison = (u8 *)ret + size;
	Size psize = (a->buf + a->cap) - poison;
	POISON(poison, psize);
#endif

	return ret;
}

__attribute((always_inline))
static inline void
perm(u32 *p)
{
	*p += 0x33333333;
	*p ^= *p >> 15;
	*p *= 0xACDC5AAD;
	*p ^= *p >> 16;
	*p *= 0x1337AAAD;
	*p ^= *p >> 15;
}

extern int
main(void)
{
	Arena a[1] = {{ .cap = 1 << 28 }};
	a->buf = malloc(a->cap);
	if (a->buf == NULL) {
		abort();
	}
	// force all the pages to be mapped to avoid skewing the benchmark
	memset(a->buf, 0xCD, a->cap);
	POISON(a->buf, a->cap);

#if DEBUG
	enum { N = 1 << 10 };
#else
	enum { N = 1 << 20 };
#endif
	printf("           init\t\tpermute\t\tmem-usage\n");
	clock_t start;

	{
		Arena scratch = *a;
		start = clock();
		u32 *v = arena_alloc(&scratch, N * sizeof *v);
		for (Size i = 0; i < N; ++i) {
			v[i] = i;
		}
		printf("array:     %ld", clock() - start);

		start = clock();
		for (Size i = 0; i < N; ++i) {
			perm(v + i);
		}
		printf("\t\t%ld\t\t%jd\n", clock() - start, scratch.off);
	}

	{
		typedef struct list { u32 val; struct list *next; } List;
		static List nil[1] = {{ .next = nil }};
		POISON(nil, sizeof nil);

		Arena scratch = *a;
		List *head = nil;
		List *tail = nil;

		start = clock();
		for (Size i = 0; i < N; ++i) {
			List *tmp = arena_alloc(&scratch, sizeof *tmp);
			*(tail == nil ? &head : &tail->next) = tmp;
			tail = tmp;
			tail->next = nil;
			tail->val = i;
		}
		printf("list:      %ld", clock() - start);

		start = clock();
		for (List *it = head; it != nil; it = it->next) {
			perm(&it->val);
		}
		printf("\t\t%ld\t\t%jd\n", clock() - start, scratch.off);
	}

	{
		// NOTE: both gcc and clang (especially clang) seems to perform
		// significantly worse if it's odd or non-power-of-2
		enum { BN = 32 };
		typedef struct list { struct list *next; int i; u32 val[BN]; } List;
		static List nil[1] = {{ .next = nil, .i = BN }};
		// unfortunately, there's no "read-only" protection mode in ASan
		/* POISON(nil, sizeof nil); */

		Arena scratch = *a;
		List *head = arena_alloc(&scratch, sizeof *head);
		head->i = 0; head->next = nil;
		List *tail = head;

		start = clock();
		for (Size i = 0; i < N; ++i) {
			if (tail->i == BN) {
				tail->next = arena_alloc(&scratch, sizeof tail->next[0]);
				tail = tail->next;
				tail->i = 0; tail->next = nil;
			}
			tail->val[tail->i++] = i;
		}
		printf("u-list:    %ld", clock() - start);

		start = clock();
		for (List *it = head; it != nil; it = it->next) {
			// doesn't seem to make any noticeable difference on my system
			//__builtin_prefetch(it->next);

			// NOTE: doing `k < it->i` in the loop condition slows
			// things down *massively*, hence the manual hoisting
			// out to `n`
			for (int k = 0, n = it->i; k < n; ++k) {
				perm(it->val + k);
			}
		}
		printf("\t\t%ld\t\t%jd\n", clock() - start, scratch.off);
	}
}
