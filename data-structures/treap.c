// some implementation notes:
//
// using a PRNG for random priority would A) require passing around the PRNG
// state or using a global state (both of which are undesirable) B) require
// additional memory for storing a `priority` member per node.
//
// both of these problem can be avoided by storing the priority _implicitly_
// inside the node - either by hashing the node pointer itself or by hashing
// the key - as a source of randomness. this implementation does the former by
// default, and the latter if HASHKEY is defined.
//
// additionally you can save one pointer (8 bytes on most machine) per element
// by using a "parent-stack" instead of storing the parent pointer inside the
// node. aside from saving memory, this massively simplifies the tree-rotation
// as well.
// even when inserting ~4million keys, the treap depth doesn't get beyond ~57.
// as such, this implementation simply has a 128 limit on the parent-stack.
//
// an arena is used for backing the treap. this allows for fast allocation,
// easier memory management, and O(1) tree destruction compared to chasing and
// free-ing each node individually in O(n) time.
//
// for a benchmark: see data-structures/search-tree-benchmark.c
//
// build: gcc -o out data-structures/treap.c -g3 -fsanitize=address,undefined -DDEBUG [-DHASHKEY]
//
// ref: https://en.wikipedia.org/wiki/Treap
// ref: https://en.wikipedia.org/wiki/Region-based_memory_management
#include <stddef.h>
#include <stdint.h>

#include <stdio.h>
#include <stdlib.h>

#define ASSERT(X)  ((X) ? (void)0 : __builtin_unreachable())

typedef uint8_t    u8;
typedef uint64_t   u64;
typedef ptrdiff_t  Size;
typedef struct { u8 *s; Size len; } Str;
typedef struct Treap {
	Str key;
	struct Treap *child[2];
} Treap;
// experiment: https://www.rfleury.com/p/the-codepath-combinatoric-explosion
static const Treap TREAP_NIL[1] = {{
#define TREAP_NIL ((Treap *)TREAP_NIL)
	.child  = { TREAP_NIL, TREAP_NIL },
}};

typedef struct { u8 *off, *end; } Arena;
enum { ALIGN = sizeof(void *) };

static Arena *
arena_create(void *mem, Size n)
{
	static const Arena nil_arena[1] = {{ .off = (void *)nil_arena, .end = (void *)nil_arena }};
	Size aligned_arena = sizeof(Arena) + (-sizeof(Arena) & (ALIGN - 1));
	ASSERT(n > aligned_arena); ASSERT((ALIGN & (ALIGN-1)) == 0);
	Arena *ret = (void *)nil_arena;
	if (mem != NULL) {
		ret = mem;
		ret->off = (u8 *)mem + aligned_arena;
		ret->end = (u8 *)mem + n;
	}
	return ret;
}

__attribute((alloc_size(2), malloc))
static void *
arena_alloc(Arena *a, Size size)
{
	Size rsize = size + (-size & (ALIGN - 1));
	Size avail = a->end - a->off;
	if (rsize > avail) {
		abort();
	}
	void *ret = a->off;
	a->off += rsize;
	return ret;
}

static int
str_cmp(Str a, Str b)
{
	if (a.len != b.len) {
		return (a.len < b.len) - (a.len > b.len);
	}
	for (Size i = 0; i < a.len; ++i) {
		int r = (a.s[i] < b.s[i]) - (a.s[i] > b.s[i]);
		if (r != 0) {
			return r;
		}
	}
	return 0;
}

static int
read_line(Arena *a, Str *s, FILE *f)
{
	Str max = { a->off, a->end - a->off };
	int rc = 0;
	Size len;
	for (len = 0; len < max.len; ++len) {
		int c = fgetc(f);
		if (c == EOF || c == '\n') {
			rc = (c == EOF && len == 0) ? -1 : 0;
			break;
		} else {
			max.s[len] = c;
		}
	}
	int oom = (len == max.len);
	*s = (Str){ arena_alloc(a, len + oom), len };
	ASSERT(s->s == max.s);
	return rc;
}

static uintptr_t
treap_priority(Treap *p)
{
	enum { W = sizeof(uintptr_t) * 4 };
	uintptr_t m = 0x1337ACDCBABE5AAD;
#ifdef HASHKEY
	uintptr_t h = 55555 + p->key.len;
	for (Size i = 0; i < p->key.len; ++i) {
		h ^= p->key.s[i];
		h *= m;
	}
	h ^= h >> (W + 1);
	return h;
#else
	uintptr_t h = (uintptr_t)p;
	h ^= h >> (W + 1);
	h *= m;
	h ^= h >> (W - 1);
	return h;
#endif
}

static int
treap_insert(Arena *a, Treap **t, Str key)
{
	enum { CAP = 128 };
	Treap *pstack[CAP], *parent = NULL, **insert = t;
	int phead = 0;
	while (*insert != TREAP_NIL) {
		parent = *insert;
		if (phead == CAP) abort();
		pstack[phead++] = parent;
		switch (str_cmp(parent->key, key)) {
		case  0: return 1; break;
		case -1: insert = parent->child + 0; break;
		case  1: insert = parent->child + 1; break;
		default: ASSERT(!"unreachable"); break;
		}
	}
	ASSERT(*insert == TREAP_NIL);
	Treap *new = arena_alloc(a, sizeof *new);
	*new = (Treap){
		.key = key,
		.child = { TREAP_NIL, TREAP_NIL },
	};
	*insert = new;

	uintptr_t new_priority = treap_priority(new);
	while (phead > 0 && treap_priority((parent = pstack[--phead])) < new_priority) {
		if (phead > 0) { // update grandpa
			Treap *gp = pstack[phead-1];
			gp->child[gp->child[1] == parent] = new;
		} else { // otherwise update root
			*t = new;
		}
		int d = parent->child[1] == new;
		parent->child[d] = new->child[!d]; // update parent
		new->child[!d] = parent; // update node
	}

	return 0;
}

static void
treap_validate(Treap *t, Treap *parent)
{
#if DEBUG
	if (t == TREAP_NIL) {
		return;
	}
	if (parent != TREAP_NIL) {
		ASSERT(treap_priority(parent) >= treap_priority(t));
		int d = parent->child[1] == t;
		int cmp = str_cmp(parent->key, t->key);
		if (d) {
			ASSERT(cmp > 0);
		} else {
			ASSERT(cmp < 0);
		}
	}
	treap_validate(t->child[0], t);
	treap_validate(t->child[1], t);
#endif
}

static int
treap_depth(Treap *t, int depth)
{
	if (t == TREAP_NIL) {
		return depth;
	}
	int l = treap_depth(t->child[0], depth + 1);
	int r = treap_depth(t->child[1], depth + 1);
	return l > r ? l : r;
}

extern int
main(void)
{
	enum { ASIZE = 1 << 20 };
	Arena *a = arena_create(malloc(ASIZE), ASIZE);
	Treap *t = TREAP_NIL;

	for (;;) {
		Arena checkpoint = *a;
		Str line;
		if (read_line(a, &line, stdin) < 0) {
			break;
		}
		int exists = treap_insert(a, &t, line);
		if (exists) {
			*a = checkpoint; // discard the allocations
			puts("Exists! Discarding...");
		} else {
			puts("Doesn't exist, inserting...");
		}
		treap_validate(t, TREAP_NIL);
	}
	printf("max depth :: %d\n", treap_depth(t, 0));

#if DEBUG
	free(a);
#endif
	fflush(stdout);
	return ferror(stdout) || ferror(stdin);
}
