# isdigit

[Experimenting and benchmarking various implementation of `isdigit()`][ar].

[ar]:  https://nrk.neocities.org/articles/isdigit-multi-implementation.html
