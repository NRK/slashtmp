/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

static int
isdigit_simple(unsigned char c)
{
	return c >= '0' && c <= '9';
}

static int
isdigit_lut256(unsigned char c)
{
	static const unsigned char tbl[256] = {
		['0'] = 1, ['1'] = 1, ['2'] = 1, ['3'] = 1, ['4'] = 1,
		['5'] = 1, ['6'] = 1, ['7'] = 1, ['8'] = 1, ['9'] = 1,
	};
	return tbl[c];
}

static int
isdigit_lut32(unsigned char c)
{
	enum { UBITS = sizeof(unsigned int) * 8 }; /* assumes CHAR_BIT == 8 */
	static const unsigned int tbl[256 / UBITS] = {
		['0' / UBITS] = 1023U << ('0' % UBITS),
	};
	return tbl[c / UBITS] & (1U << (c % UBITS));
}

static int
isdigit_bithacks(unsigned char c)
{
	unsigned int ge0 = (unsigned int)c + ((0xFFU - '0') + 1);
	unsigned int gt9 = (unsigned int)c + (0xFFU - '9');
	return (ge0 ^ gt9) >> 8;
}

#ifdef TEST

#include <stdio.h>
#include <ctype.h>
#include <assert.h>

extern int
main(void)
{
	unsigned int i;
	for (i = 0; i < 256; ++i) {
		assert(isdigit_simple(i)   == !!isdigit((unsigned char)i));
		assert(isdigit_lut256(i)   == !!isdigit((unsigned char)i));
		assert(!!isdigit_lut32(i)  == !!isdigit((unsigned char)i));
		assert(isdigit_bithacks(i) == !!isdigit((unsigned char)i));
	}
	printf("TESTS: [PASS]\n");
	printf("=============\n");

	return 0;
}

#endif /* TEST */

#if defined(BENCH_ONEOFF) || defined(BENCH_LOOP)

#define _XOPEN_SOURCE 700L
#include <stdio.h>
#include <time.h>
#define TDIFF(t1, t2) \
	((double)(t1.tv_sec  - t2.tv_sec)  * 1000 + \
	 (double)(t1.tv_nsec - t2.tv_nsec) / 1E6)

#endif /* defined(BENCH_ONEOFF) || defined(BENCH_LOOP) */


#ifdef BENCH_ONEOFF
extern int
main(void)
{
	struct timespec time_start, time_end;
	unsigned char c = getchar();
	clock_gettime(CLOCK_MONOTONIC, &time_start);
#if defined(BENCH_SIMPLE)
	volatile int _ = isdigit_simple(c);
#elif defined(BENCH_LUT256)
	volatile int _ = isdigit_lut256(c);
#elif defined(BENCH_LUT32)
	volatile int _ = isdigit_lut32(c);
#elif defined(BENCH_BITHACKS)
	volatile int _ = isdigit_bithacks(c);
#endif
	clock_gettime(CLOCK_MONOTONIC, &time_end);
	printf("%f\n", TDIFF(time_end, time_start));
	return 0;
}
#endif /* BENCH_ONEOFF */

#ifdef BENCH_LOOP
extern int
main(void)
{
	struct timespec time_start, time_end;
	unsigned int i;
	unsigned char buf[1 << 16];

	if (fread(buf, 1, sizeof buf, stdin) != sizeof buf)
		return 1;

	clock_gettime(CLOCK_MONOTONIC, &time_start);
	for (i = 0; i < sizeof buf; ++i) {
#if defined(BENCH_SIMPLE)
		volatile int _ = isdigit_simple(buf[i]);
#elif defined(BENCH_LUT256)
		volatile int _ = isdigit_lut256(buf[i]);
#elif defined(BENCH_LUT32)
		volatile int _ = isdigit_lut32(buf[i]);
#elif defined(BENCH_BITHACKS)
		volatile int _ = isdigit_bithacks(buf[i]);
#endif
	}
	clock_gettime(CLOCK_MONOTONIC, &time_end);
	printf("%f\n", TDIFF(time_end, time_start));

	return 0;
}
#endif /* BENCH_LOOP */
