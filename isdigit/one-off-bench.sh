#!/bin/sh

niter=$2
lows=$(( niter / 50 )) # 2%
tmp="${TMPDIR:-/tmp}/one-off-bench-results"
trap 'rm -f $tmp' EXIT

n=0
while [ "$n" -lt "$niter" ]; do
  "$1" < /dev/urandom
  n=$(( n + 1 ))
done > "$tmp"

do_avg() {
  paste -s -d '+' |
    { printf '('; cat ; printf ') / %s' "$1"; } |
    paste -s -d ''  |
    printf '%f ' "$(</dev/stdin bc -l)"
}

printf '%18s: ' "$1"
printf 'avg: '; do_avg "$niter" < "$tmp"
printf "%s" '| 2% lows: '; <"$tmp" sort -nr | head -n "$lows" | do_avg "$lows"
echo
