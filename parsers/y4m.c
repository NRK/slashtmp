// freestanding y4m parser: https://wiki.multimedia.cx/index.php/YUV4MPEG2
// only parses the format, doesn't do any colorspace conversion.
// comments and frame parameters are ignored.
//
// This is free and unencumbered software released into the public domain.
// For more information, please refer to <https://unlicense.org/>

#include <limits.h>
#include <stddef.h>
#include <stdint.h>

#ifndef Y4M_PARSE_API
#define Y4M_PARSE_API extern
#endif

enum Y4mParseStatus {
	Y4M_PARSE_OK,
	Y4M_PARSE_NOT_Y4M,
	Y4M_PARSE_EOF,
	Y4M_PARSE_CORRUPTED,
	Y4M_PARSE_UNSUPPORTED,
};

typedef struct {
	ptrdiff_t w, h;
	int64_t frametime_us; // frametime in micro-seconds
	enum {
		Y4M_PARSE_CS_UNSUPPORTED = -1,
		Y4M_PARSE_CS_420, // default picked from ffmpeg
		Y4M_PARSE_CS_420JPEG,
		Y4M_PARSE_CS_420MPEG2, // supported by ffmpeg
		Y4M_PARSE_CS_420PALDV,
		Y4M_PARSE_CS_422,
		Y4M_PARSE_CS_444,
		Y4M_PARSE_CS_MONO,
	} colour_space;
	enum {
		Y4M_PARSE_IL_PROGRESSIVE,
		Y4M_PARSE_IL_TOP,
		Y4M_PARSE_IL_BOTTOM,
		Y4M_PARSE_IL_MIXED,
	} interlacing;
	int pixel_aspect_w, pixel_aspect_h;

	const void *frame_data;
	ptrdiff_t frame_data_len;
	const void *y, *u, *v;
	ptrdiff_t y_stride, u_stride, v_stride;

	// private
	const uint8_t *p, *end;
} Y4mParse;

Y4M_PARSE_API enum Y4mParseStatus y4m_parse_init(
	Y4mParse *res, const void *buffer, ptrdiff_t size
);
Y4M_PARSE_API enum Y4mParseStatus y4m_parse_frame(Y4mParse *res);

#ifndef Y4M_PARSE_ASSERT
	#if    defined(DEBUG) &&  defined(__GNUC__)
		#define Y4M_PARSE_ASSERT(X)   ((X) ? (void)0 : __builtin_trap())
	#elif  defined(DEBUG) && !defined(__GNUC__)
		#define Y4M_PARSE_ASSERT(X)   ((X) ? (void)0 : (void)(*(volatile int *)0 = 0))
	#elif !defined(DEBUG) &&  defined(__GNUC__)
		#define Y4M_PARSE_ASSERT(X)   ((X) ? (void)0 : __builtin_unreachable())
	#elif !defined(DEBUG) && !defined(__GNUC__)
		#define Y4M_PARSE_ASSERT(X)   ((void)0)
	#endif
#endif

// implementation

static int
y4m__int(ptrdiff_t *out, const uint8_t **p, const uint8_t *end)
{
	uint64_t n = 0;
	const uint8_t *s = *p;
	for (; s < end && *s >= '0' && *s <= '9'; ++s) {
		n = (n * 10) + (*s - '0');
		if (n > INT_MAX) {
			return 0;
		}
	}
	*out = n;
	*p = s;
	return 1;
}

static int
y4m__match(
	const char *match, ptrdiff_t mlen,
	const uint8_t **p, const uint8_t *end
)
{
	const uint8_t *m = (uint8_t *)match, *mend = m + mlen;
	const uint8_t *s = *p;
	for (; s < end && m < mend && *s == *m; ++s, ++m) {}
	if (m == mend) {
		*p = s;
		return 1;
	}
	return 0;
}

static enum Y4mParseStatus
y4m__parse_params(Y4mParse *res, const uint8_t **start, const uint8_t *end)
{
	ptrdiff_t number_of_frames, seconds;
	ptrdiff_t tmp_w, tmp_h;
	const uint8_t *peek, *p = *start;

	for (;;) {
		if (end - p <= 0) {
			return Y4M_PARSE_CORRUPTED;
		}
		switch (*p++) {
		case ' ':
			break;
		case '\n':
			*start = p;
			if (res->w <= 0 || res->h <= 0 || res->frametime_us == 0) {
				return Y4M_PARSE_CORRUPTED;
			}
			return Y4M_PARSE_OK;
			break;
		case 'W':
			if (!y4m__int(&res->w, &p, end)) {
				return Y4M_PARSE_CORRUPTED;
			}
			break;
		case 'H':
			if (!y4m__int(&res->h, &p, end)) {
				return Y4M_PARSE_CORRUPTED;
			}
			break;
		case 'F':
			if (!y4m__int(&number_of_frames, &p, end) ||
			    !y4m__match(":", 1, &p, end) ||
			    !y4m__int(&seconds, &p, end) ||
			    number_of_frames == 0 || seconds == 0)
			{
				return Y4M_PARSE_CORRUPTED;
			}
			res->frametime_us = ((int64_t)seconds*1000000) / number_of_frames;
			break;
		case 'I':
			if (y4m__match("p", 1, &p, end)) {
				res->interlacing = Y4M_PARSE_IL_PROGRESSIVE;
			} else if (y4m__match("t", 1, &p, end)) {
				res->interlacing = Y4M_PARSE_IL_TOP;
			} else if (y4m__match("b", 1, &p, end)) {
				res->interlacing = Y4M_PARSE_IL_BOTTOM;
			} else if (y4m__match("m", 1, &p, end)) {
				res->interlacing = Y4M_PARSE_IL_MIXED;
			} else {
				return Y4M_PARSE_CORRUPTED;
			}
			break;
		case 'C':
			res->colour_space = Y4M_PARSE_CS_UNSUPPORTED;
			if (y4m__match("mono", 4, &p, end)) {
				res->colour_space = Y4M_PARSE_CS_MONO;
			} else if (y4m__match("420jpeg", 7, &p, end)) {
				res->colour_space = Y4M_PARSE_CS_420JPEG;
			} else if (y4m__match("420mpeg2", 8, &p, end)) {
				res->colour_space = Y4M_PARSE_CS_420MPEG2;
			} else if (y4m__match("420paldv", 8, &p, end)) {
				res->colour_space = Y4M_PARSE_CS_420PALDV;
			} else if (y4m__match("420", 3, &p, end)) {
				res->colour_space = Y4M_PARSE_CS_420;
			} else if (y4m__match("422", 3, &p, end)) {
				res->colour_space = Y4M_PARSE_CS_422;
			} else if (y4m__match("444", 3, &p, end)) {
				res->colour_space = Y4M_PARSE_CS_444;
			}
			peek = p; /* peek to avoid falsly matching things like "420p16" */
			if (res->colour_space == Y4M_PARSE_CS_UNSUPPORTED ||
			    (!y4m__match(" ",  1, &peek, end) &&
			     !y4m__match("\n", 1, &peek, end)))
			{
				return Y4M_PARSE_UNSUPPORTED;
			}
			break;
		case 'A':
			if (!y4m__int(&tmp_w, &p, end) ||
			    !y4m__match(":", 1, &p, end) ||
			    !y4m__int(&tmp_h, &p, end))
			{
				return Y4M_PARSE_CORRUPTED;
			}
			res->pixel_aspect_w = (int)tmp_w;
			res->pixel_aspect_h = (int)tmp_h;
			break;
		case 'X': /* TODO */
			for (; p < end && *p != ' ' && *p != '\n'; ++p) {}
			break;
		default:
			return Y4M_PARSE_CORRUPTED;
			break;
		}
	}
	Y4M_PARSE_ASSERT(0 && "unreachable");
}

Y4M_PARSE_API enum Y4mParseStatus
y4m_parse_init(Y4mParse *res, const void *buffer, ptrdiff_t size)
{
	const char magic[] = "YUV4MPEG2 ";

	*res = (Y4mParse){
		.w = -1, .h = -1,
		.p = buffer,
		.end = (uint8_t *)buffer + size
	};

	if (!y4m__match(magic, sizeof(magic) - 1, &res->p, res->end)) {
		return Y4M_PARSE_NOT_Y4M;
	}
	return y4m__parse_params(res, &res->p, res->end);
}

Y4M_PARSE_API enum Y4mParseStatus
y4m_parse_frame(Y4mParse *res)
{
	ptrdiff_t npixels, sdiv, voff;
	const uint8_t *p = res->p, *end = res->end;

	Y4M_PARSE_ASSERT(p <= end);
	if (p == end) {
		return Y4M_PARSE_EOF;
	}
	if (!y4m__match("FRAME", 5, &p, end)) {
		return Y4M_PARSE_CORRUPTED;
	}
	// NOTE: skip frame params, ffmpeg seems to do the same...
	for (; p < end && *p != '\n'; ++p) {}
	if (p == end) {
		return Y4M_PARSE_CORRUPTED;
	}
	++p; /* skip '\n' */

	res->frame_data = p;
	npixels = res->w * res->h;
	switch (res->colour_space) {
	case Y4M_PARSE_CS_420JPEG:
	case Y4M_PARSE_CS_420MPEG2:
	case Y4M_PARSE_CS_420PALDV:
	case Y4M_PARSE_CS_420:
		res->frame_data_len = npixels * 3 / 2;
		sdiv = 2;
		voff = (npixels * 5) / 4;
		break;
	case Y4M_PARSE_CS_422:
		res->frame_data_len = npixels * 2;
		sdiv = 2;
		voff = (npixels * 3) / 2;
		break;
	case Y4M_PARSE_CS_444:
		res->frame_data_len = npixels * 3;
		sdiv = 1;
		voff = npixels * 2;
		break;
	case Y4M_PARSE_CS_MONO:
		res->frame_data_len = npixels;
		break;
	default:
		return Y4M_PARSE_UNSUPPORTED;
		break;
	}
	if (end - p < res->frame_data_len) {
		return Y4M_PARSE_CORRUPTED;
	}

	res->y = p;
	res->y_stride = res->w;
	if (res->colour_space == Y4M_PARSE_CS_MONO) {
		res->u = res->v = NULL;
		res->u_stride = res->v_stride = 0;
	} else {
		res->u = p + npixels;
		res->v = p + voff;
		res->u_stride = res->v_stride = res->w / sdiv;
	}

	res->p = p + res->frame_data_len; /* advance to next potential frame */
	Y4M_PARSE_ASSERT(res->p <= end);

	return Y4M_PARSE_OK;
}

// END Y4mParse library




#if defined(Y4M_PARSE_EXAMPLE)
// example program. parses a y4m file from stdin and prints out some info.
//	$ cc -D Y4M_PARSE_EXAMPLE -o y4m y4m.c
//	$ ./y4m < image.y4m
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>

extern int
main(void)
{
	static uint8_t buf[1 << 24];
	ptrdiff_t buf_sz = fread(buf, 1, sizeof buf, stdin);
	if (ferror(stdin) || !feof(stdin)) {
		fprintf(stderr, "failed to read stdin\n");
		return 1;
	}

	Y4mParse y4m;
	int res = y4m_parse_init(&y4m, buf, buf_sz);
	if (res != Y4M_PARSE_OK) {
		fprintf(stderr, "failed to parse y4m (%d)\n", res);
		return 1;
	}

	printf("w: %td\nh: %td\n", y4m.w, y4m.h);
	printf("frametime(ms): %"PRId64"\n", y4m.frametime_us/1000);
	printf(
		"progressive: %s\n",
		y4m.interlacing == Y4M_PARSE_IL_PROGRESSIVE ? "yes" : "no"
	);
	printf(
		"colors: %s\n",
		y4m.colour_space != Y4M_PARSE_CS_MONO ? "yes" : "no"
	);
	printf("pixel aspect ratio: ");
	if (y4m.pixel_aspect_w == 0 || y4m.pixel_aspect_h == 0) {
		printf("unknown");
	} else {
		printf("%f", (double)y4m.pixel_aspect_w/y4m.pixel_aspect_h);
	}
	printf(" (%d/%d)\n", y4m.pixel_aspect_w, y4m.pixel_aspect_h);

	for (int nframes = 0; y4m_parse_frame(&y4m) == Y4M_PARSE_OK; ++nframes) {
		char C[] = { 'Y', 'U', 'V' };
		const uint8_t *P[] = { y4m.y, y4m.u, y4m.v };
		const ptrdiff_t L[] = { y4m.y_stride, y4m.u_stride, y4m.v_stride };

		printf("frame %04d = {\n", nframes);
		for (int k = 0; k < 3; ++k) {
			unsigned long s = 0;
			for (int i = 0; i < L[k]; ++i) {
				s += P[k][i];
			}
			printf("\t%c sum = %lu\n", C[k], s);
		}
		printf("}\n");
	}
}

#elif defined(__AFL_FUZZ_INIT)
// fuzzing with afl++:
//	$ afl-clang-fast -D DEBUG -g3 -fsanitize=address,undefined -fsanitize-undefined-trap-on-error -o y4m y4m.c
//	$ mkdir i o
//	$ cp some_y4m_files i/
//	$ afl-fuzz -ii -oo ./y4m
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

__AFL_FUZZ_INIT();

extern int
main(void)
{
#ifdef __AFL_HAVE_MANUAL_CONTROL
	__AFL_INIT();
#endif
	unsigned char *afl_buf = __AFL_FUZZ_TESTCASE_BUF;
	while (__AFL_LOOP(10000)) {
		size_t len = __AFL_FUZZ_TESTCASE_LEN;
		unsigned char *buf = memcpy(malloc(len), afl_buf, len);
		Y4mParse y4m;
		if (y4m_parse_init(&y4m, buf, len) != Y4M_PARSE_OK) {
			continue;
		}
		while (y4m_parse_frame(&y4m) == Y4M_PARSE_OK) {
			volatile uint8_t *p = (void *)y4m.frame_data;
			volatile uint8_t *end = p + y4m.frame_data_len;
			// destroy the data!
			for (; p < end; *p++ = 0x0) {}
		}
	}

	return 0;
}

#endif
