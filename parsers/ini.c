// shabby line-based INI parser
//
// all the "stable features" are supported:
// https://en.wikipedia.org/wiki/INI_file#Stable_features
// although most of the "varying features" are not.
//
// the parser converts the key to lower case and also decodes some escape
// sequences *in place*, so the memory provided to the parser must have write
// permission.
//
// mainly written for prototyping/educational purposes.

// API

#include <stddef.h>

#ifndef INI_API
	#define INI_API extern
#endif
#ifndef INI_ASSERT
	#if    defined(DEBUG) &&  defined(__GNUC__)
		#define INI_ASSERT(X)   ((X) ? (void)0 : __builtin_trap())
	#elif  defined(DEBUG) && !defined(__GNUC__)
		#define INI_ASSERT(X)   ((X) ? (void)0 : (void)(*(volatile int *)0 = 0))
	#elif !defined(DEBUG) &&  defined(__GNUC__)
		#define INI_ASSERT(X)   ((X) ? (void)0 : __builtin_unreachable())
	#elif !defined(DEBUG) && !defined(__GNUC__)
		#define INI_ASSERT(X)   ((void)0)
	#endif
#endif

typedef struct { unsigned char *s; size_t len; } IniStr;
typedef struct {
	IniStr ini;

	IniStr section;
	IniStr key, val;
	IniStr err_line;
	size_t lineno;
} IniState;

typedef enum { INI_ERR = -1, INI_DONE, INI_KV } IniType;
INI_API IniType ini_next(IniState *ctx);

// implementation

#define INI__ISBLANK(X)   ((X) == ' ' || (X) == '\t')

static void
ini__makelower(IniStr *s)
{
	for (size_t n = 0; n < s->len; ++n) {
		s->s[n] += (s->s[n] >= 'A' && s->s[n] <= 'Z')[(int []){0x0, 0x20}];
	}
}

INI_API IniType
ini_next(IniState *ctx)
{
	// clean state
	ctx->key = ctx->val = (IniStr){0};
	ctx->err_line = (IniStr){0};

	while (ctx->ini.len > 0) {
		++ctx->lineno;
		unsigned char *end = ctx->ini.s + ctx->ini.len;
		struct { unsigned char *s, *e; } line = { .s = ctx->ini.s };
		// trim leading blanks
		while (line.s < end && INI__ISBLANK(*line.s))
			++line.s;
		for (line.e = line.s; line.e < end && *line.e != '\n'; ++line.e)
			;
		// advance context
		ctx->ini.s = line.e + (line.e < end);
		ctx->ini.len = end - ctx->ini.s;
		INI_ASSERT(ctx->ini.len < ((size_t)-1 >> 2));
		// trim trailing blanks
		while (line.e > line.s && INI__ISBLANK(line.e[-1]))
			--line.e;
		if (line.e - line.s == 0) {
			continue; // blank line
		}
		INI_ASSERT(line.e - line.s > 0);

		ctx->err_line = (IniStr){ .s = line.s, .len = line.e - line.s };
		switch (*line.s) {
		case '[':
			INI_ASSERT(line.e > line.s);
			if (line.e[-1] != ']') {
				return INI_ERR;
			}
			ctx->section.s = line.s + 1;
			ctx->section.len = (line.e - line.s) - 2;
			if (ctx->section.len == 0) {
				return INI_ERR;
			}
			ini__makelower(&ctx->section);
			break;
		case ';': /* ignore comments */
			break;
		default:;
			unsigned char *e = line.s;
			while (e < line.e && *e != '=' && *e != ':')
				++e;
			if (e - line.s == 0 || e == line.e) {
				return INI_ERR;
			}
			ctx->key.s = line.s;
			ctx->key.len = e - line.s;
			while (ctx->key.len > 0 &&   /* trim right */
			       INI__ISBLANK(ctx->key.s[ctx->key.len - 1]))
			{
				--ctx->key.len;
			}
			ini__makelower(&ctx->key);

			INI_ASSERT(*e == '=' || *e == ':');
			++e; // skip '='
			while (e < line.e && INI__ISBLANK(*e))
				++e;

			// shabby unquoting hack
			if (*e == '"' && line.e[-1] == '"') {
				++e;
				--line.e;
			}
			// support for \n \t \\ and \" escapes
			unsigned char *r = e, *w = r;
			while (r < line.e) {
				unsigned char ch = *r++;
				switch (ch * (r < line.e)) {
				case '\\':;
					unsigned char ch2 = *r++;
					switch (ch2) {
					case 'n': *w++ = '\n'; break;
					case 't': *w++ = '\t'; break;
					case '\\': case '"':
					default:
						*w++ = ch2;
						break;
					}
					break;
				default:
					*w++ = ch;
					break;
				}
			}

			ctx->val.s = e;
			ctx->val.len = w - e;
			return INI_KV;
			break;
		}
	}

	return INI_DONE;
}

// END Ini library



// example program + fuzz target
//
// example: reads ini file from stdin and prints it out:
//	$ cc -DINI_EXAMPLE -o ini ini.c
//	$ printf '[Section]\nname="me"\nAge :\t0x64\n' | ./ini
//	$ ./ini < some_ini_file.ini
//
// fuzzing with afl++:
//	$ afl-clang-fast -g3 -fsanitize=address,undefined -fsanitize-undefined-trap-on-error -o ini ini.c
//	$ mkdir i o
//	$ printf '[Section]\nname="me"\nAge :\t0x64\n' > i/sample.ini
//	$ afl-fuzz -ii -oo ./ini

// TODO: a more exhaustive test suit
#if defined(INI_EXAMPLE)

#include <stdio.h>

static size_t
print_str(IniStr s)
{
	return s.len > 0 ? fwrite(s.s, 1, s.len, stdout) : 0;
}

extern int
main(void)
{
	unsigned char buf[BUFSIZ];
	size_t len = fread(buf, 1, sizeof buf, stdin);

	IniState state = { .ini = { .s = buf, .len = len } }, *ctx = &state;
	for (;;) {
		switch (ini_next(ctx)) {
		case INI_KV:
			if (ctx->section.s)
				print_str(ctx->section);
			else
				fputs("GLOBAL", stdout);
			putchar('.');
			print_str(ctx->key);
			fputs("\t==>\t", stdout);
			print_str(ctx->val);
			putchar('\n');
			break;
		case INI_ERR:
			printf("Error in line: ");
			print_str(ctx->err_line);
			putchar('\n');
			return 1;
			break;
		case INI_DONE:
			return 0;
			break;
		}
	}

	INI_ASSERT(!"unreachable");
}

#elif defined(__AFL_FUZZ_INIT)

#include <unistd.h>
#include <string.h>
#include <stdlib.h>

__AFL_FUZZ_INIT();

extern int
main(void)
{
#ifdef __AFL_HAVE_MANUAL_CONTROL
	__AFL_INIT();
#endif

	unsigned char *afl_buf = __AFL_FUZZ_TESTCASE_BUF;
	while (__AFL_LOOP(10000)) {
		size_t len = __AFL_FUZZ_TESTCASE_LEN;
		unsigned char *buf = malloc(len);
		if (!buf) continue;
		memcpy(buf, afl_buf, len);

		IniState state = { .ini = { .s = buf, .len = len }, };
		for (;;) {
			IniType type = ini_next(&state);
			if (type == INI_ERR || type == INI_DONE)
				break;
		}
	}
	return 0;
}
#endif
