#if 0
gcc -o fullscreen-cmd fullscreen-cmd.c \
    -g3 -fsanitize=address,undefined \
    -lX11
exit $?
#endif
// run command when entering/exiting fullscreen
// ad-hoc program for my personal usage, not meant to be used by others

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#include <spawn.h>
#include <signal.h>
#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xatom.h>
#include <X11/Xproto.h>

extern char **environ;

static int (*xerrorxlib)(Display *, XErrorEvent *) = 0;

static int
xerror(Display *dpy, XErrorEvent *ee)
{
	if (ee->error_code == BadWindow
	|| (ee->request_code == X_SetInputFocus && ee->error_code == BadMatch)
	|| (ee->request_code == X_PolyText8 && ee->error_code == BadDrawable)
	|| (ee->request_code == X_PolyFillRectangle && ee->error_code == BadDrawable)
	|| (ee->request_code == X_PolySegment && ee->error_code == BadDrawable)
	|| (ee->request_code == X_ConfigureWindow && ee->error_code == BadMatch)
	|| (ee->request_code == X_GrabButton && ee->error_code == BadAccess)
	|| (ee->request_code == X_GrabKey && ee->error_code == BadAccess)
	|| (ee->request_code == X_CopyArea && ee->error_code == BadDrawable))
	{
		return 0;
	}
	fprintf(stderr, "fatal error: request code=%d, error code=%d\n",
		ee->request_code, ee->error_code);
	return xerrorxlib(dpy, ee); /* may call exit */
}

extern int
main(void)
{
	Display *dpy = XOpenDisplay(NULL);
	Window root = DefaultRootWindow(dpy);
	int root_h, root_w;
	{
		XWindowAttributes attr;
		if (XGetWindowAttributes(dpy, root, &attr) == 0) {
			fprintf(stderr, "failed to get root window attributes\n");
			return 1;
		}
		root_h = attr.height;
		root_w = attr.width;
	}

	Atom _NET_ACTIVE_WINDOW = XInternAtom(dpy, "_NET_ACTIVE_WINDOW", 0);
	Atom _NET_WM_STATE = XInternAtom(dpy, "_NET_WM_STATE", 0);
	Atom _NET_WM_STATE_FULLSCREEN = XInternAtom(dpy, "_NET_WM_STATE_FULLSCREEN", 0);
	if (_NET_ACTIVE_WINDOW == None ||
	    _NET_WM_STATE == None || _NET_WM_STATE_FULLSCREEN == None)
	{
		fprintf(stderr, "Error: initializing atoms\n");
		return 1;
	}

	XSelectInput(dpy, root, SubstructureNotifyMask);

	enum { STATE_UNINIT, STATE_FS, STATE_NOT_FS } prev_state = STATE_NOT_FS, next_state;

	xerrorxlib = XSetErrorHandler(xerror);

	{ /* no zombies */
		struct sigaction sa;
		sigemptyset(&sa.sa_mask);
		sa.sa_flags = SA_NOCLDSTOP | SA_NOCLDWAIT | SA_RESTART;
		sa.sa_handler = SIG_IGN;
		sigaction(SIGCHLD, &sa, NULL);
	}

	for (;;) {
		XEvent ev;
		XNextEvent(dpy, &ev);
		Atom ret_type;
		int actual_format_return;
		unsigned long nitems_return, rem_bytes;

		/* static unsigned int nwakeups = 0; */
		/* fprintf(stderr, "[%6u] woke up\n", ++nwakeups); */

		next_state = STATE_NOT_FS;

		Window *focus_win = NULL;
		int res = XGetWindowProperty(
			dpy, root, _NET_ACTIVE_WINDOW, 0, 1, False,
			XA_WINDOW, &ret_type, &actual_format_return,
			&nitems_return, &rem_bytes,
			(unsigned char **)&focus_win
		);
		if (nitems_return == 1 && actual_format_return == 32 &&
		    rem_bytes == 0 && ret_type == XA_WINDOW &&
		    focus_win != NULL)
		{
			XWindowAttributes attr;
			if (XGetWindowAttributes(dpy, *focus_win, &attr) != 0) {
				// HACK: assumes this means fullscreen
				// but that's no necessary
				if (attr.height == root_h &&
				    attr.width  == root_w)
				{
					next_state = STATE_FS;
				} else {
					next_state = STATE_NOT_FS;
				}
			}
		}
		XFree(focus_win);

		if (prev_state != next_state) {
			switch (next_state) {
			case STATE_FS: {
				fprintf(stderr, "Entered FULLSCREEN\n");
				pid_t pid;
				char *cmd[] = {
					"pkill", "xbanish", NULL
				};
				int err = posix_spawnp(
					&pid, cmd[0], NULL, NULL, cmd, environ
				);
				if (err != 0) {
					fprintf(stderr, "Error: posix_spawnp: %s\n", strerror(err));
				}
			} break;
			case STATE_NOT_FS: {
				fprintf(stderr, "Leaving FULLSCREEN\n");
				pid_t pid;
				char *cmd[] = {
					"xbanish", "-m", "+960-0", NULL
				};
				int err = posix_spawnp(
					&pid, cmd[0], NULL, NULL, cmd, environ
				);
				if (err != 0) {
					fprintf(stderr, "Error: posix_spawnp: %s\n", strerror(err));
				}
			} break;
			}
		}
		prev_state = next_state;

	}

	XCloseDisplay(dpy);
	return 0;
}
