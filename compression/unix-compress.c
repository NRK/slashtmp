#if 0
gcc -o /tmp/test compression/unix-compress.c \
	-Wall -Wshadow -Wno-unused \
	-g3 -DDEBUG -fsanitize=address,undefined -fsanitize-trap
exit $?
#endif
// ancient unix compress tool. uses LZW encoding.
//
// !!!!!!!!!!!!!!!!!!
// !!! DO NOT USE !!!
// !!!!!!!!!!!!!!!!!!
//
// this is merely some prototyping code to learn about LZW. as such the code
// doesn't do any bounds checking and thus might contain buffer overflows. the
// decoder is recursive which could result in stack overflow.
// doesn't generate RESET codes. doesn't handle large files due to not having a
// streaming interface.
//
// For a proper implementation, see: https://codeberg.org/NRK/nix-compress
//
// ref: https://youtube.com/watch?v=vDXmJ-6mizE
//      https://youtube.com/watch?v=1cJL9Va80Pk
// ref: https://en.wikipedia.org/wiki/Lempel%E2%80%93Ziv%E2%80%93Welch
// ref: https://en.wikipedia.org/wiki/compress_(software)
// ref: http://warp.povusers.org/EfficientLZW/index.html
#include <limits.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

typedef uint8_t     u8;
typedef uint16_t    u16;
typedef uint32_t    u32;
typedef uint64_t    u64;
typedef int8_t      i8;
typedef int16_t     i16;
typedef int32_t     i32;
typedef int64_t     i64;
typedef ptrdiff_t   Size;

#define NOP()           ((void)0)
#define SIZEOF(...)     ((Size)sizeof(__VA_ARGS__))
#define ARRLEN(...)     (SIZEOF(__VA_ARGS__) / SIZEOF(0[__VA_ARGS__]))
#define BRK()           __asm ("int3; nop")
#ifdef __GNUC__
	// when debugging, use gcc/clang and compile with
	// `-fsanitize=undefined -fsanitize-undefined-trap-on-error`
	// it'll trap if an unreachable code-path is ever reached.
	#define ASSERT(X)  ((X) ? (void)0 : __builtin_unreachable())
#else
	#define ASSERT(X)  ((void)0)
#endif

#define MAGIC_1 0x1F
#define MAGIC_2 0x9D

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef struct {  // LSB
	uint32_t accum;
	int      bitpos;
	u8      *cur;
	u8      *end;
} BitIo;

// FIXME: OOB
static uint32_t
bitread(BitIo *bi, int nbits)
{
	ASSERT(nbits > 0 && nbits <= 16);
	while (bi->bitpos < nbits) {
		bi->accum |= (*bi->cur++ << bi->bitpos);
		bi->bitpos += 8;
	}
	u32 ret = bi->accum & (((u32)1 << nbits) - 1);
	bi->accum >>= nbits;
	bi->bitpos -= nbits;
	return ret;
}

// FIXME: OOB
// assumes high bits of `c` are clear
static void
bitwrite(BitIo *bo, u32 c, int nbits)
{
	ASSERT(nbits > 0 && nbits <= 16);
	ASSERT(bo->bitpos >= 0 && bo->bitpos < 8);
	bo->accum |= (c << bo->bitpos);
	bo->bitpos += nbits;
	while (bo->bitpos >= 8) {
		*bo->cur++ = (u8)bo->accum;
		bo->accum >>= 8;
		bo->bitpos -= 8;
	}
}

typedef struct {
	u16 index; u8 ch;
} SymTab;

// TODO: do something non recursive?
static i32
decode(SymTab *symtab, i32 code, u8 **outp)
{
	ASSERT(code >= 0 && code <= UINT16_MAX);
	if (code < 256) {
		**outp = (u8)code;
		*outp += 1;
		return code;
	} else {
		i32 ret = decode(symtab, symtab[code].index, outp);
		**outp = (u8)symtab[code].ch;
		*outp += 1;
		return ret;
	}
}

extern int
main(int argc, char *argv[])
{
	static u8 buf[1<<26];
	static u8 outbuf[1<<28]; // should be big enough :3

	int decompress = 0;
	int compress_without_block = 0;
	if (argc > 1) {
		decompress = strcmp(argv[1], "-d") == 0;
		compress_without_block = strcmp(argv[1], "-C") == 0;
		if ((!decompress && !compress_without_block) || argc > 2) {
			fprintf(stderr, "unknown argument(s)\n");
			return 1;
		}
	}

	size_t n = fread(buf, 1, sizeof buf, stdin);
	if (ferror(stdin)) {
		fprintf(stderr, "Error: reading stdin\n");
		return 1;
	}
	if (!feof(stdin)) {
		fprintf(stderr, "Error: out of buffer, file too large\n");
		return 1;
	}

	if (!decompress) {
		int maxbits = 16;
		u8 *outcur = outbuf;
		*outcur++ = MAGIC_1;
		*outcur++ = MAGIC_2;
		if (compress_without_block) {
			*outcur++ = 0x00 | maxbits;
		} else {
			*outcur++ = 0x80 | maxbits;
		}

		typedef struct { u32 string; u16 code; } CodeTable;
		enum {
			CT_EXP  = 17, // worst case ~50% load factor
			CT_SIZE = 1 << CT_EXP,
			CT_IDX_MASK = CT_SIZE - 1,
			CT_STEP_SHIFT = 32 - (CT_EXP - 1),
		};
		static CodeTable ctbl[CT_SIZE];
		u16 curcode = *buf;
		int curbits = 9;
		i32 nextcode = compress_without_block ? 256 : 257;
		i32 maxcode = (1 << maxbits);
		u32 bitswritten = 0;
		BitIo bo[1] = {{ .cur = outcur, .end = outbuf + sizeof outbuf }};
		for (u8 *icur = buf+1, *iend = buf+n; icur < iend; ++icur) {
			u8 ch = *icur;
			u32 nextstring = (((u32)curcode + 1) << 8) | ch;
			u32 hash = nextstring * 0xACDC5555; hash ^= hash >> 15;
			u32 step = (hash >> CT_STEP_SHIFT) | 0x1;
			u32 idx  = hash & CT_IDX_MASK;
			for (CodeTable *t;;) {
				t = ctbl + idx;
				if (t->string == nextstring) {
					curcode = t->code;
					break;
				} else if (t->string == 0) {
					bitwrite(bo, curcode, curbits);
					bitswritten += curbits;
					curcode = ch;
					if (nextcode < maxcode) {
						t->string = nextstring;
						t->code = nextcode;
						if (nextcode == (1 << curbits) && curbits < maxbits) {
							while (bitswritten % (curbits * 8) != 0) {
								bitwrite(bo, 0, 1);
								bitswritten += 1;
							}
							bitswritten = 0;
							++curbits;
						}
						++nextcode;
					}
					break;
				} else {
					idx += step;
					idx &= CT_IDX_MASK;
				}
			}
		}
		if (n > 0) {
			bitwrite(bo, curcode, curbits);
		}
		int needpad = -bo->bitpos & 7;
		if (needpad) {
			bitwrite(bo, 0, needpad);
		}
		ASSERT(bo->bitpos == 0);
		ASSERT((uintptr_t)bo->cur < (uintptr_t)(outbuf + sizeof outbuf));
		fwrite(outbuf, 1, bo->cur - outbuf, stdout);
		return 0;
	} else {
		if (n < 3 || buf[0] != MAGIC_1 || buf[1] != MAGIC_2) {
			fprintf(stderr, "Error: not a Z file\n");
			return 1;
		}
		bool blockmode = buf[2] & 0x80;
		int maxbits = buf[2] & 0x1F;
		if (buf[2] & 0x60) {
			fprintf(stderr, "Error: reserved bits not zero\n");
			return 1;
		}
		if (maxbits > 16 || maxbits < 9) {
			fprintf(stderr, "Error: unsupported maxbits: %d\n", maxbits);
			return 1;
		}

		static SymTab symtab[1<<16];
		i32 maxsyms = (i32)1 << maxbits;
		i32 cursym = 256 + !!blockmode;

		BitIo bi[1] = {{ .cur = buf + 3, .end = buf + n }};
		u64 bitsread = 0;
		i32 curcode = -1;
		int bits = 9;
		u8 *outcur = outbuf;
		while (bi->cur < bi->end) {
			int code = bitread(bi, bits);
			bitsread += bits;
			if (code == 256 && blockmode) {
				// alignment bug in the original compress tool
				//fprintf(stderr, "INFO: processing CLEAR code\n");
				while (bitsread % (8 * bits) != 0) {
					bitread(bi, 1);
					bitsread += 1;
				}
				bitsread = 0;

				bits = 9;
				curcode = -1;
				cursym = 256 + !!blockmode;
				memset(symtab, 0x0, sizeof symtab);
				continue;
			}

			u8 ch;
			if (code < cursym) {
				ch = decode(symtab, code, &outcur);
			} else if (code == cursym && curcode != -1) {
				// special case: cScSc
				ch = decode(symtab, curcode, &outcur);
				*outcur++ = ch;
			} else {
				// corrupt?
				fprintf(stderr, "Error: corrupt input (?) (bitsread %ld)\n", (long)bitsread);
				abort();
			}

			if (cursym < maxsyms && curcode != -1) {
				symtab[cursym++] = (SymTab){
					.index = curcode,
					.ch    = ch,
				};
				if (cursym == (1 << bits) && bits < maxbits) {
					while (bitsread % (8 * bits) != 0) {
						bitread(bi, 1);
						bitsread += 1;
					}
					bitsread = 0;
					++bits;
				}
			}
			curcode = code;
		}

		ASSERT((uintptr_t)outcur < (uintptr_t)(outbuf + sizeof outbuf));
		fwrite(outbuf, 1, outcur - outbuf, stdout);
		return 0;
	}
}
