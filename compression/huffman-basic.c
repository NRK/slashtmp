#if 0
gcc -o /tmp/test compression/huffman-basic.c \
	-Wall -Wshadow -Wno-unused \
	-g3 -DDEBUG -fsanitize=address,undefined -fsanitize-trap "$@"
#	-march=native -O3 -g3 "$@"
exit $?
#endif
//
// !!!!!!!!!!!!!!!!!!
// !!! DO NOT USE !!!
// !!!!!!!!!!!!!!!!!!
//
// prototype code, very unsafe and unreliable.
#include <limits.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

typedef uint8_t     u8;
typedef uint16_t    u16;
typedef uint32_t    u32;
typedef uint64_t    u64;
typedef int8_t      i8;
typedef int16_t     i16;
typedef int32_t     i32;
typedef int64_t     i64;

#ifdef __GNUC__
	#define ASSERT(X)  ((X) ? (void)0 : __builtin_unreachable())
#else
	#define ASSERT(X)  ((void)0)
#endif
#define ARRLEN(...)     (sizeof(__VA_ARGS__) / sizeof(0[__VA_ARGS__]))
#define MAX(A, B)       ((A) > (B) ? (A) : (B))
#define SWAP(A, B) do { \
	__typeof__(A) tmp__ = (A); (A) = (B); (B) = tmp__; \
} while (0)

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

typedef struct {
	i32 freq;
	u32 leaf0 : 11;
	u32 leaf1 : 11;
	u32 sym   : 10;
} Huff;

typedef struct {
	u8  symbol[256];
	union {
		i32 freq[256];
		i32 codelen[256];
	};
	u32 code[256];
} Prefix;

static void
heap_validate(Huff *heap, int len)
{
	for (int k = 0; k < len; ++k) {
		int l = (k + 1) * 2 - 1;
		int r = (k + 1) * 2;
		if (l < len) {
			ASSERT(heap[k].freq <= heap[l].freq);
		}
		if (r < len) {
			ASSERT(heap[k].freq <= heap[r].freq);
		}
	}
}

static void
heap_push(Huff *h, int *hlen, Huff item)
{
	int index = *hlen;
	h[index] = item;
	*hlen += 1;
	int parent = (index - 1) / 2;
	while (index > 0 && h[parent].freq > h[index].freq) {
		Huff tmp = h[parent];
		h[parent] = h[index];
		h[index] = tmp;
		index = parent;
		parent = (index - 1) / 2;
	}
	heap_validate(h, *hlen);
}

static Huff
heap_pop(Huff *h, int *hlen)
{
	ASSERT(*hlen > 0);
	Huff ret = *h;
	h[0] = h[--*hlen];
	int fix = 0;
	int fixcost = h->freq;
	for (;;) {
		int l = (fix + 1) * 2 - 1;
		int r = (fix + 1) * 2;
		int lcost = (l < *hlen) ? h[l].freq : INT_MAX;
		int rcost = (r < *hlen) ? h[r].freq : INT_MAX;
		int small = (lcost <= rcost) ? l : r;
		int smallcost = (lcost <= rcost) ? lcost : rcost;
		if (small < *hlen && smallcost < fixcost) {
			Huff tmp = h[fix];
			h[fix] = h[small];
			h[small] = tmp;
			fix = small;
		} else {
			break;
		}
	}
	heap_validate(h, *hlen);
	return ret;
}

static void
huffcode(Huff *r, Huff *heap, i32 *codelen, int depth)
{
	if (r->sym > 0) {
		ASSERT(r->leaf0 == 0); ASSERT(r->leaf1 == 0);
		/* ASSERT(codelen[r->sym - 1] == 0); */
		codelen[r->sym - 1] = depth;
	} else {
		if (r->leaf0 > 0) {
			huffcode(heap + r->leaf0, heap, codelen, depth + 1);
		}
		if (r->leaf1 > 0) {
			huffcode(heap + r->leaf1, heap, codelen, depth + 1);
		}
	}
}

static void
huffman_heap(i32 *freq, int n)
{
	Huff heap[1024];
	int heap_len = 0;
	int heap_end = 1024;
	for (int i = 0; i < 256; ++i) {
		if (freq[i] > 0) {
			heap_push(heap, &heap_len, (Huff){
				.freq = freq[i],
				.sym = i + 1,
			});
		}
	}
	Huff *heap_tail = heap + heap_len;
	for (int i = 0, l = heap_len; i < l-1; ++i) {
		Huff a = heap_pop(heap, &heap_len);
		Huff b = heap_pop(heap, &heap_len);
		int a_idx = --heap_end;
		int b_idx = --heap_end;
		heap[a_idx] = a;
		heap[b_idx] = b;
		Huff new = {
			.freq = a.freq + b.freq,
			.leaf0 = a_idx,
			.leaf1 = b_idx,
		};
		heap_push(heap, &heap_len, new);
	}
	u8 codelen[256] = {0};
	huffcode(heap, heap, freq, 0);
}

// a: symbol frequency. must be sorted in decreasing order (high -> low)
// n: number of symbols
static void
huffman_inplace(i32 *a, int n)
{
	if (n == 0) { return; }
	// NOTE: generally, i think this should be 1.
	// but the file format this program produces includes the size of the
	// decompressed file. so using 0 bits gives us free RLE for data with
	// only one symbol.
	if (n == 1) { a[0] = 0; return; }

	int root = 0;
	int leaf = 1;
	a[root] += a[leaf++];
	for (int next = 1; next < (n - 1); ++next) {
		if (leaf >= n || a[root] < a[leaf]) {
			a[next] = a[root];
			a[root++] = next;
		} else {
			a[next] = a[leaf++];
		}

		if (leaf >= n || (root < next && a[root] < a[leaf])) {
			a[next] += a[root];
			a[root++] = next;
		} else {
			a[next] += a[leaf++];
		}
	}

	root = n - 2;
	a[root] = 0;
	for (int i = n - 3; i >= 0; --i) {
		a[i] = a[a[i]] + 1;
	}

	int avail = 1;
	int used = 0;
	int depth = 0;
	int cursor = n;
	root = n - 2;
	while (avail > 0) {
		while (root >= 0 && a[root] == depth) {
			++used;
			--root;
		}
		while (avail > used) {
			a[--cursor] = depth;
			--avail;
		}
		avail = 2 * used;
		depth += 1;
		used = 0;
	}
}

typedef struct {
	u32 accum;
	int bitpos;
	u8 *cursor;
} BitIo;

static void
bitwrite(BitIo *bo, u32 c, int nbits)
{
	ASSERT(nbits >= 0 && nbits <= 16);
	ASSERT(bo->bitpos >= 0 && bo->bitpos < 8);
	bo->accum |= c << bo->bitpos;
	bo->bitpos += nbits;
	while (bo->bitpos >= 8) {
		*bo->cursor++ = (u8)bo->accum;
		bo->accum >>= 8;
		bo->bitpos -= 8;
	}
}

static u32
bitread(BitIo *bi, int nbits)
{
	ASSERT(nbits >= 0 && nbits <= 16);
	while (bi->bitpos < nbits) {
		bi->accum |= (*bi->cursor++ << bi->bitpos);
		bi->bitpos += 8;
	}
	u32 ret = bi->accum & ((UINT32_C(1) << nbits) - 1);
	bi->accum >>= nbits;
	bi->bitpos -= nbits;
	return ret;
}

static u16
bitreverse16(u16 v, int l)
{
	u8 lo = v & 0xFF;
	u8 hi = v >> 8;
	// ref: https://graphics.stanford.edu/~seander/bithacks.html#ReverseByteWith64Bits
	lo = ((lo * 0x80200802ULL) & 0x0884422110ULL) * 0x0101010101ULL >> 32;
	hi = ((hi * 0x80200802ULL) & 0x0884422110ULL) * 0x0101010101ULL >> 32;
	u32 rev = ((u32)lo << 8) | hi;
	return (u16)(rev >> (16 - l));
}

extern int
main(int argc, char *argv[])
{
	static u8 buf[1<<26];
	static u8 outbuf[1<<28];

	/* { */
	/* 	int A[] = { 1, 1, 1, 2, 2, 2, 3, 4, 4, 7 }; */
	/* 	/1* int A[] = { 2, 4, 8 }; *1/ */
	/* 	inplace_huffman(A, sizeof A / sizeof *A); */
	/* } */

	int decompress = 0;
	if (argc > 1) {
		decompress = strcmp(argv[1], "-d") == 0;
		if (!decompress || argc > 2) {
			fprintf(stderr, "unknown argument(s)\n");
			return 1;
		}
	}

	size_t buf_len = fread(buf, 1, sizeof buf, stdin);
	if (ferror(stdin)) {
		fprintf(stderr, "Error: reading stdin\n");
		return 1;
	}
	if (!feof(stdin)) {
		fprintf(stderr, "Error: out of buffer, file too large\n");
		return 1;
	}

	if (!decompress) {
		Prefix f[1] = {0};
		for (u8 *p = buf, *end = buf + buf_len; p < end; ++p) {
			++f->freq[*p];
		}

		int nsyms = 0;
		for (int i = 0; i < ARRLEN(f->freq); ++i) {
			if (f->freq[i] > 0) {
				f->freq[nsyms] = f->freq[i];
				f->symbol[nsyms] = i;
				for (int k = nsyms;
				     k > 0 && f->freq[k - 1] > f->freq[k];
				     --k)
				{
					SWAP(f->freq[k - 1], f->freq[k]);
					SWAP(f->symbol[k - 1], f->symbol[k]);
				}
				++nsyms;
			}
		}

#ifdef HEAP
		// Classic huffman algorithm using a heap/priority-queue
		huffman_heap(f->freq, nsyms);
#else
		// See: "In-place calculation of minimum-redundancy codes" by
		// Alistair Moffat & Jyrki Katajainen
		huffman_inplace(f->freq, nsyms);
#endif

		if (nsyms > 0 && f->codelen[0] >= 16) {
			fprintf(stderr, "maxcodelen >= 16\n");
			abort();
		}

		{
			u32 c = 0;
			for (int i = nsyms - 1; i >= 0; --i) {
				f->code[i] = bitreverse16(c, f->codelen[i]);
				++c;
				if (i - 1 >= 0) {
					c <<= (f->codelen[i - 1] - f->codelen[i]);
				}
			}
		}

		u8 *outcur = outbuf;
		{
			*outcur++ = (buf_len >>  0) & 0xFF;
			*outcur++ = (buf_len >>  8) & 0xFF;
			*outcur++ = (buf_len >> 16) & 0xFF;
			*outcur++ = (buf_len >> 24) & 0xFF;
		}
		for (int i = 0; i < nsyms; ) {
			int curlen = f->codelen[i];
			int n = 1;
			while (n + i < nsyms && n < 15 && f->codelen[i + n] == curlen) {
				++n;
			}
			ASSERT(curlen < 16);
			*outcur++ = (curlen << 4) | (n);
			for (int k = 0; k < n; ++k) {
				u8 c = f->symbol[i + k];
				*outcur++ = c;
				/* fprintf( */
				/* 	stderr, "0x%02X -> %02d\n", */
				/* 	c, curlen */
				/* ); */
			}
			i += n;
		}
		*outcur++ = 0x0;

		u16 code[256];
		u8  codelen[256];
		for (int i = 0; i < nsyms; ++i) {
			code[f->symbol[i]] = f->code[i];
			codelen[f->symbol[i]] = f->codelen[i];
		}

		/* for (int i = 0; i < nsyms; ++i) { */
		/* 	u8 c = f->symbol[i]; */
		/* 	fprintf( */
		/* 		stderr, "%c (0x%02x) -> <%02d, 0b%16.*b>\n", */
		/* 		isgraph(c) ? c : '?', c, */
		/* 		f->codelen[i], f->codelen[i], f->code[i] */
		/* 	); */
		/* } */

		BitIo bo[1] = {{ .cursor = outcur }};
		for (u8 *p = buf, *end = buf + buf_len; p < end; ++p) {
			bitwrite(bo, code[*p], codelen[*p]);
		}
		int needpad = -bo->bitpos & 0x7;
		if (needpad) {
			bitwrite(bo, 0, needpad);
		}
		fwrite(outbuf, 1, bo->cursor - outbuf, stdout);
	} else {
		u8 *icur = buf;
		u8 *iend = buf + buf_len;
		u32 sz = ((u32)icur[3] << 24) | (icur[2] << 16) | (icur[1] << 8) | icur[0];
		icur += 4;
		u32 code[256] = {0};
		u8 symbol[256] = {0};
		u8 codelen[256] = {0};
		int nsyms = 0;
		/* fprintf(stderr, "=================\n"); */
		for (;;) {
			u8 c = *icur++;
			if (c == 0) {
				break;
			}
			int l = c & 0xF;
			int clen = c >> 4;
			for (int i = 0; i < l; ++i) {
				symbol[nsyms] = *icur++;
				codelen[nsyms] = clen;
				/* fprintf( */
				/* 	stderr, "0x%02X -> %02d\n", */
				/* 	symbol[nsyms], codelen[nsyms] */
				/* ); */
				++nsyms;
			}
		}
		{
			u32 c = 0;
			for (int i = nsyms - 1; i >= 0; --i) {
				code[i] = bitreverse16(c, codelen[i]);
				++c;
				if (i - 1 >= 0) {
					c <<= (codelen[i - 1] - codelen[i]);
				}
			}
		}

		/* for (int i = 0; i < nsyms; ++i) { */
		/* 	u8 c = symbol[i]; */
		/* 	fprintf( */
		/* 		stderr, "%c (0x%02x) -> <%02d, 0b%16.*b>\n", */
		/* 		isgraph(c) ? c : '?', c, */
		/* 		codelen[i], codelen[i], code[i] */
		/* 	); */
		/* } */

		static i16 states[1<<18] = {0};
		int maxnode = 0;
		for (int i = 0; i < nsyms; ++i) {
			int node = 1;
			int c = code[i];
			for (int k = 0; k < codelen[i]; ++k) {
				ASSERT(states[node] <= 0);
				states[node] = -1;
				node *= 2;
				node += c & 0x1;
				c >>= 1;
			}
			states[node] = symbol[i] + 1;
			maxnode = MAX(node, maxnode);
		}
		/* fprintf(stderr, "maxnode: %d\n", maxnode); */

		u8 *outcur = outbuf;
		BitIo bi[1] = {{ .cursor = icur }};
		for (u32 read = 0; read < sz; ++read) {
			int node = 1;
			for (;;) {
				if (states[node] == 0) {
					ASSERT(!"unreachable");
				} else if (states[node] == -1) {
					node *= 2;
					node += bitread(bi, 1);
				} else if (states[node] > 0) {
					*outcur++ = states[node] - 1;
					break;
				} else {
					ASSERT(!"unreachable");
				}
			}
		}
		fwrite(outbuf, 1, outcur - outbuf, stdout);
	}

	fflush(stdout);
	return ferror(stdout);
}

// TODO: length limited heuristics
// TODO: package merge
