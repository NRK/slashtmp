// simple qoi decoder: https://qoiformat.org/
//
// This is free and unencumbered software released into the public domain.
// For more information, please refer to <https://unlicense.org/>

#include <limits.h>
#include <stddef.h>
#include <stdint.h>

// API

#ifndef QOIDEC_API
	#define QOIDEC_API extern
#endif
#ifndef QOIDEC_ASSERT
	#if    defined(DEBUG) &&  defined(__GNUC__)
		#define QOIDEC_ASSERT(X)   ((X) ? (void)0 : __builtin_trap())
	#elif  defined(DEBUG) && !defined(__GNUC__)
		#define QOIDEC_ASSERT(X)   ((X) ? (void)0 : (void)(*(volatile int *)0 = 0))
	#elif !defined(DEBUG) &&  defined(__GNUC__)
		#define QOIDEC_ASSERT(X)   ((X) ? (void)0 : __builtin_unreachable())
	#elif !defined(DEBUG) && !defined(__GNUC__)
		#define QOIDEC_ASSERT(X)   ((void)0)
	#endif
#endif

typedef struct {
	uint32_t w, h;
	// this is where the decoded ARGB32 pixels will be put.
	// to be allocated by the caller by at least `.data_size` bytes before calling qoi_dec().
	uint32_t *data;
	ptrdiff_t npixels, data_size;
	uint8_t channels, colorspace;

	// private
	const uint8_t *p, *end;
} QoiDecCtx;

typedef enum {
	QOIDEC_OK,
	QOIDEC_NOT_QOI, QOIDEC_CORRUPTED, QOIDEC_ZERO_DIM, QOIDEC_TOO_LARGE,
} QoiDecStatus;
QOIDEC_API QoiDecStatus qoi_dec_init(QoiDecCtx *ctx, const void *buffer, ptrdiff_t size);
QOIDEC_API QoiDecStatus qoi_dec(QoiDecCtx *ctx);

// implementation

QOIDEC_API QoiDecStatus
qoi_dec_init(QoiDecCtx *ctx, const void *buffer, ptrdiff_t size)
{
	typedef uint32_t u32;
	QOIDEC_ASSERT(size >= 0);
	*ctx = (QoiDecCtx){0};

	ctx->p = buffer;
	ctx->end = ctx->p + size;
	if (size < 14 ||
	    !((ctx->p[0] == 'q') & (ctx->p[1] == 'o') &
	      (ctx->p[2] == 'i') & (ctx->p[3] == 'f')))
	{
		return QOIDEC_NOT_QOI;
	}
	ctx->p += 4;

	ctx->w = (u32)ctx->p[0] << 24 | ctx->p[1] << 16 | ctx->p[2] << 8 | ctx->p[3];
	ctx->p += 4;
	ctx->h = (u32)ctx->p[0] << 24 | ctx->p[1] << 16 | ctx->p[2] << 8 | ctx->p[3];
	ctx->p += 4;
	if (ctx->w == 0 || ctx->h == 0) {
		return QOIDEC_ZERO_DIM;
	}
	if ((PTRDIFF_MAX/4)/ctx->w < ctx->h) {
		return QOIDEC_TOO_LARGE;
	}
	ctx->npixels = (ptrdiff_t)ctx->w * ctx->h;
	ctx->data_size = ctx->npixels * 4;

	ctx->channels   = *ctx->p++;
	ctx->colorspace = *ctx->p++;
	if (!(ctx->channels == 3 || ctx->channels == 4) ||
	    ctx->colorspace & ~0x1u || ctx->end - ctx->p < 8 /* end marker */)
	{
		return QOIDEC_CORRUPTED;
	}

	return QOIDEC_OK;
}

QOIDEC_API QoiDecStatus
qoi_dec(QoiDecCtx *ctx)
{
	typedef uint8_t  u8; typedef uint32_t u32;
	QOIDEC_ASSERT(ctx->data != NULL);
	QOIDEC_ASSERT(ctx->p != NULL && ctx->end != NULL);
	QOIDEC_ASSERT(ctx->end - ctx->p >= 8);
	QOIDEC_ASSERT(ctx->p[-14] == 'q' && ctx->p[-13] == 'o');
	QOIDEC_ASSERT(ctx->p[-12] == 'i' && ctx->p[-11] == 'f');

	typedef struct { u8 b, g, r, a; } Clr;
	Clr t[64] = {0}, l = { .a = 0xFF };

	const u8 *p = ctx->p, *end = ctx->end;
	if ((*p >> 6) == 0x3 && (*p & 0x3F) < 62) { // ref: https://github.com/phoboslab/qoi/issues/258
		t[(0xFF * 11) % 64] = l;
	}
	for (ptrdiff_t widx = 0; widx < ctx->npixels;) {
		QOIDEC_ASSERT(p <= end);
		if (end - p < 8) {
			return QOIDEC_CORRUPTED;
		}
		u32 c;
		int dg; u8 tmp;
		u8 op = *p++;
		switch (op) {
		case 0xFF:
			l.r = *p++; l.g = *p++; l.b = *p++; l.a = *p++;
			break;
		case 0xFE:
			l.r = *p++; l.g = *p++; l.b = *p++;
			break;
		default:
			switch (op >> 6) {
			case 0x3:
				tmp = (op & 0x3F) + 1;
				if (ctx->npixels - widx < tmp) {
					return QOIDEC_CORRUPTED;
				}
				c = (u32)l.a << 24 | l.r << 16 | l.g << 8 | l.b;
				for (int k = 0; k < tmp; ++k) {
					ctx->data[widx++] = c;
				}
				goto next_iter;
				break;
			case 0x0:
				if (op == *p) {
					return QOIDEC_CORRUPTED; // seriously?
				}
				l = t[op & 0x3F];
				goto no_table;
				break;
			case 0x1:
				l.r += ((op >> 4) & 0x3) - 2;
				l.g += ((op >> 2) & 0x3) - 2;
				l.b += ((op >> 0) & 0x3) - 2;
				break;
			case 0x2:
				tmp = *p++;
				QOIDEC_ASSERT((tmp >> 8)  == 0);
				dg = (op & 0x3F) - 32;
				l.r += dg + ((tmp >> 4)  - 8);
				l.g += dg;
				l.b += dg + ((tmp & 0xF) - 8);
				break;
			}
			break;
		}

		t[(l.r*3 + l.g*5 + l.b*7 + l.a*11) % 64] = l;
no_table:
		ctx->data[widx++] = (u32)l.a << 24 | l.r << 16 | l.g << 8 | l.b;
next_iter:
		(void)0;
	}
	if (end - p < 8 ||
	    !((p[0] == 0) & (p[1] == 0) & (p[2] == 0) & (p[3] == 0) &
	      (p[4] == 0) & (p[5] == 0) & (p[6] == 0) & (p[7] == 1)))
	{
		return QOIDEC_CORRUPTED;
	}

	return QOIDEC_OK;
}

// END QoiDec library



#if defined(QOIDEC_EXAMPLE)
// example program. decodes a qoi image and writes a farbfeld image to stdout.
//	$ cc -DQOIDEC_EXAMPLE qoi-dec.c
//	$ ./a.out < in.qoi > out.ff
//	$ some_image_viewer_that_can_view_ff out.ff

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

#include <error.h>

extern int
main(void)
{
	static uint8_t file[1 << 24];
	ptrdiff_t file_size = fread(file, 1, sizeof file, stdin);
	if (ferror(stdin) || !feof(stdin)) {
		error(1, 0, "failed to read stdin");
	}

	QoiDecCtx qoi;
	if (qoi_dec_init(&qoi, file, file_size) != QOIDEC_OK) {
		error(1, 0, "failed to initailize qoi");
	}
	if ((qoi.data = malloc(qoi.data_size)) == NULL) {
		error(1, errno, 0);
	}
	if (qoi_dec(&qoi) != QOIDEC_OK) {
		error(1, 0, "failed to decode qoi image");
	}

	// write out a farbfeld image
	uint8_t hdr[] = {
		'f', 'a', 'r', 'b', 'f', 'e', 'l', 'd',
		(qoi.w >> 24) & 0xFF, (qoi.w >> 16) & 0xFF,
		(qoi.w >>  8) & 0xFF, (qoi.w >>  0) & 0xFF,
		(qoi.h >> 24) & 0xFF, (qoi.h >> 16) & 0xFF,
		(qoi.h >>  8) & 0xFF, (qoi.h >>  0) & 0xFF,
	};
	fwrite(hdr, 1, sizeof hdr, stdout);
	for (ptrdiff_t i = 0; i < qoi.npixels; ++i) {
		uint32_t c = qoi.data[i];
		uint8_t a = c >> 24, r = c >> 16, g = c >> 8, b = c;
		uint8_t buf[] = {
			r, -(r&0x1), g, -(g&0x1), b, -(b&0x1), a, -(a&0x1),
		};
		fwrite(buf, 1, sizeof buf, stdout);
	}
	fflush(stdout);
	if (ferror(stdout)) {
		error(1, 0, "writing to stdout failed");
	}

	free(qoi.data);
	return 0;
}

#elif defined(__AFL_FUZZ_INIT)

// fuzzing with afl++:
//	$ afl-clang-fast -DDEBUG -g3 -fsanitize=address,undefined -fsanitize-undefined-trap-on-error -o qoi-dec qoi-dec.c
//	$ mkdir i o
//	$ cp some_qoi_files i/
//	$ afl-fuzz -ii -oo ./qoi-dec
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

__AFL_FUZZ_INIT();

extern int
main(void)
{
#ifdef __AFL_HAVE_MANUAL_CONTROL
	__AFL_INIT();
#endif

	unsigned char *afl_buf = __AFL_FUZZ_TESTCASE_BUF;
	while (__AFL_LOOP(10000)) {
		size_t len = __AFL_FUZZ_TESTCASE_LEN;
		unsigned char *buf = memcpy(malloc(len), afl_buf, len);
		QoiDecCtx qoi;
		if (qoi_dec_init(&qoi, buf, len) != QOIDEC_OK) {
			continue;
		}
		if ((qoi.data = malloc(qoi.data_size)) == NULL) {
			continue;
		}
		if (qoi_dec(&qoi) != QOIDEC_OK) {
			continue;
		}
	}

	return 0;
}

#endif
