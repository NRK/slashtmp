// simple qoi encoder: https://qoiformat.org/
//
// This is free and unencumbered software released into the public domain.
// For more information, please refer to <https://unlicense.org/>

#include <limits.h>
#include <stddef.h>
#include <stdint.h>

#ifndef QOIENC_API
	#define QOIENC_API extern
#endif
#ifndef QOIENC_ASSERT
	#define QOIENC_ASSERT(...) ((__VA_ARGS__) ? (void)0 : __builtin_unreachable())
#endif

typedef struct {
	uint8_t   data[15];
	int8_t    len;
} QoiEncResult;

typedef struct {
	uint32_t   prev;
	uint8_t    run       : 7;
	uint8_t    no_alpha  : 1;
	uint32_t   dict[64];
} QoiEncCtx;

enum { QOIENC_NO_ALPHA = 1 << 0, QOIENC_IS_SRGB = 1 << 1 };

/// ctx is assumed to be zero initialized by the caller
QOIENC_API QoiEncResult
qoi_enc_init(QoiEncCtx *ctx, uint32_t width, uint32_t height, uint32_t flags)
{
	QoiEncResult res = { .len = 14 };
	uint8_t *p = res.data;

	QOIENC_ASSERT((flags & ~UINT32_C(3)) == 0);
	QOIENC_ASSERT(ctx->run == 0 && ctx->dict[63] == 0x0);

	*p++ = 'q'; *p++ = 'o'; *p++ = 'i'; *p++ = 'f';
	*p++ = (uint8_t)(width  >> 24); *p++ = (uint8_t)(width  >> 16);
	*p++ = (uint8_t)(width  >>  8); *p++ = (uint8_t)(width  >>  0);
	*p++ = (uint8_t)(height >> 24); *p++ = (uint8_t)(height >> 16);
	*p++ = (uint8_t)(height >>  8); *p++ = (uint8_t)(height >>  0);
	*p++ = (flags & QOIENC_NO_ALPHA) ? 3 : 4;
	*p++ = (flags & QOIENC_IS_SRGB)  ? 0 : 1;
	ctx->prev = UINT32_C(0xFF) << 24;
	ctx->no_alpha = !!(flags & QOIENC_NO_ALPHA);

	return res;
}

QOIENC_API QoiEncResult
qoi_enc(QoiEncCtx *ctx, uint32_t pixel)
{
	enum { OP_RUN = 0xC0, OP_IDX = 0x0, OP_DIFF = 0x40, OP_LUMA = 0x80,
	       OP_RGB = 0xFE, OP_RGBA = 0xFF };
	QoiEncResult res = {0};
	uint8_t *p = res.data;

	if (ctx->no_alpha) {
		pixel |= UINT32_C(0xFF) << 24;
	}

	if (pixel == ctx->prev) {
		if (++ctx->run == 62) {
			*p++ = OP_RUN | (ctx->run - 1);
			ctx->run = 0;
		}
		return (res.len = (int8_t)(p - res.data)), res;
	}

	if (ctx->run > 0) {
		*p++ = OP_RUN | (ctx->run - 1);
		ctx->run = 0;
	}
	uint32_t prev = ctx->prev;
	ctx->prev = pixel;

	uint32_t a = ((pixel >> 24) & 0xFF), r = ((pixel >> 16) & 0xFF),
	         g = ((pixel >>  8) & 0xFF), b = ((pixel >>  0) & 0xFF);
	uint32_t idx = (r * 3 + g * 5 + b * 7 + a * 11) & 63;
	if (ctx->dict[idx] == pixel) {
		*p++ = OP_IDX | idx;
		return (res.len = (int8_t)(p - res.data)), res;
	}
	ctx->dict[idx] = pixel;

	int8_t dr = (int8_t)(r - ((prev >> 16) & 0xFF));
	int8_t dg = (int8_t)(g - ((prev >>  8) & 0xFF));
	int8_t db = (int8_t)(b - ((prev >>  0) & 0xFF));
	int8_t dr_g = dr - dg;
	int8_t db_g = db - dg;
	if (a != (prev >> 24)) {
		*p++ = OP_RGBA; *p++ = r; *p++ = g; *p++ = b; *p++ = a;
	} else if (dr >= -2 && dg >= -2 && db >= -2 &&
	           dr <=  1 && dg <=  1 && db <= 1)
	{
		*p++ = OP_DIFF | (((dr + 2) & 0x3) << 4) |
		       (((dg + 2) & 0x3) << 2) | (((db + 2) & 0x3) << 0);
	} else if (dg >= -32 && dg < 32 &&
	           dr_g >= -8 && dr_g < 8 && db_g >= -8 && db_g < 8)
	{
		*p++ = OP_LUMA | ((dg + 32) & 0x3F);
		*p++ = ((dr_g + 8) << 4) | (db_g + 8);
	} else {
		*p++ = OP_RGB; *p++ = r; *p++ = g; *p++ = b;
	}
	return (res.len = (int8_t)(p - res.data)), res;
}

QOIENC_API QoiEncResult
qoi_enc_finish(QoiEncCtx *ctx)
{
	QoiEncResult res = {0};
	uint8_t *p = res.data;
	if (ctx->run > 0) {
		*p++ = 0xC0 | (ctx->run - 1);
	}
	*p++ = 0x0; *p++ = 0x0; *p++ = 0x0; *p++ = 0x0;
	*p++ = 0x0; *p++ = 0x0; *p++ = 0x0; *p++ = 0x1;
	return (res.len = (int8_t)(p - res.data)), res;
}

//////////// END QoiEnc library


#ifdef DEMO
#include <stdio.h>

static void
out(QoiEncResult res)
{
	fwrite(res.data, 1, res.len, stdout);
}

extern int
main(void)
{
	enum { W = 6, H = 6, L = W*H };
	uint32_t pix[L] = {
		0x00FF0000, 0x00FF0000, 0x00FF0000, 0x00FF0000, 0x00FF0000, 0x00FF0000,
		0x00FF0000, 0x00FF0000, 0x00FF0000, 0x00FF0000, 0x00FF0000, 0x00FF0000,
		0xFF00FF00, 0xFF00FF00, 0xFF00FF00, 0xFF00FF00, 0xFF00FF00, 0xFF00FF00,
		0xFF00FF00, 0xFF00FF00, 0xFF00FF00, 0xFF00FF00, 0xFF00FF00, 0xFF00FF00,
		0xFFFF0000, 0xFFEB6962, 0x8000FE00, 0x8000FB00, 0x8000FE00, 0x8000FB00,
		0xFFFF0000, 0xFFEB6962, 0x8000FE00, 0x8000FB00, 0x8000FE00, 0x8000FB00,
	};

	QoiEncCtx ctx[1] = {0};
	out(qoi_enc_init(ctx, W, H, 0x0));
	for (int i = 0; i < L; ++i) {
		out(qoi_enc(ctx, pix[i]));
	}
	out(qoi_enc_finish(ctx));
	fflush(stdout);
	return ferror(stdout);
}
#endif
