#if 0
gcc -o /tmp/test compression/lz77.c \
	-Wall -Wshadow -Wno-unused \
	-g3 -DDEBUG -fsanitize=address,undefined -fsanitize-trap "$@"
#	-march=native -O3 -g3 "$@"
exit $?
#endif
// experimenting with a custom lz77 based scheme
//
// !!!!!!!!!!!!!!!!!!
// !!! DO NOT USE !!!
// !!!!!!!!!!!!!!!!!!
//
// the entire thing is completely and entirely unsafe and will run into buffer
// overflow and whatnot on unexpected/malicious input.
#include <limits.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

typedef uint8_t     u8;
typedef uint16_t    u16;
typedef uint32_t    u32;
typedef uint64_t    u64;
typedef int8_t      i8;
typedef int16_t     i16;
typedef int32_t     i32;
typedef int64_t     i64;

#ifdef __GNUC__
	// when debugging, use gcc/clang and compile with
	// `-fsanitize=undefined -fsanitize-undefined-trap-on-error`
	// it'll trap if an unreachable code-path is ever reached.
	#define ASSERT(X)  ((X) ? (void)0 : __builtin_unreachable())
#else
	#define ASSERT(X)  ((void)0)
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

enum {
	WINDOW_EXP  = 10,
	WINDOW_SIZE = 1 << WINDOW_EXP,  // [0, 1024)
	WINDOW_MASK = WINDOW_SIZE - 1,
	LEN_EXP     = 5,
	LEN_MIN     = 3,
	LEN_MAX     = (1 << LEN_EXP) + LEN_MIN,  // [3, 1027)
	LIT_MAX     = (1 << 7),   // [1, 128]
	HT_EXP      = 10,
	HT_SIZE     = 1 << HT_EXP,
	HT_MASK     = HT_SIZE - 1,
	HT_BUCKET   = 32,
};

typedef struct {
	u32 offset;
	u32 len;
} Match;

static u32
hash(u8 *icur)
{
	u32 h0 = (((u32)icur[1] << 8) | icur[0]) * 0x9D665555;
	u32 h1 = (u32)icur[2] * 0xAE299555;
	h0 >>= 17;
	h0 ^= h1 >> 15;
	return h0 & HT_MASK;
}

static void
insert(u8 *icur, u8 *iend, u8 *ibase, u32 *htab, u8 *tcap)
{
	if (iend - icur < 3) {
		return;
	}
	u32 h = hash(icur);
	u32 pos = (u32)(icur - ibase);
#if 1
	u8 l = (icur - ibase) & (HT_BUCKET - 1);
#elif 0
	static u64 rng = 1;
	u8 l = (rng >> 32) & (HT_BUCKET - 1);
	rng *= UINT64_C(0xACDC13379D665555);
	rng += 0x1337;
#else
	u8 l = tcap[h];
	tcap[h] = (l + 1) & (HT_BUCKET - 1);
#endif
	htab[(h * HT_BUCKET) + l] = pos;
}

static Match
match(u8 *icur, u8 *iend, u8 *ibase, u32 window_len, u32 len_min, u32 len_max, u32 *htab, u8 *tcap)
{
	Match ret = {0};
	if (iend - icur < 3) {
		return ret;
	}
	u32 h = hash(icur);
	u32 curpos = (u32)(icur - ibase);
	u32 *bucket = htab + (h * HT_BUCKET);
	for (int i = 0; i < HT_BUCKET; ++i) {
		u32 pos = bucket[i];
		if (pos > (curpos - WINDOW_SIZE)) {
			u8 *m0 = ibase + pos;
			u8 *m1 = icur;
			u32 mlen = 0;
			while (m1 < iend && *m0 == *m1 && mlen+1 < len_max) {
				++m0, ++m1, ++mlen;
			}
			if (mlen >= len_min && mlen > ret.len) {
				ret.offset = icur - (ibase + pos);
				ret.len = mlen;
				if (ret.len == len_max-1) {
					break;
				}
			}
		}
	}
	return ret;
}

#ifdef LOG
	#define log(fmt, ...) fprintf(stderr, fmt "\n", __VA_ARGS__)
#else
	#define log(fmt, ...) ((void)0)
#endif

extern int
main(int argc, char *argv[])
{
	static u8 buf[1<<26];
	static u8 outbuf[1<<28];

	int decompress = 0;
	if (argc > 1) {
		decompress = strcmp(argv[1], "-d") == 0;
		if (!decompress || argc > 2) {
			fprintf(stderr, "unknown argument(s)\n");
			return 1;
		}
	}

	size_t buf_len = fread(buf, 1, sizeof buf, stdin);
	if (ferror(stdin)) {
		fprintf(stderr, "Error: reading stdin\n");
		return 1;
	}
	if (!feof(stdin)) {
		fprintf(stderr, "Error: out of buffer, file too large\n");
		return 1;
	}

	if (!decompress) {
		_Alignas(64) static u32 htab[HT_SIZE * HT_BUCKET];
		static u8  tcap[HT_SIZE];

		u8 litbuf[LIT_MAX];
		int lhead = 0;
		u8 *ocur = outbuf;
		for (u8 *icur = buf, *iend = buf + buf_len; icur < iend; ) {
			Match m = match(icur, iend, buf, WINDOW_SIZE, LEN_MIN, LEN_MAX, htab, tcap);
			if (m.len == 0) {
				litbuf[lhead++] = *icur;
			}

			if (lhead > 0 && (lhead == LIT_MAX || m.len > 0 || icur+1 == iend)) {
				// flush lit
				log("LIT   :: %d", lhead);
				*ocur++ = (u8)(0x80 | (lhead - 1));
				ASSERT(lhead > 0);
				for (int i = 0; i < lhead; ++i) {
					*ocur++ = litbuf[i];
				}
				lhead = 0;
			}

			if (m.len > 0) {
				// emit match
				log("MATCH :: <%02d,%d>", m.len, -(int)m.offset);
				ASSERT(m.len >= LEN_MIN && m.len < LEN_MAX);
				*ocur++ = ((m.len - LEN_MIN) << 2) | (m.offset & 0x03);
				*ocur++ = m.offset >> 2;

				int i = m.len;
				for (; i >= 4; i -= 4) {
					insert(icur++, iend, buf, htab, tcap);
					insert(icur++, iend, buf, htab, tcap);
					insert(icur++, iend, buf, htab, tcap);
					insert(icur++, iend, buf, htab, tcap);
				}
				while (i --> 0) {
					insert(icur++, iend, buf, htab, tcap);
				}
			} else {
				insert(icur++, iend, buf, htab, tcap);
			}
		}
		fwrite(outbuf, 1, ocur - outbuf, stdout);
	} else {
		u8 *ocur = outbuf;
		for (u8 *icur = buf, *iend = buf + buf_len; icur < iend; ) {
			ASSERT(icur < iend);
			if (*icur & 0x80) {
				int l = (*icur++ & 0x7F) + 1;
				ASSERT(l > 0);
				for (int i = 0; i < l; ++i) {
					*ocur++ = *icur++;
				}
			} else {
				int l = (*icur >> 2) + LEN_MIN;
				i32 off = (icur[0] & 0x03) | (icur[1] << 2);
				ASSERT(l >= LEN_MIN && off > 0);
				u8 *s = ocur - off;
				icur += 2;
				for (int i = 0; i < l; ++i) {
					*ocur++ = *s++;
				}
			}
		}
		fwrite(outbuf, 1, ocur - outbuf, stdout);
	}

	fflush(stdout);
	return ferror(stdout);
}
