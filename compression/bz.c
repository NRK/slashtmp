#if 0
gcc -o /tmp/test compression/bz.c \
	-Wall -Wshadow -Wno-unused \
	-g3 -DDEBUG -fsanitize=address,undefined -fsanitize-trap
exit $?
#endif
// experimenting with MTF, RLE and Golomb-Rice Coding.
//
// TODO: the file is named bz.c because the plan was for it to contain a
// burrows-wheeler transformation, but currently it doesn't.
//
// !!!!!!!!!!!!!!!!!!
// !!! DO NOT USE !!!
// !!!!!!!!!!!!!!!!!!
//
// the entire thing is completely and entirely unsafe and will run into buffer
// overflow and whatnot on unexpected/malicious input.

#include <limits.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

typedef uint8_t     u8;
typedef uint16_t    u16;
typedef uint32_t    u32;
typedef uint64_t    u64;
typedef int8_t      i8;
typedef int16_t     i16;
typedef int32_t     i32;
typedef int64_t     i64;
typedef ptrdiff_t   Size;

#define SIZEOF(...)     ((Size)sizeof(__VA_ARGS__))
#define ARRLEN(...)     (SIZEOF(__VA_ARGS__) / SIZEOF(0[__VA_ARGS__]))
#define BRK()           __asm ("int3; nop")
#define SWAP(a, b, T)   do { T tmp__ = b; b = a; a = tmp__; } while (0)
#define MIN(A, B)       ((A) < (B) ? (A) : (B))
#ifdef __GNUC__
	// when debugging, use gcc/clang and compile with
	// `-fsanitize=undefined -fsanitize-undefined-trap-on-error`
	// it'll trap if an unreachable code-path is ever reached.
	#define ASSERT(X)  ((X) ? (void)0 : __builtin_unreachable())
#else
	#define ASSERT(X)  ((void)0)
#endif

// LSB bit routines
typedef struct {
	u32 accum;
	int bitpos;
	u8 *cur;
} BitIO;

static void
bitwrite(BitIO *bo, u32 c, int nbits)
{
	ASSERT(nbits > 0 && nbits <= 8);
	bo->accum |= c << bo->bitpos;
	bo->bitpos += nbits;
	if (bo->bitpos >= 8) {
		*bo->cur++ = (u8)bo->accum;
		bo->accum >>= 8;
		bo->bitpos -= 8;
	}
}

static u32
bitread(BitIO *bo, int nbits)
{
	ASSERT(nbits > 0 && nbits <= 8);
	if (bo->bitpos < nbits) {
		bo->accum |= (*bo->cur++ << bo->bitpos);
		bo->bitpos += 8;
	}
	u32 ret = bo->accum & (((u32)1 << nbits) - 1);
	bo->accum >>= nbits;
	bo->bitpos -= nbits;
	return ret;
}

static Size
m2f_enc(u8 *in, u8 *iend, u8 *out)
{
	u8 tbl[256];
	for (int i = 0; i < 256; ++i) {
		tbl[i] = i;
	}

	u8 *outcur = out;
	for (u8 *icur = in; icur < iend; ++icur) {
		int found = 0;
		for (int i = 0; i < 256; ++i) {
			if (tbl[i] == *icur) {
				*outcur++ = (u8)i;
				for (int k = i; k > 0; --k) {
					u8 tmp = tbl[k];
					tbl[k] = tbl[k-1];
					tbl[k-1] = tmp;
				}
				found = 1;
				break;
			}
		}
		ASSERT(found);
	}
	return outcur - out;
}

static Size
m2f_dec(u8 *in, u8 *iend, u8 *out)
{
	u8 tbl[256];
	for (int i = 0; i < 256; ++i) {
		tbl[i] = i;
	}
	u8 *outcur = out;
	for (u8 *icur = in; icur < iend; ++icur) {
		u8 code = *icur;
		*outcur++ = tbl[code];
		for (int k = code; k > 0; --k) {
			u8 tmp = tbl[k];
			tbl[k] = tbl[k-1];
			tbl[k-1] = tmp;
		}
	}
	return outcur - out;
}


static Size
rle4_enc(u8 *in, u8 *iend, u8 *out)
{
	u32 last4 = (in < iend) ? (*in ^ 0xFF) : 0;
	u8 *outcur = out;
	for (u8 *icur = in; icur < iend;) {
		u8 ch = *icur++;
		u32 test = (u32)ch * 0x01010101;
		*outcur++ = ch;
		last4 = (last4 << 8) | ch;

		if (test == last4) {
			int run = 0;
			for (; run < 255 && icur < iend;) {
				if (*icur == ch) {
					++icur;
					++run;
				} else {
					break;
				}
			}
			ASSERT(run < 256);
			*outcur++ = (u8)run;
			if (icur < iend) {
				last4 = *icur ^ 0xFF;
			}
		}
	}
	return outcur - out;
}

static Size
rle4_dec(u8 *in, u8 *iend, u8 *out)
{
	u32 last4 = (in < iend) ? (*in ^ 0xFF) : 0;
	int run = 0;
	u8 *outcur = out;
	for (u8 *icur = in; icur < iend;) {
		u8 ch = *icur++;
		u32 test = (u32)ch * 0x01010101;
		*outcur++ = ch;
		last4 = (last4 << 8) | ch;
		if (last4 == test) {
			if (!(icur < iend)) {
				return -1;
			}
			for (int r = *icur++; r > 0; --r) {
				*outcur++ = ch;
			}
			last4 = ch ^ 0xFF;
		}
	}
	return outcur - out;
}

enum {
	BLOCK_SIZE = 1 << 10,
	MIN_K      = 2,
	MAX_K      = 7,
};

static Size
golomb_rice_enc(u8 *in, u8 *iend, u8 *out)
{
	BitIO bo[1] = {{ .cur = out }};
	int lastk = -1;
	for (u8 *icur = in; icur < iend;) {
		Size nbytes = MIN((iend - icur), BLOCK_SIZE);
		Size outputsize[9] = {0};
		for (int i = 0; i < nbytes; ++i) {
			u8 ch = icur[i];
			for (int k = MIN_K; k <= MAX_K; ++k) {
				int u = ch >> k;
				int b = ch & ((1 << k) - 1);
				outputsize[k] += u + 1 + k;
			}
		}

		Size best_compression = PTRDIFF_MAX;
		int best_k = -1;
		for (int k = MIN_K; k <= MAX_K; ++k) {
			if (outputsize[k] < best_compression) {
				lastk = k;
				best_k = k;
				best_compression = outputsize[k];
			}
		}
		ASSERT(best_k > 0 && best_k <= 7);
		int K = best_k, Msk = (1 << K) - 1;
		bitwrite(bo, K-1, 3);
		for (int i = 0; i < nbytes; ++i) {
			u8 ch = icur[i];
			int b = ch & Msk;
			for (int u = ch >> K; u > 0; --u) {
				bitwrite(bo, 1, 1);
			}
			bitwrite(bo, 0, 1);
			bitwrite(bo, b, K);
		}

		icur += nbytes;
	}
	if (lastk > 0) {
		int eof = (0xFF >> lastk) + 1;
		for (; eof; --eof) {
			bitwrite(bo, 1, 1);
		}
		bitwrite(bo, 0, 1);
	}
	int padding = -bo->bitpos & 7;
	if (padding) {
		bitwrite(bo, 0, padding);
	}
	ASSERT(bo->bitpos == 0);
	return bo->cur - out;
}

static Size
golomb_rice_dec(u8 *in, u8 *iend, u8 *out)
{
	BitIO bo[1] = {{ .cur = in }};
	u8 *outcur = out;
	int symread = 0;
	int K, M;
	for (;;) {
		if (symread == 0) {
			K = bitread(bo, 3) + 1;
			if (!(K >= MIN_K && K <= MAX_K)) {
				return -1;
			}
			M = 1 << K;
			symread = BLOCK_SIZE;
		}

		int eof = (0xFF >> K) + 1;
		int u = 0;
		while (bitread(bo, 1) == 1 && u <= eof) {
			++u;
		}
		if (u == eof) {
			break;
		}
		int b = bitread(bo, K);
		u8 ch = (u * M) + b;
		*outcur++ = ch;
		--symread;
	}
	return outcur - out;
}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

extern int
main(int argc, char *argv[])
{
	static u8 buf[1<<28];
	static u8 outbuf[1<<28]; // should be big enough :3

	int decompress = 0;
	if (argc > 1) {
		decompress = strcmp(argv[1], "-d") == 0;
		if (!decompress || argc > 2) {
			fprintf(stderr, "unknown argument(s)\n");
			return 1;
		}
	}

	Size n = fread(buf, 1, sizeof buf >> 2, stdin);
	if (ferror(stdin)) {
		fprintf(stderr, "Error: reading stdin\n");
		return 1;
	}
	if (!feof(stdin)) {
		fprintf(stderr, "Error: out of buffer, file too large\n");
		return 1;
	}

	enum {
		M2F = 1,
		RLE = 1,
		GR  = 1,
	};

	if (!decompress) {
		u8 *buffers[2] = { buf, outbuf };
		Size inlen = n;

		if (M2F) {
			inlen = m2f_enc(buffers[0], buffers[0] + inlen, buffers[1]);
			SWAP(buffers[0], buffers[1], u8 *);
		}

		if (RLE) {
			inlen = rle4_enc(buffers[0], buffers[0] + inlen, buffers[1]);
			SWAP(buffers[0], buffers[1], u8 *);
		}

		if (GR) {
			inlen = golomb_rice_enc(buffers[0], buffers[0] + inlen, buffers[1]);
			SWAP(buffers[0], buffers[1], u8 *);
		}

		fwrite(buffers[0], 1, inlen, stdout);
	} else {
		u8 *buffers[2] = { buf, outbuf };
		Size inlen = n;

		if (GR) {
			inlen = golomb_rice_dec(buffers[0], buffers[0] + inlen, buffers[1]);
			SWAP(buffers[0], buffers[1], u8 *);
			if (inlen < 0) { fprintf(stderr, "Error: corrupt file\n"); abort(); }
		}

		if (RLE) {
			inlen = rle4_dec(buffers[0], buffers[0] + inlen, buffers[1]);
			SWAP(buffers[0], buffers[1], u8 *);
			if (inlen < 0) { fprintf(stderr, "Error: corrupt file\n"); abort(); }
		}

		if (M2F) {
			inlen = m2f_dec(buffers[0], buffers[0] + inlen, buffers[1]);
			SWAP(buffers[0], buffers[1], u8 *);
			if (inlen < 0) { fprintf(stderr, "Error: corrupt file\n"); abort(); }
		}

		fwrite(buffers[0], 1, inlen, stdout);
	}

	fflush(stdout);
	return ferror(stdout);
}
