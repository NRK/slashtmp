#if 0
gcc -o /tmp/test compression/toycompress.c \
	-Wall -Wshadow -Wno-unused \
	-g3 -DDEBUG -fsanitize=address,undefined -fsanitize-trap
exit $?
#endif
// | char set (32 bytes) | padding bits (1 byte) | compressed data |
// ref: https://youtube.com/watch?v=J_M2fvkOPkc
#include <limits.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

typedef uint8_t     u8;
typedef uint16_t    u16;
typedef uint32_t    u32;
typedef uint64_t    u64;
typedef int8_t      i8;
typedef int16_t     i16;
typedef int32_t     i32;
typedef int64_t     i64;
typedef ptrdiff_t   Size;

#ifdef __GNUC__
	// when debugging, use gcc/clang and compile with
	// `-fsanitize=undefined -fsanitize-undefined-trap-on-error`
	// it'll trap if an unreachable code-path is ever reached.
	#define ASSERT(X)  ((X) ? (void)0 : __builtin_unreachable())
#else
	#define ASSERT(X)  ((void)0)
#endif

static int
clz32(u32 v)
{
#if __GNUC__
	_Static_assert(sizeof(int) == 4 && (unsigned char)-1 == 255, "32bit int");
	return v ? __builtin_clz(v) : 32;
#else
	int i;
	for (i = 0; i < 32 && (v >> 31) == 0; ++i, v <<= 1) {}
	return i;
#endif
}

static int
nbits_from_nsyms(int nsyms)
{
	ASSERT(nsyms >= 0 && nsyms <= 256);
	int nbits = 32 - clz32(nsyms | 0x1);
	return nbits;
}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
	uint32_t accum;
	int bitpos;
	u8 *cur;
} BitOut;

// assumes the high bits of `ch` are clear
static void
bitwrite(BitOut *o, int ch, int nbits)
{
	ASSERT(nbits >= 0 && nbits <= 8);
	o->accum <<= nbits;
	o->accum |= ch;
	o->bitpos += nbits;
	if (o->bitpos >= 8) {
		*o->cur++ = (u8)(o->accum >> (o->bitpos - 8));
		o->bitpos -= 8;
		o->accum &= (1 << o->bitpos) - 1;
	}
}

typedef struct {
	uint32_t accum;
	int bitpos;
	u8 *cur;
} BitIn;

static int
bitread(BitIn *bi, int nbits)
{
	ASSERT(nbits >= 0 && nbits <= 8);
	if (bi->bitpos < nbits) {
		bi->accum <<= 8;
		bi->accum |= *bi->cur++;
		bi->bitpos += 8;
	}
	int ret = bi->accum >> (bi->bitpos - nbits);
	bi->bitpos -= nbits;
	bi->accum &= (1 << bi->bitpos) - 1;
	return ret;
}

static int
extract(u8 *buf, Size len, u8 *out)
{
	if (len < 33) {
		fprintf(stderr, "Error: missing char set. data is corrupted\n");
		return -1;
	}

	u8 map[256] = {0};
	int nsyms = 0;
	for (Size i = 0; i < 256; ++i) {
		u8 idx = i >> 3;
		u8 bit = 1 << (i & 0x7);
		if (buf[idx] & bit) {
			map[nsyms++] = i;
		}
	}
	int nbits = nbits_from_nsyms(nsyms);
	ASSERT(nbits > 0 && nbits <= 8);
	int padbits = buf[32];
	Size outbits  = (len - 33) * 8 - padbits;
	Size outbytes = outbits / nbits;
	if (padbits > 7 || outbits < 0 || (outbits % nbits) != 0) {
		return -2;
	}

	BitIn bi[1] = {{ .cur = buf + 33 }};
	for (Size i = 0; i < outbytes; ++i) {
		int ch = bitread(bi, nbits);
		ASSERT(ch >= 0 && ch <= 255);
		// ASSERT(ch < nsyms); // TODO: consider corrupt?
		out[i] = map[ch];
	}
	return outbytes;
}

static Size
compress(u8 *buf, Size len, u8 *out)
{
	u8 set[32] = {0};
	int nsyms = 0;
	for (Size i = 0; i < len; ++i) {
		u8 ch = buf[i];
		u8 idx = ch >> 3;
		u8 bit = 1 << (ch & 0x7);
		nsyms += (set[idx] & bit) == 0;
		set[idx] |= bit;
	}
	int nbits = nbits_from_nsyms(nsyms);
	ASSERT(nbits > 0 && nbits <= 8);
	u8 map[256] = {0};
	for (Size i = 0, code = 0; i < 256; ++i) {
		int idx = i >> 3;
		int bit = 1 << (i & 0x7);
		if (set[idx] & bit) {
			map[i] = code++;
		}
	}
	int totalbits = len * nbits;
	int padbits = -totalbits & 7;

	BitOut o[1] = {{ .cur = out }};
	/// header
	for (Size i = 0; i < sizeof set; ++i) {
		bitwrite(o, set[i], 8);
	}
	bitwrite(o, padbits, 8);
	/// data
	for (Size i = 0; i < len; ++i) {
		bitwrite(o, map[buf[i]], nbits);
	}
	/// padding
	bitwrite(o, 0, padbits);
	ASSERT(o->bitpos == 0);

	return o->cur - out;
}

extern int
main(int argc, char *argv[])
{
	static u8 buf[1<<16];
	static u8 outbuf[(1<<16) + 33];

	int decompress = 0;
	if (argc > 1) {
		decompress = strcmp(argv[1], "-d") == 0;
		if (!decompress || argc > 2) {
			fprintf(stderr, "unknown argument(s)\n");
			return 1;
		}
	}

	size_t n = fread(buf, 1, sizeof buf, stdin);
	if (ferror(stdin)) {
		fprintf(stderr, "Error: reading stdin\n");
		return 1;
	}
	if (!feof(stdin)) {
		fprintf(stderr, "Error: out of buffer, file too large\n");
		return 1;
	}

#ifdef DEBUG
	{
		u8 testbuf[32], res;
		BitOut o[1] = {{ .cur = testbuf }};
		bitwrite(o, 1, 3);
		bitwrite(o, 2, 3);
		bitwrite(o, 3, 3);
		bitwrite(o, 0, 16-9);
		BitIn bi[1] = {{ .cur = testbuf }};
		res = bitread(bi, 3); ASSERT(res == 1);
		res = bitread(bi, 3); ASSERT(res == 2);
		res = bitread(bi, 3); ASSERT(res == 3);
	}

	#define TCASE(INPUT) do { \
		u8 input[] = "" INPUT ""; \
		u8 encbuf[sizeof INPUT * 2 + 33]; \
		u8 decbuf[sizeof INPUT * 2 + 33]; \
		Size enclen = compress(input, sizeof input - 1, encbuf); \
		Size declen = extract(encbuf, enclen, decbuf); \
		ASSERT(declen == sizeof input - 1); \
		ASSERT(memcmp(decbuf, input, sizeof input - 1) == 0); \
	} while (0);

	TCASE("");
	TCASE("hello");
	TCASE("1111111111111");
	TCASE("12121212");
	TCASE("\x00\x01\x02\x03\xFF");
#endif // DEBUG

	Size len = decompress ? extract(buf, n, outbuf) : compress(buf, n, outbuf);
	if (len < 0) {
		return 1;
	}
	fwrite(outbuf, 1, len, stdout);
	fflush(stdout);
	return ferror(stdout);
}
