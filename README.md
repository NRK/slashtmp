# slashtmp

This repo contains various code snippets which are small/useless/incomplete etc.

I've had plenty of such projects scattered through out my file-system, a lot of
them have been lost to system restarts due to the fact that they were in `/tmp`
directory, which on my system is a ramdisk. That's also where the name of the
repo comes from; with the "/" written out.

## Notable projects

| Name | Description |
| :--  | :--         |
| [qoi-dec.c](./compression/qoi-dec.c) | Fast, zero-dependency QOI decoder |
| [qoi-enc.c](./compression/qoi-enc.c) | Streaming zero-dependency QOI encoder |
| [radix-integer.c](./sort/radix-integer.c) | Generic radix/american-flag sort implementation that works on both signed and unsigned integer key |
| [heapsort.c](./sort/heapsort.c) | Optimized heapsort implementation with a `qsort` like interface |
| [ini.c](./parsers/ini.c) | Simple, zero-dependency ini parser |
| [y4m.c](./parsers/y4m.c) | Zero-dependency y4m parser |
| [xxhash32-stream.c](./hash/xxhash32-stream.c), [xxhash64-stream.c](./hash/xxhash64-stream.c) | Streaming interface for xxhash32 and xxhash64 |
| [xxhash32.c](./hash/xxhash32.c) | One pass interface for xxhash32 |
| [fate](./misc/fate) | Simple task scheduler |

## License

All the source code in this repo, unless otherwise stated, is available under
[MPL v2.0](LICENSE).
