#ifndef TOMIC__H
#define TOMIC__H

/* ergonomic wrappers around C11's <stdatomic.h>
 * so that you don't need a widescreen monitor to read your CAS loops.
 * released into the public domain, do whatever the fuck you want with it.
 */

#define TOMIC _Atomic
#define tomic_load(P, MO)           atomic_load_explicit((P), memory_order_##MO)
#define tomic_store(P, VAL, MO)     atomic_store_explicit((P), (VAL), memory_order_##MO)
#define tomic_fch_or(P, VAL, MO)    atomic_fetch_or_explicit((P), (VAL), memory_order_##MO)
#define tomic_fch_and(P, VAL, MO)   atomic_fetch_and_explicit((P), (VAL), memory_order_##MO)
#define tomic_fch_xor(P, VAL, MO)   atomic_fetch_xor_explicit((P), (VAL), memory_order_##MO)
#define tomic_fch_add(P, VAL, MO)   atomic_fetch_add_explicit((P), (VAL), memory_order_##MO)
#define tomic_fch_sub(P, VAL, MO)   atomic_fetch_sub_explicit((P), (VAL), memory_order_##MO)
#define tomic_xchg(P, VAL, MO)      atomic_exchange_explicit((P), (VAL), memory_order_##MO)
#define tomic_fence(MO)             atomic_thread_fence(memory_order_##MO)
#define tomic_cas(P, OLD, NEW, MO_SUCC, MO_FAIL) \
	atomic_compare_exchange_strong_explicit((P), (OLD), (NEW), memory_order_##MO_SUCC, memory_order_##MO_FAIL)
#define tomic_cas_weak(P, OLD, NEW, MO_SUCC, MO_FAIL) \
	atomic_compare_exchange_weak_explicit((P), (OLD), (NEW), memory_order_##MO_SUCC, memory_order_##MO_FAIL)

/* NOTE: the api above can also be adapted to use gcc atomic builtins if you wish.
 * the gcc builtins predates C11 and thus might be more portable in practice.
 */
#if 0 /* example of how to use gcc's builtins */
	#define TOMIC
	#define tomic__acquire    __ATOMIC_ACQUIRE
	#define tomic_load(P, MO) __atomic_load_n((P), tomic__##MO)
#endif

#endif /* TOMIC__H */
