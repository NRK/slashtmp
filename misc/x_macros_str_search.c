/*
 * $ cc -o /tmp/x x_macros_str_search.c
 */

#include <stdio.h>
#include <string.h>

#define DIR_TABLE(DIR_X) \
	DIR_X(north) \
	DIR_X(south) \
	DIR_X(east) \
	DIR_X(weast) \

#define DIR_ENUM(X) dir_##X ,
enum directions {
	DIR_TABLE(DIR_ENUM)
	dir_error
};

enum { dirstr_max_len = 6 };
#define DIR_STR(X) #X ,
static const char dirstr[][dirstr_max_len] = {
	DIR_TABLE(DIR_STR)
};

/* or for C11, use static_assert()
 * #define DIR_ASSERT(X) static_assert(sizeof #X <= dirstr_max_len, "string too long");
 */
#define DIR_ASSERT(X) int X[ sizeof #X <= dirstr_max_len ? 1 : -1 ];
struct dir__str_size_check {
	DIR_TABLE(DIR_ASSERT)
};

static enum directions
str2dir(const char *s)
{
	enum directions d;
	for (d = 0; d < dir_error; ++d) {
		if (strcmp(s, dirstr[d]) == 0)
			break;
	}
	return d;
}

extern int
main(int argc, char *argv[])
{
	enum directions d;

	if (argc < 2) {
		fprintf(stderr, "no argument\n");
		return 1;
	}

	d = str2dir(argv[1]);
	switch (d) {
	case dir_error:
		puts("bad!");
		return 1;
		break;
	default:
		printf("you go: ");
		puts(dirstr[d]);
		return 0;
		break;
	}
}
