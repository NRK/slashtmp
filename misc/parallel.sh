#!/bin/sh
#
# demo on how to run tasks in parallel using only posix shell/utility.
# this is mostly education stuff just to scratch an itch.
# on "real" programs, i'd just use `xargs -P` which should be fairly portable.

task_max=4
task_running=0
task_pipe="${TMPDIR:-/tmp}/task_pipe_$$"

task_wait_one() {
	read task_pid < "$task_pipe"
	wait "$task_pid"  # optional, avoids race.
	task_running=$(( task_running - 1 ))
}

task_wait() {
	while [ "$task_running" -gt 0 ]; do
		task_wait_one
	done
}

task_enqueue() {
	[ "$task_running" -eq "$task_max" ] && task_wait_one
	task_running=$(( task_running + 1 ))
	(
		task_pid="$BASHPID"
		[ -z "$task_pid" ] && task_pid="$(sh -c 'echo $PPID')"
		"$@"
		echo "$task_pid" >> "$task_pipe"
	) &
}

### demo

dummy_program() {
	echo "[$1] starting..." >&2
	sleep "$1"
	echo "[$1] finished! (^o^)7" >&2
}

cleanup() {
	trap "" TERM
	kill -TERM "-$$"
	wait
	rm -f "$task_pipe"
	exit "$1"
}

main() {
	trap 'cleanup 0' EXIT
	trap 'cleanup 2' TERM INT
	mkfifo "$task_pipe" || exit 1

	for i in 1 2 3 4 5 6 7 8 9 10; do
		task_enqueue dummy_program "$i"
	done
	task_wait
}

main
