/*
 * print all fibonacci numbers upto 100th
 *
 * uses __int128 if available: https://gcc.gnu.org/onlinedocs/gcc/extensions-to-the-c-language-family/128-bit-integers.html
 * otherwise uses a software emulated 128 bit integer.
 */

#include <assert.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <inttypes.h>

#ifndef USE_128
	#define USE_128 (__SIZEOF_INT128__ == 16)
#endif

#if USE_128
typedef unsigned __int128  u128;
static char *
u128_to_str_core(char buf[static 64], u128 n)
{
	char *q = buf + 64;
	*--q = '\0';
	do {
		*--q = (n % 10) + '0';
	} while (n /= 10);
	return q;
}
#define u128_to_str(N) u128_to_str_core((char [64]){0}, (N))

#else

typedef union {
	uint64_t v[2];
	struct { uint64_t lo, hi; };
} u128;

static char *
u128_to_str_core(char buf[static 64], u128 n)
{
	char *p = buf + 64;
	*--p = '\0';
	do {
		u128 q = {0}, r = {0};
		// binary long division
		for (int i = 127; i >= 0; --i) {
			r.hi = (r.hi << 1) | (r.lo >> 63);
			r.lo <<= 1;
			r.lo |= (n.v[i / 64] >> (i & 63)) & 0x1;
			if (r.hi > 0 || r.lo >= 10) {
				int c = r.lo < 10;
				r.lo -= 10;
				r.hi -= c;
				q.v[i / 64] |= UINT64_C(1) << (i & 63);
			}
		}
		n = q;
		assert(r.hi == 0 || r.lo <= 9);
		*--p = (r.lo + '0');
	} while (n.hi > 0 || n.lo > 0);
	return p;
}
#define u128_to_str(N) u128_to_str_core((char [64]){0}, (N))

static u128
u128_add(u128 a, u128 b)
{
	u128 ret;
	int c = ((uint64_t)-1 - a.lo) < b.lo;
	ret.lo = a.lo + b.lo;
	if ((uint64_t)-1 - c - a.hi < b.hi)
		abort(); // overflow
	ret.hi = a.hi + b.hi + c;
	return ret;
}
#endif

extern int
main(void)
{
	enum { N = 100 }; /* can go upto 187 with 128bit integers */

	{
		char s[] = "fib(0) => 0\nfib(1) => 1\n";
		fwrite(s, 1, sizeof s - 1, stdout);
	}

#if USE_128
	u128 l[3] = { [2] = 1 };
	for (int i = 2; i < N; ++i) {
		l[0] = l[1];
		l[1] = l[2];
		if ((u128)-1 - l[0] < l[1])
			abort(); // overflow
		l[2] = l[0] + l[1];
		printf("fib(%d) => ", i);
		puts(u128_to_str(l[2]));
	}
#else
	u128 l[3] = { [2] = { .lo = 1 } };
	for (int i = 2; i < N; ++i) {
		l[0] = l[1];
		l[1] = l[2];
		l[2] = u128_add(l[0], l[1]);
		printf("fib(%d) => ", i);
		puts(u128_to_str(l[2]));
	}
#endif

	/* printf("USE_128 = %s\n", (char *[]){ "no", "yes"}[USE_128]); */
	fflush(stdout);
	return ferror(stdout);
}

#if 0
fib(92) => 7540113804746346429
fib(93) => 12200160415121876738
fib(94) => 19740274219868223167
fib(95) => 31940434634990099905
fib(96) => 51680708854858323072
fib(97) => 83621143489848422977
fib(98) => 135301852344706746049
fib(99) => 218922995834555169026
#endif
