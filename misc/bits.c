#if 0
set -e
gcc -o /tmp/bits-test misc/bits.c -march=x86-64-v3 \
	-Wall -Wextra -Wconversion \
	-DTEST -g3 -fsanitize=address,undefined -fsanitize-trap
/tmp/bits-test
echo "[PASS] hardware"
gcc -o /tmp/bits-test misc/bits.c -DUSE_SW_EMULATION \
	-Wall -Wextra -Wconversion \
	-DTEST -g3 -fsanitize=address,undefined -fsanitize-trap
/tmp/bits-test
echo "[PASS] software"
exit 0
#endif

#include <limits.h>
#include <stdint.h>

typedef uint8_t  u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

static int
clz32(u32 v)
{
#if   __GNUC__ && UINT32_MAX == UINT_MAX  && !defined(USE_SW_EMULATION)
	return (v == 0) ? 32 : __builtin_clz(v);
#elif __GNUC__ && UINT32_MAX == ULONG_MAX && !defined(USE_SW_EMULATION)
	return (v == 0) ? 32 : __builtin_clzl(v);
#else
	int i;
	for (i = 0; i < 32 && (v >> 31) == 0; ++i, v <<= 1) {}
	return i;
#endif
}

static int
clz64(u64 v)
{
#if __GNUC__ && UINT64_MAX == ULLONG_MAX && !defined(USE_SW_EMULATION)
	return (v == 0) ? 64 : __builtin_clzll(v);
#else
	int i;
	for (i = 0; i < 64 && (v >> 63) == 0; ++i, v <<= 1) {}
	return i;
#endif
}

static int
ctz32(u32 v)
{
#if   __GNUC__ && UINT32_MAX == UINT_MAX  && !defined(USE_SW_EMULATION)
	return (v == 0) ? 32 : __builtin_ctz(v);
#elif __GNUC__ && UINT32_MAX == ULONG_MAX && !defined(USE_SW_EMULATION)
	return (v == 0) ? 32 : __builtin_ctzl(v);
#else
	// https://graphics.stanford.edu/~seander/bithacks.html#ZerosOnRightMultLookup
	static const u8 lut[32] = {
		0, 1, 28, 2, 29, 14, 24, 3, 30, 22, 20, 15, 25, 17, 4, 8,
		31, 27, 13, 23, 21, 19, 16, 7, 26, 12, 18, 6, 11, 5, 10, 9
	};
	u32 mul = (v & -v) * UINT32_C(0x077CB531);
	return (v == 0) ? 32 : lut[mul >> 27];
#endif
}

static int
ctz64(u64 v)
{
#if __GNUC__ && UINT64_MAX == ULLONG_MAX && !defined(USE_SW_EMULATION)
	return (v == 0) ? 64 : __builtin_ctzll(v);
#else
	int i;
	for (i = 0; i < 64 && (v & 0x1) == 0; ++i, v >>= 1) {}
	return i;
#endif
}

static int
popcnt32(u32 v)
{
#if   __GNUC__ && UINT32_MAX == UINT_MAX  && !defined(USE_SW_EMULATION)
	return __builtin_popcount(v);
#elif __GNUC__ && UINT32_MAX == ULONG_MAX && !defined(USE_SW_EMULATION)
	return __builtin_popcountl(v);
#else
	int i;
	for (i = 0; v != 0; ++i) { v &= v-1; }
	return i;
#endif
}

static int
popcnt64(u64 v)
{
#if __GNUC__ && UINT64_MAX == ULLONG_MAX && !defined(USE_SW_EMULATION)
	return __builtin_popcountll(v);
#else
	int i;
	for (i = 0; v != 0; ++i) { v &= v-1; }
	return i;
#endif
}

static u32
rotr32(u32 v, int n)
{
	#if __GNUC__
	if (!(n >= 0 && n < 32)) __builtin_unreachable();
	#endif
	return (v >> n) | (v << (-n & 31));
}

static u32
rotl32(u32 v, int n)
{
	#if __GNUC__
	if (!(n >= 0 && n < 32)) __builtin_unreachable();
	#endif
	return (v << n) | (v >> (-n & 31));
}

static u64
rotr64(u64 v, int n)
{
	#if __GNUC__
	if (!(n >= 0 && n < 64)) __builtin_unreachable();
	#endif
	return (v >> n) | (v << (-n & 63));
}

static u64
rotl64(u64 v, int n)
{
	#if __GNUC__
	if (!(n >= 0 && n < 64)) __builtin_unreachable();
	#endif
	return (v << n) | (v >> (-n & 63));
}

#if __has_include(<immintrin.h>) && __BMI2__ && !defined(USE_SW_EMULATION)
	#include <immintrin.h>
	static u32 pext32(u32 v, u32 mask) { return _pext_u32(v, mask); }
#else
	static u32
	pext32(u32 v, u32 mask)
	{
		u32 ret = 0;
		u32 test = 0x1;
		while (mask != 0) {
			u32 ontarget = mask & test;
			ret |= v & ontarget;
			test <<= !!ontarget;
			v    >>= !ontarget;
			mask >>= !ontarget;
		}
		return ret;
	}
#endif

/////// loads & stores
// TODO: stores

static uint16_t
load16le(void *vp)
{
	uint8_t *p = vp;
	return (uint16_t)(p[0] << 0 | (uint16_t)p[1] << 8);
}

static uint32_t
load32le(void *vp)
{
	uint8_t *p = vp;
	return (uint32_t)p[0] <<  0 | (uint32_t)p[1] <<  8 |
	       (uint32_t)p[2] << 16 | (uint32_t)p[3] << 24;
}

static uint64_t
load64le(void *vp)
{
	uint8_t *p = vp;
	return (uint64_t)p[0] <<  0 | (uint64_t)p[1] <<  8 |
	       (uint64_t)p[2] << 16 | (uint64_t)p[3] << 24 |
	       (uint64_t)p[4] << 32 | (uint64_t)p[5] << 40 |
	       (uint64_t)p[6] << 48 | (uint64_t)p[7] << 56;
}

static uint16_t
load16be(void *vp)
{
	uint8_t *p = vp;
	return (uint16_t)(p[1] << 0 | (uint16_t)p[0] << 8);
}

static uint32_t
load32be(void *vp)
{
	uint8_t *p = vp;
	return (uint32_t)p[3] <<  0 | (uint32_t)p[2] <<  8 |
	       (uint32_t)p[1] << 16 | (uint32_t)p[0] << 24;
}

static uint64_t
load64be(void *vp)
{
	uint8_t *p = vp;
	return (uint64_t)p[7] <<  0 | (uint64_t)p[6] <<  8 |
	       (uint64_t)p[5] << 16 | (uint64_t)p[4] << 24 |
	       (uint64_t)p[3] << 32 | (uint64_t)p[2] << 40 |
	       (uint64_t)p[1] << 48 | (uint64_t)p[0] << 56;
}


/////////// tests

#ifdef TEST
#define ASSERT(X) ((X) ? (void)0 : __builtin_unreachable())
#define ARRLEN(X) ((int)(sizeof(X) / sizeof(0[X])))
extern int
main(void)
{
	{ // clz32
		struct { u32 in; int exp; } t[] = {
			{ 0, 32 },
			{ (u32)-1, 0 }, { 0x80000000, 0 },
			{ 1, 31 },
			{ 2, 30 }, { 3, 30 },
		};
		for (int i = 0; i < ARRLEN(t); ++i) {
			ASSERT(clz32(t[i].in) == t[i].exp);
		}
	}
	{ // clz64
		struct { u64 in; int exp; } t[] = {
			{ 0, 64 },
			{ (u64)-1, 0 }, { 0x8000000000000000, 0 },
			{ 1, 63 },
			{ 2, 62 }, { 3, 62 },
		};
		for (int i = 0; i < ARRLEN(t); ++i) {
			ASSERT(clz64(t[i].in) == t[i].exp);
		}
	}
	{ // ctz32
		struct { u32 in; int exp; } t[] = {
			{ 0, 32 },
			{ (u32)-1, 0 }, { 0x80000000, 31 },
			{ 1, 0 },
			{ 2, 1 }, { 3, 0 },
		};
		for (int i = 0; i < ARRLEN(t); ++i) {
			ASSERT(ctz32(t[i].in) == t[i].exp);
		}
	}
	{ // ctz64
		struct { u64 in; int exp; } t[] = {
			{ 0, 64 },
			{ (u64)-1, 0 }, { 0x8000000000000000, 63 },
			{ 1, 0 },
			{ 2, 1 }, { 3, 0 },
		};
		for (int i = 0; i < ARRLEN(t); ++i) {
			ASSERT(ctz64(t[i].in) == t[i].exp);
		}
	}
	{ // popcnt32
		struct { u32 in; int exp; } t[] = {
			{ 0, 0 }, { 1, 1 }, { 2, 1 },
			{ 0xF, 4 }, { 0xF00D, 7 }, { 0xF00, 4 },
			{ (u32)-1, 32 }
		};
		for (int i = 0; i < ARRLEN(t); ++i) {
			ASSERT(popcnt32(t[i].in) == t[i].exp);
		}
	}
	{ // popcnt64
		struct { u64 in; int exp; } t[] = {
			{ 0, 0 }, { 1, 1 }, { 2, 1 },
			{ 0xF, 4 }, { 0xF00D, 7 }, { 0xF00, 4 },
			{ (u32)-1, 32 }, { (u64)-1, 64 }
		};
		for (int i = 0; i < ARRLEN(t); ++i) {
			ASSERT(popcnt64(t[i].in) == t[i].exp);
		}
	}
	{ // rotr32
		struct { u32 in0; int in1; u32 exp; } t[] = {
			{ 0, 31, 0 }, { 0, 0, 0 }, { 0, 1, 0 },
			{ 1, 1, 0x80000000 }, { 0x80000000, 31, 1 },
			{ 1, 31, 2 }, { 2, 2, 0x80000000 },
			{ 0x40000002, 4, 0x24000000 },
		};
		for (int i = 0; i < ARRLEN(t); ++i) {
			ASSERT(rotr32(t[i].in0, t[i].in1) == t[i].exp);
		}
	}
	{ // rotl32
		struct { u32 in0; int in1; u32 exp; } t[] = {
			{ 0, 31, 0 }, { 0, 0, 0 }, { 0, 1, 0 },
			{ 1, 1, 2 }, { 0x80000000, 31, 0x80000000>>1 },
			{ 1, 31, 0x80000000 }, { 2, 2, 2<<2 },
			{ 0x40000002, 4, 0x24 },
		};
		for (int i = 0; i < ARRLEN(t); ++i) {
			ASSERT(rotl32(t[i].in0, t[i].in1) == t[i].exp);
		}
	}
	(void)rotr64;
	(void)rotl64;
	(void)pext32;

	{
		char buf[] = { 0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77 };
		ASSERT(load16le(buf) == 0x1100);
		ASSERT(load32le(buf) == 0x33221100);
		ASSERT(load64le(buf) == 0x7766554433221100);
		ASSERT(load16be(buf) == 0x0011);
		ASSERT(load32be(buf) == 0x00112233);
		ASSERT(load64be(buf) == 0x0011223344556677);
	}
}
#endif
