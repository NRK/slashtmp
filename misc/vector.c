// demonstration of type-safe generic vector
// using a `void *` implementation underneath.
// Pros:
//   - type safety
//   - less binary bloat since the main logic is implemented once
//     compared to "pure macro" or "template instantiation" approach.
//   - when speed is important, user can manually use
//     `__attribute((flatten, always_inline))` wrapper which will be
//     equal to speed compared to pure macro or template instantiation.
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h> // abort()

static void *
vec__grow(void *v, ptrdiff_t *cap, ptrdiff_t elem_size)
{
	void *ret = NULL;
	ptrdiff_t newcap;
	// multiplication overflow check
	// notice the division by `elem_size` (rather than `newcap`), which is
	// usually a constant and thus the compiler can optimize it out.
	if ((PTRDIFF_MAX/2 / elem_size) > *cap) {
		newcap = *cap ? (*cap * 2) : 8;
		ret = realloc(v, newcap * elem_size);
	}
	if (ret == NULL) { // either realloc failure or mul-overflow
		abort(); // .. or some other approprite error handler (e.g longjmp)
	}
	*cap = newcap;
	return ret;
}

// interface
////////////

// for declaring a dynamic array.
// typedef it if you want to pass it around: `typedef vec(int) vec_int;`
#define vec(T) struct { \
	T *data; \
	ptrdiff_t len, cap; \
}

// returns a pointer where the new element should be assigned.
// NOTE: evaluates the argument more than once.
#define vec_push(V) ( \
	( (V)->data = ((V)->len >= (V)->cap) ? \
		vec__grow((V)->data, &(V)->cap, (ptrdiff_t)sizeof((V)->data[0])) : \
		(V)->data), \
	(V)->data + (V)->len++ \
)

// for completeness. unnecessary when using an arena:
// https://www.rfleury.com/p/untangling-lifetimes-the-arena-allocator
#define vec_destroy(V) free((V)->data)


// DEMO
///////

#include <stdio.h>
extern int
main(void)
{
	vec(int) v = {0};

	for (int i = 0, n = 1; i < 16; ++i, n <<= 1) {
		*vec_push(&v) = n;
	}

	// type-safety demonstration
	int *p = &(int){69};
	*vec_push(&v) = p; // causes a warning due to type mismatch.
	*vec_push(&v) = *p; // fine

	for (ptrdiff_t i = 0; i < v.len; ++i) {
		printf("v[%jd] => %d\n", i, v.data[i]);
	}

	vec_destroy(&v);
}
