/*
 * golden ratio for N bits (rounded to the nearest odd number).
 * `((2 power N) / 1.61803)`
 */
#define GOLDEN_RATIO_16  UINT16_C(0x9E37)
#define GOLDEN_RATIO_32  UINT32_C(0x9E379349)
#define GOLDEN_RATIO_64  UINT64_C(0x9E3793492EEDC3F7)

/* https://en.wikipedia.org/wiki/Fowler%E2%80%93Noll%E2%80%93Vo_hash_function#FNV_prime */
#define FNV1a_PRIME32 UINT32_C(0x01000193)
#define FNV1a_PRIME64 UINT64_C(0x00000100000001B3)
