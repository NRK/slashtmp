// finds duplicate executables in $PATH
//
// the code is highly over-engineered since I wanted to try out concurrent
// hash-trie. the output is also not sorted, pipe the output to sort(1) to get
// more deterministic results.
//
// build:    (NOTE: windows port is incomplete)
//   linux:  gcc -o pathdup pathdup.c -O2
//   windows 8+ (w64devkit):  gcc -o pathdup pathdup.c -nostartfiles -lntdll -O2
//
// ref: https://nrk.neocities.org/articles/hash-trees-and-tries
// ref: https://nullprogram.com/blog/2023/09/30/#as-a-concurrent-hash-map
#include <limits.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

typedef uint8_t     u8;
typedef uint16_t    u16;
typedef uint32_t    u32;
typedef uint64_t    u64;
typedef int32_t     i32;
typedef ptrdiff_t   Size;
typedef struct { u8 *s; Size len; } Str;
typedef struct { u8 *buf, *off, *end; int fd; int err; } Out;
typedef struct { u8 *beg, *end; } Arena;
enum { AZERO = 0x1 };

#define NOP()           ((void)0)
#define SIZEOF(...)     ((Size)sizeof(__VA_ARGS__))
#define MIN(A, B)       ((A) < (B) ? (A) : (B))
#define MAX(A, B)       ((A) > (B) ? (A) : (B))
#define ARRLEN(...)     (SIZEOF(__VA_ARGS__) / SIZEOF(0[__VA_ARGS__]))
#define S(X)            ((Str){ .s = (u8 *)("" X ""), .len = ARRLEN(X) - 1})
#ifdef __GNUC__
	// when debugging, use gcc/clang and compile with
	// `-fsanitize=undefined -fsanitize-undefined-trap-on-error`
	// it'll trap if an unreachable code-path is ever reached.
	#define ASSERT(X)  ((X) ? (void)0 : __builtin_unreachable())
#else
	#define ASSERT(X)  ((void)0)
#endif

// TODO: nail down the memory ordering
#define ATOMIC_TODO __ATOMIC_SEQ_CST

#define CLR_RESET    "\033[0m"
#define CLR_RED      "\033[31m"
#define CLR_GREEN    "\033[32m"

//////// os

typedef struct {
	Str dirname;
	void *buffer;
	Size bufsize;
	////////////////
	int  init;
	///////////////
	int dirfd;
	void *next;
	void *end;
	///////////////
	void *dirhandle;
} NextExec;
enum { NEXT_EXEC_INCLUDE_SYMLINKS = 0x1 };
static Str os_next_executable(NextExec *ctx, Arena scratch, int flags);
enum { RC_OOM = -2 };
static void os_exit(int) __attribute((noreturn));
static void out_flush(Out *);
static void futex_wait(void *p, int expected);
static void futex_wake(void *p);

///////

#ifdef __clang__
	#pragma clang diagnostic ignored "-Wgnu-alignof-expression"
#endif

__attribute((alloc_size(2, 3), malloc))
static void *
arena_alloc(Arena *a, Size cnt, Size size, Size align, int flags)
{
	Size avail = a->end - a->beg;
	Size pad = -(uintptr_t)a->beg & (align - 1);
	if (!((avail-pad) >= 0 && cnt <= (avail-pad)/size)) {
		os_exit(RC_OOM);
	}
	u8 *ret = a->beg + pad;
	a->beg += (size*cnt) + pad;
	if (flags & AZERO) for (Size i = 0; i < (size*cnt); ++i) {
		ret[i] = 0x0;
	}
	return ret;
}
#define arena_item(A, PTR) ((PTR) = arena_alloc( \
	(A), 1, SIZEOF(0[PTR]), (Size)_Alignof(0[PTR]), AZERO \
))
#define arena_array(A, PTR, CNT) ((PTR) = arena_alloc( \
	(A), (CNT), SIZEOF(0[PTR]), (Size)_Alignof(0[PTR]), 0x0 \
))
#define arena_arrayz(A, PTR, CNT) ((PTR) = arena_alloc( \
	(A), (CNT), SIZEOF(0[PTR]), (Size)_Alignof(0[PTR]), AZERO \
))

static Arena *
arena_subarena(Arena *a, Size sz)
{
	ASSERT(sz >= 0);
	Size remain = a->end - a->beg;
	if (sz > remain) {
		os_exit(RC_OOM);
	}
	Arena tmp = { a->end - sz, a->end };
	a->end -= sz;
	Arena *ret = arena_item(&tmp, ret);
	*ret = tmp;
	return ret;
}

static Str
str_from_cstr(const char *s)
{
	Str ret = { (u8 *)s };
	if (s) for (; ret.s[ret.len] != '\0'; ++ret.len) {}
	return ret;
}

static bool
str_eq(Str a, Str b)
{
	if (a.len != b.len) {
		return false;
	}
	for (Size i = 0; i < a.len; ++i) {
		if (a.s[i] != b.s[i]) {
			return false;
		}
	}
	return true;
}

static Str
str_shave_head(Str s, Size n)
{
	ASSERT(s.len >= n);
	s.s   += n;
	s.len -= n;
	return s;
}

static Str
str_grab_head(Str s, Size n)
{
	ASSERT(s.len >= n);
	s.len = n;
	return s;
}

static Str
str_grab_tail(Str s, Size n)
{
	ASSERT(s.len >= n);
	s.s = (s.s + s.len) - n;
	s.len = n;
	return s;
}

static bool
str_match_start(Str s, Str match)
{
	if (s.len < match.len) {
		return false;
	}
	return str_eq(match, str_grab_head(s, match.len));
}

static bool
str_match_end(Str s, Str match)
{
	if (s.len < match.len) {
		return false;
	}
	return str_eq(match, str_grab_tail(s, match.len));
}

static Str
str_tok(Str *s, u8 ch)
{
	while (s->len > 0 && s->s[0] == ch) {
		++s->s;
		--s->len;
	}
	Str ret = { s->s };
	while (s->len > 0 && s->s[0] != ch) {
		++s->s;
		--s->len;
		++ret.len;
	}
	return ret;
}

static u64
str_hash(Str s)
{
	u64 h = 55555;
	for (Size i = 0; i < s.len; ++i) {
		h ^= s.s[i];
		h *= 1111111111111111111;
	}
	h ^= s.len;
	h *= 1111111111111111111;
	h ^= h >> 33;
	return h;
}

static Str
str_copy(Str dst, Str src)
{
	ASSERT(dst.len >= src.len);
	ASSERT(dst.len > 0 ? dst.s != NULL : 1);
	for (Size i = 0; i < src.len; ++i) {
		dst.s[i] = src.s[i];
	}
	return (Str){ dst.s ? dst.s + src.len : 0, dst.len - src.len };
}

static Str
str_alloc(Arena *a, Size len)
{
	Str ret = { NULL, len };
	arena_array(a, ret.s, ret.len);
	return ret;
}

static Str
str_dup(Arena *a, Str s)
{
	Str ret = str_alloc(a, s.len);
	for (Size i = 0; i < s.len; ++i) {
		ret.s[i] = s.s[i];
	}
	return ret;
}

static Out
out_create(int fd, u8 *buf, u8 *end)
{
	ASSERT(buf < end);
	Out ret = {0};
	ret.buf = ret.off = buf;
	ret.end = end;
	ret.fd  = fd;
	return ret;
}

static void
out_str(Out *out, Str s)
{
	for (Size i = 0; !out->err && i < s.len; ) {
		if (out->off == out->end) {
			out_flush(out);
		} else while (out->off < out->end && i < s.len) {
			*out->off++ = s.s[i++];
		}
	}
}

///////////////

enum { MAP_EXP = 2, MAP_SHIFT = 64 - MAP_EXP };
typedef struct IndexList {
	Size index;
	struct IndexList *next;
} IndexList;
typedef struct Map {
	Str exename;
	IndexList *index_list;
	struct Map *dupnext;
	struct Map *child[1<<MAP_EXP];
} Map;

// TODO: review this more thoroughly
static void
map_upsert(Map **m, Str s, Size path_index, Map **duphead, Arena *a)
{
	Map *found = NULL;
	for (u64 h = str_hash(s); 1; NOP()) {
		Arena scratch = *a;
		Map *np = __atomic_load_n(m, ATOMIC_TODO);
		if (np == NULL) {
			Map *new = arena_item(&scratch, new);
			new->exename = str_dup(&scratch, s);
			int res = __atomic_compare_exchange_n(
				m, &np, new, 0, ATOMIC_TODO, ATOMIC_TODO
			);
			if (res) {
				found = new;
				*a = scratch;
				break;
			} else {
				continue; // otherwise retry
			}
		} else if (str_eq(np->exename, s)) {
			found = np;
			break;
		} else {
			m = np->child + (h >> MAP_SHIFT);
			h <<= MAP_EXP;
		}
	}
	ASSERT(found != NULL);

	IndexList *new = arena_item(a, new);
	new->index = path_index;
	for (int success = 0; !success; NOP()) {
		success = __atomic_compare_exchange_n(
			&found->index_list, &new->next, new, // expecting NULL the first time around
			0, ATOMIC_TODO, ATOMIC_TODO
		);
	}
	if (new->next != NULL && new->next->next == NULL) {
		// if we're the 2nd in the list, it's our job to add the node
		// to the duplicate list
		found->dupnext = __atomic_load_n(duphead, ATOMIC_TODO);
		for (int success = 0; !success; NOP()) {
			success = __atomic_compare_exchange_n(
				duphead, &found->dupnext, found,
				0, ATOMIC_TODO, ATOMIC_TODO
			);
		}
	}
}

typedef struct {
	Str      dirname;
	Size     path_index;
	Map    **map;
	Map    **duphead;
	Arena   *a;
	int      next_free_slave;
	int      self_id;
	int      active;
	int     *free_slavelist;
} WorkQueue;
static const Str WORKER_DIE = { (u8 [1]){ 'F' }, 1 };

typedef struct {
	Arena a;
	Str path;
	u8 path_sep;
	Out out[1];
	/* Out eout[1]; */
	WorkQueue *queue;
	int queue_len;
	int *free_slavelist;
} AppCtx;

static WorkQueue *
worker_acquire(WorkQueue *queue, Size len, int *free_slavelist)
{
	WorkQueue *q;
	int next, slot;
	// slot == -1 means all slaves busy. wait until one is free
	while ((slot = __atomic_load_n(free_slavelist, ATOMIC_TODO)) == -1) {
		futex_wait(free_slavelist, -1);
	}
	// at this point, the queue is confirmed to be not empty since there's
	// only a single master thread that peels slaves off the queue
	do {
		ASSERT(slot >= 0 && slot < len);
		q = queue + slot;
		next = q->next_free_slave;
	} while (!__atomic_compare_exchange_n(free_slavelist, &slot, next, 0, ATOMIC_TODO, ATOMIC_TODO));
	ASSERT(q == queue + slot);
	ASSERT(q->active == 0);
	return q;
}

static int
run(AppCtx *ctx)
{
	Arena *a = &ctx->a;
	Size thread_arena_size = (a->end - a->beg) / (ctx->queue_len + 2);
	for (Size i = 0; i < ctx->queue_len; ++i) {
		ctx->queue[i].a = arena_subarena(a, thread_arena_size);
	}

	Map *map = NULL;
	Map *dups = NULL;
	Size pathcount = 0;
	for (Str s = ctx->path, dirname; (dirname = str_tok(&s, ctx->path_sep)).len > 0; NOP()) {
		WorkQueue *q = worker_acquire(ctx->queue, ctx->queue_len, ctx->free_slavelist);
		q->dirname = dirname;
		q->path_index = pathcount;
		q->map = &map;
		q->duphead = &dups;
		__atomic_store_n(&q->active, 1, ATOMIC_TODO);
		futex_wake(&q->active);
		++pathcount;
	}

	Str *paths = arena_array(a, paths, pathcount);
	Size *index = arena_array(a, index, pathcount);
	{
		Size i = 0;
		for (Str s = ctx->path, dirname; (dirname = str_tok(&s, ctx->path_sep)).len > 0; NOP()) {
			paths[i++] = dirname;
		}
	}

	for (Size i = 0; i < ctx->queue_len; ++i) {
		WorkQueue *q = worker_acquire(ctx->queue, ctx->queue_len, ctx->free_slavelist);
		q->dirname = WORKER_DIE;
		__atomic_store_n(&q->active, 1, ATOMIC_TODO);
		futex_wake(&q->active);
	}

	for (Map *it = dups; it != NULL; it = it->dupnext) {
		out_str(ctx->out, S(CLR_RED));
		out_str(ctx->out, it->exename);
		out_str(ctx->out, S(CLR_RESET));
		out_str(ctx->out, S(" => "));
		Size cnt = 0;
		for (IndexList *iit = it->index_list; iit != NULL; iit = iit->next) {
			ASSERT(cnt < pathcount);
			index[cnt] = iit->index;
			for (Size k = cnt; k > 0 && index[k-1] > index[k]; --k) {
				Size tmp = index[k-1];
				index[k-1] = index[k];
				index[k] = tmp;
			}
			++cnt;
		}

		for (Size i = 0; i < cnt; ++i) {
			out_str(ctx->out, S(CLR_GREEN));
			out_str(ctx->out, paths[index[i]]);
			out_str(ctx->out, S(CLR_RESET));
			if (i+1 < cnt) {
				out_str(ctx->out, S(" -> "));
			}
		}
		out_str(ctx->out, S("\n"));
	}
	out_flush(ctx->out);
	return ctx->out->err;
}

static void *
slave(void *pIn)
{
	WorkQueue *q = pIn;
	for (;;) {
		for (int n; (n = __atomic_load_n(&q->active, ATOMIC_TODO)) != 1; NOP()) {
			futex_wait(&q->active, n);
		}

		if (q->dirname.s == WORKER_DIE.s) {
			break;
		}

		{
			NextExec state[1] = {0};
			state->dirname = q->dirname;
			union { u64 align; u8 v[1<<11]; } buf;
			state->bufsize = SIZEOF(buf.v);
			state->buffer  = buf.v;

			int flags = NEXT_EXEC_INCLUDE_SYMLINKS;
			for (Str exename;
			     (exename = os_next_executable(state, *q->a, flags)).len > 0;
			     NOP())
			{
				map_upsert(q->map, exename, q->path_index, q->duphead, q->a);
			}
		}

		//__atomic_store_n(&q->active, 0, ATOMIC_TODO);
		q->active = 0;
		int next = __atomic_load_n(q->free_slavelist, ATOMIC_TODO);
		do {
			q->next_free_slave = next;
		} while (!__atomic_compare_exchange_n(q->free_slavelist, &next, q->self_id, 0, ATOMIC_TODO, ATOMIC_TODO));
		futex_wake(q->free_slavelist);
	}
	return NULL;
}


////////////////////

#ifdef __unix__
#include <stdlib.h>
#include <string.h>

#include <dirent.h>
#include <unistd.h>
#include <fcntl.h>

#include <pthread.h>
#include <linux/futex.h>      /* Definition of FUTEX_* constants */
#include <sys/syscall.h>      /* Definition of SYS_* constants */
#include <sys/mman.h>

static Size
sys_getdents64(int dirfd, void *buf, Size len)
{
	return syscall(SYS_getdents64, dirfd, buf, MIN(len, INT_MAX));
}

static Str
os_next_executable(NextExec *ctx, Arena scratch, int flags)
{
	if (!ctx->init) {
		Str nul = S("\0");
		Str dirz = str_alloc(&scratch, ctx->dirname.len + nul.len);
		str_copy(str_copy(dirz, ctx->dirname), nul);
		int fd = open((char *)dirz.s, O_RDONLY | O_DIRECTORY);
		if (fd < 0) {
			return (Str){0};
		}
		ctx->dirfd = fd;
		ctx->init = true;
	}

	typedef struct {
		u64        d_ino;    /* 64-bit inode number */
		u64        d_off;    /* 64-bit offset to next structure */
		unsigned short d_reclen; /* Size of this dirent */
		unsigned char  d_type;   /* File type */
		char           d_name[]; /* Filename (null-terminated) */
	} Ent64;

	ASSERT(((uintptr_t)ctx->buffer & (_Alignof(Ent64) - 1)) == 0);
	for (;;) {
		if (ctx->next == NULL || ctx->next == ctx->end) {
			Size res = sys_getdents64(ctx->dirfd, ctx->buffer, ctx->bufsize);
			if (res <= 0) {
				close(ctx->dirfd);
				return S("");
			}
			ctx->next = ctx->buffer;
			ctx->end = (u8 *)ctx->next + res;
		}

		Ent64 *e = ctx->next;
		Str exename = str_from_cstr(e->d_name);
		ctx->next = (u8 *)e + e->d_reclen;
		if (!(e->d_type == DT_REG ||
		      ((flags & NEXT_EXEC_INCLUDE_SYMLINKS) && e->d_type == DT_LNK)))
		{
			continue;
		}
		if (str_eq(exename, S(".")) || str_eq(exename, S(".."))) {
			continue;
		}
		if (faccessat(ctx->dirfd, (char *)exename.s, X_OK, 0x0) == 0) {
			return exename;
		}
	}
	ASSERT(!"unreachable");
}

static void
out_flush(Out *out)
{
	Size n = out->off - out->buf;
	out->err = out->err || write(out->fd, out->buf, n) != n;
	out->off = out->buf;
}

__attribute((noreturn)) static void
os_exit(int rc)
{
	if (rc < 0) {
		if (rc == RC_OOM) {
			Str oom = S("out of memory\n");
			Size n = write(2, oom.s, oom.len); (void)n;
		}
		abort();
	} else {
		exit(rc);
	}
}

static void
futex_wait(void *p, int expected)
{
	syscall(SYS_futex, p, FUTEX_WAIT, expected, NULL);
}

static void
futex_wake(void *p)
{
	syscall(SYS_futex, p, FUTEX_WAKE, 1);
}

extern int
main(int argc, char *argv[], char *envp[])
{
	enum { NMEM = 1 << 24, NOUT = 1 << 12, NSLAVES = 4 };
	AppCtx ctx[1] = {0};
	ctx->path = S("/usr/local/bin:/bin:/usr/bin");
	ctx->path_sep = ':';
	for (int i = 0; envp[i] != NULL; ++i) {
		Str e = str_from_cstr(envp[i]);
		Str prefix = S("PATH=");
		if (str_match_start(e, prefix)) {
			Str val = str_shave_head(e, prefix.len);
			if (val.len > 0) {
				ctx->path = val;
			}
			break;
		}
	}

	ctx->a.beg = mmap(0, NMEM, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANON, -1, 0);
	if (ctx->a.beg == MAP_FAILED) {
		ctx->a.end = MAP_FAILED;
	} else {
		ctx->a.end = ctx->a.beg + NMEM;
	}

	u8 *outbuf = arena_array(&ctx->a, outbuf, NOUT);
	ctx->out[0] = out_create(1, outbuf, outbuf + NOUT);

#ifdef _SC_NPROCESSORS_ONLN
	ctx->queue_len = sysconf(_SC_NPROCESSORS_ONLN) - 1;
#endif
	ctx->queue_len = MAX(ctx->queue_len, NSLAVES);
	arena_arrayz(&ctx->a, ctx->queue, ctx->queue_len);
	arena_item(&ctx->a, ctx->free_slavelist);
	for (Size i = 0; i < ctx->queue_len; ++i) {
		ctx->queue[i].self_id = i;
		ctx->queue[i].next_free_slave = (i + 1 == ctx->queue_len) ? -1 : (i + 1);
		ctx->queue[i].free_slavelist = ctx->free_slavelist;
		pthread_t p;
		int res = pthread_create(&p, NULL, slave, ctx->queue + i);
		if (res != 0) {
			os_exit(-1);
		}
	}

	int rc = run(ctx);
	return rc;
}

#elif defined(_WIN32)

////
#define MEM_COMMIT        0x1000
#define MEM_RESERVE       0x2000
#define PAGE_READWRITE    0x04
#define W32_PATH_MAX      260

typedef struct {
	u32  attr;
	u32  create[2], access[2], write[2];
	u32  size[2];
	u32  reserved[2];
	u8   name[W32_PATH_MAX];
	u8   altname[14];
	u32  reserved_again[2];
	u16  reserved_yet_again;
} FindData;

////
#define W32(r) __declspec(dllimport) r __stdcall
W32(void)    DebugBreak(void);
W32(void)    ExitProcess(int) __attribute((noreturn));
W32(void)    ExitThread(int) __attribute((noreturn));
W32(void *)  VirtualAlloc(void *, Size, int, int);
W32(int)     GetEnvironmentVariableA(u8 *, u8 *, Size);
typedef i32 ThreadFunc(void *) __attribute((stdcall));
W32(void *)  CreateThread(void *, Size, ThreadFunc *, void *, i32 *);
W32(void *)  GetStdHandle(i32);
W32(i32)     WriteFile(void *, void *, i32, i32 *, void *);
W32(bool)    RtlWaitOnAddress(void *, void *, Size, i32);
W32(void)    RtlWakeAddressSingle(void *);
W32(void *)  FindFirstFileA(u8 *, FindData *out);
W32(bool)    FindNextFileA(void *, FindData *out);

static void
futex_wait(void *p, int expected)
{
	RtlWaitOnAddress(p, &expected, SIZEOF(expected), 0);
}

static void
futex_wake(void *p)
{
	RtlWakeAddressSingle(p);
}

__attribute((noreturn)) static void
os_exit(int rc)
{
	if (rc < 0) {
		DebugBreak();
	}
	ExitProcess(rc);
}

static void
out_flush(Out *out)
{
	ASSERT(out->fd == 1 || out->fd == 2); // TODO: only supports std{out,err}
	Size n = out->off - out->buf;
	if (n == 0 || out->err) {
		return;
	}
	i32 written;
	void *h = GetStdHandle(-10 - out->fd);
	out->err = WriteFile(h, out->buf, n, &written, NULL) == 0 || written != n;
	out->off = out->buf;
}

static Str
os_next_executable(NextExec *ctx, Arena scratch, int flags)
{
	for (;;) {
		Arena retarena = { ctx->buffer, (u8 *)ctx->buffer + ctx->bufsize };
		FindData *res = arena_item(&retarena, res);
		if (!ctx->init) {
			ctx->init = true;

			Str append = S("/*\0");
			Str dirlist = str_alloc(&scratch, ctx->dirname.len + append.len);
			str_copy(str_copy(dirlist, ctx->dirname), append);

			void *h = FindFirstFileA(dirlist.s, res);
			if (h == NULL) {
				return (Str){0};
			}
			ctx->dirhandle = h;
		} else {
			if (!FindNextFileA(ctx->dirhandle, res)) {
				return (Str){0};
			}
		}

		Str ret = str_from_cstr(res->name);
		if (str_eq(ret, S(".")) || str_eq(ret, S(".."))) {
			continue;
		}
		// FIXME: figure out the proper way to check for executable
		// instead of checking .exe extension
		if (!str_match_end(ret, S(".exe"))) {
			continue;
		}
		// TODO: handle NEXT_EXEC_INCLUDE_SYMLINKS
		return ret;
	}
}

static Str
w32getenvvar(Arena *a, Str var)
{
	Arena scratch = *a;
	ASSERT(var.s[var.len-1] == '\0');
	u8 *buf = arena_array(&scratch, buf, 1<<16);
	int res = GetEnvironmentVariableA(var.s, buf, 1<<16);
	if (res == 0) {
		return (Str){0};
	}
	Str ret = str_from_cstr(buf);
	arena_array(a, ret.s, ret.len);
	return ret;
}

__attribute((noreturn, stdcall, force_align_arg_pointer))
static i32
thread_entry(void *arg)
{
	slave(arg);
	ExitThread(0);
}

__attribute((force_align_arg_pointer))
extern int
mainCRTStartup(void)
{
	enum { NMEM = 1<<24, NOUT = 1<<12, NSLAVES = 4 };
	AppCtx ctx[1] = {0};

	ctx->a.beg = VirtualAlloc(0, NMEM, MEM_RESERVE|MEM_COMMIT, PAGE_READWRITE);
	if (ctx->a.beg == NULL) {
		//os_write(2, S("aborting: VirtualAlloc() failed\n"));
		os_exit(-1);
	}
	ctx->a.end = ctx->a.beg + NMEM;

	ctx->path = w32getenvvar(&ctx->a, S("PATH\0"));
	ctx->path_sep = ';';

	u8 *outbuf = arena_array(&ctx->a, outbuf, NOUT);
	ctx->out[0] = out_create(1, outbuf, outbuf + NOUT);

	ctx->queue_len = NSLAVES;
	arena_arrayz(&ctx->a, ctx->queue, ctx->queue_len);
	arena_item(&ctx->a, ctx->free_slave);
	for (Size i = 0; i < ctx->queue_len; ++i) {
		ctx->queue[i].self_id = i;
		ctx->queue[i].next_free_slave = (i + 1 == ctx->queue_len) ? -1 : (i + 1);
		ctx->queue[i].free_slaves = ctx->free_slave;
		void *h = CreateThread(0, 0, thread_entry, ctx->queue + i, 0);
		if (h == NULL) {
			os_exit(-1);
		}
	}

	int rc = run(ctx);
	os_exit(rc);
}

#else
	#error "unsupported platform"
#endif

#if !defined(__SANITIZE_THREAD__) && defined(__has_feature)
	#define __SANITIZE_THREAD__ __has_feature(thread_sanitizer)
#endif
#if __SANITIZE_THREAD__
extern const char * __tsan_default_options(void) { return "report_thread_leaks=0"; }
#endif
