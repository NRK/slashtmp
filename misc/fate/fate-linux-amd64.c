// native linux-amd64 platform layer that interacts with the kernel directly.
// this is mostly educational, see fate-posix.c instead.
//
// $ gcc -o fate fate-linux-amd64.c -O3 -march=native -ffreestanding -nostdlib -s
//       -fipa-pta -fno-asynchronous-unwind-tables -fno-ident
//
// to optimize for size:
// $ gcc -o fate fate-linux-amd64.c -O2 -march=native -ffreestanding -nostdlib -s
//       -fipa-pta -fno-asynchronous-unwind-tables -fno-ident
//       -fno-align-jumps -fno-align-loops -fno-align-labels -fno-align-functions
//       -fira-hoist-pressure -fno-prefetch-loop-arrays -fno-tree-ch
//       -freorder-blocks-algorithm=simple

#include "fate.c"

#if 0 && __has_include("linux/elf.h")
	#include <linux/elf.h>
	#include <linux/auxvec.h>
	#include <linux/errno.h>
#else
	#include <elf.h>
	#include <errno.h>
#endif

#define SYS_READ            0
#define SYS_WRITE           1
#define SYS_CLOSE           3
#define SYS_RT_SIGACT      13
#define SYS_DUP2           33
#define SYS_NANOSLEEP      35
#define SYS_VFORK          58
#define SYS_EXECVE         59
#define SYS_EXIT           60
#define SYS_WAIT4          61
#define SYS_TIME          201
#define SYS_OPENAT        257
#define SYS_MKDIRAT       258
#define SYS_FSTATAT       262
#define SYS_UTIMENSAT     280
#define SYS_PIPE2         293

#define O_RDONLY          0x0
#define O_WRONLY          0x1
#define O_RDWR            0x2
#define O_CREAT          0x40
#define O_EXCL           0x80
#define O_NOCTTY        0x100
#define O_TRUNC         0x200
#define O_APPEND        0x400
#define O_NONBLOCK      0x800
#define O_DIRECTORY   0x10000
#define O_CLOEXEC     0x80000
#define O_NOFOLLOW    0x20000

#define WNOHANG 1

#define AT_FDCWD               -100
#define AT_SYMLINK_NOFOLLOW   0x100

#define STATBUF_SIZE  144
#define MTIME_OFF      88

#define SIG_DFL        ((void (*)(int)) 0)
#define SIG_IGN        ((void (*)(int)) 1)
#define SA_NOCLDSTOP            1
#define SA_NOCLDWAIT            2
#define SA_RESTART     0x10000000
#define SIGCHLD                17

#define syscall0(num)       syscall1(num, 0)
#define syscall1(num, arg0) ({ \
	long sys__ret; \
	register long sys__n __asm("rax") = (long)(num), sys__0 __asm("rdi") = (long)(arg0); \
	__asm volatile ( \
		"syscall\n" \
		: "=a"(sys__ret) \
		: "a"(sys__n), "r"(sys__0) \
		: "memory", "%rcx", "%r11" \
	); \
	sys__ret; \
})

#define syscall2(num, arg0, arg1) ({ \
	long sys__ret; \
	register long sys__n __asm("rax") = (long)(num), \
	sys__0 __asm("rdi") = (long)(arg0), sys__1 __asm("rsi") = (long)(arg1); \
	__asm volatile ( \
		"syscall\n" \
		: "=a"(sys__ret) \
		: "a"(sys__n), "r"(sys__0), "r"(sys__1) \
		: "memory", "%rcx", "%r11" \
	); \
	sys__ret; \
})

#define syscall3(num, arg0, arg1, arg2)   syscall4((num), (arg0), (arg1), (arg2), 0)
#define syscall4(num, arg0, arg1, arg2, arg3) ({ \
	long sys__ret; \
	register long sys__n __asm("rax") = (long)(num), \
	sys__0 __asm("rdi") = (long)(arg0), sys__1 __asm("rsi") = (long)(arg1), \
	sys__2 __asm("rdx") = (long)(arg2), sys__3 __asm("r10") = (long)(arg3); \
	__asm volatile ( \
		"syscall\n" \
		: "=a"(sys__ret) \
		: "a"(sys__n), "r"(sys__0), "r"(sys__1), "r"(sys__2), "r"(sys__3) \
		: "memory", "%rcx", "%r11" \
	); \
	sys__ret; \
})

// types
typedef struct { long sec, ns; } TimeSpec;
typedef i64 TimeFunc(i64 *);

// globals
static TimeFunc *vdso_time = NULL;

// functions

__attribute((cold))
static Str
str_errno(int e)
{
	#define X(A, B) case A: return S(B); break
	switch (e) {
	X(0,        "no error");
	X(ENOENT,   "file does not exist");
	X(EEXIST,   "file already exists");
	X(ENOMEM,   "out of memory");
	X(EPERM,    "permission denied");
	X(EACCES,   "access denied");
	X(EISDIR,   "is a directory");
	X(ENOTDIR,  "not a directory");
	X(ENOEXEC,  "exec format error");
	X(ETXTBSY,  "text busy");
	X(EINTR,    "interrupted");
	X(ENOSPC,   "out of disk space");
	X(EFBIG,    "file too big");
	X(EDQUOT,   "disk quota exceeded");
	X(EMFILE,   "process cannot open more files");
	X(ENAMETOOLONG, "filename too long");
#if DEBUG
	X(EINVAL,   "invalid argument");
	X(EBADF,    "bad fd");
	X(EFAULT,   "bad address");
#endif
	default: return S("unknown error"); break;
	}
	#undef X
}

__attribute((noreturn))
static void
sys_exit(int ec)
{
	syscall1(SYS_EXIT, ec);
	__builtin_unreachable();
}

static void
errno_check(long e __attribute((unused)))
{
#if DEBUG
	ASSERT(e != -EINVAL && e != -EBADF && e != -EFAULT);
#endif
}

static long
sys_openat(long dirfd, u8 *path, long flags, long mode)
{
	long ret = syscall4(SYS_OPENAT, dirfd, path, flags, mode);
	errno_check(ret);
	return ret;
}

static long
sys_close(long fd)
{
	ASSERT(fd >= 0);
	long ret = syscall1(SYS_CLOSE, fd);
	errno_check(ret);
	return ret;
}

// TODO: read up on `man path_resolution`
static long
exec_path(AppCtx *ctx, const Job *p)
{
	Str cmd = { .s = (u8 *)p->cmd[0] };
	for (NOP(); cmd.s[cmd.len] != '\0'; ++cmd.len) {
		if (cmd.s[cmd.len] == '/') {
			return syscall3(SYS_EXECVE, cmd.s, p->cmd, ctx->envp);
		}
	}
	long res = -1, access_err = 0;
	for (Size i = 0; i < ctx->path_len; ++i) {
		u8 buf[1<<12];
		Str dir = ctx->path[i];
		if (cmd.len + dir.len + 2 < SIZEOF(buf)) {
			Stream st = stream_create(buf, 1[&buf], -1);
			stream_appendv(&st, dir, S("/"), cmd, S("\0"));
			res = syscall3(SYS_EXECVE, buf, p->cmd, ctx->envp);
			switch (res) {
			case -EACCES: access_err = -EACCES; // fallthrough
			case -ENOENT: case -ENOTDIR: break;
			default: return res;
			}
		}
	}
	return access_err != 0 ? access_err : res;
}

// force inlining. according to man 2 vfork:
// >> The child must not return from the current function
__attribute((returns_twice, always_inline))
static inline long
sys_vfork(void)
{
	return syscall0(SYS_VFORK);
}

// noinline to avoid clang mis-compile on -O2 and above:
// https://github.com/llvm/llvm-project/issues/54447
__attribute((noinline))
static int
platform_execute(AppCtx *ctx, const Job *p, i64 t)
{
	volatile Str err = {0}, err_prefix = {0};

	// TODO?: use clone() instead?
	long res = sys_vfork();
	if (res < 0) {
		err_prefix = S("vfork(): ");
		err = str_errno(-res);
	} else if (res == 0) {
		// NOTE: vfork shares memory with parent, so errors can be
		// communicated via the local `err` var.
		// no need for pipe shenanigans.
		if (p->stdout != NULL) {
			int flags = O_WRONLY | O_CREAT | O_TRUNC | O_CLOEXEC;
			int outfd = sys_openat(AT_FDCWD, (u8 *)p->stdout, flags, 0644);
			int e = outfd < 0 ? outfd : syscall2(SYS_DUP2, outfd, 1);
			if (e < 0) {
				err_prefix = S("redirecting stdout: ");
				err = str_errno(-e);
			}
		}
		if (err.len == 0) {
			long res1 = exec_path(ctx, p);
			err = str_errno(-res1);
		}
		sys_exit(1);
	}

	int e = err.len > 0;
	stream_log_header(&ctx->stderr, t, e, p->name);
	if (e) {
		stream_appendv(&ctx->stderr, S("exec failure: "), err_prefix, err, S("\n"));
	} else {
		stream_append(&ctx->stderr, S("exec success\n"));
	}
	return e;
}

static void
platform_mtime_set(AppCtx *ctx, Str name, i64 sec)
{
	ASSERT(ctx->persist_dir >= 0);
	ASSERT(name.len > 0 && name.s[name.len - 1] == '\0');

	Str err = {0};
	int flags = O_WRONLY | O_CREAT | O_NOFOLLOW | O_CLOEXEC;
	int fd = sys_openat(ctx->persist_dir, name.s, flags, 0644);
	if (fd >= 0) {
		TimeSpec ts[2] = { [1] = { .sec = sec } };
		long res = syscall4(SYS_UTIMENSAT, fd, 0, ts, 0);
		if (res < 0) {
			errno_check(res);
			err = str_errno(-res);
		}
		sys_close(fd);
	} else {
		err = str_errno(-fd);
	}

	int e = err.len > 0;
	stream_log_header(&ctx->stderr, sec, e, name);
	if (e) {
		stream_appendv(&ctx->stderr, S("failed to set mtime: "), err, S("\n"));
	} else {
		stream_append(&ctx->stderr, S("mtime set successfully\n"));
	}
}

static i64
platform_mtime_get(const AppCtx *ctx, Str name)
{
	ASSERT(ctx->persist_dir >= 0);
	ASSERT(name.len > 0 && name.s[name.len - 1] == '\0');

	u8 buf[STATBUF_SIZE] __attribute((aligned(16)));
	if (syscall4(SYS_FSTATAT, ctx->persist_dir, name.s, buf, 0) == 0) {
		long *p __attribute((may_alias)) = (long *)(buf + MTIME_OFF);
		return *p;
	}
	return 0;
}

static void
platform_sleep(i64 sec)
{
	if (sec > 0) {
		syscall2(SYS_NANOSLEEP, &(TimeSpec){ .sec = sec }, 0);
	}
}

static void
platform_write(int fd, const void *buf, Size n)
{
	ASSERT(n >= 0);
	ASSERT(fd >= 0);
	long res = syscall3(SYS_WRITE, fd, buf, (size_t)n);
	errno_check(res);
}

static i64
platform_time(void)
{
	return vdso_time != NULL ? vdso_time(NULL) : syscall1(SYS_TIME, NULL);
}

#define ptr_off(P, OFF)  ( (void *)((uintptr_t)(P) + (OFF)) )
static void
vdso_setup(AppCtx *ctx)
{
	void *vdso = NULL;
	long *auxv __attribute((may_alias));
	for (auxv = (long *)ctx->envp; auxv[0] != 0; ++auxv) {}
	for (++auxv; auxv[0] != 0; auxv += 2) {
		if (auxv[0] == AT_SYSINFO_EHDR) {
			vdso = (void *)auxv[1];
			break;
		}
	}
	if (vdso == NULL) {
		return;
	}

	Elf64_Ehdr *e = vdso;
	u8 *const bp = vdso;
#if 1 // are these checks needed? shouldn't vdso be trusted to be valid?
	if (bp[0] != 0x7F || bp[1] != 'E' || bp[2] != 'L' || bp[3] != 'F') {
		return;
	}
	if (bp[4] != 2 /* 64-bit */) {
		return;
	}
	if (e->e_shoff == 0 || e->e_shstrndx == SHN_UNDEF) {
		return;
	}
#endif
	Elf64_Shdr *shdr = ptr_off(bp, e->e_shoff);
	u8 *strtbl = ptr_off(bp, shdr[e->e_shstrndx].sh_offset);

	Elf64_Sym *dynsym = NULL, *dynsym_end = NULL;
	u8 *dynstr = NULL;
	for (Size i = 0; i < e->e_shnum; ++i) {
		Elf64_Shdr *h = shdr + i;
		switch (h->sh_type) {
		case SHT_STRTAB: {
			Str section_name = str_from_cstr(ptr_off(strtbl, h->sh_name));
			if (str_eq(section_name, S(".dynstr"))) {
				dynstr = bp + h->sh_offset;
			}
		} break;
		case SHT_DYNSYM:
			dynsym = ptr_off(bp, h->sh_offset);
			dynsym_end = ptr_off(dynsym, h->sh_size);
			break;
		}
	}
	if (dynstr == NULL || dynsym == NULL) {
		return;
	}

	for (Elf64_Sym *sym = dynsym; sym < dynsym_end; ++sym) {
		Str name = str_from_cstr(ptr_off(dynstr, sym->st_name));
		if (str_eq(name, S("__vdso_time"))) {
			vdso_time = (TimeFunc *)(bp + sym->st_value);
			stream_log_header(&ctx->stderr, platform_time(), LOG_INFO, (Str){0});
			stream_append(&ctx->stderr, S("using vdso time()\n"));
			return;
		}
	}
}

static void
platform_persistent_dir_init(AppCtx *ctx)
{
	Str err = {0};
	Str xdg = env_find(ctx->envp, S("XDG_DATA_HOME="));
	if (xdg.len > 0) {
		int flags = O_DIRECTORY | O_CLOEXEC | O_NOFOLLOW;
		int data_fd = sys_openat(AT_FDCWD, xdg.s, flags, 0);
		if (data_fd >= 0) {
			syscall3(SYS_MKDIRAT, data_fd, "fate", 0755);
			int dirfd = sys_openat(data_fd, (u8 *)"fate", flags, 0);
			if (dirfd < 0) {
				err = str_errno(-dirfd);
			} else {
				ctx->persist_dir = dirfd;
			}
			sys_close(data_fd);
		} else {
			err = str_errno(-data_fd);
		}
	} else {
		err = S("$XDG_DATA_HOME not set");
	}
	if (err.len > 0) {
		stream_log_header(&ctx->stderr, platform_time(), LOG_FATAL, (Str){0});
		stream_appendv(&ctx->stderr, S("opening $XDG_DATA_HOME/fate: "), err, S("\n"));
		stream_flush(&ctx->stderr);
		sys_exit(1);
	}
	ASSERT(ctx->persist_dir >= 0);
}

__asm(
	".intel_syntax noprefix\n"
	".global _start\n"
	"_start:\n"
	"	mov	edi, DWORD PTR [rsp]\n"
	"	lea	rsi, [rsp+8]\n"
	"	lea	rdx, [rsi+rdi*8+8]\n"
	"	call	linux_main\n"
	"	ud2\n"
	".att_syntax\n"
);

extern void
linux_main(int argc, char **argv, char **envp)
{
	Str pathbuf[32];
	u8 stderr_buf[1<<9];
	AppCtx ctx[1] = {{
		.persist_dir = -1,
		.envp = envp,
		.stderr = stream_create(stderr_buf, 1[&stderr_buf], 2),
		.path = pathbuf,
	}};

	(void)argc; (void)argv;

	{
		struct {
			void (*handler)(int);
			unsigned long flags;
			unsigned long restorer;
			unsigned mask[8/SIZEOF(unsigned)];
		} sa = {
			.handler = SIG_DFL,
			.flags = SA_NOCLDSTOP | SA_NOCLDWAIT | SA_RESTART,
		};
		long res = syscall4(SYS_RT_SIGACT, SIGCHLD, &sa, NULL, SIZEOF(sa.mask));
		if (res != 0) {
			stream_log_header(&ctx->stderr, platform_time(), LOG_FATAL, (Str){0});
			stream_appendv(&ctx->stderr, S("sigaction failed: "), str_errno(-res), S("\n"));
			stream_flush(&ctx->stderr);
			sys_exit(1);
		}
	}

	vdso_setup(ctx);

	{
		Str path = env_find(envp, S("PATH="));
		if (path.len > 0) {
			for (Str p; str_tok(&path, ':', &p); NOP()) {
				if (ctx->path_len == ARRLEN(pathbuf)) {
					sys_exit(1);
				}
				ctx->path[ctx->path_len++] = p;
			}
		} else {
			ctx->path[ctx->path_len++] = S("/usr/local/bin");
			ctx->path[ctx->path_len++] = S("/bin");
			ctx->path[ctx->path_len++] = S("/usr/bin");
		}
	}

	fate(ctx);
	ASSERT(!"unreachable");
}
