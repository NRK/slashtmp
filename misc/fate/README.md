# fate

Ad-hoc alternative to [cron][] that also supports on-disk scheduling so that
missed tasks can be run on later boot.

Tasks are configured in `config.h`, see `config.h.example` for an example
config.

[cron]: https://en.wikipedia.org/wiki/Cron

### Building

* Simple build:

```console
$ cc -o fate fate-posix.c -s
```

* Recommended optimized build:

```console
$ gcc -o fate fate-posix.c -O3 -march=native -fipa-pta -fno-plt -s
```
