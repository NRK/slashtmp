#include "fate.c"

#include <signal.h>
#include <errno.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <spawn.h>
#include <fcntl.h>
#include <stdlib.h>
#include <sys/stat.h>

#if DEBUG
	#define errno_check(e)  ASSERT((e) != EINVAL && (e) != EBADF && (e) != EFAULT)
#else
	#define errno_check(e)  ((void)0)
#endif

static void
platform_write(int fd, const void *buf, Size n)
{
	ASSERT(n >= 0);
	ASSERT(fd >= 0);
	ssize_t res = write(fd, buf, (size_t)n);
	if (res < 0) {
		errno_check(errno);
	}
}

static i64
platform_time(void)
{
	return time(NULL);
}

static void
platform_sleep(i64 sec)
{
	if (sec > 0) {
		nanosleep(&(struct timespec){ .tv_sec = sec }, NULL);
	}
}

static void
platform_mtime_set(AppCtx *ctx, Str name, i64 sec)
{
	ASSERT(ctx->persist_dir >= 0);
	ASSERT(name.len > 0 && name.s[name.len - 1] == '\0');

	Str err = {0};
	int flags = O_WRONLY | O_CREAT | O_NOFOLLOW | O_CLOEXEC;
	int fd = openat(ctx->persist_dir, (char *)name.s, flags, 0644);
	if (fd >= 0) {
		struct timespec tv[2] = { [1] = { .tv_sec = sec } };
		if (futimens(fd, tv) < 0) {
			errno_check(errno);
			err = str_from_cstr(strerror(errno));
		}
		close(fd);
	} else {
		errno_check(errno);
		err = str_from_cstr(strerror(errno));
	}

	int e = err.len > 0;
	stream_log_header(&ctx->stderr, sec, e, name);
	if (e) {
		stream_appendv(&ctx->stderr, S("failed to set mtime: "), err, S("\n"));
	} else {
		stream_append(&ctx->stderr, S("mtime set successfully\n"));
	}
}

static i64
platform_mtime_get(const AppCtx *ctx, Str name)
{
	ASSERT(ctx->persist_dir >= 0);
	ASSERT(name.len > 0 && name.s[name.len - 1] == '\0');
	struct stat st;
	if (fstatat(ctx->persist_dir, (char *)name.s, &st, 0) == 0) {
		return st.st_mtime;
	}
	return 0;
}

static int
platform_execute(AppCtx *ctx, const Job *p, i64 t)
{
	Str err_prefix = {0};
	int err = 0;
	int outfd = -1;
	bool fa_init = false;
	posix_spawn_file_actions_t fa;

	if (p->stdout != NULL) {
		outfd = openat(AT_FDCWD, p->stdout, O_WRONLY | O_CREAT | O_TRUNC, 0644);
		if (outfd < 0) {
			err_prefix = S("redirecting stdout: ");
			err = errno;
			goto out;
		}

		err = posix_spawn_file_actions_init(&fa);
		if (err) {
			goto close_fd;
		}
		fa_init = true;

		err = err ? err : posix_spawn_file_actions_adddup2(&fa, outfd, 1);
		err = err ? err : posix_spawn_file_actions_addclose(&fa, outfd);
	}

	err = err ? err : posix_spawnp(
		&(pid_t){0}, p->cmd[0], fa_init ? &fa : NULL, NULL,
		p->cmd, ctx->envp
	);

	if (fa_init) {
		posix_spawn_file_actions_destroy(&fa);
	}
close_fd:
	if (outfd >= 0) {
		close(outfd);
	}

out:
	stream_log_header(&ctx->stderr, t, !!err, p->name);
	if (err) {
		Str errmsg = str_from_cstr(strerror(err));
		stream_appendv(&ctx->stderr, S("exec failure: "), err_prefix, errmsg, S("\n"));
	} else {
		stream_append(&ctx->stderr, S("exec success\n"));
	}
	return err;
}

// alternate version using fork()
// slower but consumes less memory (tested on linux/glibc)
#if 0
static void
platform_execute(const Job *p, i64 t)
{
	int err = 0;
	int pipe[2];

	extern int pipe2(int [2], int);
	if (pipe2(pipe, O_CLOEXEC) < 0) {
		err = errno;
		goto out;
	}

	switch (fork()) {
	case -1:
		err = errno;
		break;
	case 0:
		if (p->stdout != NULL) {
			int outfd = open(p->stdout, O_WRONLY | O_CREAT | O_TRUNC | O_CLOEXEC, 0644);
			if (outfd < 0 || dup2(outfd, 1) < 0) {
				err = errno;
			}
		}
		if (!err) {
			execvp(p->cmd[0], p->cmd);
			err = errno;
		}
		write(pipe[1], &err, sizeof err);
		_exit(1);
		break;
	}

	close(pipe[1]);
	read(pipe[0], &err, sizeof err);
	close(pipe[0]);

out:
	// error reporting
}
#endif

static void
platform_persistent_dir_init(AppCtx *ctx)
{
	Str err = {0};
	Str xdg_data = env_find(ctx->envp, S("XDG_DATA_HOME="));
	if (xdg_data.len > 0) {
		int flags = O_DIRECTORY | O_CLOEXEC | O_NOFOLLOW;
		int data_fd = openat(AT_FDCWD, (char *)xdg_data.s, flags);
		if (data_fd >= 0) {
			mkdirat(data_fd, "fate", 0755);
			int dirfd = openat(data_fd, "fate", flags);
			if (dirfd < 0) {
				err = str_from_cstr(strerror(errno));
			} else {
				ctx->persist_dir = dirfd;
			}
			close(data_fd);
		} else {
			err = str_from_cstr(strerror(errno));
		}
	} else {
		err = S("$XDG_DATA_HOME not set");
	}

	if (err.len > 0) {
		stream_log_header(&ctx->stderr, platform_time(), LOG_FATAL, (Str){0});
		stream_appendv(&ctx->stderr, S("opening $XDG_DATA_HOME/fate: "), err, S("\n"));
		stream_flush(&ctx->stderr);
		exit(1);
	}
	ASSERT(ctx->persist_dir >= 0);
}

extern int
main(void)
{
	extern char **environ;
	u8 stderr_buf[1<<9];
	AppCtx ctx[1] = {{
		.persist_dir = -1,
		.stderr = stream_create(stderr_buf, 1[&stderr_buf], 2),
		.envp = environ,
	}};

	{ // don't create zombie processes
		struct sigaction sa = {
			.sa_flags = SA_NOCLDSTOP | SA_NOCLDWAIT | SA_RESTART,
			.sa_handler = SIG_DFL,
		};
		sigemptyset(&sa.sa_mask);
		if (sigaction(SIGCHLD, &sa, NULL) != 0) {
			Str errmsg = str_from_cstr(strerror(errno));
			stream_log_header(&ctx->stderr, platform_time(), LOG_FATAL, (Str){0});
			stream_appendv(&ctx->stderr, S("sigaction failed: "), errmsg, S("\n"));
			stream_flush(&ctx->stderr);
			return 1;
		}
	}

	fate(ctx);
	ASSERT(!"unreachable");
}
