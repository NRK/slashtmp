#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>
#include <limits.h>

#define Tsec(X)    INT64_C(X)
#define Tmin(X)   (INT64_C(X) * 60)
#define Thr(X)    (INT64_C(X) * 60 * 60)
#define Tday(X)   (INT64_C(X) * 24 * 60 * 60)
#define CMD(...)  (char **)(const char *const []){ __VA_ARGS__, NULL }
#define SHCMD(X)  CMD("/bin/sh", "-c", (X))

#define static_storage  static
#define NOP()           ((void)0)
#define SIZEOF(...)     ((Size)sizeof(__VA_ARGS__))
#define ARRLEN(X)       (SIZEOF(X) / SIZEOF(0[X]))
#define S(X)            ((Str){ .s = (u8 *)(X), .len = SIZEOF(X) - 1})
#define S0(X)           ((Str){ .s = (u8 *)(X), .len = SIZEOF(X) })

#define stream_appendv(ST, ...) stream_appendv_core( \
	(ST), ((Str []){ __VA_ARGS__ }), ARRLEN( ((Str []){ __VA_ARGS__ }) ) \
)

#ifdef __GNUC__
	// when debugging, use gcc/clang and compile with
	// `-fsanitize=undefined -fsanitize-undefined-trap-on-error`
	// it'll trap if an unreachable code-path is ever reached.
	#define ASSERT(X)  ((X) ? (void)0 : __builtin_unreachable())
#else
	#define ASSERT(X)  ((void)0)
#endif

typedef uint8_t     u8;
typedef uint32_t    u32;
typedef uint64_t    u64;
typedef int16_t     i16;
typedef int64_t     i64;
typedef ptrdiff_t   Size;
typedef struct { u8 *s; Size len; } Str;
typedef struct {
	u8 *start, *end, *off;
	int fd;
} Stream;
enum { LOG_INFO, LOG_ERR, LOG_FATAL, LOG__CNT };

typedef struct {
	Str name;
	char **cmd;
	i64 interval;
	char *stdout;
	bool persist;
	// TODO: stderr;
	// TODO??: std{out,err} env subst
	// TODO??: bool subst;
} Job;

typedef struct {
	int persist_dir;
	Stream stderr;
	char **envp;
	Str *path;
	Size path_len;
	// TODO: timezone offset??
} AppCtx;

// platform API

static void   platform_sleep(i64 t);
static i64    platform_time(void);
static int    platform_execute(AppCtx *ctx, const Job *p, i64 t);
static void   platform_write(int fd, const void *buf, Size n);
static void   platform_mtime_set(AppCtx *ctx, Str name, i64 sec);
static i64    platform_mtime_get(const AppCtx *ctx, Str name);
static void   platform_persistent_dir_init(AppCtx *ctx); // may call exit

// globals

#include "config.h"

// func

static Str
str_from_cstr(char *s)
{
	Str ret = { .s = (u8 *)s };
	for (NOP(); s != NULL && s[ret.len] != '\0'; ++ret.len) {}
	return ret;
}

static bool
str_eq(Str a, Str b)
{
	if (a.len != b.len) {
		return false;
	}
	for (Size i = 0; i < a.len; ++i) {
		if (a.s[i] != b.s[i]) {
			return false;
		}
	}
	return true;
}

static bool
str_match_start(Str s, Str match)
{
	if (s.len < match.len) {
		return false;
	}
	Str head = { s.s, match.len };
	return str_eq(head, match);
}

static Str
str_shave_head(Str s, Size n)
{
	ASSERT(n >= 0);
	ASSERT(n <= s.len);
	Str ret = {0};
	if (s.s) {
		ret = (Str){ s.s + n, s.len - n };
	}
	return ret;
}

static bool
str_tok(Str *s, u8 ch, Str *out)
{
	while (s->len > 0 && s->s[0] == ch) {
		++s->s;
		--s->len;
	}
	if (s->len == 0) {
		return false;
	}
	Str ret = *s;
	for (ret.len = 0; ret.len < s->len && ret.s[ret.len] != ch; ++ret.len) {}
	s->s   += ret.len + (ret.len < s->len);
	s->len -= ret.len + (ret.len < s->len);
	*out = ret;
	return true;
}

static Str
env_find(char **envp, Str find)
{
	ASSERT(find.len > 0 && find.s[find.len - 1] == '=');
	for (Size i = 0; envp[i] != NULL; ++i) {
		Str s = str_from_cstr(envp[i]);
		if (str_match_start(s, find)) {
			Str ret = str_shave_head(s, find.len);
			ASSERT(ret.s[ret.len] == '\0');
			return ret;
		}
	}
	return (Str){0};
}

static void
stream_flush(Stream *f)
{
	Size n = f->off - f->start;
	platform_write(f->fd, f->start, n);
	f->off = f->start;
}

static Stream
stream_create(void *buf, void *end, int fd)
{
	ASSERT(buf != NULL && end != NULL);
	ASSERT((uintptr_t)end > (uintptr_t)buf);
	Stream ret = { .start = buf, .off = buf, .end = end, .fd = fd };
	return ret;
}

static void
stream_append(Stream *f, Str s)
{
	ASSERT(s.len >= 0);
	ASSERT(f->start != NULL && f->end != NULL);
	ASSERT(f->end - f->start > 0);
	if (s.len == 0) {
		return;
	}
	for (u8 *p = s.s, *end = p + s.len; p < end; NOP()) {
		ASSERT((uintptr_t)f->off >= (uintptr_t)f->start);
		ASSERT((uintptr_t)f->off <= (uintptr_t)f->end);
		if (f->off == f->end) {
			stream_flush(f);
		}
		while (f->off < f->end && p < end) {
			*f->off++ = *p++;
		}
	}
}

static void
stream_appendv_core(Stream *st, Str *sv, Size n)
{
	for (Size i = 0; i < n; ++i) {
		stream_append(st, sv[i]);
	}
}

static void
stream_timestamp(Stream *f, i64 time)
{
	char tbuf[] = "[XX:XX:XX] ";
#ifdef TZ_OFF
	time += TZ_OFF;
#endif
	int h = (time / 60 / 60) % 24;
	int m = (time / 60) % 60;
	int s = time % 60;

	tbuf[1] = (h / 10) + '0'; tbuf[2] = (h % 10) + '0';
	tbuf[4] = (m / 10) + '0'; tbuf[5] = (m % 10) + '0';
	tbuf[7] = (s / 10) + '0'; tbuf[8] = (s % 10) + '0';
	stream_append(f, S(tbuf));
}

static void
stream_log_header(Stream *stm, i64 time, int lvl, Str component)
{
	static_storage const Str l[] = {
		[LOG_INFO]  = S("INFO  "),
		[LOG_ERR]   = S("ERROR "),
		[LOG_FATAL] = S("FATAL "),
	};

	stream_timestamp(stm, time);

	ASSERT(lvl >= 0 && lvl < LOG__CNT);
	stream_append(stm, l[lvl]);

	stream_append(stm, S("[fate"));
	if (component.len > 0) {
		stream_append(stm, S("::"));
		component.len -= component.s[component.len - 1] == '\0';
		stream_append(stm, component);
	}
	stream_append(stm, S("] "));
}

static void
job_run(AppCtx *ctx, const Job *jobv, i64 *sched, Size n)
{
	for (Size i = 0; i < n; ++i) {
		i64 now = platform_time();
		if (now >= sched[i]) {
			const Job *p = jobv + i;
			int err = platform_execute(ctx, p, now);
			if (!err && p->persist) {
				platform_mtime_set(ctx, p->name, now);
			}
			sched[i] = now + p->interval;
		}
	}
}

static i64
job_next(i64 *sched, Size n)
{
	i64 min = INT64_MAX;
	for (Size i = 0; i < n; ++i) {
		ASSERT(sched[i] >= 0);
		if (sched[i] < min) {
			min = sched[i];
		}
	}
	return min;
}

static void
fate(AppCtx *ctx)
{
	i64 sched[ARRLEN(jobs)] = {0};
	bool need_persistent = false;

	for (Size i = 0; i < ARRLEN(jobs); ++i) {
		const Job *p = jobs + i;
		ASSERT(p->interval > 0);
		ASSERT(p->name.len > 0 && p->name.s[p->name.len - 1] == '\0');
		need_persistent |= p->persist;
	}

	if (need_persistent) {
		platform_persistent_dir_init(ctx);
	}
	for (Size i = 0; i < ARRLEN(sched); ++i) {
		if (jobs[i].persist) {
			i64 mtime = platform_mtime_get(ctx, jobs[i].name);
			if (mtime <= platform_time()) {
				sched[i] = mtime + jobs[i].interval;
			}
		}
	}

	do {
		job_run(ctx, jobs, sched, ARRLEN(sched));
		stream_flush(&ctx->stderr);
		i64 next = job_next(sched, ARRLEN(sched));
		platform_sleep(next - platform_time());
	} while (1);
	ASSERT(!"unreachable");
}
