#!/usr/bin/tclsh
#
# converts file and trash URI to path.

proc trashdir {} {
	global env
	if { [info exists env(XDG_DATA_HOME)] } {
		set base "$env(XDG_DATA_HOME)"
	} elseif { [info exists env(HOME)] } {
		set base "$env(HOME)/.local/share"
	} else {
		error {Cannot find $XDG_DATA_HOME or $HOME}
	}
	return "${base}/Trash/files/"
}

proc hex_decode {str} {
	set r {[format "%c" [scan "\1" "%2x"]]}
	set str [string map { "\[" "%5B" "\]" "%5D" } $str]
	set str [regsub -all {%([[:xdigit:]]{2})} $str $r]
	return [subst -nobackslashes -novariables $str]
}

proc main {} {
	global argv
	array set rep [list "file" "/"   "trash" "[trashdir]"]
	foreach uri $argv {
		switch -regexp -matchvar match -- $uri {
		"^(file):///.*" -
		"^(trash):///.*" {
			set m [lindex $match 1]
			set n [expr { [string len $m] + [string len ":///"] - 1 }]
			puts [hex_decode [string replace $uri 0 $n $rep($m)]]
		} default {
			puts $uri
		}
		}
	}
}

main
