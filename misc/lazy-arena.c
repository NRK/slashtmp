// playing around with an arena that commits lazily via a pagefault handler and
// avoids having to store any commit related state in the arena itself
//
// on linux it uses a SIGSEGV handler with sinfo_t
// on windows it uses a vectored exception handler
#include <stddef.h>
#include <stdint.h>

typedef uint8_t     u8;
typedef ptrdiff_t   Size;
typedef struct { u8 *beg, *end; } Arena;

#define ASSERT(X)  ((X) ? (void)0 : __builtin_unreachable())

/////////////////////////

__attribute((alloc_size(2, 3), malloc))
static void *
arena_alloc(Arena *a, Size cnt, Size size, Size align)
{
	Size avail = a->end - a->beg;
	Size pad = -(uintptr_t)a->beg & (align - 1);
	if (!((avail-pad) >= 0 && cnt <= (avail-pad)/size)) {
		ASSERT(!"unreachable");
	}
	u8 *ret = a->beg + pad;
	a->beg += (size*cnt) + pad;
	return __builtin_memset(ret, 0x0, size*cnt);
}

/////////////////////////

#if   defined(__unix__)

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <errno.h>
#include <sys/mman.h>
#include <unistd.h>

#ifdef DISABLE_LOG
	#define LOG(...) ((void)0)
#else
	#define LOG(...) dprintf(2, __VA_ARGS__)
#endif

static struct {
	Size pagesz;
} os_global;

static void
segfault_handler(int sig, siginfo_t *sinfo, void *idk)
{
	int saved_errno = errno;

	LOG("segfault at: %p\n", sinfo->si_addr);
	uintptr_t page_align_mask = ~(uintptr_t)(os_global.pagesz - 1); // align to page boundary
	void *addr = (void *)((uintptr_t)sinfo->si_addr & page_align_mask);
	if (mprotect(addr, 1<<18, PROT_READ | PROT_WRITE) < 0 &&  // try to commit greedily
	    mprotect(addr, os_global.pagesz, PROT_READ | PROT_WRITE) < 0)  // fall back to page size commit
	{
		// and if it still fails, then we probably got a legit segfault.
		// unset the SIGSEGV handler and let it crash on resume.
		LOG("it's over: errno{%d} addr{%p}\n", errno, sinfo->si_addr);
		signal(SIGSEGV, NULL);
	}
	errno = saved_errno;
}

static void
yes(u8 *buf, Size len)
{
	ASSERT(len % 2 == 0);
	for (Size i = 0; i < len; i += 2) {
		buf[i+0] = 'y';
		buf[i+1] = '\n';
	}
	write(1, buf, len);
}

extern int
main(void)
{
	Size MEM_SIZE = (Size)1 << 40;
	Arena a[1] = {{0}};
	os_global.pagesz = sysconf(_SC_PAGESIZE);
	if (os_global.pagesz < 0) {
		fprintf(stderr, "sysconf(_SC_PAGESIZE) failed\n");
		abort();
	}
	a->beg = mmap(0, MEM_SIZE, PROT_NONE, MAP_ANON|MAP_PRIVATE, -1, 0);
	if (a->beg == MAP_FAILED) {
		fprintf(stderr, "mmap() failed\n");
		abort();
	}
	a->end = a->beg + MEM_SIZE;

	struct sigaction sa = {0};
	sa.sa_sigaction = segfault_handler;
	int res = sigfillset(&sa.sa_mask);
	sa.sa_flags = SA_SIGINFO | SA_RESTART;
	if (res < 0 || sigaction(SIGSEGV, &sa, NULL) < 0) {
		fprintf(stderr, "failed to set up signal handler\n");
		abort();
	}

	for (int i = 0; i < 128; ++i) {
		Size l = 1<<16;
		u8 *p = arena_alloc(a, l, 1, 1);
		yes(p, l);
	}

	return 0;
}

#elif defined(_WIN32)

#define WIN32_LEAN_AND_MEAN
#include <windows.h>

static struct {
	Size pagesz;
} os_global;

static LONG
pagefault_handler(EXCEPTION_POINTERS *einfo)
{
	EXCEPTION_RECORD *e = einfo->ExceptionRecord;
	if (e->ExceptionCode != EXCEPTION_ACCESS_VIOLATION) {
		return EXCEPTION_CONTINUE_SEARCH;
	}

	uintptr_t page_align_mask = ~(uintptr_t)(os_global.pagesz - 1); // align to page boundary
	uintptr_t addr = e->ExceptionInformation[1];
	void *page = (void *)(addr & page_align_mask);
	if (VirtualAlloc(page, 1 << 18, MEM_COMMIT, PAGE_READWRITE) == NULL &&  // greedy
	    VirtualAlloc(page, os_global.pagesz, MEM_COMMIT, PAGE_READWRITE) == NULL)
	{
		return EXCEPTION_EXECUTE_HANDLER;
	} else {
		return EXCEPTION_CONTINUE_EXECUTION;
	}
}

static void
yes(u8 *buf, Size len)
{
	ASSERT(len % 2 == 0);
	for (Size i = 0; i < len; i += 2) {
		buf[i+0] = 'y';
		buf[i+1] = '\n';
	}

	HANDLE h = GetStdHandle(-11);
	DWORD junk;
	WriteFile(h, buf, len, &junk, 0);
}

__attribute((force_align_arg_pointer))
extern int
mainCRTStartup(void)
{
	Size MEM_SIZE = (Size)1 << 40;
	Arena a[1] = {{0}};

	SYSTEM_INFO info;
	GetSystemInfo(&info);
	os_global.pagesz = info.dwPageSize;
	a->beg = VirtualAlloc(0, MEM_SIZE, MEM_RESERVE, PAGE_READWRITE);
	if (a->beg == NULL) {
		ExitProcess(1);
	}
	a->end = a->beg + MEM_SIZE;

	if (AddVectoredExceptionHandler(1, pagefault_handler) == NULL) {
		ExitProcess(2);
	}

	for (int i = 0; i < 128; ++i) {
		Size l = 1<<16;
		u8 *p = arena_alloc(a, l, 1, 1);
		yes(p, l);
	}

	ExitProcess(0);
}

#endif
