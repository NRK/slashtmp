// ref: https://old.reddit.com/r/C_Programming/comments/15x02kp/torn_between_using_avlgperf_and_my_own/
// $ gcc -o out misc/perfect-hash-c-keywords.c -march=native -O3 -fopenmp
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <string.h>
#include <stdint.h>

#define TBL_LEN   64 // desired size of the hash-table

#ifdef _OPENMP
	#undef  TBL_LEN
	#define TBL_LEN  62
#endif

// https://en.cppreference.com/w/c/keyword
static const char *keywords[] = {
	"alignas", "alignof", "auto", "bool", "break", "case", "char", "const",
	"constexpr", "continue", "default", "do", "double", "else", "enum",
	"extern", "false", "float", "for", "goto", "if", "inline", "int",
	"long", "nullptr", "register", "restrict", "return", "short",
	"signed", "sizeof", "static", "static_assert", "struct", "switch",
	"thread_local", "true", "typedef", "typeof", "typeof_unqual",
	"union", "unsigned", "void", "volatile", "while",
};

static uint32_t
hash(const char *s, int len, uint32_t mul)
{
	uint32_t l = len;
	uint32_t h = l ^ ((l << 15) | (l >> 17));
	for (int i = 0; i < len; ++i) {
		h ^= (unsigned char)s[i];
		h *= mul;
	}
	// ref: https://lemire.me/blog/2016/06/27/a-fast-alternative-to-the-modulo-reduction/
	h = ((uint64_t)h * TBL_LEN) >> 32;
	assert(h < TBL_LEN);
	return h;
}

extern int
main(void)
{
	int nkeywords = sizeof keywords / sizeof *keywords;
	printf("keywords[%d]\n", nkeywords);

	#pragma omp parallel for
	for (uint64_t iter = 0; iter < (1ull << 32); ++iter) {
		// splitmix64
		uint64_t r = iter;
		r ^= r >> 30;
		r *= 0xBF58476D1CE4E5B9;
		r ^= r >> 27;
		r *= 0x94D049BB133111EB;
		r ^= r >> 31;
		uint32_t m = r | 0x1; // ensure odd multipler

		int collusion = 0;
		uint8_t seen[TBL_LEN] = {0};
		for (int i = 0; i < nkeywords; ++i) {
			uint32_t idx = hash(keywords[i], strlen(keywords[i]), m);
			if (seen[idx]) { // collusion
				collusion = 1;
				break;
			}
			seen[idx] = 1;
		}
		if (!collusion) {
			printf("found a multipler for perfect hash: 0x%"PRIX32"\n", m);
			printf(
				"load factor: %f (%d/%d)\n",
				(double)nkeywords / (double)TBL_LEN,
				nkeywords, TBL_LEN
			);
			exit(0);
		}
	}

	printf("no multipler found for a perfect hash. try increasing the table size.\n");
	return 1;
}
