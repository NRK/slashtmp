/*
 * $ c99 -o safe_va_func safe_va_func.c
 *
 * va functions are often sources of subtle bugs because the va args are not
 * type-checked and can have unintuitive promotion/conversion rules.
 *
 * there's also the problem that the va function doesn't know how many
 * arguments have been passed to it. so it needs to be done manually by the
 * caller, either via some sentinel value that marks the end of arguments (e.g
 * `execl()` using NULL to mark the end) or passing the size manually via
 * another parameter.
 *
 * with some clever usage of C99's va macros combined with compound literals,
 * these shortcomings can easily be fixed.
 *
 * this only works when all your va args are the same type, which covers
 * MAJORITY of the use-cases. but even in cases where different types might be
 * needed, it *should* be possible to extend this trick with "tagged union" and
 * some usage of C11's `_Generic`.
 */

#include <stdio.h>

static void
print_ints_core(FILE *f, const int *v, size_t n)
{
	const int *p;
	for (p = v; p < v + n; ++p)
		fprintf(f, "%d ", *p);
	fputc('\n', f);
}
#define print_ints(F, ...) print_ints_core( \
	F, (const int[]){ __VA_ARGS__ }, \
	sizeof (int []){ __VA_ARGS__ } / sizeof (int) \
)

extern int
main(void)
{
	print_ints(stdout, 1);
	print_ints(stdout, 1, 2, 3);
	print_ints(stdout, 1, 2, 3, 4, 5);

	return 0;
}
