/*
 * ref: http://blog.jgc.org/2010/12/write-your-passwords-down.html
 * build: cc mkpasstable.c
 * usage: ./a.out < /dev/urandom
 */

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>

extern int
main(void)
{
	printf("   ");
	for (int a = 'A'; a <= 'Z'; ++a) {
		printf(" %c", a);
	}
	putchar('\n');
	printf("   ");
	for (int a = 'A'; a <= 'Z'; ++a) {
		printf("--");
	}
	putchar('\n');

	for (int a = 'A'; a <= 'Z'; ++a) {
		printf("%c |", a);
		for (int _ = 'A'; _ <= 'Z'; ++_) {
			int done = 0;
			do {
				int c = getchar();
				if (c == EOF) {
					fprintf(stderr, "couldn't read from stdin\n");
					exit(1);
				}
				if (isprint((unsigned char)c) && !isblank((unsigned char)c)) {
					printf(" %c", c);
					done = 1;
				}
			} while (!done);
		}
		putchar('\n');
	}
	fflush(stdout);
	if (ferror(stdout)) {
		fprintf(stderr, "writing to stdout failed\n");
		exit(1);
	}
}
