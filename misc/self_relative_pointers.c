/* ref: https://www.gingerbill.org/article/2020/05/17/relative-pointers/#self-relative-pointers */
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

#define SRP(X) ((X) == 0 ? NULL : (void *)( (intptr_t)&(X) + (X) ))
#define SRP_CREATE(SELF, TARGET) ((intptr_t)(TARGET) - (intptr_t)&(SELF))

typedef struct {
	char *s;
	int32_t next; /* magic pointer */
} StrList;

extern int
main(int argc, char *argv[])
{
	StrList *l = NULL, *buf = malloc(argc * sizeof *buf);
	size_t buf_head = 0;
	if (buf == NULL) abort();

	for (int i = 0; i < argc; ++i) {
		StrList *tmp = buf + buf_head++;
		tmp->s = argv[i];
		tmp->next = l == NULL ? 0 : SRP_CREATE(tmp->next, l);
		l = tmp;
	}

	for (StrList *it = l; it != NULL; it = SRP(it->next)) {
		printf("%s%s", it->s, (char *[]){ "", " -> " }[!!SRP(it->next)]);
	}
	putchar('\n');
	free(buf);
}
