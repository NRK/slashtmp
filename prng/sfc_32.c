/*
 * Chris Doty-Humphrey's "Small Fast Counting" RNG (sfc) which can be found
 * inside the `practrand` source code.
 *
 * this version deviates a bit from the original in the initialization phase:
 *     - original accepts a 64bit seed, this one accepts 32bit
 *     - original sets `a` to 0 initially, this one sets it to 0xF1EA5EED (similar to jsf)
 *     - original runs 12 iteration for initialization, this one does 20.
 */
#include <stdint.h>

struct rng_state { uint32_t a, b, c, cnt; };

static uint32_t
rng32(struct rng_state *s)
{
	enum { SR = 9, SL = 3, R = 21 };
	uint32_t e;
	e    = s->a + s->b + s->cnt++;
	s->a = s->b ^ (s->b >> SR);
	s->b = s->c + (s->c << SL);
	s->c =    e + ((s->c >> (-(unsigned)R & 31)) | (s->c << R));
	return e;
}

static struct rng_state
rng32_init(uint32_t sneed)
{
	struct rng_state s;
	s.a = UINT32_C(0xF1EA5EED);
	s.b = s.c = sneed;
	s.cnt = 1;
	for (int i = 0; i < 20; ++i) {
		(void)rng32(&s);
	}
	return s;
}
