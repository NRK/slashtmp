#include <stdint.h>

/*
 * the "XSH-RR" variant. the multiplier and increment are my own.
 * "seeding" the RNG is just a matter of setting the initial state.
 * the increment could also be made part of the seed, just make sure it's odd.
 */
static uint32_t
rng32(uint64_t *state)
{
	uint64_t oldstate = *state;
	uint32_t r, v;

	*state *= UINT64_C(0x9E3793492EEDC3F7);
	*state += 0x1337;

	r = oldstate >> (64 - 5);
	v = (oldstate ^ (oldstate >> 18)) >> (32 - 5);
	v = (v >> (-r & 31)) | (v << r);
	return v;
}
