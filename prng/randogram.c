// $ gcc -o randogram prng/randogram.c
// $ ./randogram | image_viewer_with_stdin_and_pgm_support
#include <stdint.h>
#include <stdio.h>

#define W 512
#define H 512
#define IMG_SIZE (W * H)
_Static_assert(IMG_SIZE%4==0, "");

extern int
main(void)
{
	typedef uint64_t u64; typedef uint8_t u8; typedef uint32_t u32;
	u64 state = 1;
	static u8 pix[IMG_SIZE];
	for (int i = 0; i < IMG_SIZE; i += 4) {
		// pcg with random xorshit as the permutation
		state *= UINT64_C(6364136223846793005);
		state += 0x1337;
		u32 rng = state ^ (state >> (state >> 59));

		pix[i+0] = (rng >>  0) & 0xFF;
		pix[i+1] = (rng >>  8) & 0xFF;
		pix[i+2] = (rng >> 16) & 0xFF;
		pix[i+3] = (rng >> 24) & 0xFF;
	}
	printf("P5\n");
	printf("%d %d\n", W, H);
	printf("255\n");
	fwrite(pix, 1, sizeof pix, stdout);
	fflush(stdout);
	return ferror(stdout);
}
