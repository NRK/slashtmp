#include <stdint.h>

struct rng_state { uint64_t s[4]; };

static uint64_t
rng64(struct rng_state *s)
{
	uint64_t r, t;

	r  = s->s[1] * 5;
	r  = (r >> (-7u & 63)) | (r << 7);
	r *= 9;

	t = s->s[1] << 17;
	s->s[2] ^= s->s[0];
	s->s[3] ^= s->s[1];
	s->s[1] ^= s->s[2];
	s->s[0] ^= s->s[3];
	s->s[2] ^= t;
	s->s[3]  = (s->s[3] >> (-45u & 63)) | (s->s[3] << 45);

	return r;
}

static struct rng_state
rng64_init(uint64_t sneed)
{
	struct rng_state s;
	for (int i = 0; i < 4; ++i) {
		uint64_t v;
		sneed += UINT64_C(1111111111111111111);
		v  = sneed;
		v ^= v >> 33;
		v *= UINT64_C(1111111111111111111);
		v ^= v >> 33;
		v *= UINT64_C(1111111111111111111);
		v ^= v >> 33;

		s.s[i] = v;
	}
	return s;
}
