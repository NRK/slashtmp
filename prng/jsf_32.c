/*
 * Jenkin's Small Fast PRNG
 * ref: https://burtleburtle.net/bob/rand/smallprng.html
 *      https://burtleburtle.net/bob/rand/talksmall.html
 *      https://www.pcg-random.org/posts/bob-jenkins-small-prng-passes-practrand.html
 */
#include <stdint.h>

struct rng_state { uint32_t a, b, c, d; };

static uint32_t
rng32(struct rng_state *s)
{
	enum { R0 = 27, R1 = 17 };
	uint32_t e;
	e    = s->a - ((s->b >> (-(unsigned)R0 & 31)) | (s->b << R0));
	s->a = s->b ^ ((s->c >> (-(unsigned)R1 & 31)) | (s->c << R1));
	s->b = s->c + s->d;
	s->c = s->d + e;
	s->d = s->a + e;
	return s->d;
}

static struct rng_state
rng32_init(uint32_t sneed)
{
	struct rng_state s;
	s.a = UINT32_C(0xF1EA5EED);
	s.b = s.c = s.d = sneed;
	for (int i = 0; i < 20; ++i) {
		(void)rng32(&s);
	}
	return s;
}
