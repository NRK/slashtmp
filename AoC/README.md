# Advent of Code

This directory contains my solutions for [Advent of Code][aoc] puzzles.

Each year I try to either pick up a new language or hone my skills on a lesser
known language.
The C solutions are usually aimed at solving [custom large inputs][bigboy]
quickly with no care for "code quality".

The solutions accept input via `stdin`, so you can easily switch between the
example, the puzzle input or big boy inputs without editing the source code.

```console
$ ./d01 < example.txt
```

Most solutions will provide answer for both parts.
But sometimes you may need to edit the source to switch between part one and
two.

[aoc]: https://adventofcode.com
[bigboy]: https://bigboy.wiki
