#!/usr/bin/tclsh

lassign $argv ty max ;# example {10 20} input {2000000 4000000}
foreach {line} [split [read -nonewline stdin] "\n"] {
	scan $line "Sensor at x=%d, y=%d: closest beacon is at x=%d, y=%d" sx sy bx by
	set dist [expr {abs($sx - $bx) + abs($sy - $by)}]
	set tyd [expr {abs($sy - $ty)}]
	if {$tyd <= $dist} {
		set startx [expr { $sx - ($dist - $tyd) }]
		set endx   [expr { $sx + ($dist - $tyd) }]
		lappend tranges [list $startx $endx]
	}
	if {$by == $ty} { lappend problematic_beacons $bx }
	lappend sensors $sx $sy $dist
}

set problematic_beacons [lsort -integer -unique $problematic_beacons]
set tranges [lsort -integer -index 0 $tranges]
set sum 0
for {set i 0} {$i < [llength $tranges]} {incr i} {
	lassign [lindex $tranges $i] a0 a1
	if {$a0 > $a1} { continue }
	incr sum [expr {($a1 - $a0) + 1}]
	set problem [lmap x $problematic_beacons { expr {$x >= $a0 && $x <= $a1 ? $x : [continue]} }]
	incr sum [expr {-[llength $problem]}]
	for {set k [expr {$i + 1}]} {$k < [llength $tranges]} {incr k} {
		lset tranges $k 0 [expr {max($a1+1, [lindex $tranges $k 0])}]
	}
}
puts "part1: $sum"

# TODO: part 2 is deadly slow. use a better algorithm.
foreach {sx sy dist} $sensors {
	incr dist
	set startx [expr {$sx - $dist}]
	set endx $dist
	for {set d 0} {$d < $dist} {incr d} {
		set x0 [expr {$startx + $d}]
		set x1 [expr {($sx + $endx) - $d}]
		set y0 [expr {$sy + $d}]
		set y1 [expr {$sy - $d}]
		foreach {x y} [list $x0 $y0 $x0 $y1 $x1 $y0 $x1 $y1] {
			if {$x < 0 || $x > $max || $y < 0 || $y > $max} {
				continue
			}
			set found 1
			foreach {ssx ssy sdist} $sensors {
				set tdist [expr {abs($ssx - $x) + abs($ssy - $y)}]
				if {$tdist <= $sdist} {
					set found 0
					break
				}
			}
			if {$found == 1} {
				puts "gottem: $x $y = [expr {$x * 4000000 + $y}]"
				exit
			}
		}
	}
}
