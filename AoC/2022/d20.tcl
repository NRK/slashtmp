#!/usr/bin/tclsh

proc lrotate {l n} {
	if {$n != 0 && [llength $l] > 0} {
		set n [expr {$n % [llength $l]}]
		set l [concat [lrange $l $n end] [lrange $l 0 $n-1]]
	}
	return $l
}

proc mix {input times} {
	set work $input
	set inlen [llength $input]
	for {set k 0} {$k < $times} {incr k} {
		for {set i 0} {$i < $inlen} {incr i} {
			set ch_id [lindex $input $i]
			set oidx [lsearch -exact $work $ch_id]
			set ch [lindex $ch_id 0]
			set work [lreplace $work $oidx $oidx] ;# pull out the thingy
			set work [lrotate $work $ch] ;# rotate the rest
			set work [linsert $work $oidx $ch_id] ;# put back the thingy
		}
	}
	set zero_idx [lsearch -glob $work "0 {*}"]
	foreach {off} { 1000 2000 3000 } {
		lappend coord [lindex $work [expr { ($zero_idx + $off) % $inlen }] 0]
	}
	return [expr [join $coord "+"]]
}

for {set i 0} {[gets stdin line] >= 0} {incr i} {
	lappend input0 "$line {$i}"
	lappend input1 "[expr {$line * 811589153}] {$i}"
}

puts "part1: [mix $input0 1]"
puts "part2: [mix $input1 10]"
