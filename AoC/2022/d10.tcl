#!/usr/bin/tclsh

set mon [lrepeat 6 [lrepeat 40 " "]]
set mapping { "\n" " " "noop" "0" "addx" "0" }
set addQ [split [string map $mapping [read -nonewline stdin]] " "]

lassign { 1 1 20 } rX cycle check
foreach {add} $addQ {
	if {$cycle == $check} {
		incr t0 [expr {$cycle * $rX}]
		incr check 40
	}

	set crtpos [expr {($cycle - 1) % 40}]
	if {$crtpos >= $rX-1 && $crtpos <= $rX+1} {
		lset mon [expr {($cycle - 1) / 40}] $crtpos "#"
	}

	incr rX $add
	incr cycle
}
puts "part1: $t0"
puts [join [lmap l $mon { join $l {} }] "\n"]
