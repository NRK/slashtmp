#!/usr/bin/tclsh

set top [list 0 0 0 0]

while {1} {
	set ret [gets stdin line]
	if {$ret < 0 || [string len $line] == 0} {
		set top [lsort -decreasing -integer $top]
		lset top 3 0
	} else {
		lset top 3 [expr {[lindex $top 3] + $line}]
	}
	if {$ret < 0} break
}

puts "part1: [lindex $top 0]"
puts "part2: [expr {[lindex $top 0] + [lindex $top 1] + [lindex $top 2]}]"
