#!/usr/bin/tclsh

set input [read stdin]

proc funiq {line n} {
	set n0 [expr { $n - 1 }]
	for {set i $n0} {$i < [string len $line]} {incr i} {
		set s [string range $line $i-$n0 $i]
		if {[llength [lsort -unique [split $s {}]]] == $n} {
			return [expr {$i + 1}]
		}
	}
}
puts "part1: [funiq $input 4]"
puts "part2: [funiq $input 14]"
