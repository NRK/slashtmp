#!/usr/bin/tclsh

set monkeys [list]
foreach {monky} [split [read -nonewline stdin] "\n\n"] {
	foreach {line} [split $monky "\n"] {
		switch -regexp -matchvar m -- $line {
		"Monkey *" {
			lappend monkeys {}
		} "Starting items: (.*)" {
			set items [split [string map { " " "" } [lindex $m 1]] ","]
			lset monkeys end end+1 $items
		} "Operation: new = old (.) (.*)" {
			lset monkeys end end+1 [lrange $m 1 2]
		} "Test: divisible by (.*)" {
			lset monkeys end end+1 [lindex $m 1]
		} "If (true|false): throw to monkey (.*)" {
			lset monkeys end end+1 [lindex $m 2]
		} default {
			error "PANIK!!"
		}
		}
	}
}

proc solve {monkeys rounds worryOp} {
	set cnt [lrepeat [llength $monkeys] 0]
	for {set round 0} {$round < $rounds} {incr round} {
		for {set i 0} {$i < [llength $monkeys]} {incr i} {
			lassign [lindex $monkeys $i] items op test tru fls
			foreach {item} $items {
				set newOp [lmap x $op { expr { $x == "old" ? $item : $x } }]
				set worry [expr $item {*}$newOp]
				set worry [expr $worry {*}$worryOp]
				set sendTo [expr {$worry % $test == 0 ? $tru : $fls}]
				lset monkeys $sendTo 0 end+1 $worry
				lset cnt $i [expr {[lindex $cnt $i] + 1}]
			}
			lset monkeys $i 0 [list]
		}
	}
	return [expr [join [lrange [lsort -integer -decreasing $cnt] 0 1] "*"]]
}

set maxd [expr [join [lmap x $monkeys { lindex $x 2 }] "*"]]
puts "part1: [solve $monkeys 20 {/ 3}]"
puts "part1: [solve $monkeys 10000 [list "%" $maxd]]"
