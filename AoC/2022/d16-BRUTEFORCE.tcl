#!/usr/bin/tclsh

# NOTE: DO NOT RUN!!!!!!
#
# this is a really bad solution for d16 part 2 that runs in O(n!) time
# (about 7mins on my pc).
# since this is a really bad solution, I don't feel like refactoring it to
# solve both parts.
# for part 1 brute force (which is doable within reasonable time),
# see commit 7c8e386.

lassign { 0 1 2 } VALVE FLOW GOESTO

while {[gets stdin line] >= 0} {
	scan $line "Valve %s has flow rate=%d;" v flow
	regexp -all -line {.*to valves? (.*)} $line _ goesTo
	set goesTo [split [string map { ", " "#" } $goesTo] "#"]
	lappend valves [list $v $flow $goesTo]
}

proc gimme {thingy node valves} {
	return [lindex [lsearch -inline -index $::VALVE $valves $node] $thingy]
}

proc mkpath {mapName valves from toList} {
	upvar $mapName map
	array set seen [list $from 1]
	set q [list [gimme $::GOESTO $from $valves]]

	for {set n 0} {[llength $toList] > 0} {incr n} {
		set curq [lindex $q $n]
		lappend q [list]
		foreach {node} $curq {
			set idx [lsearch $toList $node]
			if {$idx >= 0} {
				set toList [lreplace $toList $idx $idx]
				set map($from,$node) [expr {$n + 1}]
				set map($node,$from) [expr {$n + 1}]
				set seen($node) 1
			}
			foreach {new} [gimme $::GOESTO $node $valves] {
				if {![info exists seen($new)]} {
					lset q end end+1 $new
					set seen($new) 1
				}
			}
		}
		if {$n > 64} { error "PANIK:\nq = {$q}\n toList = {$toList}" }
	}
}

set gud_targets [lmap x $valves { expr {[lindex $x $FLOW] > 0 ? $x : [continue]} }]
set gud_targets [lsort -integer -decreasing -index $FLOW $gud_targets]
set gud_targets [lmap x $gud_targets { lindex $x $VALVE }]

array set map {}
for {set i 0} {$i < [llength $gud_targets]} {incr i} {
	set cur [expr {$i == 0 ? "AA" : [lindex $gud_targets $i-1]}]
	mkpath map $valves $cur [lrange $gud_targets $i end]
	if {$i > 64} { error "PANIK" }
}

proc tell_me_total {l valves mapName opened_cntName} {
	upvar $mapName map
	upvar $opened_cntName opened_cnt
	set t 26
	set pressure 0
	for {set i 0} {$i < [llength $l] && $t > 0} {incr i} {
		set to [lindex $l $i]
		set from [expr {$i == 0 ? "AA" : [lindex $l $i-1]}]
		set dist $map($from,$to)
		set flow [gimme $::FLOW $to $valves]
		set tcost [expr {$dist + 1}]
		if {$tcost < $t} {
			incr opened_cnt
			incr pressure [expr {$flow * ($t - $tcost)}]
		}
		incr t [expr {-$tcost}]
	}
	return $pressure
}

# quickperm: https://www.quickperm.org/
set path $gud_targets
set pathLen [llength $path]
# NOTE: this is kinda cheating, since i'm betting on the right answer being on
# the top 11 valves - which is not guranteed at all...
set pathLen [tcl::mathfunc::min $pathLen 11]
for {set i 0} {$i <= $pathLen} {incr i} { lappend p $i }

set maxsofar 0
for {set i 1} {$i < $pathLen} {} {
	lset p $i [expr {[lindex $p $i] - 1}]
	set j [expr {$i % 2 == 1 ? [lindex $p $i] : 0}]

	set opened_cnt 0
	set total [tell_me_total $path $valves map opened_cnt]
	if {$total > 800} {
		set sortedPath [lsort [lrange $path 0 $opened_cnt-1]]
		if {![info exists mem($sortedPath)]} {
			set mem($sortedPath) $total
		} else {
			set ctotal $mem($sortedPath)
			if {$total > $ctotal} {
				set mem($sortedPath) $total
			}
		}
	}
	if {$total > $maxsofar} {
		set maxsofar $total
		set bestPath [lrange $path 0 $opened_cnt-1]
	}

	set tmp [lindex $path $j]
	lset path $j [lindex $path $i]
	lset path $i $tmp

	for {set i 1} {[lindex $p $i] == 0} {incr i} {
		lset p $i $i
	}
}

puts "$maxsofar <= {AA $bestPath}"
puts "memlen [array size mem]"

proc intersect {l0 l1} {
	foreach {v0} $l0 {
		foreach {v1} $l1 {
			if {$v0 == $v1} {
				return true
			}
		}
	}
	return false
}

set maxsofar 0
foreach {p0 t0} [array get mem] {
	foreach {p1 t1} [array get mem] {
		if {![intersect $p0 $p1]} {
			set score [expr {$t0 + $t1}]
			if {$score > $maxsofar} {
				# puts "{$p0} {$p1}"
				set maxsofar $score
			}
		}
	}
}
puts "part2: $maxsofar"
