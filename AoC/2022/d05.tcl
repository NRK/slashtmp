#!/usr/bin/tclsh

set input [read stdin]
lassign [split [string map { "\n\n" "#" "\n 1" "#" } $input] "#"] stack _ ops

set l0 [lrepeat 9 [list]]
foreach {line} [split $stack "\n"] {
	for {set i 0} {($i * 4 + 1) < [string len $line]} {incr i} {
		set item [string index $line [expr {$i * 4 + 1}]]
		if {$item != " "} {
			lset l0 $i end+1 $item
		}
	}
}

set l0 [lmap item $l0 { lreverse $item }]
set l1 $l0

proc lpop {lName n} {
	upvar $lName l
	incr n -1
	set r [lrange $l end-$n end]
	set l [lreplace $l end-$n end]
	return $r
}

proc mov {lName cnt from to popfunc} {
	upvar $lName l
	lassign [list [lindex $l $from] [lindex $l $to]] lfrom lto
	set popped [lpop lfrom $cnt]
	lappend lto {*}[$popfunc $popped]
	lset l $from $lfrom
	lset l $to $lto
}

foreach {line} [split [string trim $ops] "\n"] {
	scan $line "move %d from %d to %d" cnt from to
	incr from -1
	incr to -1

	mov l0 $cnt $from $to lreverse
	mov l1 $cnt $from $to lindex
}

puts "part1: [join [lmap stk $l0 { lindex $stk end }] {}]"
puts "part2: [join [lmap stk $l1 { lindex $stk end }] {}]"
