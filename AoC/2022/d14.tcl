#!/usr/bin/tclsh

set maxy 0
foreach {line} [split [read -nonewline stdin] "\n"] {
	foreach {x y} [split [string map { " -> " "#" "," "#" } $line] "#"] {
		if {[info exists px]} {
			set dx [expr { ($x > $px) - ($x < $px) }]
			set dy [expr { ($y > $py) - ($y < $py) }]
			while {$px != ($x+$dx) || $py != ($y+$dy)} {
				set maxy [tcl::mathfunc::max $maxy $py]
				set map($px,$py) "#"
				incr px $dx; incr py $dy;
			}
		}
		set px $x; set py $y;
	}
	unset px py
}
set floor [expr {$maxy + 2}]

proc simulate_sanddrops {} {
	global map floor
	lassign { 500 0 } x y
	for {set b 0} {$b < 3} {} {
		set b 0
		foreach {dx dy} { 0 1 -1 1 1 1 } {
			set nx [expr {$x + $dx}]
			set ny [expr {$y + $dy}]
			if {[info exists map($nx,$ny)] || $ny >= $floor} {
				incr b
			} else {
				set x $nx; set y $ny;
				break
			}
		}
	}
	set map($x,$y) "o"
	return [list $x $y]
}

for {set n 0} {![info exists p1] || ![info exists p2]} {incr n} {
	lassign [simulate_sanddrops] x y
	if {$y > $maxy && ![info exists p1]} {set p1 $n}
	if {$x == 500 && $y == 0} { set p2 [expr {$n + 1}] }
}
puts "part1: $p1"
puts "part2: $p2"
