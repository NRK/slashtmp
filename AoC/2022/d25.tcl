#!/usr/bin/tclsh

proc convert {num} {
	set ret 0
	set num [string reverse $num]
	for {set i 0} {$i < [string len $num]} {incr i} {
		set ch [string map { "-" -1 "=" -2 } [string index $num $i]]
		incr ret [expr { $ch * (5 ** $i) }]
	}
	return $ret
}

proc rev {num} {
	set carry 0
	while {$num || $carry} {
		set n [expr {($num % 5) + $carry}]
		set num [expr {$num / 5}]
		if {$n > 2} {
			set r [expr {$n - 5}]
			set carry 1
			set n $r
		} else {
			set carry 0
		}
		append ret [string map { -1 "-" -2 "=" } $n]
	}
	return [string reverse $ret]
}

while {[gets stdin line] >= 0} {
	lappend nums [convert $line]
}
set ans [expr [join $nums "+"]]
puts "ans: [rev $ans]"
