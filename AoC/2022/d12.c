/* gcc -Ofast -march=native -DNDEBUG d12.c */
#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>

#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/mman.h>

#define ARRLEN(x) (sizeof(x) / sizeof(0[x]))

typedef struct { int r, c; } Coord;

static int
find(uint8_t *map, size_t mlen, int r, int c)
{
	enum { sh = 13, max = 1ul << sh };
	assert(r > 0 && r < max);
	assert(c > 0 && c < max);
	uint32_t v = ((uint32_t)r << sh) | (uint32_t)c;
	uint32_t idx = v / sizeof *map;
	uint32_t msk = 1ul << (v % sizeof *map);
	assert(idx < mlen);
	if (map[idx] & msk) {
		return 1;
	} else {
		map[idx] |= msk;
		return 0;
	}
}

extern int
main(int argc, char *argv[])
{
	char *buf = NULL, *bufend;

	if (argc > 1) {
		int fd; struct stat st;
		fstat((fd = open(argv[1], O_RDONLY)), &st);
		buf = mmap(NULL, st.st_size, PROT_READ|PROT_WRITE, MAP_PRIVATE, fd, 0);
		bufend = buf + st.st_size;
	}

	int colcnt = (char *)memchr(buf, '\n', bufend - buf) - buf;
	int stride = colcnt + 1;
	int rowcnt = (bufend - buf) / stride;
	char *start = memchr(buf, 'S', bufend - buf);
	char *end = memchr(buf, 'E', bufend - buf);
	*start = 'a'; *end = 'z';
	Coord src, dst;
	src.r = (start - buf) / stride;
	src.c = (start - buf) % stride;
	dst.r = (end - buf) / stride;
	dst.c = (end - buf) % stride;

	static Coord q0[1 << 10], q1[ARRLEN(q0)];
	int q0h = 0, q1h = 0;
	Coord *qcur = q0, *qnxt = q1;
	int *qch = &q0h, *qnh = &q1h;
	int p1 = -1, p2 = -1;

	qcur[(*qch)++] = dst; /* search from dst to src */
	for (int n = 0; p1 < 0 || p2 < 0; ++n) {
		assert(*qch > 0);
		for (int i = 0; i < *qch; ++i) {
			Coord cur = qcur[i];
			char curlvl = buf[cur.r * stride + cur.c];
			if (cur.r == src.r && cur.c == src.c && p1 < 0) {
				p1 = n;
			}
			if (curlvl == 'a' && p2 < 0) {
				p2 = n;
			}
			if (p1 >= 0 && p2 >= 0) {
				break;
			}

			Coord diff[] = { {-1, 0}, {1, 0}, {0, -1}, {0, 1} };
			for (size_t k = 0; k < ARRLEN(diff); ++k) {
				int r = cur.r + diff[k].r;
				int c = cur.c + diff[k].c;
				int nlvl;
				if (r >= 0 && r < rowcnt && c >= 0 && c < colcnt &&
				    (nlvl = buf[r * stride + c])+1 >= curlvl)
				{
					static uint8_t map[1ul << 26];
					if (!find(map, ARRLEN(map), r+1, c+1)) {
						assert(*qnh < ARRLEN(q0));
						qnxt[(*qnh)++] = (Coord){ r, c };
					}
				}
			}
		}
		Coord *tmp = qcur;
		qcur = qnxt;
		qnxt = tmp;
		int *tmpi = qch;
		qch = qnh;
		qnh = tmpi;
		*qnh = 0;
	}
	printf("part1: %d\npart2: %d\n", p1, p2);
}
