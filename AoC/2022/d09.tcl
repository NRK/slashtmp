#!/usr/bin/tclsh

set hpos [list 0 0]
set tpos [lrepeat 9 [list 0 0]]
array set dir2v [list "R" [list 1 0] "L" [list -1 0] "U" [list 0 -1] "D" [list 0 1]]

proc newtail {hpos tpos} {
	set tdelta [lmap t $tpos h $hpos { expr { $h - $t } }]
	if {[lsearch -regexp $tdelta "-?2"] >= 0} {
		set tpos [lmap t $tpos d $tdelta { expr { $t + (($d > 0) - ($d < 0)) } }]
	}
	return $tpos
}

while {[gets stdin line] >= 0} {
	scan $line "%s %d" dir cnt
	for {set i 0} {$i < $cnt} {incr i} {
		set hpos [lmap c $hpos d $dir2v($dir) { expr { $c + $d } }]
		set prev $hpos
		set tpos [lmap x $tpos { set prev [newtail $prev $x] }]
		set visited1([join [lindex $tpos 0] ","]) 1
		set visited2([join [lindex $tpos end] ","]) 1
	}
}

puts "part1: [array size visited1]"
puts "part2: [array size visited2]"
