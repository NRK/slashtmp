#!/usr/bin/tclsh

lassign [split [string map { "\n\n" "@" } [read -nonewline stdin]] "@"] input_map ops

set input_map [split $input_map "\n"]
for {set y 0} {$y < [llength $input_map]} {incr y} {
	set line [lindex $input_map $y]
	for {set x 0} {$x < [string len $line]} {incr x} {
		set ch [string index $line $x]
		if {$ch != " "} {
			set map($x,$y) $ch
		}
	}
}
set ops [split [string map { "L" "#L#" "R" "#R#" } $ops] "#"]

set dirv [lmap {x y} {1 0 0 1 -1 0 0 -1} { list $x $y }]
lassign { 0 1 2 3 } RIGHT DOWN LEFT UP

set z [lmap key [array names map "*,0"] {
	expr { $map($key) == "." ? [split $key ","] : [continue] }
}]
set cur [lindex $z 0]
set dir $RIGHT

foreach {op} $ops {
	if {[string is digit -strict $op]} {
		for {set i 0} {$i < $op} {incr i} {
			set newpos [lmap c $cur d [lindex $dirv $dir] { expr { $c + $d } }]
			set newposKey [join $newpos ","]
			if {![info exists map($newposKey)]} {
				switch -- $dir \
					$RIGHT {
						set search "*,[lindex $cur 1]"
						set pick "0"
						set pickidx 0
					} $LEFT {
						set search "*,[lindex $cur 1]"
						set pick "end"
						set pickidx 0
					} $UP {
						set search "[lindex $cur 0],*"
						set pick "end"
						set pickidx 1
					} $DOWN {
						set search "[lindex $cur 0],*"
						set pick "0"
						set pickidx 1
					} default {
						error "pakin $dir"
					}
				set candidates [lmap cor [array names map -glob $search] {split $cor ","}]
				set newpos [lindex [lsort -integer -index $pickidx $candidates] $pick]
				set newposKey [join $newpos ","]
			}
			if {![info exists map($newposKey)]} { error "ll" }
			if {$map($newposKey) == "#"} {
				break
			} elseif {$map($newposKey) == "."} {
				if {$newpos == $cur} { error "." }
				set cur $newpos
			} else {
				error "panik"
			}
		}
	} else {
		set newdir [expr { $op == "R" ? 1 : -1 }]
		set dir [expr { $dir + $newdir }]
		set dir [expr { $dir < 0 ? ([llength $dirv] - 1) : ($dir % [llength $dirv]) }]
	}
}

set finalrow [expr { [lindex $cur 1] + 1 }]
set finalcol [expr { [lindex $cur 0] + 1 }]
set finaldir $dir
puts "$finalrow - $finalcol - $finaldir => [expr { ($finalrow * 1000) + ($finalcol * 4) + $finaldir }]"
