#!/usr/bin/tclsh

for {set y 0} {[gets stdin line] >= 0} {incr y} {
	for {set x 0} {$x < [string len $line]} {incr x} {
		if {[string index $line $x] == "#"} {
			set map($x,$y) 1
		}
	}
}

proc gimme_neighbours {x y} {
	lappend d 0 -1 1 -1 -1 -1 ;# N NE NW
	lappend d 0 1 1 1 -1 1 ;# S SE SW
	lappend d -1 0 -1 -1 -1 1; # W NW SW
	lappend d 1 0 1 -1 1 1 ;# E NE SE
	foreach {dx dy} $d {
		lappend ret [expr {$x + $dx}] [expr {$y + $dy}]
	}
	return $ret
}

lappend movLUT [list 0b111111111111 0 0]  ;# go nowhere
lappend movLUT [list 0b000000000111 0 -1] ;# go north
lappend movLUT [list 0b000000111000 0 1]  ;# go south
lappend movLUT [list 0b000111000000 -1 0] ;# go west
lappend movLUT [list 0b111000000000 1 0]  ;# go east

proc simulate_npcs {mapName movLUTName} {
	upvar $mapName map $movLUTName movLUT

	# 1st half of the round
	foreach {coord} [array names map] {
		lassign [split $coord ","] x y
		lassign { 0 0 } msk i
		foreach {nx ny} [gimme_neighbours $x $y] {
			if {![info exists map($nx,$ny)]} {
				set msk [expr {$msk | (1 << $i)}]
			}
			incr i
		}
		set px $x
		set py $y
		foreach {consider} $movLUT {
			lassign $consider cmsk dx dy
			if {($cmsk & $msk) == $cmsk} {
				set px [expr {$x + $dx}]
				set py [expr {$y + $dy}]
				break
			}
		}
		if {$px == $x && $py == $y} {
			# this elf knows his place in the world
			# and is exactly where he wants to be.
		} else {
			# unhappy elf. wants to go to some other place to seek happiness.
			if {[info exists map($px,$py)]} { error "panik" }
			lappend propose($px,$py) $coord
		}
	}

	# 2nd half
	foreach {pos candidates} [array get propose] {
		if {[info exists map($pos)]} { error "panik" }
		if {[llength $candidates] > 1} {
			continue
		} else {
			unset map($candidates)
			set map($pos) 1
		}
	}

	# shuffle the movement priorities
	set movLUT [list [lindex $movLUT 0] {*}[lrange $movLUT 2 end] [lindex $movLUT 1]]

	if {[array size propose] == 0} {
		return -1
	} else {
		return 1
	}
}

for {set i 0} {!([info exists p1] && [info exists p2])} {incr i} {
	if {[simulate_npcs map movLUT] < 0} {
		set p2 [expr {$i + 1}]
	}
	if {$i == 9} {
		set min [list  99999  99999]
		set max [list -99999 -99999]
		foreach {coord} [array names map] {
			set cur [split $coord ","]
			set min [lmap m $min c $cur { tcl::mathfunc::min $m $c }]
			set max [lmap m $max c $cur { tcl::mathfunc::max $m $c }]
		}
		set wh [lmap a $max b $min { expr { ($a - $b) + 1 } }]
		set p1 [expr "([join $wh "*"]) - [array size map]"]
	}
}
puts "part1: $p1"
puts "part2: $p2"
