#include <assert.h>
#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>

#define ABS(X) ((X) > 0 ? (X) : -(X))
#define ARRLEN(x) (sizeof(x) / sizeof(0[x]))

static void
update_tpos(int32_t t[static 2], const int32_t h[static 2])
{
	int32_t d[2] = { h[0] - t[0], 1[h] - 1[t] };
	if (ABS(d[0]) > 1 || ABS(d[1]) > 1) {
		t[0] += (d[0] > 0) - (d[0] < 0);
		t[1] += (d[1] > 0) - (d[1] < 0);
	}
}

static void
update_uniq(const int32_t t[static 2], uint64_t *uniq, uint8_t *map, size_t mlen)
{
	enum { sh = 10, off = ~(~0u << (sh - 1)) };
	assert(t[0] > -off && t[0] < off);
	assert(t[1] > -off && t[1] < off);
	uint64_t v = ((uint64_t)(t[0] + off) << sh) |
	             ((uint64_t)(t[1] + off) << 0);
	uint64_t idx = v / sizeof *map;
	uint64_t msk = 1ul << (v % sizeof *map);
	assert(idx < mlen);
	if (!(map[idx] & msk)) {
		++*uniq;
	}
	map[idx] |= msk;
}

extern int
main(void)
{
	int32_t hpos[2] = {0};
	int32_t tpos[9][2] = {0};
	uint64_t uniq[2] = {0};
	static uint8_t map[2][1ul << 21];

	for (char buf[32]; fgets(buf, sizeof buf, stdin); /* no-op */) {
		int32_t mov[2], cnt = 0;
		switch (buf[0]) {
		case 'R': mov[0] =  1; mov[1] =  0; break;
		case 'L': mov[0] = -1; mov[1] =  0; break;
		case 'U': mov[0] =  0; mov[1] = -1; break;
		case 'D': mov[0] =  0; mov[1] =  1; break;
		default: __builtin_unreachable(); break;
		}
		for (char *p = buf + 2; *p != '\n'; ++p) {
			cnt = (cnt * 10) + (*p - '0');
		}

		for (int32_t i = 0; i < cnt; ++i) {
			const int32_t (*p)[2] = &hpos;
			hpos[0] += mov[0]; hpos[1] += mov[1];
			for (size_t k = 0; k < ARRLEN(tpos); ++k) {
				update_tpos(tpos[k], *p);
				p = tpos + k;
			}
			update_uniq(tpos[0], uniq + 0, map[0], ARRLEN(map[0]));
			update_uniq(tpos[8], uniq + 1, map[1], ARRLEN(map[1]));
		}
	}
	printf("part1: %"PRIu64"\npart2: %"PRIu64"\n", uniq[0], uniq[1]);
}
