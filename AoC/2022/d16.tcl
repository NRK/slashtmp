#!/usr/bin/tclsh

lassign { 0 1 2 } VALVE FLOW GOESTO

while {[gets stdin line] >= 0} {
	scan $line "Valve %s has flow rate=%d;" v flow
	regexp -all -line {.*to valves? (.*)} $line _ goesTo
	set goesTo [split [string map { ", " "#" } $goesTo] "#"]
	set vflow($v) $flow
	set vgoesTo($v) $goesTo
}

proc mkpath {mapName vgoesToName from toList} {
	upvar $mapName map $vgoesToName vgoesTo
	array set seen [list $from 1]
	set q [list $vgoesTo($from)]

	for {set n 0} {[llength $toList] > 0} {incr n} {
		set curq [lindex $q $n]
		lappend q [list]
		foreach {node} $curq {
			set idx [lsearch $toList $node]
			if {$idx >= 0} {
				set toList [lreplace $toList $idx $idx]
				set map($from,$node) [expr {$n + 1}]
				set map($node,$from) [expr {$n + 1}]
				set seen($node) 1
			}
			foreach {new} $vgoesTo($node) {
				if {![info exists seen($new)]} {
					lset q end end+1 $new
					set seen($new) 1
				}
			}
		}
	}
}

set gud_targets [lmap {v f} [array get vflow] { expr { $f > 0 ? $v : [continue] } }]

array set map {}
for {set i 0} {$i < [llength $gud_targets]} {incr i} {
	set cur [expr {$i == 0 ? "AA" : [lindex $gud_targets $i-1]}]
	mkpath map vgoesTo $cur [lrange $gud_targets $i end]
}

array set cache {}
proc tellme_max {start t openv} {
	global map vflow gud_targets cache

	set key "$start,$t,$openv"
	if {[info exists cache($key)]} {
		return $cache($key)
	}

	set max 0
	for {set i 0} {$i < [llength $gud_targets]} {incr i} {
		if {$openv & (1 << $i)} {
			continue
		}

		set to [lindex $gud_targets $i]
		set dist $map($start,$to)
		if {$dist + 2 < $t} {
			set flo $vflow($to)
			set trem [expr {$t - ($dist + 1)}]
			set score [expr {$flo * $trem}]

			set tmp_openv [expr {$openv | (1 << $i)}]
			set n [tellme_max $to $trem $tmp_openv]
			set max [expr { max($max, $n + $score) }]
		}
	}
	set cache($key) $max
	return $max
}

puts [tellme_max "AA" 30 0x0]

set vmax [expr { (2 ** [llength $gud_targets]) - 1 }]
set p2 0
for {set v 0} {$v < $vmax} {incr v} {
	set score [expr { [tellme_max "AA" 26 $v] + [tellme_max "AA" 26 [expr {~$v & $vmax}]] }]
	set p2 [tcl::mathfunc::max $p2 $score]
	if {$v % 2048 == 0} { puts "i'm wurking on it, senpai! >_<" }
}
puts "part2: $p2"
