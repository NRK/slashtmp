#!/usr/bin/tclsh

set min { 9999 9999 9999 }
set max { 0 0 0 }
while {[gets stdin line] >= 0} {
	scan $line "%d,%d,%d" x y z
	set min [lmap a $min b [list $x $y $z] { tcl::mathfunc::min $a $b }]
	set max [lmap a $max b [list $x $y $z] { tcl::mathfunc::max $a $b }]
	set map($x,$y,$z) 1
}

proc gimme_neighbours {x y z} {
	foreach {d} { { 1 0 0 } { -1 0 0 } { 0 1 0 } { 0 -1 0 } { 0 0 1 } { 0 0 -1 } } {
		lappend ret {*}[lmap a $d b [list $x $y $z] { expr {$a + $b} }]
	}
	return $ret
}

set t0 0
foreach {coord _} [array get map] {
	scan $coord "%d,%d,%d" x y z
	foreach {nx ny nz} [gimme_neighbours $x $y $z] {
		incr t0 [expr { ![info exists map($nx,$ny,$nz)] }]
	}
}
puts "part1: $t0"

lassign $min minx miny minz
lassign $max maxx maxy maxz
array set seen [list "$minx,$miny,$minz" 1]
set q [list [list $minx $miny $minz]]
for {set n 0} {$n < [llength $q]} {incr n} {
	lassign [lindex $q $n] x y z
	foreach {nx ny nz} [gimme_neighbours $x $y $z] {
		set nkey "$nx,$ny,$nz"
		if {[info exists map($nkey)]} {
			incr t1
		} elseif {$nx < $minx-1 || $ny < $miny-1 || $nz < $minz-1 || \
		          $nx > $maxx+1 || $ny > $maxy+1 || $nz > $maxz+1 || \
		          [info exists seen($nkey)]} \
		{
			# either outside or already seen
		} else {
			set seen($nkey) 1
			lappend q [list $nx $ny $nz]
		}
	}
}
puts "part2: $t1"
