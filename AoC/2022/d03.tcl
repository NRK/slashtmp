#!/usr/bin/tclsh

proc ch2priority {ch} {
	set u [string is upper $ch]
	return [expr { [scan $ch "%c"] - [lindex { 96 38 } $u] }]
}

set m 0x1
lassign {0 0 53} t0 t1 max_chp
set map1 [set map2 [lrepeat $max_chp 0]]
while {[gets stdin line] >= 0} {
	set n [expr {[string len $line] / 2 - 1}]
	for {set i 0} {$i < [string len $line]} {incr i} {
		set chp [ch2priority [string index $line $i]]
		# part1
		if {$i <= $n} {
			lset map1 $chp 1
		} elseif {[lindex $map1 $chp] > 0} {
			incr t0 $chp
			lset map1 $chp 0
		}
		#part2
		lset map2 $chp [expr { [lindex $map2 $chp] | $m }]
	}
	set map1 [lrepeat $max_chp 0]

	set m [expr {$m << 1}]
	if {$m == 0x8} {
		for {set i 0} {$i < [llength $map2]} {incr i} {
			if {[lindex $map2 $i] == 0x7} {
				incr t1 $i
			}
		}

		set m 0x1
		set map2 [lrepeat $max_chp 0]
	}
}

puts "part1: $t0"
puts "part2: $t1"
