#!/usr/bin/tclsh

set groups [regexp -all -inline {\d+(?:\n\d+)*} [read stdin]]
set groups [lmap l $groups { expr [join $l "+"] }]
set l [lrange [lsort -integer -decreasing $groups] 0 2]
puts "part1: [lindex $l 0]"
puts "part2: [expr [join $l "+"]]"
