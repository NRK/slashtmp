/* gcc -DNDEBUG -fopenmp -march=native -Ofast d07.c */
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <assert.h>

#define ARRLEN(x) (sizeof(x) / sizeof(0[x]))
#define MIN(A, B) ((A) < (B) ? (A) : (B))

#ifndef WISH
	#define WISH 30000000
#endif
#ifndef TOTAL
	#define TOTAL 70000000
#endif

static uint64_t sizes[1 << 18];
static uint64_t sizes_head;

static int
sz_alloc(void)
{
	assert(sizes_head < ARRLEN(sizes));
	return sizes_head++;
}

extern int
main(void)
{
	static int dirstack[1 << 12] = {0};
	static char stdio_buf[1 << 14], buf[256];
	size_t dhead = 0;

	setvbuf(stdin, stdio_buf, _IOFBF, sizeof stdio_buf);
	while (fgets(buf, sizeof buf, stdin)) {
		switch (buf[0]) {
		case '$':
			if (buf[2] == 'c') { /* cd */
				if (buf[5] == '.' && buf[6] == '.') {
					assert(dhead > 0);
					--dhead;
				} else {
					assert(dhead < ARRLEN(dirstack));
					dirstack[dhead++] = sz_alloc();
				}
			} /* else ls */
			break;
		case 'd': /* dir */
			/* no-op */
			break;
		default:
		{ /* size file */
			long n = 0;
			for (char *p = buf; *p != ' '; ++p) {
				n = (n * 10) + (*p - '0');
			}
			for (size_t i = 0; i < dhead; ++i) {
				sizes[dirstack[i]] += n;
			}
		} break;
		}
	}

	uint64_t free_space = TOTAL - sizes[0];
	uint64_t sum = 0, smol = (uint64_t)-1;
	#pragma omp parallel for reduction(+: sum) reduction(min: smol)
	for (size_t i = 0; i < (size_t)sizes_head; ++i) {
		if (sizes[i] <= 100000) {
			sum += sizes[i];
		}
		if ((free_space + sizes[i]) >= WISH) {
			smol = MIN(sizes[i], smol);
		}
	}
	printf("part1: %"PRIu64"\npart2: %"PRIu64"\n", sum, smol);
}
