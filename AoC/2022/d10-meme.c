#include <stdio.h>
#include <stdlib.h>

extern int
main(void)
{
	int q[2], qh = 0, o = 20, c = 1, X = 1, t = 0, v;
	for (char b[128]; fgets(b, sizeof b, stdin); qh = 0) {
		switch (*b) {
		case 'a': q[++qh, 1] = atoi(b+5);
		case 'n': q[++qh, 0] = 0;
		}
		for (size_t i = 0; i < qh; X += i++[q], ++c) {
			t += (c * X) * (c == o);
			o += 40 * (c == o);
			putchar((char []){' ', '#'}[(v = (c-1)%40) >= X-1 && v <= X+1]);
			fputs((char *[]){"", "\n"}[c % 40 == 0], stdout);
		}
	}
	printf("p1: %d\n", t);
}
