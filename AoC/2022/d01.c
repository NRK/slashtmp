#include <stdio.h>
#include <stdlib.h>

#define ARRLEN(x) (sizeof(x) / sizeof(0[x]))
#define SWAP(a, b, tmp) ((tmp) = (a), (a) = (b), (b) = (tmp))

extern int
main(int argc, char *argv[])
{
	char buf[128];
	int top[4] = {0}, *current = top + (ARRLEN(top) - 1);

	for (int done = 0; !done; /* no-op */ ) {
		done = fgets(buf, sizeof buf, stdin) == NULL;
		if (done || buf[0] == '\n') {
			for (size_t i = 0; i < ARRLEN(top); ++i) {
				if (top[i] < top[ARRLEN(top) - 1]) {
					int tmp;
					SWAP(top[i], top[ARRLEN(top) - 1], tmp);
				}
			}
			*current = 0;
		} else {
			*current += atoi(buf);
		}
	}

	printf("part1: %d\n", top[0]);
	printf("part2: %d\n", top[0] + top[1] + top[2]);
}
