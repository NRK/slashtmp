#!/usr/bin/tclsh

# code used to try and understand how this truly works
# ref: https://eduherminio.github.io/blog/rock-paper-scissors/
#
# array set score {
# 	"1,1" "D" "1,2" "L" "1,3" "W"
# 	"2,1" "W" "2,2" "D" "2,3" "L"
# 	"3,1" "L" "3,2" "W" "3,3" "D"
# }
# for {set i 1} {$i <= 3} {incr i} {
# 	for {set k 1} {$k <= 3} {incr k} {
# 		puts "[expr {($i - $k + 4) % 3}] $score($i,$k)"
# 	}
# }

lassign {0 0} total1 total2
while {[gets stdin line] >= 0} {
	scan $line "%c %c" op me
	set op [expr {$op - [scan "A" "%c"] + 1}]
	set me [expr {$me - [scan "X" "%c"] + 1}]

	incr total1 [expr { (($me - $op + 4) % 3) * 3 + $me }]

	set pick [expr { ($me + $op) % 3 + 1 }]
	incr total2 [expr { $pick + (($me - 1) * 3) }]
}
puts "part1: $total1"
puts "part2: $total2"
