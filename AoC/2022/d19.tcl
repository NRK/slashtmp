#!/usr/bin/tclsh

package require Thread

set threadRoutines {

proc is_gt_or_eq {thisv thatv} {
	foreach {this} $thisv {that} $thatv {
		if {$this < $that} { return false }
	}
	return true
}

proc trig {n} {
	return [expr { ($n * ($n+1)) / 2 }]
}

proc dfs {t tEnd myRes myRobos bluep maxProfitableSlaves cacheName maxCacheName maxsofarName} {
	lassign { 0 1 2 3 } ORE CLAY OBD GEO
	upvar $cacheName cache $maxCacheName maxCache $maxsofarName maxsofar

	if {$t >= $tEnd} {
		return [lindex $myRes $GEO]
	}
	set key "$t,{$myRes},{$myRobos}"
	set pkey "$t,{$myRobos}"
	if {[info exists cache($key)]} {
		return $cache($key)
	}
	if {[info exists maxCache($pkey)] && [is_gt_or_eq $maxCache($pkey) $myRes]} {
		return [lindex $myRes $GEO]
	} else {
		set maxCache($pkey) $myRes
	}
	set trem [expr {$tEnd - $t}]
	set theoreticalMax [expr { [lindex $myRes $GEO] + \
		($trem * [lindex $myRes $GEO]) + [trig $trem] }]
	if {$theoreticalMax < $maxsofar} {
		return [lindex $myRes $GEO]
	}

	set max [lindex $myRes $GEO]
	# increase resources
	set newRes [lmap r $myRes s $myRobos { expr $r + $s }]
	set newT [expr {$t + 1}]

	# start building a robo if possible
	for {set i [expr { [llength $bluep] - 1 }]} {$i >= 0} {incr i -1} {
		set cost [lindex $bluep $i]
		if {[is_gt_or_eq $myRes $cost] && \
		    [lindex $myRobos $i] < [lindex $maxProfitableSlaves $i]} \
		{
			set newRobos $myRobos
			lset newRobos $i [expr {[lindex $myRobos $i] + 1}]
			set newResAfterPurchase [lmap r $newRes c $cost {
				expr { $r - $c }
			}]
			set score [dfs $newT $tEnd $newResAfterPurchase $newRobos \
				$bluep $maxProfitableSlaves \
				$cacheName $maxCacheName $maxsofarName]
			set max [expr {$max > $score ? $max : $score}]
			if {$i == $GEO} {
				# NOTE: it was pointed out in the aoc solution
				# mega-thread that this is not actually
				# correct. there can be situation where buying
				# a geode robo might not be optimal.

				# if i can buy a geode, it's always advantageous to do so.
				# so prune the rest of this branch.
				break
			}
		}
	}
	# branch when i didn't purchase any robo and am saving up.
	# but don't take this branch if i *could have* bought a geode robo.
	if {![is_gt_or_eq $myRes [lindex $bluep $GEO]]} {
		set score [dfs $newT $tEnd $newRes $myRobos \
			$bluep $maxProfitableSlaves \
			$cacheName $maxCacheName $maxsofarName]
		set max [expr {$max > $score ? $max : $score}]
	}
	# only cache the lower ends of the tree where things should be growing
	# at an exponential rate
	if {$t > ($tEnd - 14)} {
		set cache($key) $max
	}
	set maxsofar [expr {$max > $maxsofar ? $max : $maxsofar}]
	return $max
}

proc run_dfs {tEnd bp} {
	set maxsofar 0
	set maxProfitableSlaves [list]
	for {set i 0} {$i < [llength $bp]} {incr i} {
		lappend maxProfitableSlaves [
			lindex [lsort -integer [lmap b $bp { lindex $b $i }]] end
		]
	}
	lset maxProfitableSlaves end 9999 ;# geode bots are always profitable
	set ret [dfs 0 $tEnd {0 0 0 0} {1 0 0 0} $bp \
		$maxProfitableSlaves cache maxCache maxsofar]
	return $ret
}

} ;# end threadRoutines

while {[gets stdin line] >= 0} {
	lassign [regexp -all -inline {\d+} $line] _ oreCost clayCost \
		obCostOre obCostClay gCostOre gCostOb
	set b [list]
	lappend b [list $oreCost  0 0 0] ;# ore robo
	lappend b [list $clayCost 0 0 0] ;# clay robo
	lappend b [list $obCostOre $obCostClay 0 0] ;# ob robo
	lappend b [list $gCostOre 0 $gCostOb 0] ;# geode robo
	lappend bluepv $b
}

set tpool [tpool::create -minworkers 8 -maxworkers 10 -idletime 60]
for {set i 0} {$i < [llength $bluepv]} {incr i} {
	set bp [lindex $bluepv $i]
	set id [expr { $i + 1 }]
	lappend jobs0 [tpool::post -nowait $tpool [subst -nobackslashes -nocommands {
		$threadRoutines
		return [expr { $id * [run_dfs 24 {$bp}] }]
	}]]
	if {$i < 3} {
		lappend jobs1 [tpool::post -nowait $tpool [subst -nobackslashes -nocommands {
			$threadRoutines
			return [run_dfs 32 {$bp}]
		}]]
	}
}

foreach {jb} $jobs0 {
	tpool::wait $tpool $jb
	incr t0 [tpool::get $tpool $jb]
}
puts "part1: $t0"

foreach {jb} $jobs1 {
	tpool::wait $tpool $jb
	lappend t1 [tpool::get $tpool $jb]
}
puts "part2: [expr [join $t1 "*"]]"
