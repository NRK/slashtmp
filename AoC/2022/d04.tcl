#!/usr/bin/tclsh

lassign {0 0} t0 t1
while {[gets stdin line] >= 0} {
	scan $line "%d-%d,%d-%d" a0 a1 b0 b1
	incr t0 [expr { ($a0 >= $b0 && $a1 <= $b1) || ($b0 >= $a0 && $b1 <= $a1) }]
	incr t1 [expr { $b0 <= $a1 && $b1 >= $a0 }]
}
puts "part1: $t0"
puts "part2: $t1"
