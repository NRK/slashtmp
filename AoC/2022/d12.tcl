#!/usr/bin/tclsh

set map [lmap ln [split [read -nonewline stdin] "\n"] { split $ln {} }]
set rowcnt [llength $map]
set colcnt [llength [lindex $map 0]]

for {set r 0} {$r < $rowcnt} {incr r} {
	for {set c 0} {$c < $colcnt} {incr c} {
		set cur [lindex $map $r $c]
		if {[set v [expr {$cur == "S" | (($cur == "E") << 1)}]]} {
			set [lindex {src dst} $v-1] [list $r $c]
			set cur [lindex {a z} $v-1]
		}
		lset map $r $c [scan $cur "%c"]
	}
}

set q [list [list $dst]] ;# traverse backwards to solve both parts at once
array set seen [list [join $dst ","] 1]
for {set n 0} {!([info exists p1] && [info exists p2])} {incr n} {
	set curq [lindex $q $n]
	lappend q [list]
	foreach {pos} $curq {
		set curlvl [lindex $map {*}$pos]
		if {$pos == $src  && ![info exists p1]} { set p1 $n }
		if {$curlvl == 97 && ![info exists p2]} { set p2 $n }

		foreach {diff} { {-1 0} {1 0} {0 -1} {0 1} } {
			lassign [lmap p $pos d $diff { expr $p + $d }] r c
			if {$r >= 0 && $r < $rowcnt && $c >= 0 && $c < $colcnt && \
			    [lindex $map $r $c]+1 >= $curlvl && \
			    ![info exists seen($r,$c)]} \
			{
				lset q end end+1 [list $r $c]
				set seen($r,$c) 1
			}
		}
	}
}
puts "part1: $p1"
puts "part2: $p2"
