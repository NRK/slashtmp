#!/usr/bin/tclsh

array set pscore { "A" 1 "B" 2 "C" 3 }
array set x2a { "X" "A" "Y" "B" "Z" "C" }
array set wscore {
	"AA" 3 "AB" 6 "AC" 0
	"BA" 0 "BB" 3 "BC" 6
	"CA" 6 "CB" 0 "CC" 3
}

array set p2wscore { "X" 0 "Y" 3 "Z" 6 }
array set p2score {
	"AX" "C" "AY" "A" "AZ" "B"
	"BX" "A" "BY" "B" "BZ" "C"
	"CX" "B" "CY" "C" "CZ" "A"
}

set total1 0
set total2 0
while {[gets stdin line] >= 0} {
	set op [string index $line 0]
	set me [string index $line 2]
	set mea $x2a($me)

	set n [expr {$wscore($op$mea) + $pscore($mea)}]
	incr total1 $n

	set me1 $p2score($op$me)
	incr total2 [expr {$pscore($me1) + $p2wscore($me)}]
}
puts "part1: $total1"
puts "part2: $total2"
