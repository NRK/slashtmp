#!/usr/bin/tclsh

set input [read stdin]
set input [regsub -all {\n([[:digit:]])} $input "+\\1"]
set input "list [regsub -all -line "^(.*)\n" $input {[expr {\1}] }]"
set l [lsort -integer -decreasing [eval $input]]
puts "part1: [lindex $l 0]"
puts "part2: [expr {[lindex $l 0] + [lindex $l 1] + [lindex $l 2]}]"
