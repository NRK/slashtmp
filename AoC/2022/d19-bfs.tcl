#!/usr/bin/tclsh

package require Thread

set threadRoutines {

lassign { 0 1 2 3 } ORE CLAY OBS GEO

proc is_gt_or_eq {thisv thatv} {
	foreach {this} $thisv {that} $thatv {
		if {$this < $that} { return false }
	}
	return true
}

proc trig {n} {
	return [expr { ($n * ($n+1)) / 2 }]
}

proc is_state_suboptimal {t res robos tEnd maxsofar maxCacheName {strict false}} {
	upvar $maxCacheName maxCache

	set trem [expr {$tEnd - $t}]
	set theoreticalMax [expr {
		[lindex $res $::GEO] + ($trem * [lindex $robos $::GEO]) + [trig $trem]
	}]
	if {$theoreticalMax < $maxsofar} {
		return true
	}

	foreach {key val} [list "$t,{$robos}" $res "$t|{$res}" $robos] {
		if {[info exists maxCache($key)] && \
		    [is_gt_or_eq $maxCache($key) $val]} \
		{
			if {$strict} {
				if {$maxCache($key) != $val} {
					return true
				}
			} else {
				return true
			}
		} else {
			set maxCache($key) $val
		}
	}

	return false
}

proc qappend {qName state tEnd maxsofar maxCacheName} {
	upvar $qName q $maxCacheName maxCache
	lassign $state t res robos
	if {![is_state_suboptimal $t $res $robos $tEnd $maxsofar $maxCacheName]} {
		lappend q $state
	}
}

proc solve_blueprint {bp tEnd maxProfitableSlaves} {
	set maxsofar 0
	array set maxCache {}
	set q [list [list 0 { 0 0 0 0 } { 1 0 0 0 } 0xF]]

	for {set n 0} {[llength $q] > 0 && $n < [llength $q]} {} {
		set curState [lindex $q $n]
		incr n

		lassign $curState t res robos buyMask

		# periodically trim the queue
		if {$n >= (1 << 15)} {
			set q [lrange $q $n+1 end]
			set n 0
		}

		if {$t == $tEnd} {
			set geodes_collected [lindex $res $::GEO]
			if {$geodes_collected > $maxsofar} {
				set maxsofar $geodes_collected
			}
			continue
		}

		if {[is_state_suboptimal $t $res $robos $tEnd $maxsofar maxCache true]} {
			continue
		}

		# current robos generating the stuff
		set newRes [lmap r $res s $robos { expr $r + $s }]
		set tNext [expr {$t + 1}]

		# buy some robos
		set afford_mask 0x0
		for {set k 0} {$k < [llength $bp]} {incr k} {
			set roboCost [lindex $bp $k]
			set canAfford [expr {[is_gt_or_eq $res $roboCost] ? 1 : 0}]
			set afford_mask [expr { $afford_mask | ($canAfford << $k) }]
			if {$canAfford && ($buyMask & (1 << $k)) && \
			    [lindex $robos $k] < [lindex $maxProfitableSlaves $k]} \
			{
				# can afford and it's (possibly) profitable
				set tmp_res [lmap r $newRes c $roboCost {
					expr { $r - $c }
				}]

				set tmp_robos $robos
				lset tmp_robos $k [expr { [lindex $tmp_robos $k] + 1 }]

				qappend q [list $tNext $tmp_res $tmp_robos 0xF] \
					$tEnd $maxsofar maxCache
			}
		}
		# buy nothing branch
		# but don't buy nothing if we can afford all the possible robos
		if {$afford_mask != 0xF} {
			# also don't buy stuff we could've afforded
			set afford_mask [expr {~$afford_mask & 0xF}]
			qappend q [list $tNext $newRes $robos $afford_mask] \
				$tEnd $maxsofar maxCache
		}
	}

	return $maxsofar
}

proc solve {tEnd bp} {
	set maxProfitableSlaves [list]
	for {set i 0} {$i < [llength $bp]} {incr i} {
		lappend maxProfitableSlaves [
			lindex [lsort -integer [lmap b $bp { lindex $b $i }]] end
		]
	}
	lset maxProfitableSlaves end 999999 ;# geode bots are always profitable
	set ret [solve_blueprint $bp $tEnd $maxProfitableSlaves]
	return $ret
}

} ;# end threadRoutines

while {[gets stdin line] >= 0} {
	lassign [regexp -all -inline {\d+} $line] _ oreCost clayCost \
		obCostOre obCostClay gCostOre gCostOb
	set b [list]
	lappend b [list $oreCost  0 0 0] ;# ore robo
	lappend b [list $clayCost 0 0 0] ;# clay robo
	lappend b [list $obCostOre $obCostClay 0 0] ;# ob robo
	lappend b [list $gCostOre 0 $gCostOb 0] ;# geode robo
	lappend bluepv $b
}

set tpool [tpool::create -minworkers 8 -maxworkers 10 -idletime 60]
for {set i 0} {$i < [llength $bluepv]} {incr i} {
	set bp [lindex $bluepv $i]
	set id [expr { $i + 1 }]
	lappend jobs0 [tpool::post -nowait $tpool [subst -nobackslashes -nocommands {
		$threadRoutines
		return [expr { $id * [solve 24 {$bp}] }]
	}]]
	if {$i < 3} {
		lappend jobs1 [tpool::post -nowait $tpool [subst -nobackslashes -nocommands {
			$threadRoutines
			return [solve 32 {$bp}]
		}]]
	}
}

foreach {jb} $jobs0 {
	tpool::wait $tpool $jb
	incr t0 [tpool::get $tpool $jb]
}
puts "part1: $t0"

foreach {jb} $jobs1 {
	tpool::wait $tpool $jb
	lappend t1 [tpool::get $tpool $jb]
}
puts "part2: [expr [join $t1 "*"]]"
