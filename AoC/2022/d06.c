/* gcc -march=native -O3 d06.c */
#include <stdio.h>
#include <stdint.h>

static size_t
funiq(const char *buf, size_t len, int n)
{
	for (size_t i = n - 1; i < len; ++i) {
		uint32_t v = 0;
		for (size_t k = 0; k < n; ++k) {
			v |= UINT32_C(1) << buf[i - k];
		}
		if (__builtin_popcount(v) == n) {
			return i + 1;
		}
	}
	__builtin_unreachable();
}

extern int
main(void)
{
	_Alignas(64) static char buf[100000000];
	size_t len = fread(buf, 1, sizeof buf, stdin);
	for (size_t i = 0; i < len; ++i) {
		buf[i] -= 'a';
	}
	size_t n = funiq(buf, len, 4);
	size_t n0 = n - 4;
	printf("part1: %zu\npart2: %zu\n", n, n0 + funiq(buf + n0, len - n0, 14));
}
