#!/usr/bin/tclsh

proc mkcwd {cwd} {
	regsub {^//} [join $cwd "/"] "/"
}

set cwd [list]
while {[gets stdin line] >= 0} {
	switch -regexp -- $line {
	"\\$ cd .*" {
		set dir [string range $line 5 end]
		if {$dir == ".."} {
			set cwd [lreplace $cwd end end]
		} else {
			lappend cwd $dir
		}
	} "\\d+ *" {
		set sz [regexp -inline {\d+} $line]
		for {set i 0} {$i < [llength $cwd]} {incr i} {
			incr map([mkcwd [lrange $cwd 0 $i]]) $sz
		}
	}
	}
}

lassign { 0 70000000 30000000 } p0 total wish
set free [expr { $total - $map(/) }]
foreach {dir sz} [lsort -integer -index 1 -stride 2 [array get map]] {
	if {$sz <= 100000} {
		incr p0 $sz
	}
	if {$free + $sz >= $wish && ![info exists p1]} {
		set p1 $sz
	}
}
puts "part1: $p0"
puts "part2: $p1"
