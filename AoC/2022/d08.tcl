#!/usr/bin/tclsh

set grid [split [read stdin] "\n"]
set grid [lreplace [lmap x $grid { split $x {} }] end end]

lassign { 0 0 } t0 t1
for {set i 0} {$i < [llength $grid]} {incr i} {
	for {set k 0} {$k < [llength $grid]} {incr k} {
		if {$i == 0 || $i+1 == [llength $grid] || $k == 0 || $k+1 == [llength $grid]} {
			incr t0
		} else {
			set cur [lindex $grid $i $k]
			set r [lrange [lindex $grid $i] $k+1 end]
			set l [lreverse [lrange [lindex $grid $i] 0 $k-1]]
			set t [lmap x [lreverse [lrange $grid 0 $i-1]] { lindex $x $k }]
			set b [lmap x [lrange $grid $i+1 end] { lindex $x $k }]
			set low 99999
			set p2s [list]
			foreach {x} [list $r $l $t $b] {
				set low [tcl::mathfunc::min $low \
				        [lindex [lsort -integer -decreasing $x] 0]]

				for {set idx 0} {$idx < [llength $x]} {incr idx} {
					if {[lindex $x $idx] >= $cur || $idx+1 == [llength $x]} {
						lappend p2s [expr {$idx + 1}]
						break;
					}
				}
			}

			set t1 [tcl::mathfunc::max $t1 [expr [join $p2s "*"]]]
			incr t0 [expr {$low < $cur}]
		}
	}
}
puts "part1: $t0"
puts "part2: $t1"
