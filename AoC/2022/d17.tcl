#!/usr/bin/tclsh

set ops [split [read -nonewline stdin] {}]
set opsHead 0

set rok0 [list [expr 0b000111100]]
set rok1 [list \
	[expr 0b000010000] \
	[expr 0b000111000] \
	[expr 0b000010000] \
]
set rok2 [list \
	[expr 0b000001000] \
	[expr 0b000001000] \
	[expr 0b000111000] \
]
set rok3 [list \
	[expr 0b000100000] \
	[expr 0b000100000] \
	[expr 0b000100000] \
	[expr 0b000100000] \
]
set rok4 [list \
	[expr 0b000110000] \
	[expr 0b000110000] \
]

set rocks { rok0 rok1 rok2 rok3 rok4 }
set rocksLen [llength $rocks]
set stage [list]

proc pull {ops headName} {
	global stage
	upvar $headName head
	set ret [lindex $ops $head]
	set head [expr {($head + 1) % [llength $ops]}]
	return $ret
}

proc print_stage {stage} {
	for {set i [llength $stage]} {$i > 0} {} {
		incr i -1
		set st [expr {([lindex $stage $i] >> 1) & 0x7F}]
		puts "[string map { "1" "#" "0" "." } [format "|%.7b|" $st]]"
	}
	puts "+-------+"
}
proc simrock {rock op {append_idx -6}} {
	global stage
	set rhi [llength $rock]
	set shiftOp [string repeat $op 2]
	for {set i 0} {$i < $rhi} {incr i; incr append_idx} {
		set curStage [lindex $stage $append_idx]
		if {$curStage == ""} { set curStage 0 }
		lappend blockMasks [expr {$curStage | 0b100000001}]
	}
	set blocked false
	foreach {r} $rock {msk} $blockMasks {
		set r [expr "$r $shiftOp 1"]
		if {$r & $msk} {
			set blocked true
			break
		}
	}
	if {!$blocked} {
		set rock [lmap x $rock { expr "$x $shiftOp 1" }]
	}
	return $rock
}

for {set n 0} {!([info exists p1] && [info exists p2])} {incr n} {
	if {$n == 2022} {
		set p1 [llength $stage]
	}

	set pstage [llength $stage]
	set rockIdx [expr {$n % $rocksLen}]
	set rockCurName [lindex $rocks $rockIdx]
	upvar 0 $rockCurName rockCurRev
	set rockCur [lreverse $rockCurRev]
	for {set i 0} {$i < 4} {incr i} { ;# first 4 steps are free of collusion
		set op [pull $ops opsHead]
		set rockCur [simrock $rockCur $op]
	}
	for {set gotBlocked false; set off 0} {!$gotBlocked} {incr off} {
		set nextidx [expr {([llength $stage] - 1) - $off}]
		foreach {r} $rockCur {
			set stageNext [lindex $stage $nextidx]
			if {$stageNext == ""} { set stageNext 0 }
			if {$nextidx < 0 || ($r & $stageNext) != 0} {
				set gotBlocked true
				set append_idx [expr {[llength $stage] - $off}]
				foreach {r} $rockCur {
					unset -nocomplain -- curStage
					if {$append_idx < [llength $stage]} {
					} elseif {$append_idx == [llength $stage]} {
						lappend stage 0
					} else {
						error "PANIK"
					}
					set curStage [lindex $stage $append_idx]
					if {$r & $curStage} { error "PANIK" }
					set newStage [expr {$curStage | $r}]
					lset stage $append_idx $newStage
					incr append_idx
				}
				break
			}
			incr nextidx
		}
		if {!$gotBlocked} {
			set op [pull $ops opsHead]
			set append_idx [expr {[llength $stage] - ($off + 1)}]
			set rockCur [simrock $rockCur $op $append_idx]
		}
	}

	if {![info exists p2trailingTarget] && [llength $stage] > 512} {
		set key [list [lrange $stage end-31 end] $opsHead $rockIdx]
		if {[info exists map($key)]} {
			lassign $map($key) pn ph
			set range [expr {$n - $pn}]
			set height [expr {[llength $stage] - $ph}]
			set cycles [expr {(1000000000000 - $n) / $range}]
			set trailingRocks [expr {(1000000000000 - $n) % $range}]
			set p2tmp [expr {[llength $stage] + ($cycles * $height)}]
			# NOTE: off by one? but i don't get why...
			set p2trailingTarget [expr {$n + ($trailingRocks-1)}]
			set p2tmpHeight [llength $stage]
		} else {
			set map($key) [list $n [llength $stage]]
		}
	}

	if {[info exists p2trailingTarget] && $n == $p2trailingTarget} {
		set trailingHight [expr {[llength $stage] - $p2tmpHeight}]
		set p2 [expr {$p2tmp + $trailingHight}]
	}
}
puts "part1: $p1"
puts "part2: $p2"
