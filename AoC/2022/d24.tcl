#!/usr/bin/tclsh

for {set y 0} {[gets stdin line] >= 0} {incr y} {
	for {set x 0} {$x < [string len $line]} {incr x} {
		lappend map(0,$x,$y) [string index $line $x]
	}
}
set map(w) $x
set map(h) $y

proc sim_blizzards {mapName t} {
	upvar $mapName map
	set h $map(h)
	set w $map(w)
	set tnext [expr {$t + 1}]
	if {[info exists map($tnext,0,0)]} {
		return
	}
	for {set y 0} {$y < $h} {incr y} {
		for {set x 0} {$x < $w} {incr x} {
			set l $map($t,$x,$y)
			if {[lindex $l 0] == "#"} {
				lappend map($tnext,$x,$y) "#"
			} elseif {[lindex $l 0] == "."} {
				# no-op
			} else {
				foreach {b} $l {
					set tx $x
					set ty $y
					switch -- $b {
						"<" {
							set tx [expr {$x - 1}]
						} ">" {
							set tx [expr {$x + 1}]
						} "v" {
							set ty [expr {$y + 1}]
						} "^" {
							set ty [expr {$y - 1}]
						} default {
							error "panik $b"
						}
					}
					if {$tx == $w-1} {
						set tx 1
					} elseif {$tx == 0} {
						set tx [expr {$w - 2}]
					}
					if {$ty == 0} {
						set ty [expr {$h - 2}]
					} elseif {$ty == $h-1} {
						set ty 1
					}
					if {$tx == 0 || $tx == $w-1 || $ty == 0 || $ty == $h-1} {
						error "$tx $ty <-> $w $h"
					}
					lappend map($tnext,$tx,$ty) $b
				}
			}
		}
	}
	for {set y 0} {$y < $h} {incr y} {
		for {set x 0} {$x < $w} {incr x} {
			if {![info exists map($tnext,$x,$y)]} {
				lappend map($tnext,$x,$y) "."
			}
		}
	}
}

proc printmap {mapName t} {
	upvar $mapName map
	set h $map(h)
	set w $map(w)
	puts "T$t:"
	for {set y 0} {$y < $h} {incr y} {
		for {set x 0} {$x < $w} {incr x} {
			puts -nonewline "[lindex $map($t,$x,$y) 0]"
		}
		puts ""
	}
}

proc find_path {x y t mapName} {
	upvar $mapName map
	set h $map(h)
	set w $map(w)
	set startx $x
	set starty $y
	set endy [expr {$h - 1}]
	set endx [expr {$w - 2}]
	array set seen {}
	set destl [list "$endx,$endy" "$startx,$starty" "$endx,$endy"]
	set q [list [list $x $y $t $destl]]
	for {set n 0} {!([info exists p1] && [info exists p2])} {incr n} {
		lassign [lindex $q $n] x y t dest

		lassign [split [lindex $dest 0] ","] destx desty
		if {$x == $destx && $y == $desty} {
			if {[llength $dest] == 3 && ![info exists p1]} {
				set p1 $t
			} elseif {[llength $dest] == 1} {
				set p2 $t
			}
			set dest [lrange $dest 1 end]
		}

		sim_blizzards $mapName $t
		set tnext [expr {$t + 1}]
		foreach {dx dy} { 0 0 -1 0 1 0 0 -1 0 1 } {
			set tx [expr {$x + $dx}]
			set ty [expr {$y + $dy}]
			if {$tx < 0 || $ty < 0 || $tx >= $w || $ty >= $h} {
				continue
			}
			set tmap $map($tnext,$tx,$ty)
			if {[lindex $tmap 0] == "." && ![info exists seen($tnext,$tx,$ty,$dest)]} {
				lappend q [list $tx $ty $tnext $dest]
				set seen($tnext,$tx,$ty,$dest) 1
			}
		}
	}
	puts "part1: $p1"
	puts "part2: $p2"
}

find_path 1 0 0 map
