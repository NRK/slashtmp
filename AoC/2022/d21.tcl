#!/usr/bin/tclsh

proc num_or_solve {monkeysName thing} {
	upvar $monkeysName monkeys
	if {[string is alpha -strict $thing]} {
		return [solve $monkeysName $thing]
	} else {
		return $thing
	}
}

proc solve {monkeysName monky} {
	upvar $monkeysName monkeys
	set task $monkeys($monky)
	if {[llength $task] == 1} {
		return [num_or_solve $monkeysName $task]
	}
	lassign $task lhs op rhs
	set lhs [num_or_solve $monkeysName $lhs]
	set rhs [num_or_solve $monkeysName $rhs]
	# don't laugh, just forcing floating-point arithmetic here...
	return [expr "($lhs + 0.0) $op ($rhs + 0.0)"]
}

proc solve2 {monkeysName lo hi} {
	upvar $monkeysName monkeys
	lassign $monkeys(root) m0 _ m1

	set mid [expr { ($lo + $hi) / 2 }]
	foreach {point} [list $mid $hi $lo] {
		set monkeys(humn) $point
		set diff [expr {abs([solve monkeys $m0] - [solve monkeys $m1])}]
		if {$diff == 0} {
			return $point
		}
		lappend l [list $diff $point]
	}
	lassign [lmap x [lsort -real -index 0 $l] { lindex $x 1 }] lo hi
	return [solve2 $monkeysName $lo $hi]
}

while {[gets stdin line] >= 0} {
	regexp {(.*): (.*)} $line _ monky task
	set monkeys($monky) [split $task " "]
}

puts "part1: [solve monkeys "root"]"
puts "part2: [solve2 monkeys -9999999999999 9999999999999]"
