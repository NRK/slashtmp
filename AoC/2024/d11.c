#include <stdio.h>
#include <stdint.h>
#include <assert.h>

static int
digit_len(uint64_t n)
{
	int r = 0;
	do { ++r; } while (n /= 10);
	return r;
}

static uint64_t
pow10i(int n)
{
	uint64_t r = 1;
	for (int i = 0; i < n; ++i) { r *= 10; }
	return r;
}

static uint64_t
solve(uint64_t n, int steps)
{
	enum { L = 15, SD = 64 - L };
	static struct { uint64_t k, v; } cache[1ul << L];

	if (steps == 0) {
		return 1;
	}

	assert(n < (1ull << 56));
	uint64_t h = ((uint64_t)steps << 57) | n;
	h ^= h >> 33; h *= 1111111111111111111;
	uint64_t insert = h >> SD;
	if (cache[insert].k == h) {
		return cache[insert].v;
	}

	uint64_t ret;
	if (n == 0) {
		ret = solve(1, steps-1);
	} else if (digit_len(n) % 2 == 0) {
		int half = pow10i(digit_len(n) / 2);
		ret = solve(n / half, steps-1) + solve(n % half, steps-1);
	} else {
		ret = solve(n * 2024, steps-1);
	}
	cache[insert].k = h;
	cache[insert].v = ret;
	return ret;
}

extern int
main(void)
{
	uint64_t silver = 0, gold = 0;
	char ignore;
	for (int n; fscanf(stdin, "%d%c", &n, &ignore) > 0;) {
		silver += solve(n, 25);
		gold   += solve(n, 75);
	}
	printf("{%ld, %ld}\n", silver, gold);
}
