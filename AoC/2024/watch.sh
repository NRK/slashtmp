#!/bin/sh

file="${1}"
input="${2}"
entr -nsr "
	{
		set -e
		date '+>>> [%T] <<<'
		time bqn ${file} ${input}
	}
" <<_EOF
${file}
_EOF
