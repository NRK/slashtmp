# 2024

This year I'm using [BQN][] since I wanted to try out an array language.
The implementation I'm using is [CBQN][].

Unlike other years, the solutions this year don't accept `stdin` due to BQN not
having an open `stdin` handle at program startup.
Instead pass the name of the file as an argument to the script:

```console
$ bqn d01.bqn input.txt
```

[BQN]: https://mlochbaum.github.io/BQN
[CBQN]: https://github.com/dzaima/CBQN
