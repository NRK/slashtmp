#!/usr/bin/tclsh

set lines [split [read -nonewline stdin] "\n"]
set eq [lmap ln $lines {
	lmap field [split $ln ": "] {
		expr {$field == {} ? [continue] : $field}
	}
}]

proc dfs {v ops} {
	if {[llength $v] == 2} {
		lassign $v want got
		return [expr {$want == $got ? $want : 0}]
	} else {
		lassign $v want lhs rhs
		foreach op $ops {
			set new [expr "$lhs $op $rhs"]
			set newv [concat [list $want $new] [lrange $v 3 end]]
			if {[dfs $newv $ops]} {
				return $want
			}
		}
		return 0
	}
}

proc bfs {v ops} {
	lassign $v want start
	set v [lrange $v 2 end]
	set Q [list $start]
	foreach num $v {
		set nextQ {}
		foreach item $Q {
			foreach op $ops {
				set n [expr "$num $op $item"]
				if {$n <= $want} { lappend nextQ $n }
			}
		}
		set Q $nextQ
	}
	foreach res $Q {
		if {$res == $want} {
			return $want
		}
	}
	return 0
}

puts [expr [join [lmap e $eq { dfs $e {+ *} }] "+"]]
puts [expr [join [lmap e $eq { bfs $e {+ *} }] "+"]]
