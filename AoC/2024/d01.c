#include <stddef.h>
#include <assert.h>
#include <stdint.h>
#include <stdio.h>

#define ABS(a) ((a) < 0 ? -(a) : (a))

static void
lsdradix(int32_t *v, int count[4][256], ptrdiff_t len, int32_t *tmp)
{
	int32_t *src = v, *dst = tmp;
	for (int k = 0; k < 4; ++k) {
		for (ptrdiff_t acc = 0, i = 0; i < 256; ++i) {
			ptrdiff_t prefix = acc;
			acc += count[k][i];
			count[k][i] = prefix;
		}
		for (ptrdiff_t i = 0; i < len; ++i) {
			int key = (src[i] >> (k*8)) & 0xFF;
			ptrdiff_t want = count[k][key]++;
			dst[want] = src[i];
		}
		int32_t *t = src;
		src = dst;
		dst = t;
	}
}

static int
count(int32_t *v, ptrdiff_t len, int32_t needle)
{
	ptrdiff_t lo = 0, hi = len;
	ptrdiff_t found = -1;
	while (lo < hi) {
		ptrdiff_t mid = (lo + hi) / 2;
		if (v[mid] == needle) {
			found = mid;
			break;
		} else if (v[mid] > needle) {
			hi = mid;
		} else {
			lo = mid + 1;
		}
	}
	int count = 0;
	for (ptrdiff_t i = found-1; i >= 0  && v[i] == needle; --i) { ++count; }
	for (ptrdiff_t i = found;   i < len && v[i] == needle; ++i) { ++count; }
	return count;
}

extern int
main(void)
{
	enum { L = 1 << 10 };
	static int32_t l[L], r[L], scratch[L];
	static int lcount[4][256], rcount[4][256];

	ptrdiff_t lines = 0;
	while (!feof(stdin)) {
		if (fscanf(stdin, "%d %d", l+lines, r+lines) != 2)
			break;
		for (int i = 0; i < 4; ++i) {
			++lcount[i][(l[lines] >> (i*8)) & 0xFF];
			++rcount[i][(r[lines] >> (i*8)) & 0xFF];
		}
		++lines;
	}
	lsdradix(l, lcount, lines, scratch);
	lsdradix(r, rcount, lines, scratch);

	int64_t silver = 0, gold = 0;
	for (ptrdiff_t i = 0; i < lines; ++i) {
		silver += ABS(l[i] - r[i]);
		gold += l[i] * count(r, lines, l[i]);
	}

	printf("Silver: %lld  Gold: %lld\n", (long long)silver, (long long)gold);

	return 0;
}
