#!/usr/bin/luajit

local solve = function(input)
	local v, ret = { unpack(input) }, 0
	while not table.concat(v):match("^0+$") do
		ret = ret + v[#v]
		for i = 1, #v-1 do v[i] = v[i+1] - v[i] end
		v[#v] = nil
	end
	return ret
end

local table_reverse = function(t)
	local ret = {}
	for i = 1, #t do ret[i] = t[(#t + 1) - i] end
	return ret
end

local silver, gold = 0, 0
for ln in io.stdin:lines() do
	local vals = {}
	ln:gsub("(-?%d+)", function(m) table.insert(vals, m) end)
	silver = silver + solve(vals)
	gold = gold + solve(table_reverse(vals))
end
print("silver:", silver)
print("gold:", gold)
