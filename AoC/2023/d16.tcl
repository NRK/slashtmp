#!/usr/bin/tclsh

proc solve {map sr sc dr dc} {
	set Q [list $sr $sc $dr $dc]
	while {[llength $Q] > 0} {
		set Q [lassign $Q r c dr dc]
		set ch [lindex $map [incr r $dr] [incr c $dc]]
		if {$ch == "" || [info exists seen($r,$c,$dr,$dc)]} {
			continue
		}
		set seen($r,$c,$dr,$dc) "$r,$c"
		switch -glob -- "$ch,$dr" {
			".*" - "-,0" - {|,[-1]*} { lappend Q $r $c $dr $dc }
			"/*"  { lappend Q $r $c [expr -$dc] [expr -$dr] }
			{\\*} { lappend Q $r $c $dc $dr }
			"|*"  { lappend Q $r $c 1  0 $r $c -1 0 }
			"-*"  { lappend Q $r $c 0 -1 $r $c  0 1 }
		}
	}
	return [llength [lsort -unique [lmap {_ val} [array get seen] { list $val }]]]
}

set map [lmap x [split [read -nonewline stdin] "\n"] { split $x {} }]
puts "Silver: [solve $map 0 -1 0 1]"

lassign [list [llength $map] [llength [lindex $map 0]]] R C
for {set i 0} {$i < ($R > $C ? $R : $C)} {incr i} {
	lappend perm $i -1 0 1 $i $C 0 -1 -1 $i 1 0 $R $i -1 0
}
puts "Gold:   [lindex [lsort -integer -decreasing [lmap {r c dr dc} $perm { solve $map $r $c $dr $dc }]] 0]"
