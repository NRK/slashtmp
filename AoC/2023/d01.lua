#!/usr/bin/luajit

local solve = function(lines, do_words)
	local ret = 0
	for ln in lines:gmatch("[^\r\n]+") do
		local last
		for i = 1, #ln do
			local n = tonumber(ln:sub(i, i))

			local nums = {
				"one", "two", "three", "four", "five",
				"six", "seven", "eight", "nine"
			}
			if (do_words and n == nil) then
				for k,v in ipairs(nums) do
					local s = ln:sub(i, i + #v - 1)
					if (s == v) then
						n = k
					end
				end
			end

			if (n ~= nil) then
				ret = ret + (last == nil and n*10 or 0)
				last = n
			end
		end

		ret = ret + last
	end
	return ret
end

-- main
local input = io.stdin:read("*all")
print("part 1: " .. solve(input, false))
print("part 2: " .. solve(input, true))
