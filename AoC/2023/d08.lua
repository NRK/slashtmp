#!/usr/bin/luajit

local ins = io.stdin:read():lower()
io.stdin:read() -- skip blank

local map = {}
local starts, cycles = {}, {}
for ln in io.stdin:lines() do
	local node, l, r = ln:match("(%w+).*[(](%w+), (%w+)")
	map[node] = { l = l, r = r }
	if (node:match(".*A$")) then
		starts[#starts + 1] = node
	end
end

local starts_cnt = #starts
local silver = "AAA"
for i = 0, math.huge do
	local idx = (i % ins:len()) + 1
	local dir = ins:sub(idx, idx)

	if (silver ~= nil) then
		silver = map[silver][dir]
		if (silver == "ZZZ") then
			print("silver:", i + 1)
			silver = nil
		end
	end

	for k,v in pairs(starts) do
		starts[k] = map[v][dir]
		if (starts[k]:match(".*Z$")) then
			cycles[#cycles + 1] = i + 1
			starts[k] = nil
		end
	end

	if (#cycles == starts_cnt and silver == nil) then
		break
	end
end

local function gcd(a, b)
	return (b == 0) and a or gcd(b, a % b)
end
local lcm = function(a, b)
	return (a / gcd(a, b)) * b
end

local gold = 1
for k,v in ipairs(cycles) do
	gold = lcm(gold, v)
end
print("gold:", gold)
