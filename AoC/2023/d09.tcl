#!/usr/bin/tclsh

proc solve {v} {
	while {$v != [lrepeat [llength $v] 0]} {
		incr ret [lindex $v end]
		set v [lmap a [lrange $v 0 end-1] b [lrange $v 1 end] { expr {$b - $a } }]
	}
	return $ret
}

foreach ln [split [read -nonewline stdin] "\n"] {
	set v [split $ln " "]
	incr silver [solve $v]
	incr gold [solve [lreverse $v]]
}
puts "silver: $silver \ngold:   $gold"
