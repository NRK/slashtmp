#!/usr/bin/tclsh

proc score {l} {
	for {set i 0} {$i < [llength $l]} {incr i} {
		incr ret [expr {($i+1) * [lindex $l $i 1]}]
	}
	return $ret
}
proc kind {h joker} {
	lmap x [split $h {}] { incr map($x) }
	set jcnt [expr {[info exists map($joker)] ? $map($joker) : 0}]
	array unset map $joker
	set l [lsort -integer -decreasing [lmap {ch num} [array get map] { list $num }]]
	lset l 0 [expr $jcnt + "0[lindex $l 0]"]
	return "0x[string range "[join $l {}]00000" 0 4][string map [list $joker 0 A F K E Q D J C T B] $h]"
}
proc cmp {joker a b} { expr {[kind $a $joker] > [kind $b $joker]} }
proc solve {l {joker 0}} { return [score [lsort -command "cmp $joker" -index 0 $l]] }

set hands [lmap x [split [read -nonewline stdin] "\n"] { split $x }]
puts "silver: [solve $hands]\ngold:   [solve $hands J]"
