#!/usr/bin/tclsh

proc hash {s {h 0}} {
	lmap ch [split $s {}] { set h [expr {(($h + [scan $ch "%c"]) * 17) & 0xFF}] }
	return $h
}

set ins [split [read -nonewline stdin] ","]
puts "silver: [expr [join [lmap x $ins { list [hash $x] }] "+"]]"

array set boxes {}  ;# boxes($lbl) = { slot, focal, epoch }
foreach ins $ins {
	regexp -all {(\w+)([-=])(\d?)} $ins _ lbl op focal
	if {$op == "="} {
		set boxes($lbl) [expr {[info exists boxes($lbl)] ?
			[lreplace $boxes($lbl) 1 1 $focal] : [list [hash $lbl] $focal [incr epoch]]}]
	} else {
		array unset boxes $lbl
	}
}
set lenses [lsort -command {apply {{a b} {
	set r [expr {[lindex $a 0] - [lindex $b 0]}]
	return [expr {$r != 0 ? $r : [lindex $a 2] - [lindex $b 2]}]
}}} [lmap {name value} [array get boxes] { split $value " " } ]]

lassign {1 -1} slot prev_box
foreach lens $lenses {
	lassign $lens box focal
	lassign [list [expr {$box != $prev_box ? 1 : $slot + 1}] $box] slot prev_box
	incr gold [expr {($box + 1) * $focal * $slot}]
}
puts "gold:   $gold"
