// $ gcc -O3 -march=native -fopenmp -DNDEBUG d02.c
#include <assert.h>
#include <inttypes.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

typedef struct { unsigned char *s; ptrdiff_t len; } Str;

static Str
str_map_file(const char *path)
{
	Str ret = {0};
	int fd = open(path, O_RDONLY);
	if (fd >= 0) {
		struct stat statbuf;
		if (fstat(fd, &statbuf) == 0) {
			ret.len = statbuf.st_size;
			ret.s = mmap(NULL, ret.len, PROT_READ, MAP_PRIVATE, fd, 0);
			if (ret.s == MAP_FAILED) {
				ret = (Str){0};
			}
		}
		close(fd);
	}
	return ret;
}

typedef struct { int64_t sum1, sum2; } Result;

static Result
solve(Str input, ptrdiff_t start, ptrdiff_t end)
{
	if (start != 0) for (; input.s[start-1] != '\n'; ++start) {}
	for (; input.s[end-1] != '\n'; ++end) {}
	assert(end <= input.len && start < input.len);

	Result ret = {0};
	uint8_t *p = input.s + start;
	uint8_t *endp = input.s + end;

	while (p < endp) {
		p += 5;
		int64_t id = 0;
		for (; *p != ':'; ++p) { id *= 10; id += *p - '0'; }

		struct { int64_t r, g, b; } req = {0};
		for (;;) {
			for (;;) {
				for (; !(*p >= '0' && *p <= '9'); ++p) {}
				int64_t cubes = 0;
				for (; *p != ' '; ++p) { cubes = (cubes * 10) + (*p - '0'); }
				++p;

				int color = *p;
				switch (color) {
				case 'r': p += 3; if (cubes > 12) id = 0; if (cubes > req.r) req.r = cubes; break;
				case 'g': p += 5; if (cubes > 13) id = 0; if (cubes > req.g) req.g = cubes; break;
				case 'b': p += 4; if (cubes > 14) id = 0; if (cubes > req.b) req.b = cubes; break;
				}

				if (*p == '\n' || *p == ';') { break; }
			}

			if (*p == '\n') { ++p; break; }
		}

		ret.sum1 += id;
		ret.sum2 += req.r * req.g * req.b;
	}

	return ret;
}

extern int
main(int argc, char *argv[])
{
	Str input = str_map_file(argc > 1 ? argv[1] : "/dev/stdin");
	assert(input.len > 0 && input.s[input.len - 1] == '\n');

	int64_t sum1 = 0, sum2 = 0;
	ptrdiff_t step = 1 << 16;
	ptrdiff_t steps = (input.len / step) + !!(input.len % step);

	#pragma omp parallel for reduction(+:sum1,sum2)
	for (ptrdiff_t i = 0; i < steps; ++i) {
		ptrdiff_t end = (i + 1) * step;
		if (end > input.len) { end = input.len; }
		Result r = solve(input, i * step, end);

		sum1 += r.sum1; sum2 += r.sum2;
	}
	printf("part 1: %"PRIi64"\n" "part 2: %"PRIi64"\n", sum1, sum2);
}
