#!/usr/bin/luajit

local Vadd = function(a, b) return { a[1] + b[1], a[2] + b[2] } end
local Veq  = function(a, b) return a[1] == b[1] and a[2] == b[2] end
local loc = function(v) return v[1] .. ',' .. v[2] end

local function neighbours(grid, cur)
	local ret = {}
	local ch = grid[loc(cur)]
	assert(ch ~= nil)
	if (ch ~= '#') then
		for _,d in ipairs({ {-1, 0}, {0, 1}, {1, 0}, {0, -1} }) do
			local nxt = Vadd(cur, d)
			local nch = grid[loc(nxt)]
			if (not (nch == nil or nch == '#')) then
				table.insert(ret, nxt)
			end
		end
	end
	return ret
end

local function find_edges(grid, keyset, edges, start, cur, steps, seen)
	if (seen[loc(cur)] ~= nil) then
	elseif (not Veq(cur, start) and keyset[loc(cur)]) then
		table.insert(edges[loc(start)], {cur, steps})
	else
		seen[loc(cur)] = true
		for _,nxt in ipairs(neighbours(grid, cur)) do
			find_edges(grid, keyset, edges, start, nxt, steps+1, seen)
		end
		seen[loc(cur)] = nil
	end
end

local function dfs2(edges, dest, start, steps, seen)
	if (Veq(start, dest)) then
		return steps
	else
		local m = 0
		seen[loc(start)] = true
		for _,edge in ipairs(edges[loc(start)]) do
			local nxt, cost = unpack(edge)
			if (seen[loc(nxt)] == nil) then
				local res = dfs2(edges, dest, nxt, steps + cost, seen)
				m = math.max(m, res)
			end
		end
		seen[loc(start)] = nil
		return m
	end
end

local function dfs1(grid, dest, cur, steps, seen)
	if (Veq(cur, dest)) then
		return steps
	end
	if (seen[loc(cur)] ~= nil) then
		return 0
	end

	local ch = grid[loc(cur)]
	assert(ch ~= nil and ch ~= '#')
	local slope = ({
		['>'] = {0,1},  ['<'] = {0,-1}, ['^'] = {-1,0}, ['v'] = {1,0},
	})[ch]

	local m = 0
	seen[loc(cur)] = true
	if (slope ~= nil) then
		m = dfs1(grid, dest, Vadd(cur, slope), steps+1, seen)
	else
		for _,nxt in ipairs(neighbours(grid, cur)) do
			m = math.max(m, dfs1(grid, dest, nxt, steps+1, seen))
		end
	end
	seen[loc(cur)] = nil
	return m
end

local main = function()
	local grid = {}
	local row, col = 0, 0
	for ln in io.stdin:lines() do
		row = row + 1
		col = #ln
		for i=1, #ln do grid[loc({row,i})] = ln:sub(i, i) end
	end

	-- preprocessing for part2
	local keynodes = { {1, 2}, {row, col-1} }
	for r=1, row do
		for c=1, col do
			if (#neighbours(grid, {r,c}) > 2) then
				table.insert(keynodes, {r,c})
			end
		end
	end
	local edges, keyset = {}, {}
	for _,node in ipairs(keynodes) do
		edges[loc(node)] = {}
		keyset[loc(node)] = true
	end
	for _,node in ipairs(keynodes) do
		find_edges(grid, keyset, edges, node, node, 0, {})
	end

	print("Silver:", dfs1(grid, {row, col-1}, {1,2}, 0, {}))
	-- NOTE: takes a bit of time to run
	print("Gold:  ", dfs2(edges, {row, col-1}, {1,2}, 0, {}))
end

main()
