// $ gcc -O3 -march=native -fopenmp -DNDEBUG d01.c
#include <assert.h>
#include <inttypes.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

typedef struct { unsigned char *s; ptrdiff_t len; } Str;

static Str
str_map_file(const char *path)
{
	Str ret = {0};
	int fd = open(path, O_RDONLY);
	if (fd >= 0) {
		struct stat statbuf;
		if (fstat(fd, &statbuf) == 0) {
			ret.len = statbuf.st_size;
			ret.s = mmap(NULL, ret.len, PROT_READ, MAP_PRIVATE, fd, 0);
			if (ret.s == MAP_FAILED) {
				ret = (Str){0};
			}
		}
		close(fd);
	}
	return ret;
}

static int64_t
solve(Str input, ptrdiff_t start, ptrdiff_t end)
{
	if (start != 0) while (input.s[start-1] != '\n') {
		++start;
	}
	while (input.s[end-1] != '\n') {
		++end;
	}
	assert(end <= input.len && start < input.len);

	int last = -1;
	int64_t ret = 0;
	for (ptrdiff_t i = start; i < end; ++i) {
		int num = -1;
		switch (input.s[i]) {
		case '0': case '1': case '2': case '3': case '4':
		case '5': case '6': case '7': case '8': case '9':
			num = input.s[i] - '0';
			break;
		case '\n':
			assert(last >= 0);
			ret += last;
			last = -1;
			break;
#if 1
		default: {
			#define CMP(X) (memcmp(input.s + i, (X), sizeof(X) - 1) == 0)
			ptrdiff_t rem = end - i;
			switch ((rem >= 3) + (rem >= 4) + (rem >= 5)) {
			case 3: num = CMP("three") ? 3 : CMP("seven") ? 7 : CMP("eight") ? 8 : num; // fallthrough
			case 2: num = CMP("four") ? 4 : CMP("five") ? 5 : CMP("nine") ? 9 : num; // fallthrough
			case 1: num = CMP("one") ? 1 : CMP("two") ? 2 : CMP("six") ? 6 : num; break;
			}
		} break;
#endif
		}

		if (num >= 0) {
			ret += (last == -1) ? num*10 : 0;
			last = num;
		}
	}
	return ret;
}

extern int
main(int argc, char *argv[])
{
	Str input = str_map_file(argc > 1 ? argv[1] : "/dev/stdin");
	assert(input.len > 0 && input.s[input.len - 1] == '\n');

	int64_t sum = 0;
	ptrdiff_t step = 1 << 16;
	ptrdiff_t steps = (input.len / step) + !!(input.len % step);

	#pragma omp parallel for reduction(+:sum)
	for (ptrdiff_t i = 0; i < steps; ++i) {
		ptrdiff_t end = (i + 1) * step;
		if (end > input.len) {
			end = input.len;
		}
		int64_t n = solve(input, i * step, end);
		sum += n;
	}
	printf("part 2: %"PRIi64"\n", sum);
}
