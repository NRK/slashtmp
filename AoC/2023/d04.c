// $ gcc -O3 -march=native -fopenmp -DNDEBUG d04.c
// very dirty. written while half asleep.
// committed into the repo mainly for archiving purposes.
#include <assert.h>
#include <inttypes.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

typedef struct { unsigned char *s; ptrdiff_t len; } Str;

static Str
str_map_file(const char *path)
{
	Str ret = {0};
	int fd = open(path, O_RDONLY);
	if (fd >= 0) {
		struct stat statbuf;
		if (fstat(fd, &statbuf) == 0) {
			ret.len = statbuf.st_size;
			ret.s = mmap(NULL, ret.len, PROT_READ, MAP_PRIVATE, fd, 0);
			if (ret.s == MAP_FAILED) {
				ret = (Str){0};
			}
		}
		close(fd);
	}
	return ret;
}

static ptrdiff_t
solve(Str input, ptrdiff_t start, ptrdiff_t end, int64_t *wins)
{
	assert(input.s[start] == 'C');
	uint8_t *p = input.s + start;
	uint8_t *endp = input.s + end;
	ptrdiff_t lines = 0;
	while (p < endp) {
		for (; !(*p >= '0' && *p <= '9'); ++p) {}
		int card_no = 0;
		for (; *p >= '0' && *p <= '9'; ++p) {
			card_no *= 10; card_no += *p - '0';
		}

		for (; *p != ':'; ++p) {} ++p;
		uint8_t map[128] = {0};
		int sum = 0;
		int num = -1;
		int in_my_cards = 0;
		for (int should_break = 0; !should_break;) {
			switch (*p) {
			case '\n':
				should_break = 1;
				++lines;
				// fallthrough
			case ' ': case '|':
				if (num >= 0) {
					if (in_my_cards)
						sum += map[num];
					else
						map[num] = 1;
				}
				num = -1;
				in_my_cards |= *p == '|';
				++p;
				break;
			case '0': case '1': case '2': case '3': case '4':
			case '5': case '6': case '7': case '8': case '9':
				num = 0;
				for (; *p >= '0' && *p <= '9'; ++p)
					num = num*10 + (*p - '0');
				break;
			}
		}

		wins[card_no] = sum;
	}
	return lines;
}

extern int
main(int argc, char *argv[])
{
	Str input = str_map_file(argc > 1 ? argv[1] : "/dev/stdin");
	assert(input.len > 0 && input.s[input.len - 1] == '\n');

	ptrdiff_t step = 1 << 16;
	ptrdiff_t steps = (input.len / step) + !!(input.len % step);

	static int64_t wins[1<<24]; // should be enough
	int64_t lines = 0;
	#pragma omp parallel for reduction(+:lines)
	for (ptrdiff_t i = 0; i < steps; ++i) {
		ptrdiff_t start = i * step;
		if (start != 0) for (; input.s[start-1] != '\n'; ++start) {}
		ptrdiff_t end = (i + 1) * step;
		if (end > input.len) { end = input.len; }
		for (; input.s[end-1] != '\n'; ++end) {}
		lines += solve(input, start, end, wins);
	}

	int64_t sum = 0;
	#pragma omp parallel for reduction(+:sum)
	for (ptrdiff_t i = 0; i < lines; ++i) {
		if (wins[i])
			sum += 1ll << (wins[i] - 1);
	}

	int64_t sum2 = lines;
	int64_t *copies = calloc(lines, sizeof *copies);
	for (ptrdiff_t i = 0; i < lines; ++i) {
		int64_t incr = copies[i] + 1;
		for (ptrdiff_t k = i + 1; k <= i + wins[i]; ++k) {
			copies[k] += incr;
			sum2 += incr;
		}
	}

	printf("part 1: %lld\npart 2: %lld\n", (long long)sum, (long long)sum2);
}
