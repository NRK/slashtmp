#!/usr/bin/luajit

local split = function(s, ch)
	local ret = {}
	local pat = ch == "" and "." or ("[^" .. (ch or " \n\t") .. "]+")
	s:gsub(pat, function(m) ret[#ret + 1] = m end)
	return ret
end

local hash = function(s)
	local h = 0
	for _,ch in ipairs(split(s, "")) do
		h = ((h + ch:byte()) * 17) % 256
	end
	return h
end

local main = function()
	local epoch = 0
	local steps = split(io.stdin:read("*line"), ',')
	local labels = {}
	local silver, gold = 0, 0
	for _,step in ipairs(steps) do
		silver = silver + hash(step)

		local label, op, focal = step:match("(%a+)([-=])(%d?)")
		if (op == "=") then
			if (labels[label] == nil) then
				labels[label] = {focal = focal, epoch = epoch, box = hash(label)}
				epoch = epoch + 1
			else
				labels[label].focal = focal
			end
		else
			labels[label] = nil
		end
	end
	local sorted_labels = {}
	for _,v in pairs(labels) do table.insert(sorted_labels, v) end
	table.sort(sorted_labels, function(a, b)
		if (a.box ~= b.box) then return a.box < b.box end
		return a.epoch < b.epoch
	end)
	local slot, prev_box = 0, -1
	for _,v in ipairs(sorted_labels) do
		slot, prev_box = ((prev_box == v.box) and slot + 1 or 1), v.box
		gold = gold + ((v.box + 1) * slot * v.focal)
	end

	print("silver:", silver, "\ngold:", gold)
end

main()
