#!/usr/bin/luajit

local parse = function()
	local galaxies, incel_rows, incel_cols = {}, {}, {}
	local row, col = 1, 1
	local chad_cols = {}

	for ln in io.stdin:lines() do
		local chad_row = false
		col = 1
		for ch in ln:gmatch(".") do
			if (ch == '#') then
				galaxies[#galaxies + 1] = { row, col }
				chad_row = true
				chad_cols[col] = true
			end

			col = col + 1
		end
		if (not chad_row) then
			incel_rows[#incel_rows + 1] = row
		end

		row = row + 1
	end

	for c = 1, col-1 do
		if (chad_cols[c] == nil) then
			incel_cols[#incel_cols + 1] = c
		end
	end

	return galaxies, incel_rows, incel_cols
end

local solve = function(galaxies, from, to, incel_rows, incel_cols, incel_penalty)
	local min = function(a, b) if (a < b) then return a, b end return b, a end
	local ins = function(x, a, b) return x >= a and x <= b end
	local r0, r1 = min(galaxies[from][1], galaxies[to][1])
	local c0, c1 = min(galaxies[from][2], galaxies[to][2])
	local ret = (r1 - r0) + (c1 - c0)
	for _,v in ipairs(incel_rows) do ret = ret + (ins(v, r0, r1) and (incel_penalty - 1) or 0) end
	for _,v in ipairs(incel_cols) do ret = ret + (ins(v, c0, c1) and (incel_penalty - 1) or 0) end
	return ret
end

local galaxies, incel_rows, incel_cols = parse()
local silver, gold = 0, 0
for from = 1, #galaxies do
	for to = from+1, #galaxies do
		silver = silver + solve(galaxies, from, to, incel_rows, incel_cols, 2)
		gold = gold + solve(galaxies, from, to, incel_rows, incel_cols, 1000000)
	end
end
print("silver: ", silver)
print("gold: ", gold)
