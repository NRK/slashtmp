#!/usr/bin/luajit

local split = function(s, ch)
	local ret = {}
	local pat = ch == "" and "." or ("[^" .. (ch or " \n\t") .. "]+")
	s:gsub(pat, function(m) ret[#ret + 1] = m end)
	return ret
end

local function Queue(init)
	return {
		head = 1, tail = #init + 1, items = {unpack(init)},
		push = function(Q, item)
			Q.items[Q.tail] = item
			Q.tail = Q.tail + 1
		end,
		pop = function(Q)
			assert(not Q:empty())
			local ret = Q.items[Q.head]
			Q.items[Q.head] = nil
			Q.head = Q.head + 1
			return ret
		end,
		empty = function(Q) return Q.head == Q.tail end
	}
end

local push = function(modules, state, golden_state, current_iteration)
	local lows, highs = 0, 0
	local Q = Queue{ { "broadcaster", false } }
	while (not Q:empty()) do
		local name, signal, sender = unpack(Q:pop())
		local m = modules[name]

		for k,v in pairs(golden_state.cycle) do
			local delta = current_iteration - golden_state.cycle[k]
			if (state[golden_state.parent][k] and delta > 0) then
				golden_state.cycle[k] = delta
			end
		end

		assert(type(signal) == "boolean")
		if (signal) then
			highs = highs + 1
		else
			lows = lows + 1
		end

		if (m == nil) then
			-- nop
		elseif (m.typ == "") then
			for _,dests in ipairs(m.dests) do
				Q:push({ dests, false, name })
			end
		elseif (m.typ == "%") then
			if (not signal) then
				assert(type(state[name]) == "boolean")
				state[name] = not state[name]
				for _,dests in ipairs(m.dests) do
					Q:push({ dests, state[name], name })
				end
			end
		elseif (m.typ == "&") then
			state[name][sender] = signal
			local all_high = true
			for _,v in pairs(state[name]) do
				all_high = all_high and v
			end
			for _,dests in ipairs(m.dests) do
				Q:push({ dests, not all_high, name })
			end
		else
			assert(false)
		end
	end
	return lows, highs, hit
end

local function gcd(a, b) return (b == 0) and a or gcd(b, a % b) end
local lcm = function(a, b) return (a / gcd(a, b)) * b end

local main = function()
	local modules, state, parents = {}, {}, {}
	for ln in io.stdin:lines() do
		local typ, name, dests = ln:match("([&%%]?)(%a+) %-> (.*)")
		modules[name] = { typ = typ, dests = {} }
		if (typ == "%") then
			state[name] = false
		end
		for _,d in ipairs(split(dests, ", ")) do
			table.insert(modules[name].dests, d)
			if (parents[d] == nil) then parents[d] = {} end
			table.insert(parents[d], name)
		end
	end

	for name,v in pairs(modules) do
		if (v.typ == '&') then
			v.parents = parents[name]
			state[name] = {}
			for _,v in pairs(v.parents) do
				state[name][v] = false
			end
		end
	end

	-- HACK: assume the output ("rx") only has a single parent, and that
	-- the parent is a NAND gate.
	local output = "rx"
	assert(#parents[output] == 1)
	assert(modules[parents[output][1]].typ == "&")
	local golden_state = { parent = parents[output][1], cycle = {} }
	for k,v in pairs(parents[golden_state.parent]) do
		golden_state.cycle[v] = 0
	end

	local lows, highs, silver_solved = 0, 0, false
	for i=1, math.huge do
		local l, h = push(modules, state, golden_state, i)

		if (i <= 1000) then
			lows = lows + l
			highs = highs + h
			if (i == 1000) then
				print("Silver:", lows * highs)
				silver_solved = true
			end
		end

		local num = 1
		for k,v in pairs(golden_state.cycle) do
			if (v == 0) then num = -1; break; end
			num = lcm(num, v)
		end
		if (not gold and num > 0) then
			print("Gold:  ", string.format("%.0f", num))
			golden_state.solved = true
		end

		if (silver_solved and golden_state.solved) then
			break
		end
	end
end

main()
