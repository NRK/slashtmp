#!/usr/bin/luajit

local Map = function(t, f)
	local ret = {}
	for k,v in pairs(t) do ret[k] = f(k, v) end
	return ret
end
local split = function(s, ch)
	local ret = {}
	local pat = ch == "" and "." or ("[^" .. (ch or " \n\t") .. "]+")
	s:gsub(pat, function(m) ret[#ret + 1] = m end)
	return ret
end

local Vadd  = function(a, b) return { a[1] + b[1], a[2] + b[2] } end
local Vsub  = function(a, b) return { a[1] - b[1], a[2] - b[2] } end
local Vmul  = function(a, b) return { a[1] * b[1], a[2] * b[2] } end
local Vdiv  = function(a, b) return { a[1] / b[1], a[2] / b[2] } end
local Vdiff = function(a, b) return { math.abs(a[1] - b[1]), math.abs(a[2] - b[2]) } end
local Veq   = function(a, b) return a[1] == b[1] and a[2] == b[2] end
local Vstr  = function(v) return v[1] .. ',' .. v[2] end

local X,Y,Z = 1,2,3  -- readable way to index the vectors

-- [min, max]
local function in_range(x, range)
	return x >= range[1] and x <= range[2]
end

local function linear_algebra(a, ad, b, bd, limit)
	-- a + ad * t = ar
	-- (ax + adx * t), (ay + ady * t)
	--
	-- x1 = x0 + adx * t
	-- t = (x1 - x0) / adx
	-- t = (y1 - y0) / ady
	-- t need to be same:
	-- (x1 - x0) / adx == (y1 - y0) / ady
	-- ady * x1 - ady * x0 = adx * y1 - adx * y0   : unknowns are x1, y1
	-- ady * x1 - adx * y1 = ady * x0 - adx * y0
	-- A * x1 + B * y1 = C    : standard form -> ax + by = c
	-- A = ady, B = -adx, C = ady * x0 - adx * y0
	local F = function(a, d)
		local A = d[Y]
		local B = -d[X]
		local C = d[Y] * a[X] - d[X] * a[Y]
		return A, B, C
	end
	-- A0 * x + B0 * y = C0
	-- A1 * x + B1 * y = C1
	--
	-- Matrix form:
	--
	-- F        7  F   7    F    7
	-- | A0  B0 |  | x |  = | C0 |
	-- | A1  B1 |  | y |  = | C1 |
	-- L        J  L   J    L    J
	local A0, B0, C0 = F(a, ad)
	local A1, B1, C1 = F(b, bd)
	local det = (A0 * B1) - (B0 * A1)
	if (det ~= 0) then
		-- cramer's rule: https://youtube.com/watch?v=jBsC34PxzoM
		-- C0 B0
		-- C1 B1
		local x = ((C0*B1) - (B0*C1)) / det
		-- A0 C0
		-- A1 C1
		local y = ((A0*C1) - (C0*A1)) / det
		if (in_range(x, limit) and in_range(y, limit)) then
			-- a + ad * t = a1
			-- t = (a1 - a) / ad
			local ta = Vdiv(Vsub({x, y}, a), ad)
			local tb = Vdiv(Vsub({x, y}, b), bd)
			-- ta.x should be equal to ta.y and same for tb, but
			-- since we're dealing with large FP numbers the
			-- rounding error can get pretty high (~0.02).
			-- so just test both of them.
			if (ta[X] < 0 or tb[X] < 0 or ta[Y] < 0 or tb[Y] < 0) then
				-- one of these walked backwards in time
			else
				return 1
			end
		end
	end
	return 0
end

local function newton_raphson(p0, d0, p1, d1, limit)
	-- a + ad * v = b + bd * u
	-- u := (a + ad * v - b) / bd
	-- solve the equation using newton's method
	local E = 0.02  -- adjust for rounding errors, this is finicky
	local solve = function(p0, d0, p1, d1, v)
		local tmp = Vadd(p0, Vmul(d0, {v, v}))
		tmp = Vsub(tmp, p1)
		tmp = Vdiv(tmp, d1)
		return tmp[1] - tmp[2], tmp[1]
	end
	local u, v = nil, nil
	local lo, hi = 0, limit[2]  -- picking the hi limit is a bit finicky
	local iter = 0
	while (lo < hi and lo >= 0 and hi >= 0) do
		local mid = (hi + lo) / 2.0
		local reslo  = solve(p0, d0, p1, d1, lo)
		local resmid, raw = solve(p0, d0, p1, d1, mid)
		local reshi  = solve(p0, d0, p1, d1, hi)
		if (math.abs(resmid - 0) < E or iter == 2^14) then
			u, v = raw, mid
			break
		end
		if (math.abs(reshi) > math.abs(reslo)) then
			hi = mid
		else
			lo = mid
		end
		iter = iter + 1
	end
	if (u ~= nil and u >= 0 and v >= 0) then
		local intersect = Vadd(p0, Vmul(d0, {v, v}))
		if (in_range(intersect[X], limit) and in_range(intersect[Y], limit)) then
			return 1
		end
	end
	return 0
end

local main = function()
	local hails = {}
	for ln in io.stdin:lines() do
		local tmp = Map(split(ln, ", @"), function(k, v) return tonumber(v) end)
		local x, y, z, dx, dy, dz = unpack(tmp)
		hails[#hails + 1] = { pos = {x, y, z}, delta = {dx, dy, dz} }
	end

	-- local limit = {7, 27}  -- example
	local limit = {200000000000000, 400000000000000}
	local silver_la, silver_nr = 0, 0
	for i=1, #hails do
		for ii=i+1, #hails do
			local V2 = function(v) return { v[1], v[2] } end
			local args = {
				V2(hails[i].pos),  V2(hails[i].delta),
				V2(hails[ii].pos), V2(hails[ii].delta),
				limit
			}
			silver_la = silver_la + linear_algebra(unpack(args))
			silver_nr = silver_nr + newton_raphson(unpack(args))
		end
	end
	print("Silver:", silver_la, "(linear algebra)")
	print("Silver:", silver_nr, "(newton-raphson)")
end

main()
