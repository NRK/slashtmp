#!/usr/bin/luajit

local sum1, sum2 = 0, 0
for ln in io.stdin:lines() do
	local id = tonumber(ln:match("^Game (%d+):"))
	ln = ln:gsub("^Game %d+: ", "")

	local req = { r = 0, g = 0, b = 0 }
	local max = { r = 12, g = 13, b = 14 }
	for set in ln:gmatch("[^\n;]+") do
		for cube, color in set:gmatch("%S-(%d+) (%a)") do
			cube = tonumber(cube)
			req[color] = math.max(req[color], cube)
			if (cube > max[color]) then
				id = 0
			end
		end
	end

	sum1 = sum1 + id
	sum2 = sum2 + (req.r * req.g * req.b)
end

print("part 1:", sum1)
print("part 2:", sum2)
