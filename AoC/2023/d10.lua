#!/usr/bin/luajit
-- vim: fdm=marker:

local up    = function(r, c) return { r - 1, c } end
local down  = function(r, c) return { r + 1, c } end
local left  = function(r, c) return { r, c - 1 } end
local right = function(r, c) return { r, c + 1 } end

local loc = function(r, c)
	if (type(r) == "table") then
		assert(c == nil and #r >= 2)
		return r[1] .. ',' .. r[2]
	end
	return r .. ',' .. c
end

local point_eq = function(a, b)
	assert(#a == 2 and #b == 2)
	return a[1] == b[1] and a[2] == b[2]
end

local main = function()
	local map, start = {}, {}
	local row, col = 1, 1

	for ln in io.stdin:lines() do
		col = 1
		for ch in ln:gmatch(".") do
			local d = {
				["|"] = { up,    down  }, ["-"] = { left,  right },
				["L"] = { up,    right }, ["J"] = { up,    left  },
				["F"] = { down,  right }, ["7"] = { down,  left  },
			}
			if (ch == "S") then
				start = { row, col }
			else
				map[loc(row, col)] = { nxt = d[ch] or {}, loop = false }
			end

			col = col + 1
		end
		row = row + 1
	end

	map[loc(start)] = { nxt = {}, loop = true }
	for _,delta in ipairs({ up, down, left, right }) do
		local ncord = delta(unpack(start))
		local nnode = map[loc(ncord)] or { nxt = {} }
		for _,dir in pairs(nnode.nxt) do
			if (point_eq(start, dir(unpack(ncord)))) then
				table.insert(map[loc(start)].nxt, delta)
			end
		end
	end

	local shoelace = function(a, b) return (a[1] * b[2]) - (a[2] * b[1]) end
	local steps, area = 0, 0
	local prev, cur = { -1, -1 }, start
	repeat
		for _,nxtf in pairs(map[loc(cur)].nxt) do
			local nxt = nxtf(unpack(cur))
			if (not point_eq(prev, nxt)) then
				prev, cur = cur, nxt
				break
			end
		end
		map[loc(cur)].loop = true
		steps = steps + 1
		area = area + shoelace(cur, prev)
	until (point_eq(cur, start))
	area = math.abs(area) * 0.5

	-- visualization: {{{
	local goes = function(node, dir) return node.nxt[1] == dir or node.nxt[2] == dir end
	for r = 1, row-1 do
		for c = 1, col-1 do
			local node = map[loc(r, c)]
			if (node.loop) then
				if (goes(node, up) and goes(node, down)) then
					io.stdout:write('║')
				elseif (goes(node, left) and goes(node, right)) then
					io.stdout:write('═')
				elseif (goes(node, down) and goes(node, right)) then
					io.stdout:write('╔')
				elseif (goes(node, down) and goes(node, left)) then
					io.stdout:write('╗')
				elseif (goes(node, up) and goes(node, right)) then
					io.stdout:write('╚')
				elseif (goes(node, up) and goes(node, left)) then
					io.stdout:write('╝')
				else
					assert(false)
				end
			else
				io.stdout:write('-')
			end
		end
		io.stdout:write('\n')
	end
	-- }}}

	print("silver:", steps / 2)
	print("gold:", area - (steps / 2) + 1) -- pick's theorem
end

main()
