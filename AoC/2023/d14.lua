#!/usr/bin/luajit

local split = function(s)
	local ret = {}
	s:gsub(".", function(m) ret[#ret + 1] = m end)
	return ret
end

local roll = function(mapIn, dr, dc)
	local map = {unpack(mapIn)}
	local rstart, rend, rstep = unpack((dr == 1) and {#map, 1, -1} or {1, #map, 1})
	local cstart, cend, cstep = unpack((dc == 1) and {#map[1], 1, -1} or {1, #map[1], 1})
	for r = rstart, rend, rstep do
		for c = cstart, cend, cstep do
			if (map[r][c] == 'O') then
				local nr = r + dr
				local nc = c + dc
				while (map[nr] ~= nil and map[nr][nc] == '.') do
					map[nr][nc] = 'O'
					map[nr - dr][nc - dc] = '.'
					nr = nr + dr
					nc = nc + dc
				end
			end
		end
	end
	return map
end

local pressure = function(map)
	local ret = 0
	for k,v in ipairs(map) do
		ret = ret + table.concat(v):gsub("[^O]", ""):len() * (#map - (k - 1))
	end
	return ret
end

local main = function()
	local map = {}
	for ln in io.stdin:lines() do
		map[#map + 1] = split(ln)
	end
	print("silver:", pressure(roll(map, -1, 0)))

	local seen, pres = {}, {}
	for i = 1, 1000000000 do
		for _,dir in ipairs({ {-1,0}, {0,-1}, {1,0}, {0,1} }) do
			map = roll(map, unpack(dir))
		end
		local key = {}
		for k,v in ipairs(map) do key[#key + 1] = table.concat(v) end
		key = table.concat(key)
		if (seen[key] ~= nil) then
			local off = seen[key]
			local clen = i - off
			local mod = (1000000000 - off) % clen
			local gold = off + mod
			print("gold:", pres[gold])
			break
		else
			pres[i] = pressure(map)
			seen[key] = i
		end
	end
end

main()
