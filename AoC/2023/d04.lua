#!/usr/bin/luajit

local sum = 0
local lines = 0
local copies = {}
local ncopies = 0
for ln in io.stdin:lines() do
	local win = {}
	for num in ln:match(".*:(.*)|"):gmatch("%d+") do
		win[num] = true
	end
	local nwins = 0
	for num in ln:match(".*|(.*)"):gmatch("%d+") do
		if (win[num]) then
			nwins = nwins + 1
		end
	end

	-- part 1
	if (nwins > 0) then
		sum = sum + math.pow(2, nwins-1)
	end

	-- part 2
	local incr = 1 + (copies[lines] or 0)
	for i = lines+1, lines+nwins do
		copies[i] = (copies[i] or 0) + incr
		ncopies = ncopies + incr
	end

	lines = lines + 1
end

print("part 1: ", sum)
print("part 2: ", lines+ncopies)
