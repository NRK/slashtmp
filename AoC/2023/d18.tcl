#!/usr/bin/tclsh

proc solve {dy dx step area borders x} {
	incr area [expr { $x * ($dy * $step) }] ;# green's theorem
	return [list $area [incr borders $step] [expr {$x + ($dx * $step)}]]
}
proc finalize {area borders unused_x} {
	return [expr {($area - ($borders / 2) + 1) + $borders}]
}

array set dirs { R {0 1} D {1 0} L {0 -1} U {-1 0} }
lassign [lrepeat 2 {0 0 0}] silver gold
foreach ln [split [read -nonewline stdin] "\n"] {
	scan $ln {%s %d (#%x)} dir step hex
	set silver [solve {*}$dirs($dir) $step {*}$silver]
	set dir2 [string map { 0 R 1 D 2 L 3 U } [expr {$hex & 0xF}]]
	set gold [solve {*}$dirs($dir2) [expr {$hex >> 4}] {*}$gold]
}
puts "Silver:\t[finalize {*}$silver]\nGold:\t[finalize {*}$gold]"
