#!/usr/bin/luajit

-- "template" for copy pasting when starting a new day
-- vim: fdm=marker:
-- Debugger: {{{
local D = require("debugger")   -- https://github.com/slembcke/debugger.lua
D.tty_in = io.open("/dev/tty")
D.read = function(p) io.stdout:write(p) return D.tty_in:read("*line") end
D.auto_where = 3
D.p = D.pretty
assert = D.assert
-- }}}
-- basic stuff that should've been builtin: {{{
local table_len = function(t)
	local ret = 0
	for k,v in pairs(t) do ret = ret + 1 end
	return ret
end
local function table_copy(t)
	local ret = {}
	for k,v in pairs(t) do
		if (type(v) == "table") then ret[k] = table_copy(v)
		else ret[k] = v end
	end
	return ret
end
-- }}}
-- functional stuff: {{{
local Map = function(t, f)
	local ret = {}
	for k,v in pairs(t) do ret[k] = f(k, v) end
	return ret
end
local Filter = function(t, cond)
	local ret = {}
	for k,v in ipairs(t) do if (cond(k, v)) then ret[#ret + 1] = v end end
	return ret
end
local Filter2 = function(t, cond)
	local ret = {}
	for k,v in pairs(t) do if (cond(k, v)) then ret[k] = v end end
	return ret
end
local Reduce = function(t, start, op)
	local ret = start
	for k,v in pairs(t) do ret = op(ret, v) end
	return ret
end
local Fold = Reduce
-- }}}
-- misc: {{{
local split = function(s, ch)
	local ret = {}
	local pat = ch == "" and "." or ("[^" .. (ch or " \n\t") .. "]+")
	s:gsub(pat, function(m) ret[#ret + 1] = m end)
	return ret
end
local Mtranspose = function(m)
	local ret = {}
	for r = 1, #m do
		for c = 1, #m do  -- TODO: assume r == c ?
			if (ret[c] == nil) then ret[c] = {} end
			ret[c][r] = m[r][c]
		end
	end
	return ret
end
-- small stuff
local DIRS = { {-1, 0}, {0, 1}, {1, 0}, {0, -1} }
local Sum  = function(a, b) return a + b end
local Diff = function(a, b) return math.abs(a - b) end
local Vadd = function(a, b) return { a[1] + b[1], a[2] + b[2] } end
local Vsub = function(a, b) return { a[1] - b[1], a[2] - b[2] } end
local Vmul = function(a, b) return { a[1] * b[1], a[2] * b[2] } end
local Veq  = function(a, b) return a[1] == b[1] and a[2] == b[2] end
local Vstr = function(v) return v[1] .. ',' .. v[2] end
-- data structures
local function Queue(init)
	return {
		head = 1, tail = #init + 1, items = {unpack(init)},
		push = function(Q, item)
			Q.items[Q.tail] = item
			Q.tail = Q.tail + 1
		end,
		pop = function(Q)
			assert(not Q:empty())
			local ret = Q.items[Q.head]
			Q.items[Q.head] = nil
			Q.head = Q.head + 1
			return ret
		end,
		empty = function(Q) return Q.head == Q.tail end
	}
end
local function Stack(init)
	return {
		items = {unpack(init)},
		push = function(Q, item) Q.items[#Q.items + 1] = item end,
		pop = function(Q)
			assert(not Q:empty())
			local ret = Q.items[#Q.items]
			Q.items[#Q.items] = nil
			return ret
		end,
		empty = function(Q) return #Q.items == 0 end
	}
end
-- item[1] is considered the "priority".
-- negate the priority to turn this into a max-heap.
local function MinHeap(init)
	local ret = {
		items = {unpack(init)},
		push = function(Q, item)
			local new = #Q.items + 1
			Q.items[new] = item
			local parent = math.floor(new / 2)
			while (new ~= 1 and Q.items[parent][1] > Q.items[new][1]) do
				Q.items[new], Q.items[parent] =
					Q.items[parent], Q.items[new]
				new = parent
				parent = math.floor(new / 2)
			end
			Q:validate_()
		end,
		pop = function(Q)
			assert(not Q:empty())
			local ret = Q.items[1]
			local adjust, last = 1, #Q.items
			Q.items[adjust] = nil
			Q.items[adjust] = Q.items[last]
			Q.items[last] = nil
			local cost = function(idx) return (Q.items[idx] or {math.huge})[1] end
			repeat
				local l, r = (adjust * 2), (adjust * 2) + 1
				local smaller = (cost(l) <= cost(r)) and l or r
				if (Q.items[smaller] and
				    Q.items[adjust][1] > Q.items[smaller][1])
				then
					Q.items[smaller], Q.items[adjust] =
						Q.items[adjust], Q.items[smaller]
					adjust = smaller
				end
			until (adjust ~= smaller)
			Q:validate_()
			return ret
		end,
		empty = function(Q) return #Q.items == 0 end,
		validate_ = function(Q)
			for i=1, #Q.items do
				local l, r = (i * 2), (i * 2) + 1
				assert(Q.items[l] == nil or Q.items[i][1] <= Q.items[l][1])
				assert(Q.items[r] == nil or Q.items[i][1] <= Q.items[r][1])
			end
		end,
	}
	table.sort(ret.items, function(a, b) return a[1] < b[1] end)
	ret:validate_()
	return ret
end
-- }}}

local main = function()
	local input = io.stdin:read("*all")
	local lines = split(input, "\n")
	local grid = {}
	local R, C = 0, 0
	for lineno,ln in ipairs(lines) do
		R = R + 1
		C = #ln
		for i=1, C do grid[Vstr({R, i})] = ln:sub(i, i) end
	end

	for lineno,ln in ipairs(lines) do
		print(ln)
	end
end

D.call(main)
