#!/usr/bin/luajit

local mkneighbours = function(ir, ic)
	local ret = {}
	for r = ir-1, ir+1 do
		for c = ic-1, ic+1 do
			ret[#ret + 1] = r .. "," .. c
		end
	end
	return ret
end

local input = {}
local star_map = {}
local lines = 0
for ln in io.stdin:lines() do
	local col = 0
	for ch in ln:gmatch(".") do
		input[lines .. "," .. col] = ch
		if (ch == '*') then
			star_map[lines .. "," .. col] = {}
		end
		col = col + 1
	end
	lines = lines + 1
end

local sum = 0
for r = 0, lines-1 do
	local num, near, near_stars = 0, false, {}

	for c = 0, math.huge do
		local ch = input[r .. "," .. c]
		if (ch == nil or tonumber(ch) == nil) then
			for k,v in pairs(near_stars) do
				star_map[k][#star_map[k] + 1] = num
			end
			sum = sum + (near and num or 0)

			-- reset state
			num, near, near_stars = 0, false, {}

			if (ch == nil) then
				break
			end
		elseif (tonumber(ch) ~= nil) then
			num = (num * 10) + tonumber(ch)
			for k,v in ipairs(mkneighbours(r, c)) do
				near = near or (input[v] and input[v]:match("[^%d.]"))
				if (input[v] == '*') then
					near_stars[v] = true
				end
			end
		end
	end
end

local sum2 = 0
for k,v in pairs(star_map) do
	if (#v == 2) then
		sum2 = sum2 + (v[1] * v[2])
	end
end

print("part 1:", sum)
print("part 2:", sum2)
