#!/usr/bin/luajit

local Map = function(t, f)
	local ret = {}
	for k,v in pairs(t) do ret[k] = f(k, v) end
	return ret
end
local split = function(s, ch)
	local ret = {}
	local pat = ch == "" and "." or ("[^" .. (ch or " \n\t") .. "]+")
	s:gsub(pat, function(m) ret[#ret + 1] = m end)
	return ret
end

local loc = function(x, y, z) return x .. ',' .. y .. ',' .. z end

local brickid = function(brick)
	local tmp = Map(brick, function(k, v) return table.concat(v, ",") end)
	return tmp.x .. "|" .. tmp.y .. "|" .. tmp.z
end

local blockit = function(blocked, brick)
	local id = brickid(brick)
	for z=brick.z[1], brick.z[2] do
		for y=brick.y[1], brick.y[2] do
			for x=brick.x[1], brick.x[2] do
				assert(blocked[loc(x,y,z)] == nil)
				blocked[loc(x,y,z)] = id
			end
		end
	end
end

local table_append = function(t, id, item)
	if (t[id] == nil) then t[id] = {} end
	table.insert(t[id], item)
end

local move_down = function(blocked, supporting, supported_by, b)
	local z = b.z[1]
	local blocked_byV = {}
	local can_go_down = true
	while (z > 1 and can_go_down) do
		for y=b.y[1], b.y[2] do
			for x=b.x[1], b.x[2] do
				local blocked_by = blocked[loc(x,y,z-1)]
				if (blocked_by ~= nil) then
					can_go_down = false
					blocked_byV[blocked_by] = true
				end
			end
		end
		if (can_go_down) then
			z = z - 1
		end
	end
	local zd = b.z[1] - z
	b.z = Map(b.z, function(k, v) return v - zd end)
	local id = brickid(b)
	for k,_ in pairs(blocked_byV) do
		table_append(supported_by, id, k)
		table_append(supporting, k, id)
	end
	blockit(blocked, b)
end

local function would_fall(id, rekt, supporting, supported_by)
	local sum = 0
	rekt[id] = true
	for _,s in ipairs(supporting[id] or {}) do
		local all_rekt = true
		for _,v in pairs(supported_by[s]) do
			if (not rekt[v]) then
				all_rekt = false
				break
			end
		end
		if (all_rekt) then
			sum = sum + 1 + would_fall(s, rekt, supporting, supported_by)
		end
	end
	return sum
end

local main = function()
	local bricks = {}
	local min = function(a, b)
		if (a < b) then return { a, b } end
		return { b, a }
	end
	for ln in io.stdin:lines() do
		local a = Map(split(ln, "~"), function(k, v)
			return Map(split(v, ","), function(k,v) return tonumber(v) end)
		end)
		table.insert(bricks, {
			x = min(a[1][1], a[2][1]),
			y = min(a[1][2], a[2][2]),
			z = min(a[1][3], a[2][3]),
		})
	end
	table.sort(bricks, function(a, b) return a.z[1] < b.z[1] end)

	local blocked, supporting, supported_by = {}, {}, {}
	for _,b in ipairs(bricks) do
		move_down(blocked, supporting, supported_by, b)
	end

	local silver = 0
	for idx,b in ipairs(bricks) do
		local safe = 1
		for _,s in ipairs(supporting[brickid(b)] or {}) do
			if (#supported_by[s] == 1) then
				safe = 0
				break
			end
		end
		silver = silver + safe
	end
	print("Silver:", silver)

	local gold = 0
	for idx,b in ipairs(bricks) do
		gold = gold + would_fall(brickid(b), {}, supporting, supported_by)
	end
	print("Gold:", gold)
end

main()
