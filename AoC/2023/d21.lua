#!/usr/bin/luajit

local table_len = function(t)
	local ret = 0
	for k,v in pairs(t) do ret = ret + 1 end
	return ret
end

local function Queue(init)
	return {
		head = 1, tail = #init + 1, items = {unpack(init)},
		push = function(Q, item)
			Q.items[Q.tail] = item
			Q.tail = Q.tail + 1
		end,
		pop = function(Q)
			assert(not Q:empty())
			local ret = Q.items[Q.head]
			Q.items[Q.head] = nil
			Q.head = Q.head + 1
			return ret
		end,
		empty = function(Q) return Q.head == Q.tail end
	}
end

local Vadd = function(a, b) return { a[1] + b[1], a[2] + b[2] } end
local loc = function(v) return v[1] .. ',' .. v[2] end

local solve = function(map, rows, cord, target, rollover)
	local DIRS = { {1, 0}, {-1, 0}, {0, 1}, {0, -1} }
	local seen = {}
	local lookingfor = target % 2
	local endings = 0

	local Q = Queue{ { cord, cord, 0 } }
	while (not Q:empty()) do
		local cur, modcur, steps = unpack(Q:pop())
		local ch = map[loc(modcur)]
		local skey = loc(cur)

		if (ch ~= "." or seen[skey] ~= nil) then
			-- nop
		else
			seen[skey] = true
			if (steps % 2 == lookingfor) then
				endings = endings + 1
			end
			for _,d in ipairs(DIRS) do
				local nxt = Vadd(cur, d)
				local modnxt = nxt
				if (rollover) then
					modnxt = Vadd(modcur, d)
					if (map[loc(modnxt)] == nil) then
						if (modnxt[1] <= 0) then modnxt[1] = rows end
						if (modnxt[2] <= 0) then modnxt[2] = rows end
						if (modnxt[1] > rows) then modnxt[1] = 1 end
						if (modnxt[2] > rows) then modnxt[2] = 1 end
					end
					assert(map[loc(modnxt)] ~= nil)
				end
				if (steps + 1 <= target) then
					Q:push({ nxt, modnxt, steps + 1 })
				end
			end
		end
	end
	return endings
end

local main = function()
	local map, row, col, start = {}, 0, 0, nil
	for ln in io.stdin:lines() do
		row = row + 1
		for c=1, #ln do
			map[loc({row, c})] = ln:sub(c, c)
			if (ln:sub(c, c) == "S") then
				start = { row, c }
				map[loc({row, c})] = "."
			end
		end
		col = #ln
	end
	assert(row == col)

	local solve_quad = function(points, n)
		-- a*x^2 + b * x + C = res
		-- a*0 + b*0 + C = 3770
		-- C == 3770
		--
		-- a*1^2 + b * 1 + 3770 = 33665
		-- a + b = 33665 - 3770
		-- a := 33665 - 3770 - b
		--
		-- a*2^2 + b * 2 + 3770 = 93356
		-- a4 + 2b = 93356 - 3770
		--
		-- 4 * (33665 - 3770 - b) + 2b = 93356 - 3770
		-- 4*33665 - 4*3770 - 4b + 2b = 93356 - 3770
		-- 2b - 4b = 93356 - 3770 - 4*33665 + 4*3770
		-- -2b = 93356 - 3770 - 4*33665 + 4*3770
		-- b := (93356 - 3770 - 4*33665 + 4*3770) / -2
		local c = points[1]
		local b = (points[3] - c - 4*points[2] + 4*c) / -2
		local a = points[2] - c - b
		return a, b, c
	end

	print("Silver:", solve(map, row, start, 64, false))
	-- NOTE: only works on the input. not a general solution.
	local mid = math.floor(row/2)
	local points = {
		solve(map, row, start, mid, true),
		solve(map, row, start, mid + row, true),
		solve(map, row, start, mid + row*2, true),
	}
	local a,b,c = solve_quad(points)
	local n = 26501365
	local x = math.floor(n/row)
	local res = (a * x^2) + (b * x) + c
	print("Gold:  ", string.format("%.0f", res))
end

main()
