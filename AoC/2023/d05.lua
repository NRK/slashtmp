#!/usr/bin/luajit

local seeds = {}
for seed in io.stdin:read("*line"):gmatch("%d+") do
	seeds[#seeds + 1] = tonumber(seed)  -- dynamic typing is fucking retarded
end

local map = {}
local map_cnt = 0  -- 1 based indexing is fucking retarded
for ln in io.stdin:lines() do
	if (ln == "") then
		-- nop
	elseif (ln:match(":$")) then
		map_cnt = map_cnt + 1
		map[map_cnt] = {}
	else
		local to, from, len = ln:match("(%d+) (%d+) (%d+)")
		map[map_cnt][#map[map_cnt] + 1] = {
			start  = tonumber(from),
			eend   = from + (len - 1),
			offset = to - from,
			-- reverse mapping
			rstart    = tonumber(to),
			rend      = to + (len - 1),
			roffset   = from - to,
		}
	end
end

local min = 999999999999
for _,seed in ipairs(seeds) do
	local num = seed
	for _,t in ipairs(map) do
		for _,m in ipairs(t) do
			if (num >= m.start and num <= m.eend) then
				num = num + m.offset
				break
			end
		end
	end
	min = math.min(min, num)
end
print("silver: ", min)

-- NOTE: reverse brute force solution. takes a while to run (~30 seconds).
--       see d05.tcl for a range based solution that runs instantly.
for _,t in ipairs(map) do
	table.sort(t, function(a, b) return a.rstart < b.rstart end)
end
for loc = 0, math.huge do
	local seed = loc
	for mi = #map, 1, -1 do
		for _,m in ipairs(map[mi]) do
			if (seed >= m.rstart and seed <= m.rend) then
				seed = seed + m.roffset
				break
			elseif (seed < m.rstart) then
				break
			end
		end
	end
	for i = 1, #seeds, 2 do
		local eend = seeds[i] + (seeds[i+1] - 1)
		if (seed >= seeds[i] and seed <= eend) then
			print("gold: ", loc)
			return
		end
	end
end
