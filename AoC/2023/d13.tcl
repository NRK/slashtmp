#!/usr/bin/tclsh

proc transpose {matrix} {
	for {set i 0} {$i < [llength [lindex $matrix 0]]} {incr i} {
		lappend res [lsearch -all -inline -subindices -index $i $matrix *]
	}
	return $res
}

proc collapse {up down} {
	foreach u $up d $down {
		if {$u == "" || $d == ""} { break }
		incr ret [expr [join [lmap x $u y $d { expr {$x != $y} }] "+"]]
	}
	return $ret
}

proc solve {map expect} {
	for {set i 1} {$i < [llength $map]} {incr i} {
		if {[collapse [lreverse [lrange $map 0 $i-1]] [lrange $map $i end]] == $expect} {
			return [expr {$i * 100}]
		}
	}
	return [expr { [solve [transpose $map] $expect] / 100 }]
}

foreach ln [split [string map { "\n\n" "|" } [read -nonewline stdin]] "|"] {
	set map [lmap x [split $ln "\n"] { split $x {} }]
	incr silver [solve $map 0]
	incr gold [solve $map 1]
}
puts "silver: $silver \ngold:   $gold"
