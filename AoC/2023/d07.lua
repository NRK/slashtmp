#!/usr/bin/luajit

local hand_type_uncached = function(freq)
	if (freq[5]) then
		return 7
	elseif (freq[4]) then
		return 6
	elseif (freq[3] and freq[2]) then
		return 5
	elseif (freq[3]) then
		return 4
	elseif (freq[2] == 2) then
		return 3
	elseif (freq[2] == 1) then
		return 2
	else
		return 1
	end
end

local function hand_freq(hand, joker)
	local map = {}
	for ch in hand:gmatch(".") do
		map[ch] = (map[ch] or 0) + 1
	end
	if (joker) then
		local max, maxch = 0, "J"
		for ch,num in pairs(map) do
			if (ch ~= "J" and num > max) then
				max, maxch = num, ch
			end
		end
		return hand_freq(hand:gsub("J", tostring(maxch)), false)
	else
		local freq = {}
		for k,v in pairs(map) do
			freq[v] = (freq[v] or 0) + 1
		end
		return freq
	end
end

local hand_type = function(hand, joker, cache)
	local ret = cache[hand]
	if (ret == nil) then
		ret = hand_type_uncached(hand_freq(hand, joker))
		cache[hand] = ret
	end
	return ret
end

local hand_cmp = function(a, b, pow, joker, cache)
	local at, bt = hand_type(a, joker, cache), hand_type(b, joker, cache)
	if (at ~= bt) then
		return at < bt
	end
	for i = 1, a:len() do
		local ach, bch = a:sub(i, i), b:sub(i, i)
		if (ach ~= bch) then
			return pow[ach] < pow[bch]
		end
	end
	return false
end

local solve = function(cards, joker)
	local pow = {
		["A"] = 13, ["K"] = 12, ["Q"] = 11, ["J"] = 10, ["T"] =  9,
		["9"] =  8, ["8"] =  7, ["7"] =  6, ["6"] =  5, ["5"] =  4,
		["4"] =  3, ["3"] =  2, ["2"] =  1,
	}
	if (joker) then
		pow["J"] = 0
	end
	local cache = {}
	table.sort(cards, function(a, b) return hand_cmp(a.hand, b.hand, pow, joker, cache) end)
	local ret = 0
	for k,v in ipairs(cards) do
		ret = ret + (k * v.score)
	end
	return ret
end

local cards = {}
for ln in io.stdin:lines() do
	local h, score = ln:match("(%w+) (%d+)")
	cards[#cards + 1] = { hand = h, score = tonumber(score), }
end

print("silver:", solve(cards))
print("gold:", solve(cards, true))
