#!/usr/bin/luajit

local function solve(workflows, part, current)
	local OP = {
		['<'] = function(l, r) return l < r end,
		['>'] = function(l, r) return l > r end,
	}
	for k,v in ipairs(workflows[current]) do
		local lhs_idx, op, rhs, dest = unpack(v)
		if (OP[op](part[lhs_idx], rhs)) then
			if (dest:match("^[RA]$")) then
				return dest == "A"
			else
				return solve(workflows, part, dest)
			end
		end
	end
	assert(false)
end

local function satisfy(constrains, rule, accept)
	local lhs_idx, op, rhs, dest = unpack(rule)
	local c = constrains[lhs_idx]
	-- [min,max)
	if (op == '<') then -- x < 500
		if (accept) then -- [min,rhs)
			c[2] = math.min(rhs, c[2])
		else -- [rhs,max)
			c[1] = math.max(rhs, c[1])
		end
	elseif (op == '>') then -- x > 500
		if (accept) then -- [rhs+1,max)
			c[1] = math.max(rhs + 1, c[1])
		else -- [min,rhs+1)
			c[2] = math.min(rhs + 1, c[2])
		end
	end
	return constrains
end

local function solve2(workflows, parents, current, idx, constrains)
	local w = workflows[current]
	for i=idx, 1, -1 do
		constrains = satisfy(constrains, w[i], i == idx)
	end
	if (current == "in") then
		return constrains
	end
	local parent_name, parent_idx = unpack(parents[current])
	return solve2(workflows, parents, parent_name, parent_idx, constrains)
end

local main = function()
	local input = io.stdin:read("*all")
	local workflows, parents, accepts = {}, {}, {}
	for ln in input:match("(.*)\n\n"):gmatch("[^\n]+") do
		local name, rules = ln:match("(%a+){(.*)}")
		workflows[name] = {}
		for r in rules:gmatch("[^,]+") do
			local lhs, op, rhs, dest = r:match("(%a)([<>])(%d+):(%a+)")
			if (lhs == nil) then
				lhs, op, rhs, dest = "x", ">", -1, r  -- always true
			end
			table.insert(workflows[name], { lhs, op, tonumber(rhs), dest })
			if (dest == "A") then
				table.insert(accepts, {name, #workflows[name]})
			else
				parents[dest] = { name, #workflows[name] }
			end
		end
	end

	local silver = 0
	for ln in input:match(".-\n\n(.*)"):gmatch("[^\n]+") do
		local part, sum = {}, 0
		for catagory,rating in ln:gmatch("(%a)=(%d+)") do
			part[catagory] = tonumber(rating)
			sum = sum + tonumber(rating)
		end
		if (solve(workflows, part, "in")) then
			silver = silver + sum
		end
	end
	print("Silver:", silver)

	local gold = 0
	for _,v in ipairs(accepts) do
		local R = { x={1,4001}, m={1,4001}, a={1,4001}, s={1,4001} }
		local C = solve2(workflows, parents, v[1], v[2], R)
		local P = 1
		for _,v in pairs(C) do P = P * (v[2] - v[1]) end
		gold = gold + P
	end
	print("Gold:  ", string.format("%.0f", gold))
end

main()
