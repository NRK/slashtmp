#!/usr/bin/luajit

local table_len = function(t)
	local ret = 0
	for k,v in pairs(t) do ret = ret + 1 end
	return ret
end

local function table_append(t, idx, item)
	if (t[idx] == nil) then t[idx] = {} end
	table.insert(t[idx], item)
end

local function find_cut3(cons, start)
	local supernode = {
		cut = 0,
		edges = {}, nodes = {},
		consume = (function(self, g, node)
			assert(self.nodes[node] == nil)
			self.cut = self.cut - (self.edges[node] or 0)
			self.nodes[node] = true
			self.edges[node] = nil
			for _,edge in ipairs(g[node]) do
				-- discard edges pointing back into the supernode
				if (self.nodes[edge] == nil) then
					local n = self.edges[edge] or 0
					self.edges[edge] = n + 1
					self.cut = self.cut + 1  -- self.cut, LOL
				end
			end
		end)
	}

	supernode:consume(cons, start)
	local remaining = table_len(cons) - 1
	while (remaining > 0) do
		if (supernode.cut == 3) then
			return table_len(supernode.nodes)
		end
		local node, best = nil, -1
		for k,v in pairs(supernode.edges) do
			if (v > best) then
				node, best = k, v
			end
		end
		supernode:consume(cons, node)
		remaining = remaining - 1
	end
	return -1
end

local main = function()
	local cons = {}
	for ln in io.stdin:lines() do
		local node, edges = ln:match("(%a+): (.*)")
		for edge in edges:gmatch("[^ ]+") do
			table_append(cons, node, edge)
			table_append(cons, edge, node)
		end
	end
	-- basically Stoer-Wagner but with a single iteration. which means it's
	-- not guaranteed to find the solution the first time around. so we run
	-- it a bunch of times with different starting nodes. still not
	-- guaranteed (i think) but should have high probability of success
	-- similar to Krager's algorithm.
	for node,_ in pairs(cons) do
		local r = find_cut3(cons, node)
		if (r > 0) then
			print("Silver:", r * (table_len(cons) - r))
			break
		end
	end
end

main()
