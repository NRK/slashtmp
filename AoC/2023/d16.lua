#!/usr/bin/luajit

local table_len = function(t)
	local ret = 0
	for k,v in pairs(t) do ret = ret + 1 end
	return ret
end

local function up(r, c)    return r - 1, c end
local function down(r, c)  return r + 1, c end
local function left(r, c)  return r, c - 1 end
local function right(r, c) return r, c + 1 end
local function loc(r, c)   return r .. ',' .. c end

local function beam(grid, r, c, going)
	local transition = {
		[right] = {
			['-']  = { right }, ['|']  = { up, down },
			['\\'] = { down },  ['/']  = { up },
		},
		[left] = {
			['-']  = { left }, ['|']  = { up, down },
			['\\'] = { up },   ['/']  = { down },
		},
		[up] = {
			['-']  = { left, right }, ['|']  = { up },
			['\\'] = { left },        ['/']  = { right },
		},
		[down] = {
			['-']  = { left, right }, ['|']  = { down },
			['\\'] = { right },       ['/']  = { left },
		},
	}
	local cycle, hits = {}, {}
	local q = { { r, c, going } }
	local pop = function(q) local ret = q[#q]; q[#q] = nil; return ret; end

	while (#q > 0) do
		local r, c, going = unpack(pop(q))
		local nr, nc = going(r, c)
		local nch = grid[loc(nr, nc)]
		local hkey = loc(nr, nc)
		local ckey = hkey .. ',' .. tostring(going)

		if (nch == nil) then
			-- nop
		elseif (cycle[ckey] ~= nil) then
			-- cycle
		else
			hits[hkey] = true
			cycle[ckey] = true
			for _,dir in ipairs(transition[going][nch] or { going }) do
				q[#q + 1] = { nr, nc, dir }
			end
		end
	end
	return table_len(hits)
end

local main = function()
	local grid, row, col = {}, 1, 1
	for ln in io.stdin:lines() do
		col = 1
		for i=1, #ln do
			grid[loc(row, col)] = ln:sub(i, i)
			col = col + 1
		end
		row = row + 1
	end
	print("Silver:", beam(grid, 1, 0, right))

	local gold = 0
	for r = 1, row-1 do
		gold = math.max(gold, beam(grid, r, 0, right))
		gold = math.max(gold, beam(grid, r, col, left))
	end
	for c = 1, col-1 do
		gold = math.max(gold, beam(grid, 0, c, down))
		gold = math.max(gold, beam(grid, row, c, up))
	end
	print("Gold:  ", gold)
end

main()
