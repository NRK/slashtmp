#!/usr/bin/luajit

local parse = function(ln)
	local ret = {}
	ln:gsub("(%d+)", function(m) ret[#ret + 1] = m end)
	return ret
end

local solve = function(t, d)
	-- equation obtained via wolframalpha: (t - x) * x = d+1
	local magic = math.sqrt(math.pow(t, 2) - 4 * (d+1))
	local min = math.ceil((t - magic) * 0.5)
	local max = math.floor((t + magic) * 0.5)
	return (max - min) + 1
end

local time = parse(io.stdin:read("*line"))
local dist = parse(io.stdin:read("*line"))

local silver = 1
for tidx,t in ipairs(time) do
	silver = silver * solve(t, dist[tidx])
end
local gold = solve(table.concat(time), table.concat(dist))

print("silver:", silver)
print("gold:", gold)
