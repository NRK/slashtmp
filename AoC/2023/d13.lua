#!/usr/bin/luajit

local split = function(s)
	local ret = {}
	s:gsub(".", function(m) ret[#ret + 1] = m end)
	return ret
end

local repair = function(l0, l1)
	assert(#l0 == #l1)
	l0, l1 = { unpack(l0) }, { unpack(l1) }
	for i = 1, #l0 do
		if (l0[i] ~= l1[i]) then
			l0[i] = l1[i]
			return table.concat(l0) == table.concat(l1)
		end
	end
	assert(false)
end

local cmp = function(m, i, k, have_repair)
	local match = table.concat(m[i]) == table.concat(m[k])
	if (not match and have_repair) then
		match = repair(m[i], m[k])
		have_repair = false
	end
	return match, have_repair
end

local reflect = function(m, do_repair)
	local rows, cols = #m, #m[1]
	for i = 1, rows-1 do
		local have_repair = do_repair
		local match, have_repair = cmp(m, i, i + 1, have_repair)
		if (match) then
			local match = true
			local u, d = i-1, i+2
			while (m[u] and m[d] and match) do
				match, have_repair = cmp(m, u, d, have_repair)
				u = u - 1
				d = d + 1
			end

			if (match and (not do_repair or not have_repair)) then
				return i
			end
		end
	end
	return 0
end

local rc_reverse = function(m)
	local ret = {}
	for r = 1, #m do
		for c = 1, #m[r] do
			if (ret[c] == nil) then ret[c] = {} end
			ret[c][r] = m[r][c]
		end
	end
	return ret
end

local solve = function(m, do_repair)
	local r = reflect(m, do_repair) * 100
	return r > 0 and r or reflect(rc_reverse(m), do_repair)
end

local main = function()
	local map = {}
	local silver, gold = 0, 0
	repeat
		local ln = io.stdin:read("*line")
		if (ln == "" or ln == nil) then
			silver = silver + solve(map, false)
			gold = gold + solve(map, true)
			map = {}
		else
			map[#map + 1] = split(ln)
		end
	until (ln == nil)
	print("silver:", silver)
	print("gold:", gold)
end

main()
