#!/usr/bin/luajit

-- utils
local split = function(s, ch)
	local ret = {}
	local pat = ch == "" and "." or ("[^" .. (ch or " \n\t") .. "]+")
	s:gsub(pat, function(m) ret[#ret + 1] = m end)
	return ret
end
local Vadd = function(a, b) return { a[1] + b[1], a[2] + b[2] } end
local Vsub = function(a, b) return { a[1] - b[1], a[2] - b[2] } end
local Veq  = function(a, b)
	assert(#a == 2 and #b == 2)
	return a[1] == b[1] and a[2] == b[2]
end

local loc = function(m, cord) return m[cord[1]] and m[cord[1]][cord[2]] end

local function MinHeap(init)
	local ret = {
		items = {unpack(init)},
		push = function(Q, item)
			local new = #Q.items + 1
			Q.items[new] = item
			local parent = math.floor(new / 2)
			while (new ~= 1 and Q.items[parent][1] > Q.items[new][1]) do
				Q.items[new], Q.items[parent] =
					Q.items[parent], Q.items[new]
				new = parent
				parent = math.floor(new / 2)
			end
		end,
		pop = function(Q)
			assert(not Q:empty())
			local ret = Q.items[1]
			local adjust, last = 1, #Q.items
			Q.items[adjust] = nil
			Q.items[adjust] = Q.items[last]
			Q.items[last] = nil
			local cost = function(idx) return (Q.items[idx] or {math.huge})[1] end
			repeat
				local l, r = (adjust * 2), (adjust * 2) + 1
				local smaller = (cost(l) <= cost(r)) and l or r
				if (Q.items[smaller] and
				    Q.items[adjust][1] > Q.items[smaller][1])
				then
					Q.items[smaller], Q.items[adjust] =
						Q.items[adjust], Q.items[smaller]
					adjust = smaller
				end
			until (adjust ~= smaller)
			return ret
		end,
		empty = function(Q) return #Q.items == 0 end,
	}
	table.sort(ret.items, function(a, b) return a[1] < b[1] end)
	return ret
end

local function solve(grid, min, max)
	local C = table.concat
	local ALL_DIRS = { {-1, 0}, {0, -1}, {1, 0}, {0, 1} }  -- up, left, down, right
	local dest = {#grid, #grid[1]}
	local seen = {}

	-- { prev_cost, cur, dir, same_dir_cnt }
	local Q = MinHeap{ { -grid[1][1], {1, 1}, {-9,-9}, -1 } }
	while (not Q:empty()) do
		local prev_cost, cur, dir, same_dir_cnt = unpack(Q:pop())
		local cur_cost = prev_cost + loc(grid, cur)
		local seen_key = C(cur, ',') .. ' ' ..  C(dir, ',') .. ' ' .. same_dir_cnt

		if (Veq(cur, dest) and same_dir_cnt >= min) then
			return cur_cost
		elseif (seen[seen_key] ~= nil and seen[seen_key] <= cur_cost) then
			-- nop
		else
			seen[seen_key] = cur_cost
			for _,nxtdir in ipairs(ALL_DIRS) do
				local nxt = Vadd(cur, nxtdir)
				if (loc(grid, nxt) == nil) then
					-- nop
				elseif (Veq(nxtdir, { -dir[1], -dir[2] })) then
					-- nop
				elseif (Veq(nxtdir, dir)) then
					if (same_dir_cnt < max) then
						Q:push({ cur_cost, nxt, nxtdir, same_dir_cnt + 1 })
					end
				else
					if (same_dir_cnt >= min or same_dir_cnt == -1) then
						Q:push({ cur_cost, nxt, nxtdir, 1 })
					end
				end
			end
		end
	end
	assert(false)
end

local grid = {}
for ln in io.stdin:lines() do
	grid[#grid + 1] = split(ln, "")
end
print("Silver:", solve(grid, 1, 3))
print("Gold:  ", solve(grid, 4, 10))
