#!/usr/bin/tclsh
namespace path {tcl::mathop tcl::mathfunc}

proc overlap {a b} {
	lassign [concat $a $b] a0 a1 b0 b1
	lassign [set overlap [list [max $a0 $b0] [min $a1 $b1]]] o0 o1
	set remaining_start [list $a0 $o0]
	set remaining_end   [list $o1 $a1]
	return [list $overlap $remaining_start $remaining_end]
}

proc range_len {range} {
	return [- [lindex $range 1] [lindex $range 0]]
}

proc solve {seeds mappings} {
	set min 999999999999999
	set Q $seeds
	foreach map $mappings {
		set nextQ {}
		while {[llength $Q] > 0} {
			set Q [lassign $Q seed]
			foreach range $map {
				lassign $range lo hi off
				lassign [overlap $seed $range] o s e
				if {[range_len $o] > 0} {
					lappend nextQ [lmap x $o { list [+ $x $off] }]
					if {[range_len $s] > 0} {
						lappend Q $s
					}
					if {[range_len $e] > 0} {
						lappend Q $e
					}
					set seed {0 0}
					break
				}
			}
			if {[range_len $seed] > 0} {
				lappend nextQ $seed
			}
		}
		set Q $nextQ
	}
	return [lindex [lsort -integer -index 0 $Q] 0 0]
}

set seeds [regexp -all -inline {\d+} [gets stdin]]
foreach block [split [string map {\n\n |} [read -nonewline stdin]] "|"] {
	set m [regexp -all -inline {\d+} $block]
	lappend mappings [lmap {to from len} $m {
		list $from [+ $from $len] [- $to $from]
	}]
}
puts "silver: [solve [lmap x $seeds { list $x [+ $x 1] }] $mappings]"
puts "gold:   [solve [lmap {x len} $seeds { list $x [+ $x $len] }] $mappings]"
