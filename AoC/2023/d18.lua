#!/usr/bin/luajit

local solve = function(plan)
	local shoelace = function(a, b) return (a[1] * b[2]) - (a[2] * b[1]) end
	local area, borders, last = 0, 0, {1, 1}
	for _,v in ipairs(plan) do
		local dir, step = v[1], v[2]
		local vec = { dir[1] * step, dir[2] * step }
		local new = { last[1] + vec[1], last[2] + vec[2] }
		area = area + shoelace(last, new)
		borders = borders + step
		last = new
	end
	area = math.abs(area) * 0.5
	local inside = area - (borders/2) + 1
	return borders + inside
end

local DIRS = {
	["R"] = {  0,  1 }, ["0"] = {  0,  1 },
	["D"] = {  1,  0 }, ["1"] = {  1,  0 },
	["L"] = {  0, -1 }, ["2"] = {  0, -1 },
	["U"] = { -1,  0 }, ["3"] = { -1,  0 },
}
local plan, golden_plan = {}, {}
for ln in io.stdin:lines() do
	local dir, step, color = ln:match("(%a) (%d+) [(]#(%x+)[)]")
	local step2 = color:sub(1, #color - 1)
	local dir2  = color:sub(#color, #color)
	table.insert(plan, { DIRS[dir], tonumber(step) })
	table.insert(golden_plan, { DIRS[dir2], tonumber(step2, 16) })
end
print("Silver:", solve(plan))
print("Gold:  ", string.format("%.0f", solve(golden_plan)))
