#!/usr/bin/luajit

local function expect(rec, idx, seq)
	for i = idx, idx + seq - 1 do
		if (not (rec[i] == '#' or rec[i] == '?')) then
			return false
		end
	end
	return rec[idx + seq] == '.' or rec[idx + seq] == '?' or rec[idx + seq] == nil
end

local function solve(rec, chidx, seqidx, cache)
	local ch, seq = rec.rec[chidx], rec.seq[seqidx]
	if (chidx > 1 and ch ~= nil) then
		assert(rec.rec[chidx-1] == '.' or rec.rec[chidx-1] == '?')
	end
	local hit = cache[chidx .. ',' .. seqidx]

	if (hit ~= nil) then
		return hit
	elseif (ch == nil and seq == nil) then
		return 1
	elseif (ch == nil and seq ~= nil) then
		return 0
	elseif (ch ~= nil and seq == nil) then
		if (not table.concat(rec.rec, '', chidx):match("#")) then
			return 1
		else
			return 0
		end
	elseif (ch ~= nil and seq ~= nil) then
		local sum = 0
		if (ch == "." or ch == "?") then
			local res = solve(rec, chidx + 1, seqidx, cache)
			cache[(chidx + 1) .. ',' .. seqidx] = res
			sum = sum + res
		end
		if (ch == "#" or ch == "?") then
			local gottem = expect(rec.rec, chidx, seq)
			if (gottem) then
				local res = solve(
					rec, chidx + seq + 1, seqidx + 1, cache
				)
				cache[(chidx + seq + 1) .. ',' .. (seqidx + 1)] = res
				sum = sum + res
			end
		end
		return sum
	else
		assert(false)
	end
end

local main = function()
	local records = {}
	for ln in io.stdin:lines() do
		local puz, seq = ln:match("(.*) (.*)")
		records[#records + 1] = { rec = {}, seq = {} }
		for ch in puz:gmatch(".") do
			table.insert(records[#records].rec, ch)
		end
		for n in seq:gmatch("%d+") do
			table.insert(records[#records].seq, tonumber(n))
		end
	end

	local trep = function(t, times, joiner)
		local ret = {}
		for i = 1, times do
			for k,v in ipairs(t) do table.insert(ret, v) end
			if (joiner and i ~= times) then table.insert(ret, joiner) end
		end
		return ret
	end

	local silver, gold = 0, 0
	for k,r in ipairs(records) do
		silver = silver + solve(r, 1, 1, {})
		gold = gold + solve(
			{ rec = trep(r.rec, 5, '?'), seq = trep(r.seq, 5) },
			1, 1, {}
		)
	end
	print("silver:", silver)
	print("gold:", gold)
end

main()
