// streaming api for xxhash64:
// https://github.com/Cyan4973/xxHash/blob/dev/doc/xxhash_spec.md#xxh64-algorithm-description
//
// on gcc/clang performance should be similar as the reference implementation.
// msvc (v19.38) fails at folding shift+or into a single `mov`.
//
// This is free and unencumbered software released into the public domain.
// For more information, please refer to <https://unlicense.org/>
#include <stddef.h>
#include <stdint.h>

//////////////// private helpers

#define XXH64__P1 UINT64_C(0x9E3779B185EBCA87)
#define XXH64__P2 UINT64_C(0xC2B2AE3D27D4EB4F)
#define XXH64__P3 UINT64_C(0x165667B19E3779F9)
#define XXH64__P4 UINT64_C(0x85EBCA77C2B2AE63)
#define XXH64__P5 UINT64_C(0x27D4EB2F165667C5)

static uint64_t
xxh64__rotl(uint64_t v, unsigned int n)
{
	return (v << n) | (v >> (-n & 63));
}

static uint64_t
xxh64__load64le(const uint8_t *p)
{
	return (uint64_t)p[0] <<  0 | (uint64_t)p[1] <<  8 |
	       (uint64_t)p[2] << 16 | (uint64_t)p[3] << 24 |
	       (uint64_t)p[4] << 32 | (uint64_t)p[5] << 40 |
	       (uint64_t)p[6] << 48 | (uint64_t)p[7] << 56;
}

static uint32_t
xxh64__load32le(const uint8_t *p)
{
	return (uint32_t)p[0] <<  0 | (uint32_t)p[1] <<  8 |
	       (uint32_t)p[2] << 16 | (uint32_t)p[3] << 24;
}

static uint64_t
xxh64__round(uint64_t acc, uint64_t lane)
{
	acc += lane * XXH64__P2;
	return xxh64__rotl(acc, 31) * XXH64__P1;
}

static uint64_t
xxh64__merge(uint64_t a, uint64_t b)
{
	a ^= xxh64__round(0, b);
	a *= XXH64__P1;
	return a + XXH64__P4;
}

static void
xxh64__process(uint64_t state[static restrict 4], const uint8_t *restrict p)
{
	state[0] = xxh64__round(state[0], xxh64__load64le(p +  0));
	state[1] = xxh64__round(state[1], xxh64__load64le(p +  8));
	state[2] = xxh64__round(state[2], xxh64__load64le(p + 16));
	state[3] = xxh64__round(state[3], xxh64__load64le(p + 24));
}

//////////////// public API

typedef struct {
	uint64_t state[4];
	uint64_t bytes_total;
	uint8_t buf[32];
	int buf_len;
} XXH64;

static XXH64
hash_init(uint64_t sneed)
{
	XXH64 ctx[1] = {0};
	ctx->state[0] = sneed + XXH64__P1 + XXH64__P2;
	ctx->state[1] = sneed + XXH64__P2;
	ctx->state[2] = sneed;
	ctx->state[3] = sneed - XXH64__P1;
	return *ctx;
}

static void
hash_append(XXH64 *restrict ctx, const void *restrict key, size_t len)
{
	const uint8_t *restrict p = key;
	size_t l = len;
	// if there's data in ctx->buf, try to fill it up
	if (ctx->buf_len > 0) for (; l > 0 && ctx->buf_len < 32; ++p, --l) {
		ctx->buf[ctx->buf_len++] = *p;
	}
	// flush it if it's filled
	if (ctx->buf_len == 32) {
		xxh64__process(ctx->state, ctx->buf);
		ctx->buf_len = 0;
	}
	// process stripes, no copy
	for (; l >= 32; p += 32, l -= 32) {
		xxh64__process(ctx->state, p);
	}
	// tail end of the input goes to the buffer
	for (; l > 0; ++p, --l) {
		ctx->buf[ctx->buf_len++] = *p;
	}
	ctx->bytes_total += len;
}

static uint64_t
hash_finalize(XXH64 *restrict ctx)
{
	uint64_t acc = ctx->state[2] + XXH64__P5; // for < 32 bytes input
	if (ctx->bytes_total >= 32) {
		acc = xxh64__rotl(ctx->state[0],  1) +
		      xxh64__rotl(ctx->state[1],  7) +
		      xxh64__rotl(ctx->state[2], 12) +
		      xxh64__rotl(ctx->state[3], 18);
		acc = xxh64__merge(acc, ctx->state[0]);
		acc = xxh64__merge(acc, ctx->state[1]);
		acc = xxh64__merge(acc, ctx->state[2]);
		acc = xxh64__merge(acc, ctx->state[3]);
	}

	acc = acc + ctx->bytes_total;

	uint8_t *p = ctx->buf;
	size_t l = ctx->buf_len;

	for (; l >= 8; p += 8, l -= 8) {
		acc ^= xxh64__round(0, xxh64__load64le(p));
		acc  = xxh64__rotl(acc, 27) * XXH64__P1;
		acc += XXH64__P4;
	}

	for (; l >= 4; p += 4, l -= 4) {
		acc ^= xxh64__load32le(p) * XXH64__P1;
		acc  = xxh64__rotl(acc, 23) * XXH64__P2;
		acc += XXH64__P3;
	}

	for (; l > 0; ++p, --l) {
		acc ^= *p * XXH64__P5;
		acc  = xxh64__rotl(acc, 11) * XXH64__P1;
	}

	acc ^= acc >> 33; acc *= XXH64__P2;
	acc ^= acc >> 29; acc *= XXH64__P3;
	acc ^= acc >> 32;
	return acc;
}


#ifdef DEMO
#include <stdio.h>
#include <inttypes.h>
#ifdef __unix__
	#include <time.h>
#endif

static uint64_t
hash(const void *k, size_t len, uint64_t sneed)
{
	XXH64 ctx = hash_init(sneed);
	uint64_t rng = (uintptr_t)&hash * UINT64_C(5555); rng ^= (uintptr_t)&rng;
	for (const uint8_t *p = k, *end = p + len; p < end;) {
		rng = rng * UINT64_C(1111111111111111111) + 0x1337;
		// XXX: use larger range if benchmarking for speed
		ptrdiff_t n = rng >> (64 - 6);  // [0, 64)
		ptrdiff_t avail = end - p;
		n = (n <= avail) ? n : avail;
		hash_append(&ctx, p, n);
		p += n;
	}
	return hash_finalize(&ctx);
}

int main(void)
{
	{
		char buf[] = "sensitive young man with autism";
		uint64_t h = hash(buf, sizeof buf, 0);
		printf("%016" PRIX64 "\n", h);  // A1F30032681EAE55
	}

	{
		char buf[] = "hello, world!";
		uint64_t h = hash(buf, sizeof buf, 0);
		printf("%016" PRIX64 "\n", h);  // C793351A9F13023A
	}

	{
		uint64_t h = hash(0, 0, 0);
		printf("%016" PRIX64 "\n", h);  // EF46DB3751D8E999
	}

#ifdef __unix__
	{
		static char buf[1<<24];
		char *p = buf;
		__asm ("" : "+r"(p));

		struct timespec time_start, time_end;
		clock_gettime(CLOCK_MONOTONIC, &time_start);
		uint64_t h = hash(p, sizeof buf, 0);
		clock_gettime(CLOCK_MONOTONIC, &time_end);
		uint64_t d = time_end.tv_sec - time_start.tv_sec;
		d *= UINT64_C(1000) * UINT64_C(1000);
		d += (time_end.tv_nsec - time_start.tv_nsec) / UINT64_C(1000);
		printf("%016" PRIX64 "\n", h); // 87D2A1B6E1163EF1
		printf("time => %" PRIu64 "us\n", d);
	}
#endif

	fflush(stdout);
	return ferror(stdout);
}
#endif // DEMO
