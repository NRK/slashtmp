// streaming api for xxhash32:
// https://github.com/Cyan4973/xxHash/blob/dev/doc/xxhash_spec.md#xxh32-algorithm-description
//
// on gcc/clang performance should be similar as the reference implementation.
// msvc (v19.38) fails at folding shift+or into a single `mov`.
//
// This is free and unencumbered software released into the public domain.
// For more information, please refer to <https://unlicense.org/>
#include <stddef.h>
#include <stdint.h>

//////////////// private helpers

#define XXH32__P1 UINT32_C(0x9E3779B1)
#define XXH32__P2 UINT32_C(0x85EBCA77)
#define XXH32__P3 UINT32_C(0xC2B2AE3D)
#define XXH32__P4 UINT32_C(0x27D4EB2F)
#define XXH32__P5 UINT32_C(0x165667B1)

static uint32_t
xxh32__rotl(uint32_t v, int n)
{
	return (v << n) | (v >> (-n & 31));
}

static uint32_t
xxh32__load32le(const uint8_t *p)
{
	return (uint32_t)p[0] <<  0 | (uint32_t)p[1] <<  8 |
	       (uint32_t)p[2] << 16 | (uint32_t)p[3] << 24;
}

static void
xxh32__process(uint32_t state[static restrict 4], const uint8_t *restrict buf)
{
	for (int i = 0; i < 4; ++i) {
		state[i] += xxh32__load32le(buf + i*4) * XXH32__P2;
		state[i] = xxh32__rotl(state[i], 13) * XXH32__P1;
	}
}

//////////////// public api

typedef struct {
	uint32_t state[4];
	uint64_t bytes_total;
	uint8_t buf[16];
	int buf_len;
} XXH32;

static XXH32
hash_init(uint32_t sneed)
{
	XXH32 ctx[1] = {0};
	ctx->state[0] = sneed + XXH32__P1 + XXH32__P2;
	ctx->state[1] = sneed + XXH32__P2;
	ctx->state[2] = sneed;
	ctx->state[3] = sneed - XXH32__P1;
	return *ctx;
}

static void
hash_append(XXH32 *restrict ctx, const void *restrict key, size_t len)
{
	const uint8_t *p = key;
	size_t l = len;
	// if there's data in ctx->buf, try to fill it up
	if (ctx->buf_len > 0) for (; l > 0 && ctx->buf_len < 16; ++p, --l) {
		ctx->buf[ctx->buf_len++] = *p;
	}
	// flush it if it's filled
	if (ctx->buf_len == 16) {
		xxh32__process(ctx->state, ctx->buf);
		ctx->buf_len = 0;
	}
	// process blocks directly from the input, no copy
	for (; l >= 16; p += 16, l -= 16) {
		xxh32__process(ctx->state, p);
	}
	// tail end of the input goes to the buffer
	for (; l > 0; ++p, --l) {
		ctx->buf[ctx->buf_len++] = *p;
	}
	ctx->bytes_total += len;
}

static uint32_t
hash_finalize(XXH32 *ctx)
{
	// special case initialization, state[2] contains the seed
	uint32_t acc = ctx->state[2] + XXH32__P5;
	if (ctx->bytes_total >= 16) {
		// step 3: converge
		acc = xxh32__rotl(ctx->state[0],  1) +
		      xxh32__rotl(ctx->state[1],  7) +
		      xxh32__rotl(ctx->state[2], 12) +
		      xxh32__rotl(ctx->state[3], 18);
	}

	acc += (uint32_t)ctx->bytes_total; // step 4: add len

	// step 5: final consumption
	const uint8_t *p = ctx->buf;
	size_t l = ctx->buf_len;
	for (; l >= 4; p += 4, l -= 4) {
		acc += xxh32__load32le(p) * XXH32__P3;
		acc  = xxh32__rotl(acc, 17) * XXH32__P4;
	}
	for (; l > 0; ++p, --l) {
		acc += *p * XXH32__P5;
		acc  = xxh32__rotl(acc, 11) * XXH32__P1;
	}

	// step 6: mix
	acc ^= acc >> 15; acc *= XXH32__P2;
	acc ^= acc >> 13; acc *= XXH32__P3;
	acc ^= acc >> 16;
	return acc; // step 7: return :)
}


#ifdef DEMO
#include <stdio.h>
#include <inttypes.h>
#ifdef __unix__
	#include <time.h>
#endif

static uint32_t
hash(const void *k, size_t len, uint32_t sneed)
{
	XXH32 ctx = hash_init(sneed);
	uint64_t rng = (uintptr_t)&hash * UINT64_C(5555); rng ^= (uintptr_t)&rng;
	for (const uint8_t *p = k, *end = p + len; p < end;) {
		rng = rng * UINT64_C(1111111111111111111) + 0x1337;
		// XXX: use larger range if benchmarking for speed
		ptrdiff_t n = rng >> (64 - 5);  // [0, 32)
		ptrdiff_t avail = end - p;
		n = (n <= avail) ? n : avail;
		hash_append(&ctx, p, n);
		p += n;
	}
	return hash_finalize(&ctx);
}

int main(void)
{
	{
		char buf[] = "sensitive young man with autism";
		uint32_t h = hash(buf, sizeof buf, 0);
		printf("%08" PRIX32 "\n", h);  // 61CC6658
	}

	{
		char buf[] = "hello, world!";
		uint32_t h = hash(buf, sizeof buf, 0);
		printf("%08" PRIX32 "\n", h);  // 66E943F5
	}

	{
		uint32_t h = hash(0, 0, 0);
		printf("%08" PRIX32 "\n", h);  // 02CC5D05
	}

#ifdef __unix__
	{
		static char buf[1<<24];
		char *p = buf;
		__asm ("" : "+r"(p));

		struct timespec time_start, time_end;
		clock_gettime(CLOCK_MONOTONIC, &time_start);
		uint32_t h = hash(p, sizeof buf, 0);
		clock_gettime(CLOCK_MONOTONIC, &time_end);
		uint64_t d = time_end.tv_sec - time_start.tv_sec;
		d *= UINT64_C(1000) * UINT64_C(1000);
		d += (time_end.tv_nsec - time_start.tv_nsec) / UINT64_C(1000);
		printf("%08" PRIX32 "\n", h); // 0F64E81C
		printf("time => %" PRIu64 "us\n", d);
	}
#endif

	fflush(stdout);
	return ferror(stdout);
}
#endif
