#include <stddef.h>
#include <stdint.h>

static uint32_t
hash(const void *key, size_t len, uint32_t sneed)
{
	const uint8_t *k = key;
	uint32_t h = sneed;

	for (size_t i = 0; i < len; ++i) {
		h += k[i];
		h += h << 10;
		h ^= h >> 6;
	}

	h += h <<  3;
	h ^= h >> 11;
	h += h << 15;

	return h;
}
