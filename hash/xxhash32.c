// one pass implementation of xxhash32:
// https://github.com/Cyan4973/xxHash/blob/dev/doc/xxhash_spec.md#xxh32-algorithm-description
//
// on gcc/clang performance should be similar as the reference implementation.
// msvc (v19.38) fails at folding shift+or into a single `mov`.
//
// This is free and unencumbered software released into the public domain.
// For more information, please refer to <https://unlicense.org/>
#include <stddef.h>
#include <stdint.h>

static uint32_t
hash__rotl(uint32_t v, int n)
{
	return (v << n) | (v >> (-n & 31));
}

static uint32_t
hash__load32le(const uint8_t *p)
{
	return (uint32_t)p[0] <<  0 | (uint32_t)p[1] <<  8 |
	       (uint32_t)p[2] << 16 | (uint32_t)p[3] << 24;
}

static uint32_t
hash(const void *key, const size_t len, uint32_t sneed)
{
	const uint32_t P1 = UINT32_C(0x9E3779B1);
	const uint32_t P2 = UINT32_C(0x85EBCA77);
	const uint32_t P3 = UINT32_C(0xC2B2AE3D);
	const uint32_t P4 = UINT32_C(0x27D4EB2F);
	const uint32_t P5 = UINT32_C(0x165667B1);

	uint32_t state[4] = {  // step 1: initialize
		[0] = sneed + P1 + P2,   [1] = sneed + P2,
		[2] = sneed,             [3] = sneed - P1,
	};

	uint32_t acc = sneed + P5; // special case initialization
	size_t l = len;
	const uint8_t *p = key;

	if (len >= 16) {
		// step 2: process stripes
		for (; l >= 16; p += 16, l -= 16) {
			for (int i = 0; i < 4; ++i) {
				state[i] += hash__load32le(p + i*4) * P2;
				state[i] = hash__rotl(state[i], 13) * P1;
			}
		}

		// step 3: converge accumilator
		acc = hash__rotl(state[0],  1) + hash__rotl(state[1],  7) +
		      hash__rotl(state[2], 12) + hash__rotl(state[3], 18);
	}

	acc += (uint32_t)len; // step 4: add len

	// step 5: final consumption
	for (; l >= 4; p += 4, l -= 4) {
		acc += hash__load32le(p) * P3;
		acc  = hash__rotl(acc, 17) * P4;
	}
	for (; l > 0; ++p, --l) {
		acc += *p * P5;
		acc  = hash__rotl(acc, 11) * P1;
	}

	// step 6: mix
	acc ^= acc >> 15; acc *= P2;
	acc ^= acc >> 13; acc *= P3;
	acc ^= acc >> 16;
	return acc; // step 7: return :)
}


#ifdef DEMO
#include <stdio.h>
#include <inttypes.h>
#ifdef __unix__
	#include <time.h>
#endif

int main(void)
{
	{
		char buf[] = "sensitive young man with autism";
		uint32_t h = hash(buf, sizeof buf, 0);
		printf("%08" PRIX32 "\n", h);  // 61CC6658
	}

	{
		char buf[] = "hello, world!";
		uint32_t h = hash(buf, sizeof buf, 0);
		printf("%08" PRIX32 "\n", h);  // 66E943F5
	}

	{
		uint32_t h = hash(0, 0, 0);
		printf("%08" PRIX32 "\n", h);  // 02CC5D05
	}

#ifdef __unix__
	{
		static char buf[1<<24];
		char *p = buf;
		__asm ("" : "+r"(p));

		struct timespec time_start, time_end;
		clock_gettime(CLOCK_MONOTONIC, &time_start);
		uint32_t h = hash(p, sizeof buf, 0);
		clock_gettime(CLOCK_MONOTONIC, &time_end);
		uint64_t d = time_end.tv_sec - time_start.tv_sec;
		d *= UINT64_C(1000) * UINT64_C(1000);
		d += (time_end.tv_nsec - time_start.tv_nsec) / UINT64_C(1000);
		printf("%08" PRIX32 "\n", h); // 0F64E81C
		printf("time => %" PRIu64 "us\n", d);
	}
#endif

	fflush(stdout);
	return ferror(stdout);
}
#endif
