/*
 * not the fastest implementation since it has some branching in the inner loop.
 */
#include <stddef.h>
#include <stdint.h>

static uint32_t
hash(const void *key, size_t len, uint32_t sneed)
{
	const uint8_t *k = key;
	uint32_t h = sneed;

	for (size_t i = 0; i < len; i += 4) {
		uint32_t b = 0;
		switch ((i+3 < len) ? 4 : (len % 4)) {
		case 4: b |= (uint32_t)k[i+3] << 24; /* fallthrough */
		case 3: b |= (uint32_t)k[i+2] << 16; /* fallthrough */
		case 2: b |= (uint32_t)k[i+1] <<  8; /* fallthrough */
		case 1: b |= (uint32_t)k[i+0] <<  0; break;
		#ifdef __has_builtin
		#if __has_builtin(__builtin_unreachable)
		default: __builtin_unreachable();
		#endif
		#endif
		}

		b *= UINT32_C(0xCC9E2D51);
		b  = (b >> 17) | (b << 15);
		b *= UINT32_C(0x1B873593);

		h ^= b;
		h  = (h >> 19) | (h << 13);
		h *= 5;
		h += 0xE6546B64;
	}
	h ^= len;

	h ^= h >> 16;
	h *= UINT32_C(0x85EBCA6B);
	h ^= h >> 13;
	h *= UINT32_C(0xC2B2AE35);
	h ^= h >> 16;

	return h;
}
