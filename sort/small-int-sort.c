// benchmarking easy-to-roll sorting algorithms for "small" (roughly 2^7)
// integer array sorting.
//
// $ clang -o out sort/small-int-sort.c -O3 -march=native -fno-plt
//
// NOTE: gcc performs really badly here for whatever reason.
//       producing results 6~10x slower than clang.

#include <stddef.h>
#include <stdint.h>

#ifndef T
	#define T uint32_t
#endif
#define SWAP(A, B) do { T tmp = A; A = B; B = tmp; } while (0)

static void
insertion_sort(T *v, ptrdiff_t n)
{
	for (ptrdiff_t i = 1; i < n; ++i) {
		for (ptrdiff_t k = i; k > 0 && v[k-1] > v[k]; --k) {
			SWAP(v[k-1], v[k]);
		}
	}
}

static void
shellsort(T *v, ptrdiff_t n)
{
	static const int gaps[] = {
		// Ciura, unknown worst case.
		// 701, 301,
		132, 57, 23, 10, 4, 1
	};
	for (const int *g = gaps; g < 1[&gaps]; ++g) {
		for (ptrdiff_t i = *g; i < n; ++i) {
			T hole = v[i];
			ptrdiff_t k;
			for (k = i; k >= *g && v[k - *g] > hole; k -= *g) {
				v[k] = v[k - *g];
			}
			v[k] = hole;
		}
	}
}

static void
gnome_sort(T *v, ptrdiff_t n)
{
	for (ptrdiff_t i = 0; i < n; ) {
		if (i == 0 || v[i-1] <= v[i]) {
			++i;
		} else {
			SWAP(v[i-1], v[i]);
			--i;
		}
	}
}

static void
bubble_sort(T *v, ptrdiff_t n)
{
	for (;;) {
		int swapped = 0;
		ptrdiff_t newn = 0;
		for (ptrdiff_t i = 0; i < n-1; ++i) {
			if (v[i] > v[i+1]) {
				SWAP(v[i], v[i+1]);
				newn = i + 1;
				swapped = 1;
			}
		}
		if (!swapped)
			break;
		n = newn;
	}
}

// for libc qsort
static int
int_cmp(const void *a_, const void *b_)
{
	T a = *(const T *)a_;
	T b = *(const T *)b_;
	return (a > b) - (a < b);
}

#include <assert.h>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>

#define BENCH() for ( \
	struct timespec bench__e, bench__s, bench__c = {1}; \
	(clock_gettime(CLOCK_BOOTTIME, &bench__s)), bench__c.tv_sec; \
	clock_gettime(CLOCK_BOOTTIME, &bench__e), timediff_print_human(bench__e, bench__s), bench__c.tv_sec = 0)
static void
timediff_print_human(struct timespec e, struct timespec s)
{
	long sec = e.tv_sec - s.tv_sec;
	long ns = e.tv_nsec - s.tv_nsec;
	if (ns < 0) {
		--sec;
		ns += 1000000000L;
	}
	long ms = ns / 1000000;
	ns %=          1000000;
	long us = ns / 1000;
	ns %=          1000;

	if (sec) {
		fprintf(stderr, "%3ld.%03lds\n", sec, ms);
	} else if (ms) {
		fprintf(stderr, "%3ld.%03ldms\n", ms, us);
	} else if (us) {
		fprintf(stderr, "%3ld.%03ldus\n", us, ns);
	} else {
		fprintf(stderr, "%3ldns\n", ns);
	}
}

static void
init(T *v, ptrdiff_t n, uint64_t rng)
{
	for (ptrdiff_t i = 0; i < n; ++i) {
		rng = rng * 0xB0B13375AAD + 0x1337;
		v[i] = (rng >> (rng >> 59)) ^ rng;
	}
}

static void
validate(T *v, ptrdiff_t n)
{
	for (ptrdiff_t i = 0; i < n; ++i) {
		for (ptrdiff_t k = i + 1; k < n; ++k) {
			assert(v[i] <= v[k]);
		}
	}
}

extern int
main(int argc, char **argv)
{
	enum { N = 1 << 7 };
	static T buf[N];
	const uint64_t seed = (uintptr_t)argv ^ (uintptr_t)&main;

	init(buf, N, seed);
	fprintf(stderr, "libc qsort(%d)  ", N);
	BENCH() {
		qsort(buf, N, sizeof *buf, int_cmp);
	}
	validate(buf, N);

	init(buf, N, seed);
	fprintf(stderr, "bubble_sort(%d) ", N);
	// competitive with glibc qsort upto 2^6, or 2^5 without the "newn" optimization
	BENCH() {
		bubble_sort(buf, N);
	}
	validate(buf, N);

	init(buf, N, seed);
	fprintf(stderr, "gnome_sort(%d)  ", N);
	// 2^6
	BENCH() {
		gnome_sort(buf, N);
	}
	validate(buf, N);

	init(buf, N, seed);
	fprintf(stderr, "insertion(%d)   ", N);
	// 2^9
	BENCH() {
		insertion_sort(buf, N);
	}
	validate(buf, N);

	init(buf, N, seed);
	fprintf(stderr, "shellsort(%d)   ", N);
	// performance depends entirely on the gap sequence.
	BENCH() {
		shellsort(buf, N);
	}
	validate(buf, N);
}
