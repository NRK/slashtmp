// ==================================================
// introsort with some ideas taken from pdqsort
//     https://en.wikipedia.org/wiki/Introsort
//     https://github.com/orlp/pdqsort & https://arxiv.org/pdf/2106.05123
// ==================================================
// * O(N log N) worst case guarantee
// * in-place, with no heap allocation (only additional space used is for
//   recursion, which is limited to `2.25 * log N`)
// * very fast on random input
// * adaptive: will run in O(N) time if input is already sorted or reverse sorted.
// * deterministic: no random sampling for pivot selection
// * standard qsort() like interface
// ==================================================
// high level description of the algorithm:
//
// function aisort(input[]):  // recursion_limit = log2(len(input)) * 2.25
// 	if len(input) <= tiny_array_threshold:
// 		return insertion_sort(input);
//
// 	if recursion_limit_ran_out || back_to_back_M_bad_partition:   // M == 6 does good
// 		return heapsort(input); // prevents n^2 runtime, any other N log N algorithm will work
//
// 	if last_partition_was_already_partitioned && last_partition_wasnt_bad:
// 		if try_insertion_sort(input):  // this allows O(n) runtime on already sorted input
// 			return;
//
// 	if len(input) > NINTHER:   // NINTHER == 128 does good
// 		pseudo_median_of_nine_pivot();
// 	else:
// 		median_of_three_pivot();
// 	balanced_partition();
//
// 	aisort(input[0 .. left]);
// 	aisort(input[left .. end]);
// endfunction
//
// TODO: describe balanced_partition()

#include <stdint.h>
#include <stddef.h>

#ifndef AISORT_API
	#define AISORT_API static
#endif

#ifdef AISORT_REPLACE_QSORT
	#define qsort  (a, b, c, d)    aisort  ((a), (b), (c), (d))
	#define qsort_r(a, b, c, d, e) aisort_r((a), (b), (c), (d), (e))
#endif

AISORT_API void aisort_r(
	void *vp, ptrdiff_t len, ptrdiff_t item_size,
	int (*cmp)(const void *, const void *, void *), void *userptr
);
AISORT_API void aisort(
	void *vp, ptrdiff_t len, ptrdiff_t item_size,
	int (*cmp)(const void *, const void *)
);


////////////// implementation
#if !defined(AISORT_ONLY_HEADER)

////////////// utils

static inline void
aisort__swap(char *restrict a, char *restrict b, ptrdiff_t item_size)
{
#if 1 && defined(__GNUC__)
	switch (item_size) {
	char tmp[8];
	case 16: __builtin_memcpy(tmp, a, 8); __builtin_memcpy(a, b, 8); __builtin_memcpy(b, tmp, 8); a += 8, b += 8; // fallthrough
	case  8: __builtin_memcpy(tmp, a, 8); __builtin_memcpy(a, b, 8); __builtin_memcpy(b, tmp, 8); return;
	case  4: __builtin_memcpy(tmp, a, 4); __builtin_memcpy(a, b, 4); __builtin_memcpy(b, tmp, 4); return;
	}
#endif
	for (ptrdiff_t i = 0; i < item_size; ++i) {
		char tmp = a[i];
		a[i] = b[i];
		b[i] = tmp;
	}
}

static int
aisort__cmp_wrap(const void *a, const void *b, void *vfunc)
{
	// ISO C doesn't allow function pointer to void pointer conversion
	// which gets a -Wpedantic warning on gcc.
	// note that POSIX allows this.
	// smuggle it through `uintptr_t` to avoid the warning.
	uintptr_t func = (uintptr_t)vfunc;
	return ((int (*)(const void *, const void *))func)(a, b);
}

static void
aisort__median(
	char *lo, char *mid, char *hi, ptrdiff_t item_size,
	int (*cmp)(const void *, const void *, void *), void *userptr
)
{
	if (cmp(lo, hi, userptr) > 0) {
		aisort__swap(lo, hi, item_size);
	}
	if (cmp(mid, hi, userptr) > 0) {
		aisort__swap(mid, hi, item_size);
	} else if (cmp(lo, mid, userptr) > 0) {
		aisort__swap(lo, mid, item_size);
	}
}

///////////// heapsort for fallback
// not the most optimized since we're not expecting to hit this case much if at
// all. sort/heapsort.c contains a slightly more optimized version.

static inline ptrdiff_t
aisort__heap_parent(ptrdiff_t pos, unsigned int lsbit, ptrdiff_t item_size)
{
	pos -= item_size;
	pos -= (pos & lsbit) ? item_size : 0;
	return pos / 2;
}

// bottom up variant.  [root, end)  are sizes, not indices.
static void
aisort__heapify(
	char *v, ptrdiff_t root, ptrdiff_t end, ptrdiff_t item_size,
	int (*cmp)(const void *, const void *, void *), void *userptr
)
{
	ptrdiff_t leaf = root;
	ptrdiff_t lsbit = item_size & -item_size;

	for (;;) { // descent all the way down the the leaf
		ptrdiff_t child = (leaf * 2) + item_size;
		if (child >= end) {
			break;
		}
		ptrdiff_t right = child + item_size;
		if (right < end && cmp(v + right, v + child, userptr) >= 0) {
			child += item_size; // take right child whenever possible
		}
		leaf = child;
	}
	ptrdiff_t pos = leaf; // compare upwards and find correct position for the root
	while (pos != root && cmp(v + root, v + pos, userptr) > 0)
		pos = aisort__heap_parent(pos, lsbit, item_size);
	for (ptrdiff_t correct = pos; pos != root;) { // keep swapping until root is at correct position
		pos = aisort__heap_parent(pos, lsbit, item_size);
		aisort__swap(v + pos, v + correct, item_size);
	}
}

static void
aisort__heapsort(
	void *vp, ptrdiff_t len, ptrdiff_t item_size,
	int (*cmp)(const void *, const void *, void *), void *userptr
)
{
	char *v = vp;
	ptrdiff_t end = len * item_size;
	for (ptrdiff_t i = (len/2 - 1) * item_size; i >= 0; i -= item_size) {
		aisort__heapify(v, i, end, item_size, cmp, userptr);
	}
	for (ptrdiff_t e = end - item_size; e > 0; e -= item_size) {
		aisort__swap(v, v + e, item_size);
		aisort__heapify(v, 0, e, item_size, cmp, userptr);
	}
}

//////////// insertion sort

static int
aisort__insertion_sort(
	char *p, ptrdiff_t len, ptrdiff_t item_size,
	int (*cmp)(const void *, const void *, void *), void *userptr,
	ptrdiff_t thresh
)
{
	ptrdiff_t end = len * item_size;
	for (ptrdiff_t i = item_size; i < end; i += item_size) {
		for (ptrdiff_t k = i; k > 0; k -= item_size) {
			if (cmp(p + k - item_size, p + k, userptr) > 0) {
				aisort__swap(p + k - item_size, p + k, item_size);
				if (thresh-- == 0) return -1;
			} else {
				break;
			}
		}
	}
	return 0;
}

//////////// quicksort/adaptive-introsort

static void
aisort__main(
	char *v, ptrdiff_t len, ptrdiff_t item_size,
	int (*cmp)(const void *, const void *, void *), void *userptr,
	uint32_t meta
)
{
	enum { NINTHER = 128 };
	ptrdiff_t insertion_sort_thresh = 12;
	insertion_sort_thresh += (item_size < (int)sizeof(ptrdiff_t)) ? 4 : 0;

	// tiny array cut-off
	if (len <= insertion_sort_thresh) {
		aisort__insertion_sort(v, len, item_size, cmp, userptr, -1);
		return;
	}

	// prevent n^2 runtime
	uint32_t depth = meta & 0xFF;
	uint32_t hist  = meta >> 9;
	int bad_partitions = (hist & 0x3F) == 0x3F; // we got 6 back to back bad partitions
	if (depth == 0 || bad_partitions) {
		/* __builtin_printf("  [degrade] %s\n", (depth == 0) ? "recursion limit" : "bad partitioning"); */
		aisort__heapsort(v, len, item_size, cmp, userptr);
		return;
	}

	// optimistic O(n) runtime
	int was_already_partitioned = meta & (1 << 8);
	int last_partition_wasnt_bad = (hist & 0x1) == 0;
	if (was_already_partitioned && last_partition_wasnt_bad &&
	    aisort__insertion_sort(v, len, item_size, cmp, userptr, 12) == 0)
	{
		return;
	}

	// pivot selection
	ptrdiff_t l = 0;
	ptrdiff_t m = (len/2) * item_size;
	ptrdiff_t r = (len-1) * item_size;
	if (NINTHER > 0 && len > NINTHER) { // pesudo median of 9
		_Static_assert(NINTHER == 0 || NINTHER >= 32, "");
		ptrdiff_t k = (len/16) * item_size;
		aisort__median(v + k,      v + m - k,  v + r - k,   item_size, cmp, userptr);
		aisort__median(v + k*2,    v + m + k,  v + r - k*2, item_size, cmp, userptr);
		aisort__median(v + m - k,  v + m,      v + m + k,   item_size, cmp, userptr);
	}
	aisort__median(v + l, v + m, v + r, item_size, cmp, userptr);

	// partitioning
	// TODO??: use left/right partitioning as pdqsort to solve the dutch flag sort problem
	ptrdiff_t pivot = 0;
	while (cmp(v + (++pivot, l += item_size), v + m, userptr) <= 0 && l < m) {}
	while (cmp(v + (         r -= item_size), v + m, userptr) >= 0 && r > m) {}
	uint32_t partitioned_already = (l == r);
	while (l < r) {
		aisort__swap(v + l, v + r, item_size);
		m = (l == m) ? r : ((r == m) ? l : m);
		l += item_size, r -= item_size;
		++pivot;

		for (; l <= r && cmp(v + l, v + m, userptr) <= 0; l += item_size, ++pivot) {}
		for (; r >= l && cmp(v + r, v + m, userptr) >= 0; r -= item_size) {}
	}

	// recurse
	// TODO??: make it so that the pivot is excluded on recursion
	ptrdiff_t pivot_size = l;
	ptrdiff_t margin = len / 8;
	int partition_is_bad = pivot < margin || pivot > (len - margin);
	uint32_t new_hist = (hist << 1) | partition_is_bad;

	uint32_t new_meta = depth - 1; // low 8 bits
	new_meta |= partitioned_already << 8; // 9th bit
	new_meta |= new_hist << 9; // the rest

	// TODO??: recurse onto the smaller side first
	aisort__main(v,                    pivot, item_size, cmp, userptr, new_meta);
	aisort__main(v + pivot_size, len - pivot, item_size, cmp, userptr, new_meta);
}

///////////// public API

AISORT_API void
aisort_r(
	void *vp, ptrdiff_t len, ptrdiff_t item_size,
	int (*cmp)(const void *, const void *, void *), void *userptr
)
{
#if 1 && defined(__GNUC__)
	_Static_assert(sizeof(long long) == 8 && sizeof(ptrdiff_t) <= sizeof(long long), "");
	int log2 = 64 - __builtin_clzll(len|0x1);
#else
	int log2 = 0;
	for (ptrdiff_t l = (len|0x1); l != 0; l >>= 1, ++log2) {}
#endif
	int limit = log2 + log2 + log2/4; // log2 * 2.25
	aisort__main(vp, len, item_size, cmp, userptr, limit);
}

AISORT_API void
aisort(void *vp, ptrdiff_t len, ptrdiff_t item_size, int (*cmp)(const void *, const void *))
{
	aisort_r(vp, len, item_size, aisort__cmp_wrap, (void *)(uintptr_t)cmp);
}

#endif // AISORT_ONLY_HEADER
