#include <stdint.h>
#include <stddef.h>

///////////////////// Generic version with qsort_r() like interface
// optimized version, inspired by linux kernel
//
// all calculations are done on byte offsets instead of indices avoiding some
// multiplications.
//
// instead of extracting only the root, it also extracts the biggest child when
// possible. "Optimized Heap Sort [...] by Using Two-Swap Method":
// http://dx.doi.org/10.1007/978-3-319-11933-5_78
//
// a "bottom-up" style heapify is used, which avoids a lot of comparisons in
// the average case.
//
//
// other potential optimizations
// -----------------------------
// we can cache which child is bigger so that child to child comparisons can be
// avoided when traversing down. but this seems non trivial since the items can
// move around and we'd need to invalidate the cache when that happens.
//
// binary searching the upwards search can also reduce number of comparisons.
//
// both of these optimizations are described in wikipedia's bottom-up section:
// https://en.wikipedia.org/wiki/Heapsort#Bottom-up_heapsort

#if 1
static inline void
heap__swap(char *restrict a, char *restrict b, ptrdiff_t item_size)
{
#if 1 && defined(__GNUC__)
	switch (item_size) {
	char tmp[8];
	case 16: __builtin_memcpy(tmp, a, 8); __builtin_memcpy(a, b, 8); __builtin_memcpy(b, tmp, 8); a += 8, b += 8; // fallthrough
	case  8: __builtin_memcpy(tmp, a, 8); __builtin_memcpy(a, b, 8); __builtin_memcpy(b, tmp, 8); return;
	case  4: __builtin_memcpy(tmp, a, 4); __builtin_memcpy(a, b, 4); __builtin_memcpy(b, tmp, 4); return;
	}
#endif
	for (ptrdiff_t i = 0; i < item_size; ++i) {
		char tmp = a[i];
		a[i] = b[i];
		b[i] = tmp;
	}
}

static inline ptrdiff_t
heap__parent(ptrdiff_t pos, unsigned int lsbit, ptrdiff_t item_size)
{
	pos -= item_size;
	pos -= (pos & lsbit) ? item_size : 0;
	return pos / 2;
}

static void
heapsort(
	void *v, ptrdiff_t len, ptrdiff_t item_size,
	int (*cmp)(const void *, const void *, void *), void *userptr
)
{
	char *p = v;
	ptrdiff_t size = len * item_size;
	ptrdiff_t root = (len/2) * item_size;
	unsigned lsbit = item_size & -item_size;
	int skip = 0;

	if (len <= 1) return;

	for (;;) {
		if (root > 0) {
			root -= item_size << skip; // if the right child was swapped, we can skip heapifying the left
		} else if (size > (item_size * 4)) { // extract root and biggest child
			size -= item_size;
			heap__swap(p, p + size, item_size);
			skip = cmp(p + item_size, p + item_size + item_size, userptr) <= 0;
			root = item_size << skip;
			size -= item_size;
			heap__swap(p + root, p + size, item_size);
		} else if (size > 0) { // extract root
			size -= item_size;
			heap__swap(p, p + size, item_size);
		} else {
			break;
		}

		// heapify, bottom up style
		ptrdiff_t leaf = root;
		for (;;) { // descent all the way down the the leaf
			ptrdiff_t left  = (leaf * 2) + item_size;
			ptrdiff_t right = left + item_size;
			// prefetching does slightly better in some benchmarks, slightly worse in others
			// __builtin_prefetch((void *)( (uintptr_t)p + left*2 ));
			if (left >= size) {
				break;
			}
			leaf = left;
			if (right < size && cmp(p + right, p + left, userptr) >= 0) {
				leaf = right; // take right whenever possible
			}
		}
		ptrdiff_t pos = leaf; // compare upwards and find correct position for the root
		while (pos != root && cmp(p + root, p + pos, userptr) >= 0) {
			pos = heap__parent(pos, lsbit, item_size);
		}
		for (ptrdiff_t correct = pos; pos != root;) { // keep swapping until root is at correct position
			pos = heap__parent(pos, lsbit, item_size);
			heap__swap(p + pos, p + correct, item_size);
		}
	}
}
#endif

///////////////////// basic version
// extracts one element at a time, calculations are done on indices

#if 0
static inline void
heap__swap(void *p, ptrdiff_t index_a, ptrdiff_t index_b, ptrdiff_t item_size)
{
	char *restrict a = (char *)p + (index_a * item_size);
	char *restrict b = (char *)p + (index_b * item_size);
	for (ptrdiff_t i = 0; i < item_size; ++i) {
		char tmp = a[i];
		a[i] = b[i];
		b[i] = tmp;
	}
}

#if 1
// bottom up style. a lot faster on near sorted lists.
static void
heap__ify(
	char *v, ptrdiff_t root, ptrdiff_t end, ptrdiff_t item_size,
	int (*cmp)(const void *, const void *, void *), void *userptr
)
{
	ptrdiff_t leaf = root;
	for (;;) { // descent all the way down the the leaf
		ptrdiff_t child = (leaf * 2) + 1;
		if (child > end) {
			break;
		}
		ptrdiff_t left  = child * item_size;
		ptrdiff_t right = left + item_size;
		if (child+1 <= end && cmp(v + right, v + left, userptr) >= 0) {
			++child; // take right child whenever possible
		}
		leaf = child;
	}
	ptrdiff_t pos = leaf; // compare upwards and find correct position for the root
	while (pos != root && cmp(v + (root * item_size), v + (pos * item_size), userptr) > 0)
		pos = (pos - 1) / 2;
	for (ptrdiff_t correct = pos; pos != root;) { // keep swapping until root is at correct position
		pos = (pos - 1) / 2;
		heap__swap(v, pos, correct, item_size);
	}
}
#else
static void
heap__ify(
	char *v, ptrdiff_t start, ptrdiff_t end, ptrdiff_t item_size,
	int (*cmp)(const void *, const void *, void *), void *userptr
)
{
	ptrdiff_t parent = start;
	ptrdiff_t child = (parent * 2) + 1;
	while (child <= end) {
		ptrdiff_t left  = child * item_size;
		ptrdiff_t right = left + item_size;
		ptrdiff_t parent_sz = parent * item_size;
		ptrdiff_t child_sz = left;
		if (child+1 <= end && cmp(v + right, v + left, userptr) >= 0) {
			child_sz = right;
			++child;
		}
		if (cmp(v + parent_sz, v + child_sz, userptr) >= 0) {
			break;
		}
		heap__swap(v, parent, child, item_size);
		parent = child;
		child = (parent * 2) + 1;
	}
}
#endif

static void
heapsort(
	void *v, ptrdiff_t len, ptrdiff_t item_size,
	int (*cmp)(const void *, const void *, void *), void *userptr
)
{
	for (ptrdiff_t i = len/2 - 1; i >= 0; --i) {
		heap__ify(v, i, len-1, item_size, cmp, userptr);
	}
	for (ptrdiff_t i = len - 1; i >= 0; --i) {
		heap__swap(v, 0, i, item_size);
		heap__ify(v, 0, i-1, item_size, cmp, userptr);
	}
}
#endif


/////////////// with "hardcoded" type
// ~1.3x faster on integer arrays compared to the generic version
//
// for this hardcoded version, textbook top-down heapify performs significantly
// better than the "bottom-up" variant due to better cache utilization and the
// fact that comparisons and swaps become cheap due to no function pointer
// involvement.
//
// using the two-swap method seems to have no measureable impact.
#if 0
#define T int32_t

static void
heapify(T *v, ptrdiff_t start, ptrdiff_t end)
{
	ptrdiff_t parent = start;
	ptrdiff_t child = (start * 2) + 1;
	while (child <= end) {
		if (child+1 <= end && v[child+1] > v[child]) {
			++child;
		}
		if (v[parent] >= v[child]) {
			break;
		}
		T tmp = v[parent];
		v[parent] = v[child];
		v[child] = tmp;
		parent = child;
		child = (parent * 2) + 1;
	}
}

static void
heapsort(T *v, ptrdiff_t len)
{
	for (ptrdiff_t i = len/2 - 1; i >= 0; --i) {
		heapify(v, i, len-1);
	}
	for (ptrdiff_t i = len - 1; i >= 0; --i) {
		T tmp = v[i];
		v[i] = v[0];
		v[0] = tmp;
		heapify(v, 0, i-1);
	}
}
#endif
