#if 0
clang -o /tmp/test "$0" -DDEMO \
	-Wall -Wshadow -Wpedantic -Wextra -Wno-unused \
	-march=native -Ofast -fno-plt "$@"
#-g3 -DDEBUG -fsanitize=address,undefined -fsanitize-trap "$@"
exit $?
#endif
#include <stddef.h>
#include <stdint.h>

///////////////// Generic version
// works on both signed and unsigned integers of sizes 1,2,4,8

static inline uint64_t
radix__key(
	uint8_t *ptr, ptrdiff_t item_size, ptrdiff_t key_offset, ptrdiff_t key_size,
	int bias_exp, int shift, ptrdiff_t index
)
{
	uint64_t ret;
	uint8_t *p = ptr + (index * item_size) + key_offset;
	switch (key_size) {
	case 1: ret = *(uint8_t  *)p; break;
	case 2: ret = *(uint16_t *)p; break;
	case 4: ret = *(uint32_t *)p; break;
	case 8: ret = *(uint64_t *)p; break;
	default: __builtin_unreachable(); break;
	}
	return (ret + (UINT64_C(1) << bias_exp)) >> shift;
}
#define radix__key(P, S, I) radix__key((P), item_size, key_offset, key_size, bias_exp, (S), (I))

static inline void
radix__swap(void *p, ptrdiff_t index_a, ptrdiff_t index_b, ptrdiff_t item_size)
{
	uint8_t *restrict a = (uint8_t *)p + (index_a * item_size);
	uint8_t *restrict b = (uint8_t *)p + (index_b * item_size);
	for (ptrdiff_t i = 0; i < item_size; ++i) {
		uint8_t tmp = a[i];
		a[i] = b[i];
		b[i] = tmp;
	}
}

static void
radix_sort_generic(
	void *v, ptrdiff_t len, ptrdiff_t item_size,
	ptrdiff_t key_offset, ptrdiff_t key_size,
	int bias_exp, int shift
)
{
	typedef ptrdiff_t Size;
#if 1
	if (len < (1 << 5)) {
		for (Size i = 1; i < len; ++i) {
			for (Size k = i;
			     k > 0 && radix__key(v, 0, k-1) > radix__key(v, 0, k);
			     --k)
			{
				radix__swap(v, k-1, k, item_size);
			}
		}
		return;
	}
#endif

	Size count[256] = {0};
	Size limit[256];

	int last_key = 0;
	for (Size i = 0; i < len; ++i) {
		int key = radix__key(v, shift, i) & 0xFF;
		++count[key];
		last_key = key;
	}
	if (count[last_key] == len) { // bucket already sorted
		if (shift > 0) radix_sort_generic(
			v, len, item_size, key_offset, key_size, bias_exp,
			shift - 8
		);
		return;
	}

	for (Size acc = 0, i = 0; i < 256; ++i) {
		Size tmp = count[i];
		count[i] = acc;
		limit[i] = (acc += tmp);
	}

	for (Size i = 0; i < len;) {
		int key = radix__key(v, shift, i) & 0xFF;
		Size want = count[key];
		if (want == limit[key]) {
			++i;
		} else if (want == i) {
			++i;
			++count[key];
		} else {
			radix__swap(v, want, i, item_size);
			++count[key];
		}
	}

	if (shift > 0) for (Size i = 0; i < 256; ++i) {
		Size beg = i == 0 ? 0 : count[i - 1];
		Size l = count[i] - beg;
		if (l > 1) {
			radix_sort_generic(
				(uint8_t *)v + (beg * item_size), l,
				item_size, key_offset, key_size, bias_exp,
				shift - 8
			);
		}
	}
}

#define radix_sort_array(V, L) radix_sort_generic(  \
	(V), (L), sizeof 0[V], 0, sizeof 0[V], \
	((__typeof__(0[V]))-1 < 0) ? (sizeof 0[V] * 8 - 1) : 0, \
	(sizeof 0[V] * 8) - 8 \
)
#define radix_sort_struct(V, L, K) radix_sort_generic( \
	(V), (L), sizeof 0[V], \
	offsetof(__typeof__(0[V]), K), sizeof (V)->K, \
	((__typeof__((V)->K))-1 < 0) ? (sizeof((V)->K) * 8 - 1) : 0, \
	(sizeof((V)->K) * 8) - 8 \
)


/////////////// Hardcoded variant
// can be slightly faster than the generic version

#define T uint32_t

static void
uint_sort(T *v, ptrdiff_t len, int shift)
{
	typedef ptrdiff_t Size;
	Size count[256] = {0};
	Size limit[256];

	// gcc fucks this up badly. same happens to most of the other
	// "small sort" functions in ./sort/small-int-sort.c
	// https://gcc.gnu.org/bugzilla/show_bug.cgi?id=115777
#if !defined(__GNUC__) || defined(__clang__)
	if (len < (1 << 8)) {
		for (Size i = 1; i < len; ++i) {
			for (Size k = i; k > 0 && v[k-1] > v[k]; --k) {
				T tmp = v[k-1];
				v[k-1] = v[k];
				v[k] = tmp;
			}
		}
		return;
	}
#endif

	int last_key = 0;
	for (Size i = 0; i < len; ++i) {
		int key = (v[i] >> shift) & 0xFF;
		++count[key];
		last_key = key;
	}
	if (count[last_key] == len) { // bucket already sorted
		if (shift > 0) uint_sort(v, len, shift - 8);
		return;
	}

	for (Size acc = 0, i = 0; i < 256; ++i) {
		Size tmp = count[i];
		count[i] = acc;
		limit[i] = (acc += tmp);
	}

	for (Size i = 0; i < len;) {
		int key = (v[i] >> shift) & 0xFF;
		Size want = count[key];
		if (want == limit[key]) {
			++i;
		} else if (want == i) {
			++i;
			++count[key];
		} else {
			T tmp = v[want];
			v[want] = v[i];
			v[i] = tmp;
			++count[key];
		}
	}

	if (shift > 0) for (Size i = 0; i < 256; ++i) {
		Size beg = i == 0 ? 0 : count[i - 1];
		Size end = count[i];
		if (end - beg > 1) {
			uint_sort(v + beg, end - beg, shift - 8);
		}
	}
}
#define uint_sort(v, l) uint_sort((v), (l), sizeof(0[v])*8 - 8)


///////////////////// LSD variant, requires additional memory

static void
uint_sort_lsd(T *v, ptrdiff_t len, T *scratch)
{
	typedef ptrdiff_t Size;
	enum { BUCKETS = sizeof(T) };
	Size count[BUCKETS][256] = {0};

	// count all the buckers, going over the input in one pass
	for (Size i = 0; i < len; ++i) {
		for (int k = 0; k < BUCKETS; ++k) {
			int key = (v[i] >> (k * 8)) & 0xFF;
			++count[k][key];
		}
	}

	T *src = v, *dst = scratch;
	_Static_assert(sizeof *v % 2 == 0, "required so that the final dst is the input array");
	for (int k = 0; k < BUCKETS; ++k) {
		for (Size acc = 0, i = 0; i < 256; ++i) { // build prefix sum
			Size prefix = acc;
			acc += count[k][i];
			count[k][i] = prefix;
		}
		for (Size i = 0; i < len; ++i) { // put stuff into proper order
			int key = (src[i] >> (k * 8)) & 0xFF;
			Size want = count[k][key]++;
			dst[want] = src[i];
		}
		T *tmp = src;
		src = dst;
		dst = tmp;
	}
}


///////////////// Benchmark + usage demo

#ifdef DEMO
#include <stdio.h>
#include <inttypes.h>
#include <time.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>

static void
rngfill(T *v, ptrdiff_t len)
{
	uint64_t rng = 0x55555;
	for (ptrdiff_t i = 0; i < len; ++i) {
		v[i] = (rng >> 31) ^ rng;
		rng *= 1111111111111111111; rng += 0x1337;
	}
}

static void
seqfill(T *v, ptrdiff_t len)
{
	for (ptrdiff_t i = 0; i < len; ++i) {
		v[i] = i;
	}
}

static void
revfill(T *v, ptrdiff_t len)
{
	for (ptrdiff_t i = 0; i < len; ++i) {
		v[i] = len - i;
	}
}

static uint64_t
hash(void *key_in, ptrdiff_t len)
{
	uint64_t h = 0x55555 + len;
	uint8_t *key = key_in;
	for (; len >= 8; len -= 8, key += 8) {
		uint64_t v; memcpy(&v, key, 8);
		h ^= v;
		h *= 1111111111111111111;
	}
	for (ptrdiff_t i = 0; i < len; ++i) {
		h ^= key[i];
		h *= 1111111111111111111;
	}
	return h ^ (h >> 31);
}

static int
cmp(const void *ap, const void *bp)
{
	T a = *(T *)ap;
	T b = *(T *)bp;
	return a < b ? -1 : a > b;
}

// assumes tbase, t->f(), v, and len are in scope
#define BENCH(NAME, ...) do { \
	t->f(v, len); \
	clock_t beg = clock(); \
	__VA_ARGS__; \
	clock_t end = clock(); \
	long took = (long)(end - beg); \
	if (tbase < 0) tbase = took; \
	printf("%-16s =>  %016"PRIX64"  %8ld", NAME, hash(v, sizeof v), took); \
	double rel = (double)tbase / (double)took; \
	char *colors[] = { "\033[1;31m", "\033[1;32m" }; \
	printf("  (%s%.3f%s)\n", colors[rel >= 1.0], rel, "\033[0m"); \
} while (0)

extern int
main(void)
{
	if (1) {
		T v[] = { 4, 0, 3, 1, 5, 7, 2, 1 };
		T r[] = { 0, 1, 1, 2, 3, 4, 5, 7 };
		ptrdiff_t len = sizeof v / sizeof *v;
		uint_sort(v, len);
		assert(memcmp(v, r, sizeof v) == 0);
	}

	if (1) {
		static T v[1<<22];
		ptrdiff_t len = sizeof v / sizeof *v;

		typedef void FillFunc(T *, ptrdiff_t);
		struct testcase { FillFunc *f; char *name; } testcases[] = {
			{ rngfill,     "random"     },
			{ seqfill,     "sorted"     },
			{ revfill,     "reversed"   },
		};

		for (struct testcase *t = testcases; t < 1[&testcases]; ++t) {
			long tbase = -1;
			printf("[%s]\n", t->name);
			BENCH("  libc-qsort", qsort(v, len, sizeof *v, cmp));
			BENCH("  uint_sort_lsd",
				T *scratch = malloc(len * sizeof *scratch); // include allocation time too
				if (!scratch) abort();
				uint_sort_lsd(v, len, scratch);
				free(scratch);
			);
			BENCH("  uint_sort", uint_sort(v, len));
			BENCH("  radix_generic", radix_sort_array(v, len));
		}
	}

	if (1) {
		struct { int16_t val; int16_t key; } st[1<<12];
		ptrdiff_t len = sizeof st / sizeof st[0];

		uint64_t rng = 0x55555;
		for (int i = 0; i < len; ++i) {
			st[i].key = rng >> 32;
			rng *= 1111111111111111111; rng += 0x1337;
		}

		radix_sort_struct(st, len, key);

		for (int i = 1; i < len; ++i) {
			assert(st[i - 1].key <= st[i].key);
		}
	}
}
#endif // DEMO
