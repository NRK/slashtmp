#if 0
clang -o /tmp/test "$0" -std=c99 -D_GNU_SOURCE \
	-Wall -Wshadow -Wpedantic -Wextra -Wno-unused \
	-march=native -Ofast -fno-plt "$@"
#-g3 -DDEBUG -fsanitize=address,undefined -fsanitize-trap "$@"
exit $?
#endif
//////////////
#include <stdio.h>
#include <stddef.h>
#include <stdint.h>
#include <inttypes.h>
#include <time.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>

#ifndef T
#define T int32_t
#endif

#ifndef NO_HEAPSORT
	#include "heapsort.c"
#endif
#ifndef NO_AISORT
	#include "adaptive-introsort.c"
#endif

///////////// test cases

static void
rngfill(T *v, ptrdiff_t len)
{
	uint64_t rng = 0x55555;
	for (ptrdiff_t i = 0; i < len; ++i) {
		v[i] = (rng >> 31) ^ rng;
		rng *= 1111111111111111111; rng += 0x1337;
	}
}

static void
seqfill(T *v, ptrdiff_t len)
{
	for (ptrdiff_t i = 0; i < len; ++i) {
		v[i] = i;
	}
}

static void
revfill(T *v, ptrdiff_t len)
{
	for (ptrdiff_t i = 0; i < len; ++i) {
		v[i] = len - i;
	}
}

static void
eqfill(T *v, ptrdiff_t len)
{
	for (ptrdiff_t i = 0; i < len; ++i) {
		v[i] = (uintptr_t)&eqfill;
	}
}

static void
twosort(T *v, ptrdiff_t len)
{
	for (ptrdiff_t i = 0; i < len; ++i) {
		v[i] = i % (len / 2);
	}
}

static void
sorted80(T *v, ptrdiff_t len)
{
	for (ptrdiff_t i = 0; i < len; ++i) {
		v[i] = i;
	}
	ptrdiff_t gap = len/5;
	for (ptrdiff_t i = 0; i < len; i += gap) {
		v[i] = ~v[i];
	}
}

static void
smallnoise(T *v, ptrdiff_t len)
{
	uint64_t rng = 0x55555ACDC1337;
	for (ptrdiff_t i = 0; i < len; ++i) {
		int noise = (int)(rng >> (64 - 6));
		v[i] = i + noise;
		rng *= 1111111111111111111;
		rng += 0x1337;
	}
}

static void
lowdistinct(T *v, ptrdiff_t len)
{
	int bits = 4;
	uint64_t rng = 0x55555ACDC1337;
	for (ptrdiff_t i = 0; i < len; ++i) {
		rng *= 1111111111111111111; rng += 0x1337;
		v[i] = rng >> (64 - bits);
	}
}

/////////////////////

static uint64_t
hash(void *key_in, ptrdiff_t len)
{
	uint64_t h = 0x55555 + len;
	uint8_t *key = key_in;
	for (; len >= 8; len -= 8, key += 8) {
		uint64_t v; memcpy(&v, key, 8);
		h ^= v;
		h *= 1111111111111111111;
	}
	for (ptrdiff_t i = 0; i < len; ++i) {
		h ^= key[i];
		h *= 1111111111111111111;
	}
	return h ^ (h >> 31);
}

static int
T_cmp(const void *ap, const void *bp, void *unused)
{
	(void)unused;
	T a = *(T *)ap;
	T b = *(T *)bp;
	return a < b ? -1 : a > b;
}

// assumes tbase, hbase, t->f(), v, and len are in scope
#define BENCH(NAME, ...) do { \
	t->f(v, len); \
	clock_t beg = clock(); \
	__VA_ARGS__; \
	clock_t end = clock(); \
	long took = (long)(end - beg); \
	uint64_t h = hash(v, sizeof v); \
	if (tbase < 0) tbase = took, hbase = h; \
	printf("%s %-8s =>  %016"PRIX64"  %8ld", h == hbase ? "  " : "🔴", NAME, h, took); \
	double rel = (double)tbase / (double)took; \
	char *colors[] = { "\033[1;31m", "\033[1;32m" }; \
	printf("  (%s%.3f%s)\n", colors[rel >= 1.0], rel, "\033[0m"); \
} while (0)

extern int
main(void)
{
	static T v[1<<22];
	ptrdiff_t len = sizeof v / sizeof *v;
#ifdef DEBUG
	len /= 8;
	len -= 33;
#endif
	typedef void FillFunc(T *, ptrdiff_t);
	struct testcase { FillFunc *f; char *name; } testcases[] = {
		{ rngfill,     "random"      },
		{ seqfill,     "sorted"      },
		{ revfill,     "reversed"    },
		{ eqfill,      "equal"       },
		{ twosort,     "twosort"     },
		{ sorted80,    "sorted80"    },
		{ smallnoise,  "smallnoise"  },
		{ lowdistinct, "lowdistinct" },
	};

	for (struct testcase *t = testcases; t < 1[&testcases]; ++t) {
		long tbase = -1; uint64_t hbase = 0;
		printf("[%s]\n", t->name);
		BENCH("libc",    qsort_r     (v, len, sizeof *v, T_cmp, 0));
#ifndef NO_HEAPSORT
		BENCH("heap",    heapsort    (v, len, sizeof *v, T_cmp, 0));
#endif
#ifndef NO_AISORT
		BENCH("aisort",  aisort_r (v, len, sizeof *v, T_cmp, 0));
#endif
	}
}
