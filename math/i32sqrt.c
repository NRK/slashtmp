#include <stdint.h>

// based on "Square Rooting Algorithms for Integer and Floating-Point Numbers"
// by Reza Hashemian
// gives fully accurate results for [0, UINT32_MAX]
// can be tweaked to be faster at the cost of accuracy
static int32_t
i32_sqrt_reza(int32_t n)  // change arg type to uint32_t if you want
{
	int M = 32 - __builtin_clz(n | 1);
	int odd = M & 1;
	M += odd;
	int m = M / 2;
	uint32_t new_n = (uint32_t)n << odd;
	uint32_t Z = (new_n >> (m + 1)) | (1 << (m - 1));
	uint32_t z = (1 << m) - Z;
	uint32_t y = z;

	// 6 iteration to cover upto INT32_MAX
	// if `n < 2048` then no iteration is needed at all
	y = z + ((y * y) >> (m+1)); // n >= 2048
	y = z + ((y * y) >> (m+1)); // n >= 8448
	y = z + ((y * y) >> (m+1)); // n >= 132096
	y = z + ((y * y) >> (m+1)); // n >= 528384
	y = z + ((y * y) >> (m+1)); // n >= 8421376
	y = z + ((y * y) >> (m+1)); // n >= 134250496

	y = (1 << m) - y;
	uint64_t mul = odd ? UINT64_C(3037000151) : (UINT64_C(1) << 32);
	uint64_t root = ((uint64_t)y * mul) >> 32; // y * (odd ? 0.7071067 : 1.0)

	// correct the [-2, 1] error (or [-3, 1] in case of u32 arg)
	uint32_t dec = (n == 0) ? 0 : 1;
	int plus  = ((root+1)*(root+1)) <= (uint32_t)n;
	int minus = ((root*root) > (uint32_t)n) +
	            (((root-dec)*(root-dec)) > (uint32_t)n);
	// if accepting u32 arg instead of i32 then one more round of -1 is needed
	int minus2 = ((root-2)*(root-2)) > (uint32_t)n;
	minus += minus2 & ((int64_t)n >= (1ll << 31));

	return root + plus - minus;
}

// digit by digit algorithm, in base 4 (i.e 2 bits at a time)
// https://www.cantorsparadise.com/the-square-root-algorithm-f97ab5c29d6d
// https://en.wikipedia.org/wiki/Methods_of_computing_square_roots#Digit-by-digit_calculation
//
// note: 1 bit at a time has less work inside the loop but is slower than 2
// due to lower IPC.
// 3/4 bits (even with binary search) still doesn't match the speed of 2 bits,
// so 2 bits seems to be the sweet spot.
static int32_t
i32_sqrt_2bits(int32_t n) // can be changed to u32
{
	uint32_t root = 0;
	uint32_t rem = 0;
	uint32_t pull = (uint32_t)n;
	for (int i = 0; i < 8; ++i) {
		rem = (rem << 4) | (pull >> (32 - 4));
		pull <<= 4;
		root <<= 2;
		uint32_t sq;
		uint32_t root2 = root << 1;
		if (rem >= (sq = (root2 | 0x3) * 0x3)) {
			rem -= sq;
			root |= 0x3;
		} else if (rem >= (sq = (root2 | 0x2) * 0x2)) {
			rem -= sq;
			root |= 0x2;
		} else if (rem >= (sq = (root2 | 0x1) * 0x1)) {
			rem -= sq;
			root |= 0x1;
		}
	}
	return root;
}

static int32_t
i32_sqrt_1bit(int32_t n) // can be changed to u32
{
	uint32_t root = 0;
	uint32_t rem = 0;
	uint32_t pull = (uint32_t)n;
	for (int i = 0; i < 16; ++i) {
		rem = (rem << 2) | (pull >> (32 - 2));
		pull <<= 2;
		root <<= 1;
		uint32_t sq = (root << 1) | 0x1;
		if (rem >= sq) {
			rem -= sq;
			root |= 1;
		}
	}
	return root;
}

// baseline using floating point math function
#include <math.h>
static int32_t
i32_sqrt_fp(int32_t n)
{
	return (int32_t)sqrt((double)n);
}


///////////////
#include <stdio.h>
#include <time.h>

extern int
main(void)
{
	enum { L = 1<<26 };

	uint32_t h;
	clock_t now, end;

#define BENCH(FUNC) \
	h = 0; \
	now = clock(); \
	for (int64_t i = 0; i < L; ++i) { \
		int32_t r = FUNC(i); \
		h ^= h >> 15; \
		h *= 0x9D665555; \
		h ^= r; \
	} \
	end = clock(); \
	h *= 0x9D665555; h ^= h >> 15; \
	printf("%-18s  %10ld  [%08lX]\n", #FUNC ":", (long)(end - now), (unsigned long)h);

	BENCH(i32_sqrt_fp);
	BENCH(i32_sqrt_reza);
	BENCH(i32_sqrt_2bits);
	BENCH(i32_sqrt_1bit);
}
