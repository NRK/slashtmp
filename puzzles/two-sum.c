// this is mainly for testing a "commit based" arena
// along with learning native windows interfaces.
//
// the code solves the "two-sum" problem with a hash-trie.
// ref: https://nullprogram.com/blog/2023/06/26/
//
// build
// linux/unix:   $ cc  two-sum.c
// windows gcc:  $ gcc two-sum.c -nostartfiles
#include <stddef.h>
#include <stdint.h>

#define ASSERT(X)         ((X) ? (void)0 : __builtin_unreachable())
#define SIZEOF(...)       ((Size)sizeof(__VA_ARGS__))
#define ARRLEN(...)       (SIZEOF(__VA_ARGS__) / SIZEOF(0[__VA_ARGS__]))
#define ALIGNOF(...)      ((Size)_Alignof(__VA_ARGS__))

typedef uint8_t   u8;
typedef uint32_t  u32;
typedef uint64_t  u64;
typedef ptrdiff_t Size;

typedef struct {
	u8 *s;
	Size len;
} Str;
#define S(X)    ((Str){ (u8 *)(X), ARRLEN(X) - 1 })

typedef struct {
	u8 *buf;
	Size off, cap, pagesz;
	Size *commit;
} Arena;
enum { NO_ZERO = 1 <<  0 };

_Noreturn static void os_exit(int ec);
static int os_write(int fd, Str s);
static int os_commit(void *ptr, Size len);

_Noreturn static void
oom(void)
{
	os_write(2, S("aborting: out of memory\n"));
	os_exit(-1);
}

static void *
arena_alloc(Arena *a, Size size, Size cnt, Size align, int flags)
{
	u8 *p = a->buf + a->off;
	ASSERT((align & (align - 1)) == 0);
	Size pad = -(uintptr_t)p & (align - 1);
	if ((a->cap - a->off - pad) / cnt < size) {
		oom();
	}
	Size bytes = cnt * size;
	a->off += bytes + pad;
	ASSERT(a->off <= a->cap);
	if (a->off > a->commit[0]) {
		Size bytes = a->off - a->commit[0];
		bytes += -bytes & (a->pagesz - 1);
		ASSERT(bytes % a->pagesz == 0);
		if (!os_commit(a->buf + a->commit[0], bytes)) {
			oom();
		}
		a->commit[0] += bytes;
	}
	u8 *ret = p + pad;
	if (!(flags & NO_ZERO)) for (Size i = 0; i < bytes; ++i) {
		ret[i] = 0x0;
	}
	return ret;
}
#define arena_alloc(a, type, cnt, flag) \
	((type *)arena_alloc((a), SIZEOF(type), (cnt), ALIGNOF(type), flag))

static u32
rng_next(u64 *state)
{
	u64 oldstate = *state;
	*state = (*state * 1111111111111111111) + 0x1337;
	return oldstate ^ (oldstate >> (oldstate >> 59));
}

typedef struct Map {
	struct Map *next[4];
	int key;
	Size value;
} Map;
typedef struct {
	int found;
	Map *node;
} MapUpsert;

static MapUpsert
map_upsert(Arena *a, Map **m, int key)
{
	u32 h = (u32)key * 0xACDC5AAD;
	for (; *m; h <<= 2) {
		if (m[0]->key == key) {
			return (MapUpsert){ .found = 1, .node = *m };
		}
		m = m[0]->next + (h >> 30);
	}
	if (a) {
		*m = arena_alloc(a, Map, 1, 0x0);
		m[0]->key = key;
	}
	return (MapUpsert){ .node = *m };
}

// solver

typedef struct {
	int ok;
	Size a, b;
} Solve;

static Solve
solver(Arena scratch, int *v, Size len, int target)
{
	Map *m = NULL;
	Solve ret = {0};
	for (Size i = 0; i < len; ++i) {
		int comp = target - v[i];
		int key = (u32)comp * v[i];
		MapUpsert r = map_upsert(&scratch, &m, key);
		if (r.found && v[r.node->value] == comp) {
			ret.ok = 1;
			ret.a = i;
			ret.b = r.node->value;
			ASSERT(v[ret.a] + v[ret.b] == target);
			break;
		} else {
			r.node->value = i;
			ASSERT(r.node->key == key);
		}
	}
	return ret;
}

static int
app_main(Arena *a)
{
	int N = 1 << 26;
	int *v = arena_alloc(a, int, N, NO_ZERO);
	u64 rng = 55555;
	//rng = ((uintptr_t)&app_main * 55555) ^ (uintptr_t)a->buf;
	for (int i = 0; i < N; ++i) {
		v[i] = rng_next(&rng);
		v[i] &= (u32)-1 >> 2;
	}

	Solve expect = {0};
	expect.a = rng_next(&rng) & (N-1);
	expect.b = rng_next(&rng) & (N-1);
	int target = v[expect.a] + v[expect.b];
	Solve res = solver(*a, v, N, target);
	if (res.ok && v[res.a] + v[res.b] == target) {
		os_write(1, S("success!\n"));
		return 0;
	} else {
		os_exit(-1);
	}
}

// platform

#if __unix__
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>

#if !defined(MAP_ANON) /* some systems call it MAP_ANONYMOUS */
	#define MAP_ANON MAP_ANONYMOUS
#endif

_Noreturn static void
os_exit(int ec)
{
	if (ec < 0) {
		abort();
	} else {
		exit(ec);
	}
}

static int
os_write(int fd, Str s)
{
	ASSERT(s.len >= 0);
	ASSERT(fd >= 0);
	if (s.len > 0) ASSERT(s.s != NULL);
	return write(fd, s.s, s.len) == s.len;
}

static int
os_commit(void *ptr, Size len)
{
	int res = mprotect(ptr, len, PROT_READ|PROT_WRITE);
	return res == 0;
}

extern int
main(void)
{
	Arena a[1] = {{ .cap = (Size)1 << 40, .commit = &(Size){0} }};
	a->pagesz = sysconf(_SC_PAGESIZE);
	a->buf = mmap(0, a->cap, PROT_NONE, MAP_ANON|MAP_PRIVATE, -1, 0);
	if (a->buf == MAP_FAILED || a->pagesz < 0) {
		os_write(2, S("aborting: mmap() failed\n"));
		os_exit(-1);
	}

	if (0) { /* HACK: avoid faulting on first allocation */
		Size tmp_commit = 0;
		a->commit = &tmp_commit;
		a->commit = arena_alloc(a, Size, 1, 0x0);
		*a->commit = tmp_commit;
	}

	return app_main(a);
}

#elif _WIN32
#define WIN32_LEAN_AND_MEAN
#include <windows.h>

static _Noreturn void
os_exit(int ec)
{
	if (ec < 0) {
		*(volatile int *volatile)4 = 0; // TODO: abort() alternative
		__builtin_unreachable();
	} else {
		ExitProcess(ec);
	}
}

static int
os_write(int fd, Str s)
{
	ASSERT(fd == 1 || fd == 2); // TODO: only supports std{out,err}
	HANDLE h = GetStdHandle(-10 - fd);
	DWORD junk;
	int r = WriteFile(h, s.s, s.len, &junk, 0);
	return r;
}

static int
os_commit(void *ptr, Size len)
{
	void *res = VirtualAlloc(ptr, len, MEM_COMMIT, PAGE_READWRITE);
	return res == ptr;
}

__attribute((force_align_arg_pointer))
extern int
mainCRTStartup(void)
{
	Arena a[1] = {{ .cap = (Size)1 << 40, .commit = &(Size){0} }};

	SYSTEM_INFO info;
	GetSystemInfo(&info);
	a->pagesz = info.dwPageSize;
	a->buf = VirtualAlloc(0, a->cap, MEM_RESERVE, PAGE_READWRITE);
	if (a->buf == NULL) {
		os_write(2, S("aborting: VirtualAlloc() failed\n"));
		os_exit(-1);
	}

	int r = app_main(a);
	os_exit(r);
}

#else
	#error "Unsupported platform"
#endif
