// ref: https://leetcode.com/problems/symmetric-tree/
#include <assert.h>
#include <stddef.h>
#include <stdbool.h>

typedef struct Node {
	int val;
	struct Node *leaf[2];
} Node;

#ifdef RECURSIVE
static bool
tree_is_symmetric_rec(Node *left, Node *right)
{
	if (left == NULL || right == NULL)
		return left == right;
	if (left->val != right->val)
		return false;
	return tree_is_symmetric_rec(left->leaf[0], right->leaf[1]) &&
	       tree_is_symmetric_rec(left->leaf[1], right->leaf[0]);
}

static bool
tree_is_symmetric(Node *root)
{
	assert(root != NULL);
	return tree_is_symmetric_rec(root->leaf[0], root->leaf[1]);
}
#else
static bool
node_eq(Node *l, Node *r)
{
	if (l == NULL || r == NULL)
		return l == r;
	return l->val == r->val;
}

static bool
tree_is_symmetric(Node *root)
{
	assert(root != NULL);
	int head = 0;
	Node *stack[512];
	stack[head++] = root->leaf[0];
	stack[head++] = root->leaf[1];
	while (head > 0) {
		assert(head >= 2);
		Node *right = stack[--head], *left  = stack[--head];
		if (!node_eq(right, left))
			return false;
		if (left == NULL) {
			assert(right == NULL);
			continue;
		}
		assert(head+4 <= 512);
		stack[head++] = left->leaf[0]; stack[head++] = right->leaf[1];
		stack[head++] = left->leaf[1]; stack[head++] = right->leaf[0];
	}
	return true;
}
#endif

extern int
main(void)
{
	Node tree = { .val = 1 };
	tree.leaf[0] = &(Node){ .val = 2 };
	tree.leaf[1] = &(Node){ .val = 2 };
	tree.leaf[0]->leaf[0] = &(Node){ .val = 3 };
	assert(!tree_is_symmetric(&tree));
	tree.leaf[1]->leaf[1] = &(Node){ .val = 3 };
	assert(tree_is_symmetric(&tree));
	tree.leaf[1]->leaf[1] = &(Node){ .val = -1 };
	assert(!tree_is_symmetric(&tree));
}
