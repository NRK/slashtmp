/* ref: https://leetcode.com/problems/merge-sorted-array/ */

void
merge(
	int *v0, int v0size, int v0len,
	int *v1, int v1size, int v1len
)
{
#if 1
	for (int i0 = v0len-1, i1 = v1len-1, w = v0size-1; w >= 0; --w) {
		if (i1 == -1 || (i0 >= 0 && v0[i0] > v1[i1])) {
			v0[w] = v0[i0--];
		} else {
			v0[w] = v1[i1--];
		}
	}
#else // VLA version
	int reslen = v0len + v1len;
	int res[reslen]; // FIXME: VLA bad
	for (int i0 = 0, i1 = 0, w = 0; w < reslen; ++w) {
		if (i1 == v1len || (i0 < v0len && v0[i0] < v1[i1])) {
			res[w] = v0[i0++];
		} else {
			res[w] = v1[i1++];
		}
	}
	for (int i = 0; i < reslen; ++i) {
		v0[i] = res[i];
	}
#endif
}


#include <stdio.h>
#include <assert.h>
#define ARRLEN(X) (sizeof(X) / sizeof(0[X]))

extern int
main(void)
{
	int res[] = { 1, 2, 2, 3, 5, 6 };
	int v0[ARRLEN(res)] = { 1, 2, 3 };
	int v1[] = { 2, 5, 6 };
	merge(v0, ARRLEN(v0), ARRLEN(v1), v1, ARRLEN(v1), ARRLEN(v1));
	for (int i = 0; i < ARRLEN(res); ++i) {
		assert(res[i] == v0[i]);
	}
}
