// ref: https://old.reddit.com/r/dailyprogrammer/comments/onfehl/20210719_challenge_399_easy_letter_value_sum
#include <stdio.h>
#include <stdlib.h>

extern int
main(void)
{
	for (int c = 0, sum = 0; c != EOF;) {
		switch (c = getchar()) {
		case EOF:
		case '\n':
		case '\r':
			printf(" => %d\n", sum);
			sum = 0;
			break;
		default:
			if (!(c >= 'a' && c <= 'z'))
				abort();
			sum += c - 'a' + 1;
			putchar(c);
			break;
		}
	}
	fflush(stdout);
	return ferror(stdout) || ferror(stdin);
}
