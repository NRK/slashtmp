// https://leetcode.com/problems/linked-list-cycle/
#include <assert.h>
#include <stddef.h>
#include <stdint.h>

typedef struct list {
	struct list *next;
} List;

#ifdef HASH
static uint64_t
hashptr(void *p)
{
	_Static_assert(sizeof(uintptr_t) <= sizeof(uint64_t));
	uint64_t hash = (uint64_t)p;
	hash ^= hash >> 33;
	hash *= 0x1337ACDC7B0B5AAD;
	hash ^= hash >> 31;
	return hash;
}

static int
list_has_cycle(List *head)
{
	enum { EXP = 14, N = 1 << EXP }; // NOTE: max number of nodes 10^4 (which is < 2^14)
	uint64_t tbl[N] = {0}; // flirting with the stack limit here...

	for (List *node = head; node != NULL; node = node->next) {
		uint64_t hash = hashptr(node);
		assert(hash != 0);
		uint64_t idx  = hash & (N-1);
		uint64_t step = hash >> (EXP-1);
		assert(((step << EXP-1) | idx) == hash);
		step |= 0x1;
		for (;;) {
			if (tbl[idx] == hash) {
				return 1;
			} else if (tbl[idx] == 0) {
				assert(hashptr(NULL) == 0);
				tbl[idx] = hash;
				break;
			} else {
				idx = (idx + step) & (N-1);
			}
		}
	}
	return 0;
}
#else
static int
list_has_cycle(List *head)
{
	List *tortoise = head;
	List *hare = head ? head->next : NULL;
	while (hare != NULL) {
		if (tortoise == hare)
			return 1;
		tortoise = tortoise->next;
		hare = hare->next ? hare->next->next : NULL;
	}
	return 0;
}
#endif

int
main(void)
{
	List head = {0};
	assert(!list_has_cycle(&head));
	assert(!list_has_cycle(NULL));

	head.next = &(List){0};
	assert(!list_has_cycle(&head));

	head.next->next = &head;
	assert(list_has_cycle(&head));
}
