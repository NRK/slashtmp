// https://leetcode.com/problems/reverse-integer/
#include <assert.h>
#include <stdint.h>
#include <stdio.h>

// NOTE: cannot use 64bit integers
static int32_t
reverse(int32_t num)
{
	int neg = num < 0;
	uint32_t max = (uint32_t)INT32_MAX + neg;
	uint32_t n = neg ? -(uint32_t)num : num;
	uint32_t ret = 0;
	for (; n; n /= 10) {
		int d = n % 10;
		if (((max - d) / 10) < ret)
			return 0;
		assert(ret <= (ret * 10 + d));
		ret = (ret * 10) + d;
	}
	return neg ? -ret : ret;
}

int
main(void)
{
	int32_t testv[] = {
		//INT32_MIN,   0,
		123,         321,
		-123,       -321,
		120,         21,
		INT32_MAX,    0,
		INT32_MIN,    0,
		2143847412,   2147483412
	};
	int ntests = sizeof testv / sizeof *testv;
	for (int i = 0; i < ntests; i += 2) {
		int32_t r = reverse(testv[i]);
		printf("reverse(%d) => %d :: %d\n", testv[i], r, testv[i+1]);
		assert(r == testv[i+1]);
	}
}
